<?php
if (!defined('ABSPATH')) exit;

/**
 * The template for displaying search
 *
 * Template name: Search
 * @package Visit_Europe
 */

$searchTerm = '';
$title = null;

if (isset($_GET['term'])) {
  $searchTerm = sanitize_text_field($_GET['term']);
}

if (isset($_GET['s'])) {
  $searchTerm = sanitize_text_field($_GET['s']);
}

$destinationsWpQuery = new WP_Query([
  'post_type' => VisitEurope_CPT_Destination::POST_TYPE,
  's' => $searchTerm,
  'post_status' => 'publish',
  'posts_per_page' => 8,
]);

$experiencesWpQuery = new WP_Query([
  'post_type' => VisitEurope_CPT_Experience::POST_TYPE,
  's' => $searchTerm,
  'post_status' => 'publish',
  'posts_per_page' => 8,
]);

$tripsWpQuery = new WP_Query([
  'post_type' => VisitEurope_CPT_Trip::POST_TYPE,
  's' => $searchTerm,
  'post_status' => 'publish',
  'posts_per_page' => 8,
]);

$destinationIDs = [];
$experienceIDs = [];
$tripIDs = [];

$destinationTitle = VisitEurope_Content::getLocale('search.destinations');
$experiencesTitle = VisitEurope_Content::getLocale('search.experiences');
$tripsTitle = VisitEurope_Content::getLocale('search.trips');

if (is_array($destinationsWpQuery->posts) && !empty($destinationsWpQuery->posts)) {
  foreach ($destinationsWpQuery->posts as $post) {
    $destinationIDs[] = $post->ID;
  }
}

if (is_array($experiencesWpQuery->posts) && !empty($experiencesWpQuery->posts)) {
  foreach ($experiencesWpQuery->posts as $post) {
    $experienceIDs[] = $post->ID;
  }
}

if (is_array($tripsWpQuery->posts) && !empty($tripsWpQuery->posts)) {
  foreach ($tripsWpQuery->posts as $post) {
    $tripIDs[] = $post->ID;
  }
}

$limit = 12;

?>
<?php get_header(); ?>
	<div id="search" class="search">
		<?php
      $count = count($destinationIDs) + count($experienceIDs) + count($tripIDs);

      echo VisitEurope_Helper::getTemplatePart('header-banner/header-banner', [
        'layout' => VisitEurope_ACF_HeaderBanner::TYPE_STRIP,
        'subtitle' => $count . ' ' . VisitEurope_Content::getLocale('search.results'),
        'title' => $title,
      ]);
    ?>
    <section class="search-form container">
      <form method="GET">
        <div class="input-group">
          <input type="text" name="term" class="form-control" value="<?php echo $searchTerm; ?>" placeholder="<?php echo VisitEurope_Content::getLocale('search.placeholder'); ?>">
          <span class="input-group-btn">
            <button type="submit" class="btn btn-default"></button>
          </span>
        </div>
      </form>
    </section>
    <?php if (empty($destinationIDs) && empty($experienceIDs) && empty($tripIDs)) { ?>
      <section class="container search-empty">
        <p><?php echo VisitEurope_Content::getLocale('search.noResults'); ?></p>
      </section>
    <?php } else { ?>
      <?php
        if (!empty($destinationIDs)) {
          echo VisitEurope_Helper::getTemplatePart('acf/flexible-content/standard_grid-destination', [
            'headerTitle' => $destinationTitle,
            'itemIDs' => $destinationIDs,
            'limit' => $limit,
            'type' => VisitEurope_Destination::TYPE_ALL,
          ]);
        }

        if (!empty($experienceIDs)) {
          echo VisitEurope_Helper::getTemplatePart('acf/flexible-content/standard_grid-experience', [
            'headerTitle' => $experiencesTitle,
            'itemIDs' => $experienceIDs,
            'limit' => $limit,
            'type' => VisitEurope_Experience::TYPE_ALL,
          ]);
        }

        if (!empty($tripIDs)) {
          echo VisitEurope_Helper::getTemplatePart('acf/flex-ble-content/standard_grid-trips', [
            'headerTitle' => $tripsTitle,
            'itemIDs' => $tripIDs,
            'limit' => 8
          ]);
        }
      ?>
    <?php } ?>
    <?php echo VisitEurope_Helper::getTemplatePart('acf/flexible-content/flexible-content'); ?>
	</div>
<?php get_footer(); ?>
