<?php
/**
 * The template for displaying a single trip
 *
 * @package Visit_Europe
 */

global $post;

$trip = new VisitEurope_Trip_Public($post);

if (!$trip->hasValidDestination()) {
	VisitEurope_Helper::throw404();
}

$segments = get_field('segments', $post->ID);
$beforeYouGo = get_field('before_you_go', $post->ID);

// Gallery
$gallery = get_field('gallery', $post->ID);
$galleryItems = [];

if (is_array($gallery) && !empty($gallery)) {
	foreach ($gallery as $galleryItem) {
		$galleryItems[] = [
			'title' => $galleryItem['title'],
			'imageinitial' => $galleryItem['image']['sizes']['initial'],
			'imagefull' => $galleryItem['image']['sizes']['gallery_large'],
			'url' => '#'
		];
	}
}

$galleryString = VisitEurope_Helper::arrayToObjectString($galleryItems);

// Destination
$destinationID = $trip->getDestinationID();
$destinationUrl = $trip->getDestinationUrl();
$destinationType = $trip->getDestinationType();
$destinationTitle = $trip->getDestinationTitle();
$destinationImageInitialUrl = $trip->getDestinationImageUrl('initial');
$destinationImageFullUrl = $trip->getDestinationImageUrl('large');

?>
<?php get_header(); ?>
	<div id="trip-public">
		<?php
			echo VisitEurope_Helper::getTemplatePart('header-banner/header-banner', [
				'save' => [
					'id' => $post->ID,
					'type' => VisitEurope_Trip::TYPE_ALL,
					'url' => get_the_permalink($post->ID),
				],
			]);
		?>
		<?php if (is_array($segments) && !empty($segments)) { ?>
			<section class="container trip-nav">
			  <div class="trip-nav-inner">
			    <div class="row trip-nav-topbar">
			      <div class="col-xs-12 tab-container">
			        <ul class="nav nav-pills">
								<?php foreach ($segments as $key => $segment) { ?>
									<li role="presentation" class="<?php echo $key == 0 ? 'active' : null; ?>">
										<a href="#" @click.prevent="scrollToID('trip-segment-<?php echo $key; ?>')">
											<?php echo $segment['title']; ?>
										</a>
									</li>
								<?php } ?>
			        </ul>
			      </div>
			  </div>
			</section>
		<?php } ?>
    <article>
      <?php if (is_array($segments) && !empty($segments)) { ?>
        <?php foreach ($segments as $key => $segment) { ?>
          <?php
            $mapMarkers = $trip->getMapMarkersForSegment($segment);
            $mapLocation = $trip->getMapLocationForSegment($segment);
            $mapPolygon = $trip->getMapPolygonForSegment($segment);
            $mapCenterString = VisitEurope_Helper::arrayToObjectString($mapLocation);
            $mapMarkersString = VisitEurope_Helper::arrayToObjectString($mapMarkers);
            $mapPolygonString = VisitEurope_Helper::arrayToObjectString($mapPolygon);
						$hasExternalItems = is_array($segment['grid_items']) && !empty($segment['grid_items']);
          ?>
          <section class="segment <?php echo $hasExternalItems ? 'has-external' : null; ?>"
						id="trip-segment-<?php echo $key; ?>">
            <section class="segment-content container">
      				<div class="overview">
                <p class="information">
                  <?php echo VisitEurope_Content::getLocale('trip.day'); ?>
                  <?php echo $segment['start_day'] . '-' . $segment['end_day']; ?>
                  <br/>
                  <?php echo VisitEurope_Content::getLocale('trip.distance.label'); ?>:
                  <?php echo $segment['distance_km'] . VisitEurope_Content::getLocale('trip.distance.km'); ?>
                  /
                  <?php echo $segment['distance_mi'] . VisitEurope_Content::getLocale('trip.distance.mi'); ?>
                </p>
      					<header>
                  <h2><?php echo $segment['title']; ?></h2>
                </header>
                <div class="content">
                  <?php echo $segment['body']; ?>
                </div>
      				</div>
              <div class="map-container">
        				<visit-europe-gmap class="map"
        					:center='<?php echo $mapCenterString; ?>'
        					:markers='<?php echo $mapMarkersString; ?>'
                  :polyline='<?php echo $mapPolygonString; ?>'
        					:fit-map-to-marker-bounds="true"
									:markers-are-clickable="true"
        					:zoom="6">
        				</visit-europe-gmap>
              </div>
            </section>
            <?php
              if ($hasExternalItems) {
                echo VisitEurope_Helper::getTemplatePart('acf/trip/grid-items', [
                  'items' => $segment['grid_items'],
                ]);
              }
            ?>
          </section>
        <?php } ?>
      <?php } ?>
    </article>
		<?php if (!empty($gallery)) { ?>
			<section class="container carousel">
				<div class="row">
					<div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
					  <carousel-large
					    :id="'carousel-large-<?php echo uniqid(); ?>'"
					    :items='<?php echo $galleryString; ?>'
							:show-content="false"
							:show-thumbnails="true">
					  </carousel-large>
					</div>
				</div>
			</section>
		<?php } ?>
		<?php if (!empty($beforeYouGo)) { ?>
			<section class="container before-you-go">
				<div class="row">
					<div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
						<header>
							<h3><?php echo VisitEurope_Content::getLocale('trip.beforeYouGo'); ?></h3>
						</header>
						<?php echo $beforeYouGo; ?>
					</div>
				</div>
			</section>
		<?php } ?>
		<aside class="container">
			<div class="row">
				<div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
					<div class="extra-information">
						<div class="destination">
							<?php echo do_shortcode('[ve_preview type="' . $destinationType . '" title="' . $destinationTitle . '" imageinitial="' . $destinationImageInitialUrl . '" imagefull="' . $destinationImageFullUrl . '" url="' . $destinationUrl . '" id="' . $destinationID . '"]'); ?>
						</div>
						<?php echo VisitEurope_Helper::getTemplatePart('acf/trip/about'); ?>
					</div>
				</div>
			</div>
		</aside>
		<?php echo VisitEurope_Helper::getTemplatePart('acf/flexible-content/flexible-content'); ?>
	</div>
<?php get_footer(); ?>
