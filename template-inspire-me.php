<?php
if (!defined('ABSPATH')) exit;

/**
 * The template for displaying inspire me results
 *
 * Template name: Inspire Me
 * @package Visit_Europe
 */

if (!VisitEurope_Feature::isActive(VisitEurope_Feature::INSPIRE_ME)) {
	VisitEurope_Helper::throw404();
}

global $post;

$search = new VisitEurope_InspireMe_Search();
$search->fill($_GET);

$im = new VisitEurope_InspireMe();
$audiences = $im->getAudiences();
$budgets = $im->getBudgets();
$desires = $im->getDesires();
$seasons = $im->getSeasons();
$temperatures = $im->getTemperatures();

$initialDestinations = $search->getDestinations();
$initialDestinationsString = null;
$initialTrips = $search->getTrips();
$initialTripsString = null;

if (is_array($initialDestinations) && !empty($initialDestinations)) {
	$initialDestinationsString = VisitEurope_Helper::arrayToObjectString($initialDestinations);
}

if (is_array($initialTrips) && !empty($initialTrips)) {
	$initialTripsString = VisitEurope_Helper::arrayToObjectString($initialTrips);
}

$selectedAudience = intval($search->selectedAudience);
$selectedBudget = intval($search->selectedBudget);
$selectedDesires = json_encode($search->selectedDesires);
$selectedSeason = intval($search->selectedSeason);
$selectedTemperature = intval($search->selectedTemperature);

?>
<?php get_header(); ?>
	<div id="inspire-me">
    <?php
      echo VisitEurope_Helper::getTemplatePart('header-banner/header-banner', [
        'layout' => VisitEurope_ACF_HeaderBanner::TYPE_STRIP,
				'title' => '<span v-html="elements.inspireMeSearch.title"></span>',
        'subtitle' => '<span v-html="elements.inspireMeSearch.subtitle"></span>',
      ]);
    ?>
		<inspire-me
			:audiences='<?php echo VisitEurope_Helper::arrayToObjectString($audiences); ?>'
			:budgets='<?php echo VisitEurope_Helper::arrayToObjectString($budgets); ?>'
			:desires='<?php echo VisitEurope_Helper::arrayToObjectString($desires); ?>'
			:seasons='<?php echo VisitEurope_Helper::arrayToObjectString($seasons); ?>'
			:temperatures='<?php echo VisitEurope_Helper::arrayToObjectString($temperatures); ?>'
			:initial-audience="<?php echo $selectedAudience; ?>"
			:initial-budget="<?php echo $selectedBudget; ?>"
			:initial-desires="<?php echo $selectedDesires; ?>"
			<?php echo !is_null($initialDestinationsString) ?
				":initial-destinations='" . $initialDestinationsString . "'" : null; ?>
			:initial-season="<?php echo $selectedSeason; ?>"
			:initial-temperature="<?php echo $selectedTemperature; ?>"
			<?php echo !is_null($initialTripsString) ?
				":initial-trips='" . $initialTripsString . "'" : null; ?>>
		</inspire-me>
		<?php echo VisitEurope_Helper::getTemplatePart('acf/flexible-content/flexible-content'); ?>
	</div>
<?php get_footer(); ?>
