<?php
/**
 * The template for displaying all single posts
 *
 * @package Visit_Europe
 */

?>
<?php get_header(); ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main">
		<?php
			while (have_posts()) {
				the_post();
				echo VisitEurope_Helper::getTemplatePart('content/' . get_post_format());
				the_post_navigation();
			}
		?>
		</main>
	</div>
<?php get_footer(); ?>
