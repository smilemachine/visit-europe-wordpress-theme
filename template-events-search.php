<?php
if (!defined('ABSPATH')) exit;

/**
 * The template for displaying events.search
 *
 * Template name: Events Search
 * @package Visit_Europe
 */

global $post;

$search = new VisitEurope_Event_Search();
$search->fill($_GET);
$itemIDs = $search->getResultsIDs();

ob_start();
echo VisitEurope_Helper::getTemplatePart('acf/flexible-content/standard_grid-experience', [
  'type' => VisitEurope_Experience::TYPE_EVENT,
  'itemIDs' => $itemIDs,
]);
$results = ob_get_clean();

?>
<?php get_header(); ?>
	<div id="events" class="search">
		<?php
      echo VisitEurope_Helper::getTemplatePart('header-banner/header-banner', [
        'layout' => VisitEurope_ACF_HeaderBanner::TYPE_STRIP,
        'subtitle' => count($itemIDs) . ' ' . VisitEurope_Content::getLocale('search.results'),
      ]);
    ?>
    <?php if (!empty($results)) { ?>
      <?php echo $results; ?>
    <?php } else { ?>
      <section class="container search-empty">
        <p><?php echo VisitEurope_Content::getLocale('search.eventsNoResults'); ?></p>
      </section>
    <?php } ?>
    <?php echo VisitEurope_Helper::getTemplatePart('acf/flexible-content/flexible-content'); ?>
	</div>
<?php get_footer(); ?>
