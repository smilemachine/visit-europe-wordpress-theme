<?php
if (!defined('ABSPATH')) exit;

/**
 * Setup for Visit Europe
 *
 * @package Visit_Europe
 */
class VisitEurope {

  public function __construct() {
    //
  }

  /**
   * Setup VisitEurope
   */
  public function setup() {
    $theme = new VisitEurope_Theme();
    $theme->setup();

    $auth = new VisitEurope_Auth();
    $auth->setup();

    $api = new VisitEurope_API();
    $api->setup();

    $content = new VisitEurope_Content();
    $content->setup();
  }

}
