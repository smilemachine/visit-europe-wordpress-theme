<?php
if (!defined('ABSPATH')) exit;

require_once dirname(__FILE__) . '/interface-visiteurope-cpt.php';

/**
 * User Trip User Trips CTP
 *
 * @package Visit_Europe
 */
class VisitEurope_CPT_UserTrip implements VisitEurope_CPT {

  const POST_TYPE = 've_usertrip';

  /**
   * Setup the User Trips CTP
   *
   * @return void
   */
  public function setup() {
    add_action('init', [self::class, 'register']);
    add_filter('manage_' . self::POST_TYPE . '_posts_columns', [self::class, 'setupIndexColumns']);
    add_action('manage_' . self::POST_TYPE . '_posts_custom_column', [self::class, 'setupIndexCustomColumn'], 10, 2);
  }

  /**
   * Registers User Trips CTP
   *
   * @return void
   */
  public static function register() {
    register_post_type(self::POST_TYPE, [
      'labels' => [
        'name' => 'User Trips',
        'singular_name' => 'User Trip',
        'add_new' => 'Add New',
        'add_new_item' => 'Add New User Trip',
        'edit_item' => 'Edit User Trip',
        'new_item' => 'New User Trip',
        'view_item' => 'View User Trip',
        'view_items' => 'View User Trips',
        'search_items' => 'Search User Trips',
        'not_found' => 'No user trips found',
        'not_found_in_trash' => 'No user trips found in Trash',
        'parent_item_colon' => 'Parent User Trip',
        'all_items' => 'All User Trips',
        'archives' => 'User Trip Archives',
        'attributes' => 'User Trip Attributes',
        'insert_into_item' => 'Insert into user trip',
        'upload_to_this_item' => 'Insert into user trip',
      ],
      'public' => true,
      'show_ui' => true,
      'show_in_nav_menus' => false,
      'show_in_menu' => true, // some-string? Visit Europe for example
      'show_in_admin_bar' => false,
      'menu_position' => 20,
      'menu_icon' => 'dashicons-sticky',
      'hierarchical' => false,
      'supports' => ['title'],
      'taxonomies' => [],
      'has_archive' => false,
      'rewrite' => [
        'slug' => 'account-trip',
        'with_front' => false,
        'feeds' => false,
        'pages' => false,
      ]
    ]);
  }

  /**
   * Add custom column header to the User Trip index table
   *
   * @param integer $postID
   * @return string
   */
  public static function setupIndexColumns($columns) {
    $dateColumn = $columns['date'];
    unset($columns['date']);
    $columns['date'] = $dateColumn;

    return $columns;
  }

  /**
   * Add custom column content to the User Trip index table
   *
   * @param string $column
   * @param integer $postID
   * @return void
   */
  public static function setupIndexCustomColumn($column, $postID) {
    //
  }

}
