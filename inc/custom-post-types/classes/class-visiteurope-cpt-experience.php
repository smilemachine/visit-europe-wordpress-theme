<?php
if (!defined('ABSPATH')) exit;

require_once dirname(__FILE__) . '/interface-visiteurope-cpt.php';

/**
 * Experience Experiences CTP
 *
 * @package Visit_Europe
 */
class VisitEurope_CPT_Experience implements VisitEurope_CPT {

  const POST_TYPE = 've_experience';

  /**
   * Setup the Experiences CTP
   *
   * @return void
   */
  public function setup() {
    add_action('init', [self::class, 'register']);
    add_filter('manage_' . self::POST_TYPE . '_posts_columns', [self::class, 'setupIndexColumns']);
    add_action('manage_' . self::POST_TYPE . '_posts_custom_column', [self::class, 'setupIndexCustomColumn'], 10, 2);
    add_action('restrict_manage_posts', [self::class, 'setupManagePosts']);
    add_filter('parse_query', [self::class, 'setupParseQuery']);
  }

  /**
   * Registers Experiences CTP
   *
   * @return void
   */
  public static function register() {
    register_post_type(self::POST_TYPE, [
      'labels' => [
        'name' => 'Experiences',
        'singular_name' => 'Experience',
        'add_new' => 'Add New',
        'add_new_item' => 'Add New Experience',
        'edit_item' => 'Edit Experience',
        'new_item' => 'New Experience',
        'view_item' => 'View Experience',
        'view_items' => 'View Experiences',
        'search_items' => 'Search Experiences',
        'not_found' => 'No experiences found',
        'not_found_in_trash' => 'No experiences found in Trash',
        'parent_item_colon' => 'Parent Experience',
        'all_items' => 'All Experiences',
        'archives' => 'Experience Archives',
        'attributes' => 'Experience Attributes',
        'insert_into_item' => 'Insert into experience',
        'upload_to_this_item' => 'Insert into experience',
      ],
      'public' => true,
      'show_ui' => true,
      'show_in_nav_menus' => false,
      'show_in_menu' => true, // some-string? Visit Europe for example
      'show_in_admin_bar' => false,
      'menu_position' => 20,
      'menu_icon' => 'dashicons-tickets-alt',
      'hierarchical' => false,
      'supports' => ['title', 'thumbnail', 'revisions'],
      'taxonomies' => [VisitEurope_Taxonomy_Interest::TAXONOMY],
      'has_archive' => false,
      'rewrite' => [
        'slug' => 'experience',
        'with_front' => false,
        'feeds' => false,
        'pages' => false,
      ]
    ]);
  }

  /**
   * Add custom column header to the Experience index table
   *
   * @param integer $postID
   * @return string
   */
  public static function setupIndexColumns($columns) {
    $dateColumn = $columns['date'];
    unset($columns['date']);

    $columns['type'] = 'Type';
    $columns['destination'] = 'Destinations';
    $columns['date'] = $dateColumn;

    return $columns;
  }

  /**
   * Add custom column content to the Experience index table
   *
   * @param string $column
   * @param integer $postID
   * @return void
   */
  public static function setupIndexCustomColumn($column, $postID) {
    switch ($column) {
      case 'type':
        echo VisitEurope_Experience::getTypeForPostID($postID);
        break;
      case 'destination':
        $regions = VisitEurope_Experience::getRegionsForPostID($postID, [], true);
        $countries = VisitEurope_Experience::getCountriesForPostID($postID, [], true);

        echo implode(', ', $regions + $countries);
        break;
      default:
        break;
    }
  }

  /**
   * Add custom admin filters for Experiences
   *
   * @return string
   */
  public static function setupManagePosts() {
    if (!is_admin() || !isset($_GET['post_type']) || $_GET['post_type'] !== self::POST_TYPE) {
      return;
    }

    $values = [
      VisitEurope_Experience::TYPE_ARTICLE => 'Articles',
      VisitEurope_Experience::TYPE_EVENT => 'Events',
      VisitEurope_Experience::TYPE_EVENT_PRIVATE => 'Events (Private)',
      VisitEurope_Experience::TYPE_GALLERY => 'Galleries',
      VisitEurope_Experience::TYPE_PRODUCT => 'Products',
      VisitEurope_Experience::TYPE_RECIPE => 'Recipes',
      VisitEurope_Experience::TYPE_ROUTE => 'Routes',
    ];

    ob_start();
?>
<select name="<?php echo self::POST_TYPE; ?>_type_filter">
  <option value="">All Types</option>
  <?php foreach ($values as $key => $value) { ?>
    <?php
      $isSelected = false;
      $filter = self::POST_TYPE . '_type_filter';

      if (isset($_GET[$filter]) && $key == $_GET[$filter]) {
        $isSelected = true;
      }
    ?>
    <option value="<?php echo $key; ?>" <?php echo $isSelected ? 'selected' : null; ?>>
      <?php echo $value; ?>
    </option>
  <?php } ?>
</select>
<?php
    echo ob_get_clean();
  }

  /**
   * Add custom admin sorting for Experiences
   *
   * @param WP_Query $query
   * @return void
   */
  public static function setupParseQuery($query) {
    global $pagenow;

    $key = self::POST_TYPE . '_type_filter';

    if (!is_admin() || !isset($_GET['post_type']) || $_GET['post_type'] !== self::POST_TYPE || $pagenow != 'edit.php'
      || !isset($_GET[$key]) || $_GET[$key] == '') {
      return;
    }

    $sortByType = null;

    switch ($_GET[$key]) {
      case VisitEurope_Experience::TYPE_ARTICLE:
        $sortByType = 'Article';
        break;
      case VisitEurope_Experience::TYPE_EVENT:
        $sortByType = 'Event';
        break;
      case VisitEurope_Experience::TYPE_EVENT_PRIVATE:
        $sortByType = 'Event Private';
        break;
      case VisitEurope_Experience::TYPE_GALLERY:
        $sortByType = 'Gallery';
        break;
      case VisitEurope_Experience::TYPE_PRODUCT:
        $sortByType = 'Product';
        break;
      case VisitEurope_Experience::TYPE_RECIPE:
        $sortByType = 'Recipe';
        break;
      case VisitEurope_Experience::TYPE_ROUTE:
        $sortByType = 'Route';
        break;
      default:
        break;
    }

    $query->query_vars['meta_key'] = 'type';
    $query->query_vars['meta_value'] = $sortByType;
  }

}
