<?php
if (!defined('ABSPATH')) exit;

require_once dirname(__FILE__) . '/interface-visiteurope-cpt.php';

/**
 * Advertisement CTP
 *
 * @package Visit_Europe
 */
class VisitEurope_CPT_Advertisement implements VisitEurope_CPT {

  const POST_TYPE = 've_advertisement';

  /**
   * Setup the Advertisements CTP
   *
   * @return void
   */
  public function setup() {
    add_action('init', [self::class, 'register']);
  }

  /**
   * Registers Advertisement CTP
   *
   * @return void
   */
  public static function register() {
    register_post_type(self::POST_TYPE, [
      'labels' => [
        'name' => 'Advertisements',
        'singular_name' => 'Advertisement',
        'add_new' => 'Add New',
        'add_new_item' => 'Add New Advertisement',
        'edit_item' => 'Edit Advertisement',
        'new_item' => 'New Advertisement',
        'view_item' => 'View Advertisement',
        'view_items' => 'View Advertisements',
        'search_items' => 'Search Advertisements',
        'not_found' => 'No advertisements found',
        'not_found_in_trash' => 'No advertisements found in Trash',
        'parent_item_colon' => 'Parent Advertisement',
        'all_items' => 'All Advertisements',
        'archives' => 'Advertisement Archives',
        'attributes' => 'Advertisement Attributes',
        'insert_into_item' => 'Insert into advertisement',
        'upload_to_this_item' => 'Insert into advertisement',
      ],
      'public' => false,
      'show_ui' => true,
      'show_in_nav_menus' => false,
      'show_in_menu' => true, // some-string? Visit Europe for example
      'show_in_admin_bar' => false,
      'menu_position' => 20,
      'menu_icon' => 'dashicons-megaphone',
      'hierarchical' => false,
      'supports' => ['title', 'thumbnail'],
      'has_archive' => false,
      'rewrite' => [
        'slug' => 'advertisement',
        'with_front' => false,
        'feeds' => false,
        'pages' => false,
      ]
    ]);
  }

}
