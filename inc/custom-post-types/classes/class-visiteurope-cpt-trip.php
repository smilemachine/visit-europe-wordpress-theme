<?php
if (!defined('ABSPATH')) exit;

require_once dirname(__FILE__) . '/interface-visiteurope-cpt.php';

/**
 * Trip Trips CTP
 *
 * @package Visit_Europe
 */
class VisitEurope_CPT_Trip implements VisitEurope_CPT {

  const POST_TYPE = 've_trip';

  /**
   * Setup the Trips CTP
   *
   * @return void
   */
  public function setup() {
    add_action('init', [self::class, 'register']);
    add_filter('manage_' . self::POST_TYPE . '_posts_columns', [self::class, 'setupIndexColumns']);
    add_action('manage_' . self::POST_TYPE . '_posts_custom_column', [self::class, 'setupIndexCustomColumn'], 10, 2);
  }

  /**
   * Registers Trips CTP
   *
   * @return void
   */
  public static function register() {
    register_post_type(self::POST_TYPE, [
      'labels' => [
        'name' => 'Trips',
        'singular_name' => 'Trip',
        'add_new' => 'Add New',
        'add_new_item' => 'Add New Trip',
        'edit_item' => 'Edit Trip',
        'new_item' => 'New Trip',
        'view_item' => 'View Trip',
        'view_items' => 'View Trips',
        'search_items' => 'Search Trips',
        'not_found' => 'No trips found',
        'not_found_in_trash' => 'No trips found in Trash',
        'parent_item_colon' => 'Parent Trip',
        'all_items' => 'All Trips',
        'archives' => 'Trip Archives',
        'attributes' => 'Trip Attributes',
        'insert_into_item' => 'Insert into trip',
        'upload_to_this_item' => 'Insert into trip',
      ],
      'public' => true,
      'show_ui' => true,
      'show_in_nav_menus' => false,
      'show_in_menu' => true, // some-string? Visit Europe for example
      'show_in_admin_bar' => false,
      'menu_position' => 20,
      'menu_icon' => 'dashicons-location',
      'hierarchical' => false,
      'supports' => ['title', 'thumbnail'],
      'taxonomies' => [
        VisitEurope_Taxonomy_Audience::TAXONOMY,
        VisitEurope_Taxonomy_Budget::TAXONOMY,
        VisitEurope_Taxonomy_Interest::TAXONOMY,
        VisitEurope_Taxonomy_TripType::TAXONOMY,
        VisitEurope_Taxonomy_TripActivityLevel::TAXONOMY,
      ],
      'has_archive' => false,
      'rewrite' => [
        'slug' => 'trip',
        'with_front' => false,
        'feeds' => false,
        'pages' => false,
      ]
    ]);
  }

  /**
   * Add custom column header to the Trip index table
   *
   * @param integer $postID
   * @return string
   */
  public static function setupIndexColumns($columns) {
    $dateColumn = $columns['date'];
    unset($columns['date']);
    $columns['date'] = $dateColumn;

    return $columns;
  }

  /**
   * Add custom column content to the Trip index table
   *
   * @param string $column
   * @param integer $postID
   * @return void
   */
  public static function setupIndexCustomColumn($column, $postID) {
    //
  }

}
