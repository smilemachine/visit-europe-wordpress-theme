<?php
if (!defined('ABSPATH')) exit;

/**
 * Custom post type interface
 *
 * @package Visit_Europe
 */
interface VisitEurope_CPT {
  public function setup();
  public static function register();
}
