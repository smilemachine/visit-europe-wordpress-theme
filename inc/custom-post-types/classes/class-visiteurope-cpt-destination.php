<?php
if (!defined('ABSPATH')) exit;

require_once dirname(__FILE__) . '/interface-visiteurope-cpt.php';

/**
 * Destinations CTP
 *
 * @package Visit_Europe
 */
class VisitEurope_CPT_Destination implements VisitEurope_CPT {

  const POST_TYPE = 've_destination';

  /**
   * Setup the Destinations CTP
   *
   * @return void
   */
  public function setup() {
    add_action('init', [self::class, 'register']);
    add_action('add_meta_boxes', [self::class, 'addMetaBoxes']);
    add_action('save_post_' . self::POST_TYPE, [self::class, 'savePost'], 10, 2);
    add_filter('manage_' . self::POST_TYPE . '_posts_columns', [self::class, 'setupIndexColumns']);
    add_action('manage_' . self::POST_TYPE . '_posts_custom_column', [self::class, 'setupIndexCustomColumn'], 10, 2);
    add_action('restrict_manage_posts', [self::class, 'setupManagePosts']);
    add_filter('parse_query', [self::class, 'setupParseQuery']);
  }

  /**
   * Registers Destination CTP
   *
   * @return void
   */
  public static function register() {
    register_post_type(self::POST_TYPE, [
      'labels' => [
        'name' => 'Destinations',
        'singular_name' => 'Destination',
        'add_new' => 'Add New',
        'add_new_item' => 'Add New Destination',
        'edit_item' => 'Edit Destination',
        'new_item' => 'New Destination',
        'view_item' => 'View Destination',
        'view_items' => 'View Destinations',
        'search_items' => 'Search Destinations',
        'not_found' => 'No destinations found',
        'not_found_in_trash' => 'No destinations found in Trash',
        'parent_item_colon' => 'Parent Destination',
        'all_items' => 'All Destinations',
        'archives' => 'Destination Archives',
        'attributes' => 'Destination Attributes',
        'insert_into_item' => 'Insert into destination',
        'upload_to_this_item' => 'Insert into destination',
      ],
      'public' => true,
      'show_ui' => true,
      'show_in_nav_menus' => false,
      'show_in_menu' => true, // some-string? Visit Europe for example
      'show_in_admin_bar' => false,
      'menu_position' => 20,
      'menu_icon' => 'dashicons-admin-site',
      'hierarchical' => false,
      'supports' => ['title', 'editor', 'thumbnail', 'excerpt', 'revisions'],
      'taxonomies' => [],
      'has_archive' => false,
      'rewrite' => [
        'slug' => 'destination',
        'with_front' => false,
        'feeds' => false,
        'pages' => false,
      ]
    ]);
  }

  /**
   * Adds the Destination meta boxes
   *
   * @return void
   */
  public static function addMetaBoxes() {
    add_meta_box(self::POST_TYPE . '_type_meta_box', 'Destination Type', [self::class, 'renderTypeMetaBox'], self::POST_TYPE, 'side', 'default');
    add_meta_box(self::POST_TYPE . '_weather_meta_box', 'Destination Weather', [self::class, 'renderWeatherMetaBox'], self::POST_TYPE, 'side', 'default');
  }

  /**
   * Renders the Destination type meta box
   *
   * @return void
   */
  public static function renderTypeMetaBox($post) {
    $countries = VisitEurope_Destination::getCountries([], true);
    $countryList = json_encode($countries);
    $countriesSelected = VisitEurope_Destination::getCountriesForPostID($post->ID, [], true);
    $countryListSelected = json_encode(array_keys($countriesSelected));
    $currentType = VisitEurope_Destination::getTypeForPostID($post->ID);
    $types = json_encode(VisitEurope_Destination::getTypes());

    ob_start();
    wp_nonce_field(basename(__FILE__), self::POST_TYPE . '_type_meta_box_nonce');
    include dirname(__FILE__) . './../views/destination-type-meta-box.php';
    echo ob_get_clean();
  }

  /**
   * Renders the Destination weather meta box
   *
   * @return void
   */
  public static function renderWeatherMetaBox($post) {
    $veDestination = new VisitEurope_Destination($post);
    $weather = $veDestination->presentPostWeather();
    ob_start();
    wp_nonce_field(basename(__FILE__), self::POST_TYPE . '_weather_meta_box_nonce');
    include dirname(__FILE__) . './../views/destination-weather-meta-box.php';
    echo ob_get_clean();
  }

  /**
   * Save the post event
   *
   * @param integer $postID
   * @return void
   */
  public static function savePost($postID) {
    self::saveTypeMetaBox($postID);
    self::saveWeatherMetaBox($postID);
  }

  /**
   * Save the Destination type meta box
   *
   * @param integer $postID
   * @return void
   */
  private static function saveTypeMetaBox($postID) {
    $nonceKey = self::POST_TYPE . '_type_meta_box_nonce';

    if (!isset($_POST[$nonceKey]) || !wp_verify_nonce($_POST[$nonceKey], basename(__FILE__))) {
      return;
    }

    if (!current_user_can('edit_post', $postID)) {
      return;
    }

    if (isset($_POST['type'])) {
      VisitEurope_Destination::updateTypeForPostID($postID, $_POST['type']);

      if ($_POST['type'] == VisitEurope_Destination::TYPE_REGION && isset($_POST['countries'])) {
        VisitEurope_Destination::updateCountriesForPostID($postID, $_POST['countries']);
      } else {
        VisitEurope_Destination::deleteAllCountriesForPostID($postID);
      }
    }
  }

  /**
   * Save the weather associated to this post IF it's type is a Country
   *
   * @param integer $postID
   * @return void
   */
  private static function saveWeatherMetaBox($postID) {
    $nonceKey = self::POST_TYPE . '_weather_meta_box_nonce';

    if (!isset($_POST[$nonceKey]) || !wp_verify_nonce($_POST[$nonceKey], basename(__FILE__))) {
      return;
    }

    if (!current_user_can('edit_post', $postID)) {
      return;
    }

    if (isset($_POST['weather'])) {
      if (isset($_POST['type']) && $_POST['type'] == VisitEurope_Destination::TYPE_COUNTRY) {
        VisitEurope_Destination::updateWeatherForPostID($postID, $_POST['weather']);
      } else {
        VisitEurope_Destination::deleteAllWeatherForPostID($postID);
      }
    } else {
      VisitEurope_Destination::deleteAllWeatherForPostID($postID);
    }
  }

  /**
   * Add custom column header to the Experience index table
   *
   * @param integer $postID
   * @return string
   */
  public static function setupIndexColumns($columns) {
    $dateColumn = $columns['date'];
    unset($columns['date']);

    $columns['type'] = 'Type';
    $columns['date'] = $dateColumn;

    return $columns;
  }

  /**
   * Add custom column content to the Experience index table
   *
   * @param string $column
   * @param integer $postID
   * @return void
   */
  public static function setupIndexCustomColumn($column, $postID) {
    switch ($column) {
      case 'type':
        echo VisitEurope_Destination::getTypeForPostID($postID);
        break;
      default:
        break;
    }
  }

  /**
   * Add custom admin filters for Experiences
   *
   * @return string
   */
  public static function setupManagePosts() {
    if (!is_admin() || !isset($_GET['post_type']) || $_GET['post_type'] !== self::POST_TYPE) {
      return;
    }

    $values = [
      VisitEurope_Destination::TYPE_COUNTRY => 'Countries',
      VisitEurope_Destination::TYPE_REGION => 'Regions',
    ];

    ob_start();
?>
<select name="<?php echo self::POST_TYPE; ?>_type_filter">
  <option value="">All Types</option>
  <?php foreach ($values as $key => $value) { ?>
    <?php
      $isSelected = false;
      $filter = self::POST_TYPE . '_type_filter';

      if (isset($_GET[$filter]) && $key == $_GET[$filter]) {
        $isSelected = true;
      }
    ?>
    <option value="<?php echo $key; ?>" <?php echo $isSelected ? 'selected' : null; ?>>
      <?php echo $value; ?>
    </option>
  <?php } ?>
</select>
<?php
    echo ob_get_clean();
  }

  /**
   * Add custom admin sorting for Experiences
   *
   * @param WP_Query $query
   * @return void
   */
  public static function setupParseQuery($query) {
    global $pagenow;

    $key = self::POST_TYPE . '_type_filter';

    if (!is_admin() || !isset($_GET['post_type']) || $_GET['post_type'] !== self::POST_TYPE || $pagenow != 'edit.php'
      || !isset($_GET[$key]) || $_GET[$key] == '') {
      return;
    }

    $sortByType = null;
    $allowedTypes = [VisitEurope_Destination::TYPE_COUNTRY, VisitEurope_Destination::TYPE_REGION];

    if (in_array($_GET[$key], $allowedTypes)) {
      $sortByType = $_GET[$key];
    }

    $query->query_vars['meta_key'] = self::POST_TYPE . '_TYPE';
    $query->query_vars['meta_value'] = $sortByType;
  }

}
