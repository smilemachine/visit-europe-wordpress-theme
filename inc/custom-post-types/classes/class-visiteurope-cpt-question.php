<?php
if (!defined('ABSPATH')) exit;

require_once dirname(__FILE__) . '/interface-visiteurope-cpt.php';

/**
 * Questions CTP
 *
 * @package Visit_Europe
 */
class VisitEurope_CPT_Question implements VisitEurope_CPT {

  const POST_TYPE = 've_question';

  /**
   * Setup the Questions CTP
   *
   * @return void
   */
  public function setup() {
    add_action('init', [self::class, 'register']);
  }

  /**
   * Registers Question CTP
   *
   * @return void
   */
  public static function register() {
    register_post_type(self::POST_TYPE, [
      'labels' => [
        'name' => 'Questions',
        'singular_name' => 'Question',
        'add_new' => 'Add New',
        'add_new_item' => 'Add New Question',
        'edit_item' => 'Edit Question',
        'new_item' => 'New Question',
        'view_item' => 'View Question',
        'view_items' => 'View Questions',
        'search_items' => 'Search Questions',
        'not_found' => 'No questions found',
        'not_found_in_trash' => 'No questions found in Trash',
        'parent_item_colon' => 'Parent Question',
        'all_items' => 'All Questions',
        'archives' => 'Question Archives',
        'attributes' => 'Question Attributes',
        'insert_into_item' => 'Insert into question',
        'upload_to_this_item' => 'Insert into question',
      ],
      'public' => false,
      'show_ui' => true,
      'show_in_nav_menus' => false,
      'show_in_menu' => true, // some-string? Visit Europe for example
      'show_in_admin_bar' => false,
      'menu_position' => 20,
      'menu_icon' => 'dashicons-testimonial',
      'hierarchical' => false,
      'supports' => ['title', 'editor', 'thumbnail'],
      'has_archive' => false,
      'rewrite' => [
        'slug' => 'question',
        'with_front' => false,
        'feeds' => false,
        'pages' => false,
      ]
    ]);
  }

}
