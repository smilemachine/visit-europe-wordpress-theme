<?php
if (!defined('ABSPATH')) exit;

/**
 * Metabox: Experience Weather
 *
 * @package Visit_Europe
 */

?>
<div id="destination-weather-meta-box" class="ve-meta-box">
	<div class="tab-container">
		<ul class="tabs">
      <li v-for="(season, key) in seasons"
        :class="{ active:isTabSelected(key) }">
				<a href="#" @click.prevent="selectTab(key)">
					{{ season.title }}
				</a>
			</li>
		</ul>
		<div class="tab-panel"
      v-for="(season, seasonKey) in seasons"
      v-show="isTabSelected(seasonKey)">
      <table>
        <thead>
          <tr>
            <th>Mth</th>
            <th>Low</th>
            <th>High</th>
          </tr>
        </thead>
        <tbody>
          <tr v-for="(month, monthKey) in season.months">
            <td>
              {{ month.title }}
            </td>
            <td>
							<input type="number" :name="'weather[' + seasonKey + '][' + monthKey + '][low]'"
                v-model="seasons[seasonKey].months[monthKey].low">
						</td>
            <td>
							<input type="number" :name="'weather[' + seasonKey + '][' + monthKey + '][high]'"
                v-model="seasons[seasonKey].months[monthKey].high">
						</td>
          </tr>
        </tbody>
      </table>
      <div v-if="showSyncMessage" class="sync-status"
        :class="{ updated:!syncError, error:syncError }">
        {{ syncMessage }}
      </div>
      <p><a href="#" @click.prevent="syncSeason">Sync Season</a></p>
    </div>
	</div>
</div>

<script>
document.addEventListener('DOMContentLoaded', function() {
  var app = new Vue({
    el: '#destination-weather-meta-box',
		http: {
			root: VisitEuropeUrls.api
		},
    data: function () {
      var seasons = <?php echo json_encode($weather['seasons']); ?>;

      return {
        seasons,
				selectedTab: 'summer',
        syncMessage: null,
        syncError: false
      };
    },
		methods: {
			isTabSelected(tab) {
				return this.selectedTab == tab;
			},
      handleSuccessResponse(response) {
        response.json()
          .then(function(json) {
            let high = json.trip.temp_high.avg['C'];
            let low = json.trip.temp_low.avg['C'];
            let key = json.trip.period_of_record.date_start.date.monthname_short.toLowerCase();

            this.seasons[this.selectedTab].months[key].high = high;
            this.seasons[this.selectedTab].months[key].low = low;
            this.syncMessage = 'Season weather synced. Please save.';
            this.syncError = false;
          }.bind(this))
          .catch(function(error) {
            this.syncMessage = 'Error syncing weather. Please try again.';
            this.syncError = true;
          }.bind(this));
      },
      handleErrorResponse(response) {
				if (!_.isUndefined(response.body.message)) {
					this.syncMessage = response.body.message;
				} else {
					this.syncMessage = 'Error syncing weather. Please try again.';
				}

        this.syncError = true;
      },
			selectTab(tab) {
        this.syncMessage = null;
        this.syncError = false;
				this.selectedTab = tab;
			},
      syncSeason() {
        let lat = Number(jQuery('[data-name="map_lat"] input').val());
        let lng = Number(jQuery('[data-name="map_lng"] input').val());
        let season = this.seasons[this.selectedTab];

        if (lat == 0 || isNaN(lat) || lng == 0 || isNaN(lng)) {
          this.syncMessage = 'Please enter a valid latitude and longitude.';
          this.syncError = true;
        } else {
          for (var index in season.months) {
						let data = {
							params: {
			        	startDay: season.months[index].key + '01',
								endDay: season.months[index].key + '28',
								lat: lat,
								lng: lng
							}
			      }

            this.$http.post('destinations/weather', data)
              .then(response => {
                this.handleSuccessResponse(response);
              }, response => {
                this.handleErrorResponse(response);
              });
          }
        }
      }
		},
		computed: {
      showSyncMessage() {
        return this.syncMessage !== null && this.syncMessage.length > 0;
      }
    }
  });
});
</script>
