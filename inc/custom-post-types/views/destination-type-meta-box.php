<?php
if (!defined('ABSPATH')) exit;

/**
 * Metabox: Destination Type
 *
 * @package Visit_Europe
 */

?>
<div id="destination-type-meta-box" class="ve-meta-box">
  <select v-model="type" name="type">
		<option v-for="(value, key) in typeList" :value="key">{{ value }}</option>
	</select>
  <div v-if="showCountries">
    <div v-if="hasCountries">
      <h4><?php echo _x('Assign countries to this region.', 'Destination meta box: Assign countries', VisitEurope_Theme::KEY); ?></h4>
      <ul>
        <li v-for="(value, key) in countryList">
          <label>
            <input :value="key" type="checkbox" name="countries[]" v-model="countries">
            {{ value }}
          </label>
        </li>
      </ul>
    </div>
    <div v-if="!hasCountries">
      <p><?php echo _x('No countries found.', 'Destination meta box: no countries found', VisitEurope_Theme::KEY); ?></p>
    </div>
  </div>
</div>

<script>
  var app = new Vue({
    el: '#destination-type-meta-box',
    data: function () {
      var countries = <?php echo $countryListSelected; ?>;
      var countryList = <?php echo $countryList; ?>;
      var type = '<?php echo $currentType; ?>';
      var typeList = <?php echo $types; ?>;

      return {
        countries,
        countryList,
        type,
        typeList,
      };
    },
    computed: {
      showCountries: function () {
        return this.type == '<?php echo VisitEurope_Destination::TYPE_REGION; ?>';
      },
      hasCountries: function () {
        return this.countryList !== undefined && Object.keys(this.countryList).length > 0;
      }
    },
    watch: {
      type: function (newValue) {
        if (newValue == '<?php echo VisitEurope_Destination::TYPE_COUNTRY; ?>') {
          this.countries = [];
        }
      }
    }
  });
</script>
