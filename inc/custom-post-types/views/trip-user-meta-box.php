<div id="trip-user-meta-box" class="ve-meta-box">
	<select v-model="user" name="user">
		<option value=""><?php echo _x('Public Trip', 'Trip meta box: Public option', VisitEurope_Theme::KEY); ?></option>
		<option v-for="(value, key) in userList" :value="key">{{ value }}</option>
	</select>
</div>

<script>
document.addEventListener('DOMContentLoaded', function() {
  var app = new Vue({
    el: '#trip-user-meta-box',
    data: function () {
      var user = '<?php echo $user; ?>';
      var userList = <?php echo $users; ?>;

      return {
        user,
        userList,
      };
    }
  });
});
</script>
