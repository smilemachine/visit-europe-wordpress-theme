<?php
if (!defined('ABSPATH')) exit;

/**
 * Content import for Visit Europe: Events
 *
 * @package Visit_Europe
 */
class VisitEurope_Importer_Event {

  public function __construct() {
    //
  }

  /**
   * Import all events
   *
   * @param array $ids
   * @return void
   */
  public function importExistingEvents($ids = []) {
    $existing = $this->getExistingEvents($ids);

    if (is_array($existing) && !empty($existing)) {
      foreach ($existing as $event) {
        $this->createEventFromExistingEvent($event);
      }
    }
  }

  /**
   * Create a new event from an existing source
   *
   * @param stdClass $event
   * @return boolean
   */
  private function createEventFromExistingEvent($event) {
    $data = [
      'post_date' => date('Y-m-d H:i:s', $event->published_date),
      'post_content' => '',
      'post_title' => $event->title,
      'post_excerpt' => '',
      'post_status' => 'publish',
      'post_type' => VisitEurope_CPT_Experience::POST_TYPE,
      'comment_status' => 'closed',
      'ping_status' => 'closed',
    ];

    // Check if this post already exists
    $posts = get_posts([
      'posts_per_page' => 1,
      'post_type' => VisitEurope_CPT_Experience::POST_TYPE,
      'meta_query' => [
        [
          'key' => VisitEurope_Importer::KEY_IMPORT_ID,
          'value' => $event->id,
        ],
        [
          'key' => VisitEurope_Importer::KEY_IMPORT_REVISION,
          'value' => $event->revision_id,
        ],
        [
          'key' => 'field_59a9639887cd0',
          'value' => 'Event',
        ]
      ]
    ]);

    // If it doesn't exist, create it and store the original ID
    if (empty($posts)) {
      $postID = wp_insert_post($data);
      if ($postID == 0) { die('Could not create event for #' . $event->id); }

      add_post_meta($postID, VisitEurope_Importer::KEY_IMPORT_REVISION, $event->revision_id);
      add_post_meta($postID, VisitEurope_Importer::KEY_IMPORT_ID, $event->id);

      $post = get_post($postID);
    } else {
      $post = $posts[0];
    }

    // Get the feature image and assign it to this Event
    VisitEurope_Importer_Media_Drupal::setPostFeatureImageForExistingMediaID($post->ID, $event->image_id);

    // Save the meta data
    $this->saveEventContent($post, $event);
  }

  /**
   * Save the content tabs for the given event
   *
   * @param WP_Post $post
   * @param stdClass $event
   * @return void
   */
  private function saveEventContent($post, $event) {
    update_field('field_59afbbdff80f0', $event->latitude, $post->ID);  // Map lat
    update_field('field_59afbbb9f80ef', $event->longitude, $post->ID);  // Map lng
    update_field('field_59bad778ee7d6', 6, $post->ID);  // Map zoom
    update_field('field_59a9639887cd0', 'Event', $post->ID); // Type
    update_field('field_59b669daa33ad', strip_tags($event->blurb), $post->ID); // Blurb

    $startDate = date('Y-m-d', strtotime($event->start_date));
    $startTime = date('H:i', strtotime($event->start_date));
    $endDate = date('Y-m-d', strtotime($event->end_date));
    $endTime = date('H:i', strtotime($event->end_date));

    update_field('field_59bad795ee7d7', $startDate, $post->ID); // Start date
    update_field('field_59bad832ee7d9', $startTime, $post->ID); // Start time

    update_field('field_59bad824ee7d8', $endDate, $post->ID); // End date
    update_field('field_59bad84eee7da', $endTime, $post->ID); // End time

    // Pointers
    $pointers = [];
    
    if (!empty($event->city)) {
      $pointers[] = [
        'type' => 'City',
        'subtitle' => $event->city,
      ];
    }

    update_field('field_59a96b0d07e5a', $pointers, $post->ID);  // Event Pointers

    // About
    $about = [];
    $links = VisitEurope_Importer_Drupal::getExistingLinksForExistingEntity($event->id, $event->revision_id);

    foreach ($links as $link) {
      $about[] = [
        'title' => $link->title,
        'url' => $link->url,
      ];
    }

    update_field('field_59a96a32deb6d', $about, $post->ID);  // Event About

    // Tags
    $destinationIDs = [];
    $tags = [];
    $existingTags = VisitEurope_Importer_Drupal::getExistingTagsForExistingEntity($event->id, $event->revision_id);

    if (is_array($existingTags) && !empty($existingTags)) {
      foreach ($existingTags as $existingTag) {
        // If a destination exists with this tag name, use the destination instead
        $existing = get_posts([
          'post_type' => VisitEurope_CPT_Destination::POST_TYPE,
          'title' => $existingTag->tag,
          'posts_per_page' => 1,
        ]);

        if (is_array($existing) && !empty($existing)) {
          $destinationIDs[] = $existing[0]->ID;
        } else if (is_array($existing) && empty($existing)) {
          $tags[] = $existingTag->tag;
        }
      }
    }

    wp_set_post_terms($post->ID, $tags, VisitEurope_Taxonomy_Interest::TAXONOMY, false);

    // If destinations are empty, try the log-way-around for the location heirarchy
    if (empty($destinationIDs)) {
      $destination = VisitEurope_Importer_Drupal::getDestinationForExistingEntity($event->id, $event->revision_id);

      if ($destination) {
        $destinationIDs[] = $destination->ID;
      }
    }

    // Destinations
    update_field('field_59bae5504316c', $destinationIDs, $post->ID); // Event destinations

    // Gallery
    $slides = [];
    $eventMedia = VisitEurope_Importer_Drupal::getExistingGalleryForExistingEntity($event->id, $event->revision_id);

    if (is_array($eventMedia) && !empty($eventMedia)) {
      foreach ($eventMedia as $eventMediaItem) {
        $attachmentID = VisitEurope_Importer_Media::attachMediaToPostFromExistingMedia($post->ID, $eventMediaItem);
        $slides[] = [
          'title' => $eventMediaItem->title,
          'image' => $attachmentID,
        ];
      }
    }

    // Content
    $experienceContent = [];
    $experienceContent[] = [
      'acf_fc_layout' => 'content',
      'body' => $event->content,
    ];

    if (!empty($slides)) {
      $experienceContent[] = [
        'acf_fc_layout' => 'carousel',
        'slides' => $slides,
      ];
    }

    update_field('field_59a9667138809', $experienceContent, $post->ID);  // Experience content
  }

  /**
   * Get existing event IDs
   *
   * @param array $ids
   * @return array
   */
  public function getExistingEventIDs($after = 0) {
    $db = VisitEurope_Importer::originalDatabaseConnection();

    return $db->get_results('
      SELECT
        `n`.`nid` AS `id`

      FROM `etc_node` `n`

      WHERE `n`.`nid` > ' . intval($after) . '
        AND `n`.`type` = "events"

      ORDER BY `n`.`nid` ASC

      LIMIT 3
    ');
  }

  /**
   * Get existing events
   *
   * @param array $ids
   * @return array
   */
  public function getExistingEvents($ids = []) {
    $db = VisitEurope_Importer::originalDatabaseConnection();
    $idsSql = '';

    if (is_array($ids) && !empty($ids)) {
      $ids = array_map('intval', $ids);
      $idsSql = 'AND `n`.`nid` IN (' . implode(',', $ids) . ')';
    }

    return $db->get_results('
      SELECT
        `n`.`nid` AS `id`,
        `n`.`vid` AS `revision_id`,
        `n`.`created` AS `published_date`,
        `n`.`title` AS `title`,
        `body`.`body_value` AS `content`,
        `blurb`.`field_brief_description_value` AS `blurb`,
        `date`.`field_start_date_value` AS `start_date`,
        `date`.`field_start_date_value2` AS `end_date`,
        `location`.`city`,
        `location`.`latitude`,
        `location`.`longitude`,
        `image`.`field_main_picture_fid` AS `image_id`

      FROM `etc_node` `n`

      JOIN `etc_field_data_body` `body`
        ON `body`.`entity_id` = `n`.`nid`
        AND `body`.`revision_id` = `n`.`vid`

      JOIN `etc_field_data_field_brief_description` `blurb`
        ON `blurb`.`entity_id` = `n`.`nid`
        AND `blurb`.`revision_id` = `n`.`vid`

      JOIN `etc_field_data_field_start_date` `date`
        ON `date`.`entity_id` = `n`.`nid`
        AND `date`.`revision_id` = `n`.`vid`

      JOIN `etc_field_data_field_location_event` `le`
        ON `le`.`entity_id` = `n`.`nid`
        AND `le`.`revision_id` = `n`.`vid`

      JOIN `etc_location` `location`
        ON `le`.`field_location_event_lid` = `location`.`lid`

      JOIN `etc_field_data_field_main_picture` `image`
        ON `image`.`entity_id` = `n`.`nid`
        AND `image`.`revision_id` = `n`.`vid`

      WHERE `n`.`type` = "events"
        ' . $idsSql . '
    ');
  }

}
