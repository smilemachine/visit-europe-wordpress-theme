<?php
if (!defined('ABSPATH')) exit;

/**
 * Content import for Visit Europe: Regions
 *
 * @package Visit_Europe
 */
class VisitEurope_Importer_Region {

  private $language;

  public function __construct($language = 'en') {
    $this->language = $language;
  }

  /**
   * Import all regions for the given language
   *
   * @param array $ids
   * @return void
   */
  public function importExistingRegions($ids = []) {
    $existing = $this->getExistingRegions($ids);

    if (is_array($existing) && !empty($existing)) {
      foreach ($existing as $region) {
        $this->createRegionFromExistingRegion($region);
      }
    }
  }

  /**
   * Create a new region from an existing source
   *
   * @param stdClass $region
   * @return boolean
   */
  private function createRegionFromExistingRegion($region) {
    $data = [
      'post_date' => $region->published_date,
      'post_content' => $region->content,
      'post_title' => $region->title,
      'post_excerpt' => '',
      'post_status' => 'publish',
      'post_type' => VisitEurope_CPT_Destination::POST_TYPE,
      'comment_status' => 'closed',
      'ping_status' => 'closed',
    ];

    // Check if this post already exists
    $posts = get_posts([
      'posts_per_page' => 1,
      'post_type' => VisitEurope_CPT_Destination::POST_TYPE,
      'meta_query' => [
        [
          'key' => VisitEurope_Importer::KEY_IMPORT_ID,
          'value' => $region->id,
        ],
        [
          'key' => VisitEurope_Importer::KEY_IMPORT_LANGUAGE,
          'value' => $region->language,
        ],
      ],
    ]);

    // If it doesn't exist, create it and store the original ID
    if (empty($posts)) {
      $postID = wp_insert_post($data);
      if ($postID == 0) { die('Could not create region for #' . $region->id); }

      add_post_meta($postID, VisitEurope_Importer::KEY_IMPORT_LANGUAGE, $region->language);
      add_post_meta($postID, VisitEurope_Importer::KEY_IMPORT_ID, $region->id);

      $post = get_post($postID);
    } else {
      $post = $posts[0];
    }

    if ($this->language != 'en') {
      VisitEurope_Importer::connectPostToEnglish($post, $this->language);
    }

    // Get the feature image and assign it to this Region
    VisitEurope_Importer_Media::setPostFeatureImageForExistingMediaID($post->ID, $region->image_id);

    // Save the meta data
    add_post_meta($post->ID, VisitEurope_CPT_Destination::POST_TYPE . '_TYPE', VisitEurope_Destination::TYPE_REGION);
    $this->saveRegionExtras($post);
    $this->saveRegionContentTabs($post);
  }

  /**
   * Save the extras for the given region
   *
   * @param WP_Post $post
   * @return void
   */
  private function saveRegionExtras($post) {
    // We'll be running fitBounds, so the pos doesn't matter so much
    update_field('field_5975bbfe2116f', 0, $post->ID);  // Map lat
    update_field('field_5975bc2b21170', 0, $post->ID);  // Map lng
    update_field('field_5975d6eddc19e', 4, $post->ID);  // Map zoom

    // This will probably need to be manual
    update_field('field_59b6495a4db61', 0, $post->ID);  // Map overlay north
    update_field('field_59b64af44db62', 0, $post->ID);  // Map overlay south
    update_field('field_59b64b014db63', 0, $post->ID);  // Map overlay east
    update_field('field_59b64b084db64', 0, $post->ID);  // Map overlay west

    // Get the slides for the gallery
    $slides = [];
    $regionID = get_post_meta($post->ID, VisitEurope_Importer::KEY_IMPORT_ID, true);
    $regionLanguage = get_post_meta($post->ID, VisitEurope_Importer::KEY_IMPORT_LANGUAGE, true);
    $regionMedia = $this->getExistingMediaForExistingRegion($regionID, $regionLanguage);

    if (is_array($regionMedia) && !empty($regionMedia)) {
      foreach ($regionMedia as $regionMediaItem) {
        $attachmentID = VisitEurope_Importer_Media::attachMediaToPostFromExistingMedia($post->ID, $regionMediaItem);
        $slides[] = [
          'title' => $regionMediaItem->title,
          'lat' => (float) $regionMediaItem->lat,
          'lng' => (float) $regionMediaItem->lng,
          'body' => VisitEurope_Importer::getCopyrightString($regionMediaItem->copyright),
          'image' => $attachmentID,
        ];
      }
    }

    update_field('field_59b6e1f3c8998', $slides, $post->ID);  // Top things gallery
  }

  /**
   * Save the content tabs for the given region
   *
   * @param WP_Post $post
   * @return void
   */
  private function saveRegionContentTabs($post) {
    $carouselPlaces = [
      'acf_fc_layout' => 'carousel',
      'title' => 'Top places to go in ' . $post->post_title,
      'type' => 'Countries',
    ];

    $carouselThings = [
      'acf_fc_layout' => 'carousel',
      'title' => 'Top things to do in ' . $post->post_title,
      'type' => 'Things',
    ];

    $featureGridExperiencesTab1 = [
      'acf_fc_layout' => 'feature_grid',
      'type' => 'Experiences',
      'title' => 'Read more on ' . $post->post_title,
      'show_view_all' => 1,
      'inverse' => 1,
    ];

    $featureGridExperiencesTab3 = [
      'acf_fc_layout' => 'feature_grid',
      'type' => 'Experiences',
      'title' => 'Other things to do based on ' . $post->post_title,
      'show_view_all' => 1,
      'inverse' => 1,
    ];

    $featureGridTrips = [
      'acf_fc_layout' => 'feature_grid',
      'type' => 'Trips',
      'title' => 'Recommended trips',
      'show_view_all' => 1,
      'inverse' => 1,
    ];

    $interestsGrid = [
      'acf_fc_layout' => 'interests_grid',
      'type' => 'Experiences',
      'limit' => 8,
    ];

    $standardGridCountries = [
      'acf_fc_layout' => 'standard_grid',
      'type' => 'Countries',
      'title' => 'Other regions you might like',
      'url' => '/destination/regions',
      'limit' => '',
    ];

    $contentTab1 = [$carouselPlaces, $carouselThings, $featureGridExperiencesTab1];
    $contentTab2 = [$standardGridCountries, $featureGridTrips];
    $contentTab3 = [$carouselThings, $interestsGrid, $featureGridExperiencesTab3];

    update_field('field_597ae19ac6967', $contentTab1, $post->ID); // Content Tab1
    update_field('field_5975b6dcb0f72', $contentTab2, $post->ID); // Content Tab2
    update_field('field_597ae1f3a84aa', $contentTab3, $post->ID); // Content Tab3
  }

  /**
   * Get existing media items for an existing region
   *
   * @param integer $regionID
   * @param string $regionLanguage
   * @return array
   */
  private function getExistingMediaForExistingRegion($regionID, $regionLanguage) {
    $db = VisitEurope_Importer::originalDatabaseConnection();
    return $db->get_results('
      SELECT
        `mi`.`copyright` AS `copyright`,
        `mi`.`urlHDMax_id` AS `url`,
        `mi`.`city` AS `city`,
        `mi`.`region` AS `region`,
        `mi`.`landmark` AS `landmark`,
        `mm`.`title` AS `title`,
        `mm`.`lat` AS `lat`,
        `mm`.`lng` AS `lng`

      FROM `new_content_region` `r`

      JOIN `new_content_customdisplayable` `rd`
        ON `r`.`customdisplayable_ptr_id` = `rd`.`id`

      JOIN `new_content_mediaforregion` `rm`
        ON `rm`.`region_id` = `r`.`customdisplayable_ptr_id`

      JOIN `media_image` `mi`
        ON `rm`.`image_id` = `mi`.`media_ptr_id`

      JOIN `media_media` `mm`
        ON `mi`.`media_ptr_id` = `mm`.`id`

      WHERE `r`.`region_id` = ' . intval($regionID) . '
        AND `r`.`language_id` = "' . $regionLanguage . '"
    ');
  }

  /**
   * Get existing region IDs
   *
   * @param array $ids
   * @return array
   */
  public function getExistingRegionIDs($after = 0) {
    $db = VisitEurope_Importer::originalDatabaseConnection();

    return $db->get_results('
      SELECT
        `rb`.`id` AS `id`

      FROM `new_content_regionbasics` `rb`

      WHERE `rb`.`id` > ' . intval($after) . '

      ORDER BY `rb`.`id` ASC

      LIMIT 3
    ');
  }

  /**
   * Get existing regions
   *
   * @param array $ids
   * @return array
   */
  public function getExistingRegions($ids = []) {
    $db = VisitEurope_Importer::originalDatabaseConnection();
    $idsSql = '';

    if (is_array($ids) && !empty($ids)) {
      $ids = array_map('intval', $ids);
      $idsSql = 'AND `rb`.`id` IN (' . implode(',', $ids) . ')';
    }

    return $db->get_results('
      SELECT
        `rb`.`id` AS `id`,
        `rd`.`title` AS `title`,
        `r`.`general_info` AS `content`,
        `rd`.`publish_date` AS `published_date`,
        `rd`.`hero_id` AS `image_id`,
        `r`.`video_id` AS `video_id`,
        `r`.`language_id` AS `language`

      FROM `new_content_regionbasics` `rb`

      JOIN `new_content_region` `r`
        ON `rb`.`id` = `r`.`region_id`

      JOIN `new_content_customdisplayable` `rd`
        ON `r`.`customdisplayable_ptr_id` = `rd`.`id`

      WHERE
        `r`.`language_id` = "' . $this->language . '"
        ' . $idsSql . '
    ');
  }

}
