<?php
if (!defined('ABSPATH')) exit;

/**
 * Content import for Visit Europe: Routes
 *
 * @package Visit_Europe
 */
class VisitEurope_Importer_Route {

  public function __construct() {
    //
  }

  /**
   * Import all routes
   *
   * @param array $ids
   * @return void
   */
  public function importExistingRoutes($ids = []) {
    $existing = $this->getExistingRoutes($ids);

    if (is_array($existing) && !empty($existing)) {
      foreach ($existing as $route) {
        $this->createRouteFromExistingRoute($route);
      }
    }
  }

  /**
   * Create a new route from an existing source
   *
   * @param stdClass $route
   * @return boolean
   */
  private function createRouteFromExistingRoute($route) {
    $data = [
      'post_date' => date('Y-m-d H:i:s', $route->published_date),
      'post_content' => '',
      'post_title' => $route->title,
      'post_excerpt' => '',
      'post_status' => 'publish',
      'post_type' => VisitEurope_CPT_Experience::POST_TYPE,
      'comment_status' => 'closed',
      'ping_status' => 'closed',
    ];

    // Check if this post already exists
    $posts = get_posts([
      'posts_per_page' => 1,
      'post_type' => VisitEurope_CPT_Experience::POST_TYPE,
      'meta_query' => [
        [
          'key' => VisitEurope_Importer::KEY_IMPORT_ID,
          'value' => $route->id,
        ],
        [
          'key' => VisitEurope_Importer::KEY_IMPORT_REVISION,
          'value' => $route->revision_id,
        ],
        [
          'key' => 'field_59a9639887cd0',
          'value' => 'Route',
        ]
      ]
    ]);

    // If it doesn't exist, create it and store the original ID
    if (empty($posts)) {
      $postID = wp_insert_post($data);
      if ($postID == 0) { die('Could not create route for #' . $route->id); }

      add_post_meta($postID, VisitEurope_Importer::KEY_IMPORT_REVISION, $route->revision_id);
      add_post_meta($postID, VisitEurope_Importer::KEY_IMPORT_ID, $route->id);

      $post = get_post($postID);
    } else {
      $post = $posts[0];
    }

    // Get the feature image and assign it to this Route
    VisitEurope_Importer_Media_Drupal::setPostFeatureImageForExistingMediaID($post->ID, $route->image_id);

    // Save the meta data
    $this->saveRouteContent($post, $route);
  }

  /**
   * Save the content tabs for the given route
   *
   * @param WP_Post $post
   * @param stdClass $route
   * @return void
   */
  private function saveRouteContent($post, $route) {
    $destinationIDs = [];
    $destination = null;

    update_field('field_59a9639887cd0', 'Route', $post->ID); // Type
    update_field('field_59b669daa33ad', strip_tags($route->blurb), $post->ID); // Blurb

    // About
    $about = [];
    $links = VisitEurope_Importer_Drupal::getExistingLinksForExistingEntity($route->id, $route->revision_id);

    foreach ($links as $link) {
      $about[] = [
        'title' => $link->title,
        'url' => $link->url,
      ];
    }

    update_field('field_59a96a32deb6d', $about, $post->ID);  // Route About

    // Tags
    $tags = [];
    $existingTags = VisitEurope_Importer_Drupal::getExistingTagsForExistingEntity($route->id, $route->revision_id);

    if (is_array($existingTags) && !empty($existingTags)) {
      foreach ($existingTags as $existingTag) {
        // If a destination exists with this tag name, use the destination instead
        $existing = get_posts([
          'post_type' => VisitEurope_CPT_Destination::POST_TYPE,
          'title' => $existingTag->tag,
          'posts_per_page' => 1,
        ]);

        if (is_array($existing) && !empty($existing)) {
          $destinationIDs[] = $existing[0]->ID;

          if (is_null($destination)) {
            $destination = $existing[0];
          }
        } else if (is_array($existing) && empty($existing)) {
          $tags[] = $existingTag->tag;
        }
      }
    }

    wp_set_post_terms($post->ID, $tags, VisitEurope_Taxonomy_Interest::TAXONOMY, false);

    // If destinations are empty, try the log-way-around for the location heirarchy
    if (empty($destinationIDs)) {
      $destination = VisitEurope_Importer_Drupal::getDestinationForExistingEntity($route->id, $route->revision_id);

      if ($destination) {
        $destinationIDs[] = $destination->ID;
      }
    }

    // Destinations
    update_field('field_59b16cd2e8b42', $destinationIDs, $post->ID); // Route destinations

    // Gallery
    $slides = [];
    $routeMedia = VisitEurope_Importer_Drupal::getExistingGalleryForExistingEntity($route->id, $route->revision_id);

    if (is_array($routeMedia) && !empty($routeMedia)) {
      foreach ($routeMedia as $routeMediaItem) {
        $attachmentID = VisitEurope_Importer_Media::attachMediaToPostFromExistingMedia($post->ID, $routeMediaItem);
        $slides[] = [
          'title' => $routeMediaItem->title,
          'image' => $attachmentID,
        ];
      }
    }

    // Content
    $experienceContent = [];
    $experienceContent[] = [
      'acf_fc_layout' => 'content',
      'body' => $route->content,
    ];

    if (!empty($slides)) {
      $experienceContent[] = [
        'acf_fc_layout' => 'carousel',
        'slides' => $slides,
      ];
    }

    update_field('field_59a9667138809', $experienceContent, $post->ID);  // Experience content

    // Pointers
    $pointers = [];

    if (!is_null($destination)) {
      $pointers[] = [
        'type' => 'Where',
        'subtitle' => $destination->post_title,
      ];
    }

    update_field('field_59a96b0d07e5a', $pointers, $post->ID);  // Pointers

    // Save the locations
    $locations = [];
    $existingLocations = VisitEurope_Importer_Drupal::getExistingLocationsForExistingEntity($route->id, $route->revision_id);

    if (is_array($existingLocations) && !empty($existingLocations)) {
      foreach ($existingLocations as $existingLocation) {
        $locations[] = [
          'title' => $existingLocation->title,
          'lat' => $existingLocation->lat,
          'lng' => $existingLocation->lng,
          'image' => null,
        ];
      }
    }

    update_field('field_59bb144620367', $locations, $post->ID); // Locations
  }

  /**
   * Get existing route IDs
   *
   * @param array $ids
   * @return array
   */
  public function getExistingRouteIDs($after = 0) {
    $db = VisitEurope_Importer::originalDatabaseConnection();

    return $db->get_results('
      SELECT
        `n`.`nid` AS `id`

      FROM `etc_node` `n`

      WHERE `n`.`nid` > ' . intval($after) . '
        AND `n`.`type` = "routes"

      ORDER BY `n`.`nid` ASC

      LIMIT 3
    ');
  }

  /**
   * Get existing routes
   *
   * @param array $ids
   * @return array
   */
  public function getExistingRoutes($ids = []) {
    $db = VisitEurope_Importer::originalDatabaseConnection();
    $idsSql = '';

    if (is_array($ids) && !empty($ids)) {
      $ids = array_map('intval', $ids);
      $idsSql = 'AND `n`.`nid` IN (' . implode(',', $ids) . ')';
    }

    return $db->get_results('
      SELECT
        `n`.`nid` AS `id`,
        `n`.`vid` AS `revision_id`,
        `n`.`created` AS `published_date`,
        `n`.`title` AS `title`,
        `body`.`body_value` AS `content`,
        `blurb`.`field_brief_description_value` AS `blurb`,
        `image`.`field_main_picture_fid` AS `image_id`

      FROM `etc_node` `n`

      JOIN `etc_field_data_body` `body`
        ON `body`.`entity_id` = `n`.`nid`
        AND `body`.`revision_id` = `n`.`vid`

      JOIN `etc_field_data_field_brief_description` `blurb`
        ON `blurb`.`entity_id` = `n`.`nid`
        AND `blurb`.`revision_id` = `n`.`vid`

      JOIN `etc_field_data_field_main_picture` `image`
        ON `image`.`entity_id` = `n`.`nid`
        AND `image`.`revision_id` = `n`.`vid`

      WHERE `n`.`type` = "routes"
        ' . $idsSql . '
    ');
  }

}
