<?php
if (!defined('ABSPATH')) exit;

/**
 * Content import for Visit Europe: Recipes
 *
 * @package Visit_Europe
 */
class VisitEurope_Importer_Recipe {

  public function __construct() {
    //
  }

  /**
   * Import all recipes
   *
   * @param array $ids
   * @return void
   */
  public function importExistingRecipes($ids = []) {
    $existing = $this->getExistingRecipes($ids);

    if (is_array($existing) && !empty($existing)) {
      foreach ($existing as $recipe) {
        $this->createRecipeFromExistingRecipe($recipe);
      }
    }
  }

  /**
   * Create a new recipe from an existing source
   *
   * @param stdClass $recipe
   * @return boolean
   */
  private function createRecipeFromExistingRecipe($recipe) {
    $data = [
      'post_date' => date('Y-m-d H:i:s', $recipe->published_date),
      'post_content' => '',
      'post_title' => $recipe->title,
      'post_excerpt' => '',
      'post_status' => 'publish',
      'post_type' => VisitEurope_CPT_Experience::POST_TYPE,
      'comment_status' => 'closed',
      'ping_status' => 'closed',
    ];

    // Check if this post already exists
    $posts = get_posts([
      'posts_per_page' => 1,
      'post_type' => VisitEurope_CPT_Experience::POST_TYPE,
      'meta_query' => [
        [
          'key' => VisitEurope_Importer::KEY_IMPORT_ID,
          'value' => $recipe->id,
        ],
        [
          'key' => VisitEurope_Importer::KEY_IMPORT_REVISION,
          'value' => $recipe->revision_id,
        ],
        [
          'key' => 'field_59a9639887cd0',
          'value' => 'Recipe',
        ]
      ]
    ]);

    // If it doesn't exist, create it and store the original ID
    if (empty($posts)) {
      $postID = wp_insert_post($data);
      if ($postID == 0) { die('Could not create recipe for #' . $recipe->id); }

      add_post_meta($postID, VisitEurope_Importer::KEY_IMPORT_REVISION, $recipe->revision_id);
      add_post_meta($postID, VisitEurope_Importer::KEY_IMPORT_ID, $recipe->id);

      $post = get_post($postID);
    } else {
      $post = $posts[0];
    }

    // Get the feature image and assign it to this Recipe
    VisitEurope_Importer_Media_Drupal::setPostFeatureImageForExistingMediaID($post->ID, $recipe->image_id);

    // Save the meta data
    $this->saveRecipeContent($post, $recipe);
  }

  /**
   * Save the content tabs for the given recipe
   *
   * @param WP_Post $post
   * @param stdClass $recipe
   * @return void
   */
  private function saveRecipeContent($post, $recipe) {
    $destinationIDs = [];
    $destination = null;

    update_field('field_59b029a5010ae', 6, $post->ID);  // Map zoom
    update_field('field_59a9639887cd0', 'Recipe', $post->ID); // Type
    update_field('field_59b669daa33ad', strip_tags($recipe->blurb), $post->ID); // Blurb

    update_field('field_59a96d4210e30', $post->post_title, $post->ID); // Ingredients title

    $ingredients = trim(preg_replace('/\s\s+/', "\n", strip_tags($recipe->ingredients, '<strong>')));
    $ingredientsArray = [];
    $ingredientsExplode = explode('<strong>', $ingredients);

    if (count($ingredientsExplode) == 1) {
      // Could be grouped into x:xxx, x:xxx
      if (count(explode(':', $ingredients)) < 2) {
        $ingredientsArray = [[
          'title' => 'Ingredients',
          'list' => $ingredients
        ]];
      } else {
        $ingredientsTitles = [];
        $ingredientsList = [];
        $ingredientsLines = explode("\n", $ingredients);
        $ingredientsSection = 0;

        // Place x:xxx, x:xxx into sections
        foreach ($ingredientsLines as $ingredient) {
          if (strpos($ingredient, ':') !== false) {
            $ingredientsSection++;
            $ingredientsTitles[$ingredientsSection] = trim(str_replace(':', '', $ingredient));
          } else {
            if (!isset($ingredientsList[$ingredientsSection])) {
              $ingredientsList[$ingredientsSection] = [];
            }

            $ingredientsList[$ingredientsSection][] = $ingredient;
          }
        }

        // Combine them into the ingredientsArray
        if (count($ingredientsTitles) == count($ingredientsList)) {
          foreach ($ingredientsList as $key => $value) {
            if (isset($ingredientsTitles[$key])) {
              $title = ucfirst(strtolower($ingredientsTitles[$key]));
            } else {
              $title = 'Ingredients';
            }

            $ingredientsArray[] = [
              'title' => str_replace(':', '', $title),
              'list' => implode("\n", $ingredientsList[$key]),
            ];
          }
        }
      }
    } else {
      foreach ($ingredientsExplode as $ingredientsSection) {
        if (empty($ingredientsSection)) {
          continue;
        }

        $ingredientsSectionExplode = explode('</strong>', $ingredientsSection);

        if (isset($ingredientsSectionExplode[1])) {
          $title = ucfirst(strtolower(trim($ingredientsSectionExplode[0])));
          $ingredientsArray[] = [
            'title' => str_replace(':', '', $title),
            'list' => trim($ingredientsSectionExplode[1]),
          ];
        } else {
          $ingredientsArray[] = [
            'title' => 'Ingredients',
            'list' => $ingredientsSection,
          ];
        }
      }
    }

    if (count($ingredientsArray) == 1) {
      update_field('field_59a9715df78cf', [$ingredientsArray], $post->ID); // Ingredients
    } else if (count($ingredientsArray) == 2){
      $ingredientsArrayColumn1 = array_shift($ingredientsArray);
      $ingredientsArrayColumn2 = $ingredientsArray;

      update_field('field_59a9715df78cf', [$ingredientsArrayColumn1], $post->ID); // Column 1
      update_field('field_59a971ea53437', $ingredientsArrayColumn2, $post->ID); // Column 2
    } else {
      $ingredientsArrayColumn1 = array_slice($ingredientsArray, 0, 2);
      $ingredientsArrayColumn2 = array_slice($ingredientsArray, 2, count($ingredientsArray) - 1);

      update_field('field_59a9715df78cf', $ingredientsArrayColumn1, $post->ID); // Column 1
      update_field('field_59a971ea53437', $ingredientsArrayColumn2, $post->ID); // Column 2
    }


    // About
    $about = [];
    $links = VisitEurope_Importer_Drupal::getExistingLinksForExistingEntity($recipe->id, $recipe->revision_id);

    foreach ($links as $link) {
      $about[] = [
        'title' => $link->title,
        'url' => $link->url,
      ];
    }

    update_field('field_59a96a32deb6d', $about, $post->ID);  // Recipe About

    // Tags
    $tags = [];
    $existingTags = VisitEurope_Importer_Drupal::getExistingTagsForExistingEntity($recipe->id, $recipe->revision_id);

    if (is_array($existingTags) && !empty($existingTags)) {
      foreach ($existingTags as $existingTag) {
        // If a destination exists with this tag name, use the destination instead
        $existing = get_posts([
          'post_type' => VisitEurope_CPT_Destination::POST_TYPE,
          'title' => $existingTag->tag,
          'posts_per_page' => 1,
        ]);

        if (is_array($existing) && !empty($existing)) {
          $destinationIDs[] = $existing[0]->ID;

          if (is_null($destination)) {
            $destination = $existing[0];
          }
        } else if (is_array($existing) && empty($existing)) {
          $tags[] = $existingTag->tag;
        }
      }
    }

    wp_set_post_terms($post->ID, $tags, VisitEurope_Taxonomy_Interest::TAXONOMY, false);

    // If destinations are empty, try the log-way-around for the location heirarchy
    if (empty($destinationIDs)) {
      $destination = VisitEurope_Importer_Drupal::getDestinationForExistingEntity($recipe->id, $recipe->revision_id);

      if ($destination) {
        $destinationIDs[] = $destination->ID;
      }
    }

    // Destinations
    update_field('field_59a963ba87cd1', $destinationIDs, $post->ID); // Recipe destinations

    // Gallery
    $slides = [];
    $recipeMedia = VisitEurope_Importer_Drupal::getExistingGalleryForExistingEntity($recipe->id, $recipe->revision_id);

    if (is_array($recipeMedia) && !empty($recipeMedia)) {
      foreach ($recipeMedia as $recipeMediaItem) {
        $attachmentID = VisitEurope_Importer_Media::attachMediaToPostFromExistingMedia($post->ID, $recipeMediaItem);
        $slides[] = [
          'title' => $recipeMediaItem->title,
          'image' => $attachmentID,
        ];
      }
    }

    // Content
    $experienceContent = [];
    $experienceContent[] = [
      'acf_fc_layout' => 'content',
      'body' => $recipe->content,
    ];

    if (!empty($slides)) {
      $experienceContent[] = [
        'acf_fc_layout' => 'carousel',
        'slides' => $slides,
      ];
    }

    update_field('field_59a9667138809', $experienceContent, $post->ID);  // Experience content

    // Pointers
    $pointers = [];

    if (!is_null($destination)) {
      $pointers[] = [
        'type' => 'Where',
        'subtitle' => $destination->post_title,
      ];
    }

    update_field('field_59a96b0d07e5a', $pointers, $post->ID);  // Pointers
  }

  /**
   * Get existing recipe IDs
   *
   * @param array $ids
   * @return array
   */
  public function getExistingRecipeIDs($after = 0) {
    $db = VisitEurope_Importer::originalDatabaseConnection();

    return $db->get_results('
      SELECT
        `n`.`nid` AS `id`

      FROM `etc_node` `n`

      WHERE `n`.`nid` > ' . intval($after) . '
        AND `n`.`type` = "traditional_dishes"

      ORDER BY `n`.`nid` ASC

      LIMIT 3
    ');
  }

  /**
   * Get existing recipes
   *
   * @param array $ids
   * @return array
   */
  public function getExistingRecipes($ids = []) {
    $db = VisitEurope_Importer::originalDatabaseConnection();
    $idsSql = '';

    if (is_array($ids) && !empty($ids)) {
      $ids = array_map('intval', $ids);
      $idsSql = 'AND `n`.`nid` IN (' . implode(',', $ids) . ')';
    }

    return $db->get_results('
      SELECT
        `n`.`nid` AS `id`,
        `n`.`vid` AS `revision_id`,
        `n`.`created` AS `published_date`,
        `n`.`title` AS `title`,
        `directions`.`field_directions_value` AS `content`,
        `blurb`.`field_brief_description_value` AS `blurb`,
        `ingredients`.`field_ingredients_value` AS `ingredients`,
        `image`.`field_main_picture_fid` AS `image_id`

      FROM `etc_node` `n`

      JOIN `etc_field_data_field_directions` `directions`
        ON `directions`.`entity_id` = `n`.`nid`
        AND `directions`.`revision_id` = `n`.`vid`

      JOIN `etc_field_data_field_brief_description` `blurb`
        ON `blurb`.`entity_id` = `n`.`nid`
        AND `blurb`.`revision_id` = `n`.`vid`

      JOIN `etc_field_data_field_ingredients` `ingredients`
        ON `ingredients`.`entity_id` = `n`.`nid`
        AND `ingredients`.`revision_id` = `n`.`vid`

      JOIN `etc_field_data_field_main_picture` `image`
        ON `image`.`entity_id` = `n`.`nid`
        AND `image`.`revision_id` = `n`.`vid`

      WHERE `n`.`type` = "traditional_dishes"
        ' . $idsSql . '
    ');
  }

}
