<?php
if (!defined('ABSPATH')) exit;

/**
 * Content import for Visit Europe: Trip
 *
 * @package Visit_Europe
 */
class VisitEurope_Importer_Trip {

  private $language;

  public function __construct($language = 'en') {
    $this->language = $language;
  }

  /**
   * Import all trips for the given language
   *
   * @param array $ids
   * @return void
   */
  public function importExistingTrips($ids = []) {
    $existing = $this->getExistingTrips($ids);

    if (is_array($existing) && !empty($existing)) {
      foreach ($existing as $trip) {
        $this->createTripFromExistingTrip($trip);
      }
    }
  }

  /**
   * Create a new trip from an existing source
   *
   * @param stdClass $trip
   * @return boolean
   */
  private function createTripFromExistingTrip($trip) {
    $data = [
      'post_date' => $trip->published_date,
      'post_content' => '',
      'post_title' => $trip->title,
      'post_excerpt' => '',
      'post_status' => 'publish',
      'post_type' => VisitEurope_CPT_Trip::POST_TYPE,
      'comment_status' => 'closed',
      'ping_status' => 'closed',
    ];

    // Check if this post already exists
    $posts = get_posts([
      'posts_per_page' => 1,
      'post_type' => VisitEurope_CPT_Trip::POST_TYPE,
      'meta_key' => VisitEurope_Importer::KEY_IMPORT_ID,
      'meta_query' => [
        [
          'key' => VisitEurope_Importer::KEY_IMPORT_ID,
          'value' => $trip->id,
        ],
        [
          'key' => VisitEurope_Importer::KEY_IMPORT_LANGUAGE,
          'value' => $trip->language,
        ],
      ],
    ]);

    // If it doesn't exist, create it and store the original ID
    if (empty($posts)) {
      $postID = wp_insert_post($data);
      if ($postID == 0) { die('Could not create trip for #' . $trip->id); }

      add_post_meta($postID, VisitEurope_Importer::KEY_IMPORT_LANGUAGE, $trip->language);
      add_post_meta($postID, VisitEurope_Importer::KEY_IMPORT_ID, $trip->id);
      add_post_meta($postID, VisitEurope_Importer::KEY_IMPORT_SLUG, $trip->slug);

      $post = get_post($postID);
    } else {
      $post = $posts[0];

      add_post_meta($post->ID, VisitEurope_Importer::KEY_IMPORT_LANGUAGE, $trip->language);
      add_post_meta($post->ID, VisitEurope_Importer::KEY_IMPORT_ID, $trip->id);
      add_post_meta($post->ID, VisitEurope_Importer::KEY_IMPORT_SLUG, $trip->slug);
    }

    if ($this->language != 'en') {
      VisitEurope_Importer::connectPostToEnglish($post, $this->language, $trip->slug);
    }

    // Get the feature image and assign it to this Trip
    if ($trip->image_id) {
      VisitEurope_Importer_Media::setPostFeatureImageForExistingMediaID($post->ID, $trip->image_id);
    } else if (!empty($trip->video_url) && !empty($trip->video_thumbnail_url)) {
      // Save a custom header banner with a video
      $image = VisitEurope_Importer_Media::storeMediaFromURL($trip->video_url);
      $media = new stdClass;
      $media->url = $trip->video_thumbnail_url;
      $media->title = $post->post_title;
      $attachmentID = VisitEurope_Importer_Media::attachMediaToPostFromExistingMedia($post->ID, $media);
      set_post_thumbnail($post->ID, $attachmentID);

      $slides = [];
      $slides[] = [
        'field_598b141b1af2d' => 'Video',
        'field_598b13721af28' => $post->post_title,
        'field_598b13861af29' => 'Trip',
        'field_59b6523e5beec' => $trip->video_url,
        'field_598b14961af2e' => $attachmentID,
      ];

      update_field('field_598b134d1af27', $slides, $post->ID);
    } else {
      die('No hero media found.');
    }

    // Save the meta data
    $this->saveTripContent($post, $trip);
  }

  /**
   * Save the content tabs for the given trip
   *
   * @param WP_Post $post
   * @param stdClass $trip
   * @return void
   */
  private function saveTripContent($post, $trip) {
    update_field('field_59d4a74bfb9d2', $trip->content, $post->ID);  // Before You Go

    $destinationIDs = [];

    // Save interests
    $tags = [];
    $existingTags = $this->getExistingTagsForExistingTrip($trip->id);

    if (is_array($existingTags) && !empty($existingTags)) {
      foreach ($existingTags as $existingTag) {
        // If a destination exists with this tag name, use the destination instead
        $existing = get_posts([
          'post_type' => VisitEurope_CPT_Destination::POST_TYPE,
          'title' => $existingTag->tag,
          'posts_per_page' => 1,
        ]);

        if (is_array($existing) && !empty($existing)) {
          $destinationIDs[] = $existing[0]->ID;
        } else if (is_array($existing) && empty($existing)) {
          $tags[] = $existingTag->tag;
        }
      }
    }

    wp_set_post_terms($post->ID, $tags, VisitEurope_Taxonomy_Interest::TAXONOMY, false);

    // Save activity level types
    wp_set_post_terms($post->ID, [$trip->level], VisitEurope_Taxonomy_TripActivityLevel::TAXONOMY, false);

    // Save destinations
    $existingRegions = $this->getExistingRegionIDsForExistingTrip($trip->id);

    if (is_array($existingRegions) && !empty($existingRegions)) {
      foreach ($existingRegions as $existingRegion) {
        $existing = get_posts([
          'post_type' => VisitEurope_CPT_Destination::POST_TYPE,
          'meta_key' => VisitEurope_Importer::KEY_IMPORT_ID,
          'meta_value' => intval($existingRegion->id),
          'posts_per_page' => 1,
        ]);

        if (is_array($existing) && !empty($existing)) {
          $destinationIDs[] = $existing[0]->ID;
        }
      }
    }

    update_field('field_59d4ada968316', $destinationIDs, $post->ID); // Trip destinations

    // Save blurb
    update_field('field_59d4a8f1b66a7', html_entity_decode(strip_tags($trip->blurb)), $post->ID); // Blurb

    // Save steps
    $existingSteps = $this->getExistingStepsForExistingTrip($trip->id);
    $segments = [];

    if (is_array($existingSteps) && !empty($existingSteps)) {
      foreach ($existingSteps as $existingStep) {
        $locations = [];
        $gridItems = [];
        $existingPoints = json_decode($existingStep->points);
        $existingGridItems = json_decode($existingStep->external_widgets);

        if (is_array($existingPoints) && !empty($existingPoints)) {
          foreach ($existingPoints as $existingPoint) {
            $locations[] = [
              'title' => $existingPoint->name,
              'latitude' => $existingPoint->lat,
              'longitude' => $existingPoint->lng,
            ];
          }
        }

        if (is_array($existingGridItems) && !empty($existingGridItems)) {
          foreach ($existingGridItems as $existingGridItem) {
            $media = VisitEurope_Importer_Media::getExistingMediaForExistingID(intval($existingGridItem->image));
            $attachmentID = null;

            if ($media) {
              $attachmentID = VisitEurope_Importer_Media::attachMediaToPostFromExistingMedia($post->ID, $media);
            }

            $gridItems[] = [
              'type' => 'External',
              'title' => $existingGridItem->title,
              'url' => $existingGridItem->url,
              'body' => $existingGridItem->description,
              'image' => $attachmentID,
            ];
          }
        }

        $segments[] = [
          'start_day' => $existingStep->start_day,
          'end_day' => $existingStep->end_day,
          'distance_km' => $existingStep->distance_kms,
          'distance_mi' => $existingStep->distance_miles,
          'title' => $existingStep->location,
          'body' => $existingStep->overview,
          'locations' => $locations,
          'grid_items' => $gridItems,
        ];
      }
    }

    update_field('field_59d4a5c1322ab', $segments, $post->ID);  // Segments

    // Get the slides for the gallery
    $slides = [];
    $tripMedia = $this->getExistingMediaForExistingTrip($trip->id);

    if (is_array($tripMedia) && !empty($tripMedia)) {
      foreach ($tripMedia as $key => $tripMediaItem) {
        $attachmentID = VisitEurope_Importer_Media::attachMediaToPostFromExistingMedia($post->ID, $tripMediaItem);

        // Add the slides
        $slides[] = [
          'title' => $tripMediaItem->title,
          'lat' => (float) $tripMediaItem->lat,
          'lng' => (float) $tripMediaItem->lng,
          'image' => $attachmentID,
        ];
      }
    }

    update_field('field_59d4f0006a6cb', $slides, $post->ID);  // Top things gallery
  }

  /**
   * Get existing trip IDs
   *
   * @param array $ids
   * @return array
   */
  public function getExistingTripIDs($after = 0) {
    $db = VisitEurope_Importer::originalDatabaseConnection();

    return $db->get_results('
      SELECT
        `a`.`customdisplayable_ptr_id` AS `id`

      FROM `new_content_trip` `a`

      WHERE `a`.`customdisplayable_ptr_id` > ' . intval($after) . '
        AND `a`.`language_id` = "' . $this->language . '"

      ORDER BY `a`.`customdisplayable_ptr_id` ASC

      LIMIT 3
    ');
  }

  /**
   * Get existing trips
   *
   * @param array $ids
   * @return array
   */
  public function getExistingTrips($ids = []) {
    $db = VisitEurope_Importer::originalDatabaseConnection();
    $idsSql = '';

    if (is_array($ids) && !empty($ids)) {
      $ids = array_map('intval', $ids);
      $idsSql = 'AND `a`.`customdisplayable_ptr_id` IN (' . implode(',', $ids) . ')';
    }

    return $db->get_results('
      SELECT
        `a`.`customdisplayable_ptr_id` AS `id`,
        `ad`.`title` AS `title`,
        `a`.`tips` AS `content`,
        `ad`.`publish_date` AS `published_date`,
        `mi`.`media_ptr_id` AS `image_id`,
        `mv`.`urlVideo` AS `video_url`,
        `mv`.`urlThumbnail` AS `video_thumbnail_url`,
        `a`.`language_id` AS `language`,
        `ad`.`description` AS `blurb`,
        `ad`.`slug` AS `slug`,
        `a`.`level_id` AS `level`

      FROM `new_content_trip` `a`

      JOIN `new_content_customdisplayable` `ad`
        ON `ad`.`id` = `a`.`customdisplayable_ptr_id`

      LEFT JOIN `media_image` `mi`
        ON `mi`.`media_ptr_id` = `ad`.`hero_id`

      LEFT JOIN `media_video` `mv`
        ON `mv`.`media_ptr_id` = `ad`.`hero_id`

      WHERE
        `a`.`language_id` = "' . $this->language . '"
        ' . $idsSql . '
    ');
  }

  /**
   * Get an array of existing region ids for an existing trip
   *
   * @param integer $tripID
   * @return array
   */
  private function getExistingRegionIDsForExistingTrip($tripID) {
    $db = VisitEurope_Importer::originalDatabaseConnection();

    return $db->get_results('
      SELECT
        `regionbasics_id` AS `id`

      FROM `new_content_trip_regions`

      WHERE `trip_id` = ' . intval($tripID) . '
    ');
  }

  /**
   * Get an array of existing tags for an existing trip
   *
   * @param integer $tripID
   * @return array
   */
  private function getExistingTagsForExistingTrip($tripID) {
    $db = VisitEurope_Importer::originalDatabaseConnection();

    return $db->get_results('
      SELECT
        `t`.`english_name` AS `tag`

      FROM `new_content_trip_tags` `at`

      JOIN `new_content_tag` `t`
        ON `at`.`tag_id` = `t`.`id`

      WHERE `at`.`trip_id` = ' . intval($tripID) . '
    ');
  }

  /**
   * Get an array of existing steps for an existing trip
   *
   * @param integer $tripID
   * @return array
   */
  private function getExistingStepsForExistingTrip($tripID) {
    $db = VisitEurope_Importer::originalDatabaseConnection();

    return $db->get_results('
      SELECT *

      FROM `new_content_tripstep`

      WHERE `trip_id` = ' . intval($tripID) . '
    ');
  }

  /**
   * Get existing media items for an existing trip
   *
   * @param integer $tripID
   * @return array
   */
  private function getExistingMediaForExistingTrip($tripID) {
    $db = VisitEurope_Importer::originalDatabaseConnection();
    return $db->get_results('
      SELECT
        `mi`.`copyright` AS `copyright`,
        `mi`.`urlHDMax_id` AS `url`,
        `mi`.`city` AS `city`,
        `mi`.`region` AS `region`,
        `mi`.`landmark` AS `landmark`,
        `mt`.`title` AS `title`,
        `mm`.`lat` AS `lat`,
        `mm`.`lng` AS `lng`

      FROM `new_content_mediafortrip` `mt`

      JOIN `media_image` `mi`
        ON `mt`.`image_id` = `mi`.`media_ptr_id`

      JOIN `media_media` `mm`
        ON `mi`.`media_ptr_id` = `mm`.`id`

      WHERE `mt`.`trip_id` = ' . intval($tripID) . '
    ');
  }

}
