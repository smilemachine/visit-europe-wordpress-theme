<?php
if (!defined('ABSPATH')) exit;

/**
 * Content import for Visit Europe: Drupal
 *
 * @package Visit_Europe
 */
class VisitEurope_Importer_Drupal {

  public function __construct() {
    //
  }

  /**
   * Get existing links for Existing
   *
   * @param integer $entityID
   * @param integer $revisionID
   * @return array
   */
  public static function getExistingLinksForExistingEntity($entityID, $revisionID) {
    $db = VisitEurope_Importer::originalDatabaseConnection();
    return $db->get_results('
      SELECT
        `l`.`field_links_url` AS `url`,
        `l`.`field_links_title` AS `title`

      FROM `etc_field_data_field_links` `l`

      WHERE `l`.`entity_id` = ' . intval($entityID) . '
        AND `l`.`revision_id` = ' . intval($revisionID) . '

      ORDER BY `l`.`delta`
    ');
  }

  /**
   * Get existing tags for Existing
   *
   * @param integer $entityID
   * @param integer $revisionID
   * @return array
   */
  public static function getExistingTagsForExistingEntity($entityID, $revisionID) {
    $db = VisitEurope_Importer::originalDatabaseConnection();
    return $db->get_results('
      SELECT
        `td`.`name` AS `tag`

      FROM `etc_field_data_field_open_tags` `ft`

      JOIN `etc_taxonomy_term_data` `td`
        ON `td`.`tid` = `ft`.`field_open_tags_tid`

      WHERE `ft`.`entity_id` = ' . intval($entityID) . '
        AND `ft`.`revision_id` = ' . intval($revisionID) . '

      ORDER BY `ft`.`delta`
    ');
  }



  /**
   * Get existing tags for Existing
   *
   * @param integer $entityID
   * @param integer $revisionID
   * @return array
   */
  public static function getExistingLocationsForExistingEntity($entityID, $revisionID) {
    $db = VisitEurope_Importer::originalDatabaseConnection();
    return $db->get_results('
      SELECT
        `l`.`city` AS `title`,
        `l`.`latitude` AS `lat`,
        `l`.`longitude` AS `lng`

      FROM `etc_field_data_field_points` `p`

      JOIN `etc_location` `l`
        ON `l`.`lid` = `p`.`field_points_lid`

      WHERE `p`.`entity_id` = ' . intval($entityID) . '
        AND `p`.`revision_id` = ' . intval($revisionID) . '

      ORDER BY `p`.`delta`
    ');
  }

  /**
   * Get existing gallery for Existing
   *
   * @param integer $entityID
   * @param integer $revisionID
   * @return array
   */
  public static function getExistingGalleryForExistingEntity($entityID, $revisionID) {
    $db = VisitEurope_Importer::originalDatabaseConnection();
    $items = [];
    $gallery = $db->get_results('
      SELECT
        `g`.`field_gallery_alt` AS `title`,
        `f`.`uri` AS `uri`,
        `f`.`filename` AS `filename`

      FROM `etc_field_data_field_gallery` `g`

      JOIN `etc_file_managed` `f`
        ON `f`.`fid` = `g`.`field_gallery_fid`

      WHERE `g`.`entity_id` = ' . intval($entityID) . '
        AND `g`.`revision_id` = ' . intval($revisionID) . '

      ORDER BY `g`.`delta`
    ');

    $baseUrl = 'http://tastingeurope.com/sites/default/files/styles/gallery_full';

    if (is_array($gallery) && !empty($gallery)) {
      foreach ($gallery as $media) {
        $uri = str_replace('public://', 'public/', $media->uri);
        $media->url = $baseUrl . '/' . $uri;

        $items[] = $media;
      }
    }

    return $items;
  }

  /**
   * Attempt to get the destination using taxonomy terms
   *
   * @param integer $entityID
   * @param integer $revisionID
   * @return array
   */
  public static function getDestinationForExistingEntity($entityID, $revisionID) {
    $db = VisitEurope_Importer::originalDatabaseConnection();
    $items = [];
    $destinations = $db->get_results('
      SELECT
        `ptd`.`name` AS `title`

      FROM `etc_field_data_field_location_taxonomize_terms` `lt`

      JOIN `etc_taxonomy_term_data` `ltd`
        ON `lt`.`field_location_taxonomize_terms_tid` = `ltd`.`tid`

      JOIN `etc_taxonomy_term_hierarchy` `h`
        ON `ltd`.`tid` = `h`.`tid`

      JOIN `etc_taxonomy_term_data` `ptd`
        ON `h`.`parent` = `ptd`.`tid`

      WHERE `lt`.`entity_id` = ' . intval($entityID) . '
        AND `lt`.`revision_id` = ' . intval($revisionID) . '
        AND `ptd`.`vid` = 4

      LIMIT 1
    ');

    if (is_array($destinations) && !empty($destinations)) {
      $destination = $destinations[0];

      $posts = get_posts([
        'posts_per_page' => 1,
        'post_type' => VisitEurope_CPT_Destination::POST_TYPE,
        'title' => trim(ucfirst(strtolower($destination->title))),
      ]);

      if (is_array($posts) && !empty($posts)) {
        return $posts[0];
      }
    }

    return null;
  }
}
