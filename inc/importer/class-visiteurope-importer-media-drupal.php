<?php
if (!defined('ABSPATH')) exit;

/**
 * Content import for Visit Europe: Media (Drupal style)
 *
 * @package Visit_Europe
 */
class VisitEurope_Importer_Media_Drupal {

  public function __construct() {
    //
  }

  /**
   * Gets media for an existing media ID
   *
   * @param integer $mediaID
   * @return stdClass
   */
  public static function getExistingMainPictureForExistingID($mediaID) {
    $db = VisitEurope_Importer::originalDatabaseConnection();
    $media = $db->get_row('
      SELECT
        `c`.`field_copyright_main_picture_value` AS `copyright`,
        `f`.`uri` AS `uri`,
        `f`.`filename` AS `filename`,
        `p`.`field_main_picture_alt` AS `title`

      FROM `etc_field_data_field_main_picture` `p`

      JOIN `etc_file_managed` `f`
        ON `f`.`fid` = `p`.`field_main_picture_fid`

      LEFT JOIN `etc_field_data_field_copyright_main_picture` `c`
        ON `c`.`entity_id` = `p`.`entity_id`
        AND `c`.`revision_id` = `p`.`revision_id`

      WHERE `p`.`field_main_picture_fid` = ' . intval($mediaID) . '
    ');

    $baseUrl = 'http://tastingeurope.com/sites/default/files/styles/hero_image';
    $uri = str_replace('public://', 'public/', $media->uri);

    $media->url = $baseUrl . '/' . $uri;

    return $media;
  }

  /**
   * Set the feature image for a post id with a given existing media id
   *
   * @param integer $postID
   * @param integer $mediaID
   * @return integer
   */
  public static function setPostFeatureImageForExistingMediaID($postID, $mediaID) {
    $media = self::getExistingMainPictureForExistingID($mediaID);
    if (!$media) { die('Could not get media for #' . $mediaID); }

    $attachmentID = VisitEurope_Importer_Media::attachMediaToPostFromExistingMedia($postID, $media);
    set_post_thumbnail($postID, $attachmentID);

    return $attachmentID;
  }

}
