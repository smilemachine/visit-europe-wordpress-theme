<?php
if (!defined('ABSPATH')) exit;

/**
 * Content import for Visit Europe: Countries
 *
 * @package Visit_Europe
 */
class VisitEurope_Importer_Country {

  private $language;

  public function __construct($language = 'en') {
    $this->language = $language;
  }

  /**
   * Import all countries for the given language
   *
   * @param array $codes
   * @return void
   */
  public function importExistingCountries($codes = []) {
    $existing = $this->getExistingCountries($codes);

    if (is_array($existing) && !empty($existing)) {
      foreach ($existing as $country) {
        $this->createCountryFromExistingCountry($country);
      }
    }
  }

  /**
   * Create a new country from an existing source
   *
   * @param stdClass $country
   * @return boolean
   */
  private function createCountryFromExistingCountry($country) {
    $data = [
      'post_date' => $country->published_date,
      'post_content' => $country->content,
      'post_title' => $country->title,
      'post_excerpt' => '',
      'post_status' => 'publish',
      'post_type' => VisitEurope_CPT_Destination::POST_TYPE,
      'comment_status' => 'closed',
      'ping_status' => 'closed',
    ];

    // Check if this post already exists
    $posts = get_posts([
      'posts_per_page' => 1,
      'post_type' => VisitEurope_CPT_Destination::POST_TYPE,
      'meta_query' => [
        [
          'key' => VisitEurope_Importer::KEY_IMPORT_ID,
          'value' => $country->code,
        ],
        [
          'key' => VisitEurope_Importer::KEY_IMPORT_LANGUAGE,
          'value' => $country->language,
        ],
      ],
    ]);

    // If it doesn't exist, create it and store the original ID
    if (empty($posts)) {
      $postID = wp_insert_post($data);
      if ($postID == 0) { die('Could not create country for #' . $country->code); }

      add_post_meta($postID, VisitEurope_Importer::KEY_IMPORT_LANGUAGE, $country->language);
      add_post_meta($postID, VisitEurope_Importer::KEY_IMPORT_ID, $country->code);

      $post = get_post($postID);
    } else {
      $post = $posts[0];
    }

    if ($this->language != 'en') {
      VisitEurope_Importer::connectPostToEnglish($post, $this->language);
    }

    // Get the feature image and assign it to this Country
    VisitEurope_Importer_Media::setPostFeatureImageForExistingMediaID($post->ID, $country->image_id);

    // Save the meta data
    add_post_meta($post->ID, VisitEurope_CPT_Destination::POST_TYPE . '_TYPE', VisitEurope_Destination::TYPE_COUNTRY);

    $this->saveCountryExtras($post, $country);
    $this->saveCountryContentTabs($post);
    $this->saveCountryRegions($post, $country);
  }

  /**
   * Save the extras for the given country
   *
   * @param WP_Post $post
   * @param stdClass $country
   * @return void
   */
  private function saveCountryExtras($post, $country) {
    update_field('field_59b6d7cf1db86', $country->code, $post->ID); // Code

    // Update the map lat/lng
    update_field('field_5975bbfe2116f', (float) $country->lat, $post->ID);  // Map lat
    update_field('field_5975bc2b21170', (float) $country->lng, $post->ID);  // Map lng
    update_field('field_5975d6eddc19e', 4, $post->ID);  // Map zoom

    // This will probably need to be manually adjusted
    update_field('field_59b6495a4db61', (float) $country->overlay_north, $post->ID);  // Map overlay north
    update_field('field_59b64af44db62', (float) $country->overlay_south, $post->ID);  // Map overlay south
    update_field('field_59b64b014db63', (float) $country->overlay_east, $post->ID);  // Map overlay east
    update_field('field_59b64b084db64', (float) $country->overlay_west, $post->ID);  // Map overlay west

    // Get the slides for the gallery
    $slides = [];
    $countryCode = get_post_meta($post->ID, VisitEurope_Importer::KEY_IMPORT_ID, true);
    $countryLanguage = get_post_meta($post->ID, VisitEurope_Importer::KEY_IMPORT_LANGUAGE, true);
    $countryMedia = $this->getExistingMediaForExistingCountry($countryCode, $countryLanguage);

    // $country->facts contains the body for each of the slides. Each on a new line
    $slideContent = explode("\n", $country->facts);
    $slideContentNoTags = array_map('strip_tags', $slideContent);
    $slideContentBody = [];

    foreach ($slideContentNoTags as $item) {
      $test = explode('-', $item);

      if (isset($test[1])) {
        array_shift($test);
        $slideContentBody[] = trim(html_entity_decode(implode('-', $test)));
      }
    }

    if (is_array($countryMedia) && !empty($countryMedia)) {
      foreach ($countryMedia as $key => $countryMediaItem) {
        $attachmentID = VisitEurope_Importer_Media::attachMediaToPostFromExistingMedia($post->ID, $countryMediaItem);

        // Modify the body
        $body = '';

        // https://stackoverflow.com/questions/10859098/why-is-php-trim-is-
        // not-really-remove-all-whitespace-and-line-breaks
        if (isset($slideContentBody[$key])) {
          $body = trim(preg_replace('/^\p{Z}+|\p{Z}+$/u', '', ($slideContentBody[$key])));
        }

        $body .= "\n" . VisitEurope_Importer::getCopyrightString($countryMediaItem->copyright);

        // Modify the title
        $title = $countryMediaItem->title;
        $titleExplode = explode('.', $title);

        if (isset($titleExplode[1])) {
          array_shift($titleExplode);
          $title = trim(implode('.', $titleExplode));
        }

        // Add the slides
        $slides[] = [
          'title' => $title,
          'lat' => (float) $countryMediaItem->lat,
          'lng' => (float) $countryMediaItem->lng,
          'body' => trim($body),
          'image' => $attachmentID,
        ];
      }
    }

    update_field('field_59b6e1f3c8998', $slides, $post->ID);  // Top things gallery

    // Add the pointers
    $languages = array_filter(array_map('trim', explode(' ', $country->pointer_languages)));
    $moreInfoURL = parse_url($country->pointer_website);
    $pointers = [];

    if (is_array($languages) && !empty($languages)) {
      $pointers[] = ['type' => 'Language', 'subtitle' => implode(', ', $languages)];
    }

    if (!empty(trim($country->pointer_currency))) {
      $pointers[] = ['type' => 'Currency', 'subtitle' => trim($country->pointer_currency)];
    }

    if (!empty(trim($country->pointer_city))) {
      $pointers[] = ['type' => 'Capital City', 'subtitle' => trim($country->pointer_city)];
    }

    if (!empty(trim($country->pointer_timezone))) {
      $pointers[] = ['type' => 'Timezone', 'subtitle' => trim($country->pointer_timezone)];
    }

    if (is_array($moreInfoURL) && !empty($moreInfoURL) && isset($moreInfoURL['host'])) {
      $pointers[] = [
        'type' => 'More Info',
        'subtitle' => $moreInfoURL['host'],
        'url' => $country->pointer_website,
      ];
    }

    if (!empty(trim($country->pointer_airport))) {
      $pointers[] = ['type' => 'Airport', 'subtitle' => trim($country->pointer_airport)];
    }

    update_field('field_5975b9646437a', $pointers, $post->ID);  // Pointers
  }

  /**
   * Save the content tabs for the given country
   *
   * @param WP_Post $post
   * @return void
   */
  private function saveCountryContentTabs($post) {
    $carouselThings = [
      'acf_fc_layout' => 'carousel',
      'title' => 'Top things to do in ' . $post->post_title,
      'type' => 'Things',
    ];

    $featureGridExperiences = [
      'acf_fc_layout' => 'feature_grid',
      'title' => 'Other things to do based on ' . $post->post_title,
      'type' => 'Experiences',
      'inverse' => 1,
    ];

    $interestsGrid = [
      'acf_fc_layout' => 'interests_grid',
      'type' => 'Experiences',
      'limit' => 8,
    ];

    $contentTab1 = [$carouselThings, $featureGridExperiences];
    $contentTab3 = [$carouselThings, $interestsGrid, $featureGridExperiences];

    update_field('field_597ae19ac6967', $contentTab1, $post->ID); // Content Tab1
    update_field('field_597ae1f3a84aa', $contentTab3, $post->ID); // Content Tab3
  }

  /**
   * Save the regions for the given country
   *
   * @param WP_Post $post
   * @param stdClass $country
   * @return void
   */
  private function saveCountryRegions($post, $country) {
    $db = VisitEurope_Importer::originalDatabaseConnection();

    // Get the regions associated to this country
    $regionIDs = [];
    $regions = $db->get_results('
      SELECT
        `rb`.`id`

      FROM `new_content_regionbasics_countries` `rc`

      JOIN `new_content_regionbasics` `rb`
        ON `rb`.`id` = `rc`.`regionbasics_id`

      WHERE `rc`.`country_id` = "' . $country->code . '"
    ');

    if (!is_array($regions) || empty($regions)) {
      return;
    }

    foreach ($regions as $region) {
      $regionIDs[] = intval($region->id);
    }

    // Get the posts
    $regionPosts = get_posts([
      'post_type' => VisitEurope_CPT_Destination::POST_TYPE,
      'post_status' => 'publish',
      'meta_query' => [
        [
          'key' => VisitEurope_CPT_Destination::POST_TYPE . '_TYPE',
          'value' => VisitEurope_Destination::TYPE_REGION
        ],
        [
          'key' => VisitEurope_Importer::KEY_IMPORT_LANGUAGE,
          'value' => $country->language
        ],
        [
          'key' => VisitEurope_Importer::KEY_IMPORT_ID,
          'value' => $regionIDs,
          'compare' => 'IN'
        ],
      ],
    ]);

    if (!is_array($regionPosts) || empty($regionPosts)) {
      return;
    }

    // Assign this country to each of the regions
    foreach ($regionPosts as $regionPost) {
      add_post_meta($regionPost->ID, VisitEurope_CPT_Destination::POST_TYPE . VisitEurope_Content::KEY_SEPARATOR . VisitEurope_Destination::TYPE_COUNTRY, $post->ID);
    }
  }

  /**
   * Get existing media items for an existing country
   *
   * @param string $countryCode
   * @param string $countryLanguage
   * @return array
   */
  private function getExistingMediaForExistingCountry($countryCode, $countryLanguage) {
    $db = VisitEurope_Importer::originalDatabaseConnection();
    return $db->get_results('
      SELECT
        `mi`.`copyright` AS `copyright`,
        `mi`.`urlHDMax_id` AS `url`,
        `mi`.`city` AS `city`,
        `mi`.`region` AS `region`,
        `mi`.`landmark` AS `landmark`,
        `mg`.`title` AS `title`,
        `mm`.`lat` AS `lat`,
        `mm`.`lng` AS `lng`

      FROM `destinations_country` `c`

      JOIN `new_content_guide` `g`
        ON `g`.`country_id` = `c`.`code`

      JOIN `new_content_mediaforguide` `mg`
        ON `mg`.`guide_id` = `g`.`customdisplayable_ptr_id`

      JOIN `media_image` `mi`
        ON `mg`.`image_id` = `mi`.`media_ptr_id`

      JOIN `media_media` `mm`
        ON `mi`.`media_ptr_id` = `mm`.`id`

      WHERE `c`.`code` = "' . $countryCode . '"
        AND `g`.`language_id` = "' . $countryLanguage . '"
    ');
  }

  /**
   * Get existing region IDs
   *
   * @param array $after
   * @return array
   */
  public function getExistingCountryCodes($after = 'AA') {
    $db = VisitEurope_Importer::originalDatabaseConnection();

    return $db->get_results('
      SELECT
        `c`.`code` AS `code`

      FROM `destinations_country` `c`

      WHERE `c`.`code` > "' . $after . '"

      ORDER BY `c`.`code` ASC

      LIMIT 3
    ');
  }

  /**
   * Get existing countries
   *
   * @param array $codes
   * @return array
   */
  public function getExistingCountries($codes = []) {
    $db = VisitEurope_Importer::originalDatabaseConnection();
    $codesSql = '';

    if (is_array($codes) && !empty($codes)) {
      $codes = $codes;
      $codesSql = 'AND `c`.`code` IN ("' . implode('","', $codes) . '")';
    }

    return $db->get_results('
      SELECT
        `c`.`boundingBoxNorth` AS `overlay_north`,
        `c`.`boundingBoxSouth` AS `overlay_south`,
        `c`.`boundingBoxEast` AS `overlay_east`,
        `c`.`boundingBoxWest` AS `overlay_west`,
        `g`.`language_id` AS `language`,
        `g`.`language_names` AS `pointer_languages`,
        `g`.`currency` AS `pointer_currency`,
        `g`.`capital_name` AS `pointer_city`,
        `g`.`timezone` AS `pointer_timezone`,
        `g`.`website_link` AS `pointer_website`,
        `a`.`airportName` AS `pointer_airport`,
        `g`.`general_info` AS `content`,
        `cd`.`title` AS `title`,
        `cd`.`publish_date` AS `published_date`,
        `cd`.`hero_id` AS `image_id`,
        `dd`.`lat` AS `lat`,
        `dd`.`lng` AS `lng`,
        `g`.`factszone` AS `facts`,
        `c`.`code` AS `code`

      FROM `destinations_country` `c`

      JOIN `new_content_guide` `g`
        ON `g`.`country_id` = `c`.`code`

      JOIN `new_content_customdisplayable` `cd`
        ON `cd`.`id` = `g`.`customdisplayable_ptr_id`

      LEFT JOIN `destinations_destination` `dd`
        ON `dd`.`code` = `c`.`capitalCode`

      LEFT JOIN `relevantairports_airportmovementinfo` `a`
        ON `a`.`airportCode` = `c`.`relevantAirportCode_id`

      WHERE
        `g`.`language_id` = "' . $this->language . '"
        ' . $codesSql . '

      GROUP BY `c`.`code`
    ');
  }

}
