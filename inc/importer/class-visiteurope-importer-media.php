<?php
if (!defined('ABSPATH')) exit;

/**
 * Content import for Visit Europe: Regions
 *
 * @package Visit_Europe
 */
class VisitEurope_Importer_Media {

  public function __construct() {
    //
  }

  /**
   * Gets media for an existing media ID
   *
   * @param integer $mediaID
   * @return stdClass
   */
  public static function getExistingMediaForExistingID($mediaID) {
    $db = VisitEurope_Importer::originalDatabaseConnection();
    return $db->get_row('
      SELECT
        `mi`.`copyright` AS `copyright`,
        `mi`.`urlHDMax_id` AS `url`,
        `mi`.`city` AS `city`,
        `mi`.`region` AS `region`,
        `mi`.`landmark` AS `landmark`,
        `mm`.`title` AS `title`,
        `mm`.`lat` AS `lat`,
        `mm`.`lng` AS `lng`

      FROM `media_image` `mi`

      JOIN `media_media` `mm`
        ON `mi`.`media_ptr_id` = `mm`.`id`

      WHERE `mi`.`media_ptr_id` = ' . intval($mediaID) . '
    ');
  }

  /**
   * Set the feature image for a post id with a given existing media id
   *
   * @param integer $postID
   * @param integer $mediaID
   * @return integer
   */
  public static function setPostFeatureImageForExistingMediaID($postID, $mediaID) {
    $media = self::getExistingMediaForExistingID($mediaID);
    if (!$media) { die('Could not get media for #' . $mediaID); }

    $attachmentID = self::attachMediaToPostFromExistingMedia($postID, $media);
    set_post_thumbnail($postID, $attachmentID);

    return $attachmentID;
  }

  /**
   * Attach an existing media object to the given post
   *
   * @param integer $postID
   * @param stdClass $media
   * @return integer
   */
  public static function attachMediaToPostFromExistingMedia($postID, $media) {
    $file = self::storeMediaFromURL($media->url);
    if (empty($file)) {
      return null;
    }

    $attachmentID = self::attachLocalFileToPostID($file, $postID, $media);
    if (!$attachmentID) { die('Could not attach media (' . $file . ') to post #' . $postID); }

    return intval($attachmentID);
  }

  /**
   * Fetch and store media from the given url
   * https://wordpress.stackexchange.com/questions/256830/programmatically-adding-images-to-media-library
   *
   * @param string $url
   * @return string
   */
  public static function storeMediaFromURL($url) {
    $filename = basename($url);
    $cleanFilename = self::getCleanFilename($filename);
    $uploadDir = wp_upload_dir();

    // If a filename exists with this url, don't fetch it - simply return the file path
    if (file_exists($uploadDir['basedir'] . '/' . $cleanFilename)) {
      return $uploadDir['basedir'] . '/' . $cleanFilename;
    }

    // Image doesn't exist yet, go get it
    $imageData = @file_get_contents($url);

    if (!$imageData || empty($imageData)) {
      return false;
    }

    if (wp_mkdir_p($uploadDir['path'])) {
      $file = $uploadDir['path'] . '/' . $cleanFilename;
    } else {
      $file = $uploadDir['basedir'] . '/' . $cleanFilename;
    }

    $stored = file_put_contents($file, $imageData);

    if (!$stored) {
      return false;
    }

    return $file;
  }

  /**
   * Attach a local file to the given id
   *
   * @param string $file
   * @param integer $postID
   * @param stdClass $media
   * @return boolean
   */
  public static function attachLocalFileToPostID($file, $postID, $media) {
    // If the file is already attached to the post, simply return it's attachment ID
    $filename = basename($file);
    $wpFiletype = wp_check_filetype($filename, null);
    $attachmentID = null;

    // If the file is already attached, don't re-attach it
    $attachedMedia = get_attached_media('image', $postID);

    if (is_array($attachedMedia) && !empty($attachedMedia)) {
      foreach ($attachedMedia as $attachedMediaItem) {
        $existingAttachmentURL = wp_get_attachment_url($attachedMediaItem->ID);

        if ($filename == basename($existingAttachmentURL)) {
          $attachmentID = $attachedMediaItem->ID;
          break;
        }
      }
    }

    if ($attachmentID) {
      return $attachmentID;
    }

    // Create the attachment
    $subContent = [];

    if (isset($media->city)) {
      $subContent[] = trim($media->city);
    }

    if (isset($media->region)) {
      $subContent[] = trim($media->region);
    }

    if (isset($media->landmark)) {
      $subContent[] = trim($media->landmark);
    }

    $content = [
      trim(implode(', ', $subContent)),
      trim($media->title),
    ];

    if (isset($media->copyright)) {
      $content[] = VisitEurope_Importer::getCopyrightString($media->copyright);
    }

    $attachment = [
      'post_mime_type' => $wpFiletype['type'],
      'post_title' => sanitize_file_name($filename),
      'post_content' => '',
      'post_status' => 'inherit',
      'post_title' => $media->title,
      'post_content' => implode('. ', array_filter($content)),
      'post_excerpt' => '',
    ];

    if (isset($media->lat) && isset($media->lng)) {
      $attachment['meta_input'] = [
        'lat' => (float) $media->lat,
        'lng' => (float) $media->lng,
      ];
    }

    $attachmentID = wp_insert_attachment($attachment, $file, $postID);

    require_once(ABSPATH . 'wp-admin/includes/image.php');
    $attachmentData = wp_generate_attachment_metadata($attachmentID, $file);
    wp_update_attachment_metadata($attachmentID, $attachmentData);

    return $attachmentID;
  }

  /**
   * Returns a clean filename
   *
   * @param string
   * @return string
   */
  public static function getCleanFilename($filename) {
    $slugFilename = VisitEurope_Helper::getSlug($filename);
    $slugFilenameParts = explode('-', $slugFilename);
    $slugExtension = array_pop($slugFilenameParts);
    $cleanFilename = implode('-', $slugFilenameParts) . '.' . $slugExtension;

    return preg_replace('/-+/', '-', str_replace('_', '-', strtolower($cleanFilename)));
  }

  /**
   * Downloads all images referenced in html string, replaces references
   * to them in said string, and returns it.
   *
   * @param string $string
   * @return string
   */
  public static function storeAndReplaceImagesInHtmlString($string) {
    $imageFind = [];
    $imageReplace = [];

    preg_match_all('/src="([^"]+)"/', $string, $images);

    if (is_array($images) && isset($images[1]) && !empty($images[1])) {
      foreach ($images[1] as $image) {
        $filename = self::storeMediaFromURL($image);

        if (!empty($filename)) {
          $uploadDir = wp_upload_dir();
          $imageFind[] = $image;
          $imageReplace[] = $uploadDir['url'] . '/' . basename($filename);
        }
      }
    }

    return str_replace($imageFind, $imageReplace, $string);
  }

}
