<?php
if (!defined('ABSPATH')) exit;

/**
 * Content import for Visit Europe
 *
 * @package Visit_Europe
 */
class VisitEurope_Importer {

  const KEY_IMPORT_ID = 'VE_IMPORT_ID';
  const KEY_IMPORT_REVISION = 'VE_IMPORT_REVISION';
  const KEY_IMPORT_LANGUAGE = 'VE_IMPORT_LANGUAGE';
  const KEY_IMPORT_SLUG = 'VE_IMPORT_SLUG';

  public function __construct() {
    //
  }

  public function setup() {
    if (isset($_GET['import']) && isset($_GET['importFunction'])) {
      $function = $_GET['importFunction'];
      $ids = [];
      $language = 'en';
      $supportedLanguages = ['en', 'ja', 'pt', 'fr', 'es', 'ru'];

      if (isset($_GET['importIDs'])) {
        $ids = array_map('trim', explode(',', $_GET['importIDs']));
      }

      if (isset($_GET['importLanguage'])) {
        $requestedLanguage = $_GET['importLanguage'];

        if (in_array($requestedLanguage, $supportedLanguages)) {
          $language = $requestedLanguage;
        }
      }

      if (method_exists(self::class, $function)) {
        ini_set('max_execution_time', 300);
        $this->{$function}($language, $ids);
      }

      exit;
    }
  }

  /**
   * Clean the ACF importer content to ensure correct data.
   * In particular, destination association.
   *
   * @return void
   */
  private function cleanExperienceDestinations() {
    $nonArticleIDs = $this->getCleanDestinationsItems('Article', [
      'event_destinations',
      'gallery_destinations',
      'recipe_destinations',
      'route_destinations',
    ]);

    $nonEventIDs = $this->getCleanDestinationsItems('Event', [
      'article_destinations',
      'gallery_destinations',
      'recipe_destinations',
      'route_destinations',
    ]);

    $nonGalleryIDs = $this->getCleanDestinationsItems('Gallery', [
      'article_destinations',
      'event_destinations',
      'recipe_destinations',
      'route_destinations',
    ]);

    $nonRecipeIDs = $this->getCleanDestinationsItems('Recipe', [
      'article_destinations',
      'event_destinations',
      'gallery_destinations',
      'route_destinations',
    ]);

    $nonRouteIDs = $this->getCleanDestinationsItems('Route', [
      'article_destinations',
      'event_destinations',
      'gallery_destinations',
      'recipe_destinations',
    ]);

    $this->deleteMetaIDs($nonArticleIDs);
    $this->deleteMetaIDs($nonEventIDs);
    $this->deleteMetaIDs($nonGalleryIDs);
    $this->deleteMetaIDs($nonRouteIDs);
    $this->deleteMetaIDs($nonRecipeIDs);
  }

  /**
   * Delete from wp_postmeta where the ids match the given array
   *
   * @param array $metaIDs
   * @return void
   */
  private function deleteMetaIDs($metaIDs = []) {
    global $wpdb;

    if (is_array($metaIDs) && !empty($metaIDs)) {
      $wpdb->query('
        DELETE FROM `wp_postmeta`
        WHERE `meta_id` IN (' . implode(',', $metaIDs) . ')'
      );
    }
  }

  /**
   * Get the meta ID items that need cleaning
   *
   * @param string $type
   * @param array $keysToRemove
   * @return array
   */
  private function getCleanDestinationsItems($type, $keysToRemove = []) {
    global $wpdb;

    $removeKeys = [];

    if (is_array($keysToRemove) && !empty($keysToRemove)) {
      foreach ($keysToRemove as $keyToRemove) {
        $removeKeys[] = '"' . $keyToRemove . '"';
        $removeKeys[] = '"_' . $keyToRemove . '"';
      }
    }

    if (empty($removeKeys)) {
      return [];
    }

    $itemIDs = [];
    $items = $wpdb->get_results('
      SELECT
        `pm`.`meta_id`,
        `pm_type`.`meta_value` AS `type`,
        `pm`.`post_id`,
        `pm`.`meta_key`,
        `pm`.`meta_value`

      FROM `wp_postmeta` `pm`

      JOIN `wp_postmeta` `pm_type`
      ON `pm`.`post_id` = `pm_type`.`post_id`

      WHERE `pm_type`.`meta_value` = "' . $type . '"
        AND `pm`.`meta_key` IN (' . implode(',', $removeKeys) . ')

      GROUP BY `meta_id`
    ');

    if (is_array($items) && !empty($items)) {
      foreach ($items as $item) {
        $itemIDs[] = $item->meta_id;
      }
    }

    return $itemIDs;
  }

  /**
   * Returns a wpdb connection for the original database
   *
   * @return mixed
   */
  public static function originalDatabaseConnection() {
    if (defined('VE_IMPORT_DB_NAME') && defined('VE_IMPORT_DB_USER') && defined('VE_IMPORT_DB_PASSWORD')) {
      $wpdb = new wpdb(VE_IMPORT_DB_USER, VE_IMPORT_DB_PASSWORD, VE_IMPORT_DB_NAME, 'localhost');

      if ($wpdb) {
        return $wpdb;
      }
    }

    return null;
  }

  /**
   * Import all articles for the given language
   *
   * @param string $language
   * @param array $ids
   * @return void
   */
  private function importArticles($language = 'en', $ids = []) {
    $importer = new VisitEurope_Importer_Article($language);
    $articleIDs = [];

    if (!is_array($ids) || empty($ids)) {
      $articles = $importer->getExistingArticleIDs();
    } else {
      $importer->importExistingArticles($ids);

      // Get the next IDs to import
      $after = intval(end($ids));
      $articles = $importer->getExistingArticleIDs($after);
    }

    foreach ($articles as $article) {
      $articleIDs[] = $article->id;
    }

    if (!empty($articleIDs)) {
      $url = get_site_url(null) . '/?' . http_build_query([
        'import' => true,
        'importFunction' => 'importArticles',
        'importIDs' => implode(',',  $articleIDs),
        'importLanguage' => $language,
      ]);

      // Errors with too many redirects using header
      echo '<script>window.location = "' . $url . '";</script>'; exit;
    } else {
      die('All articles imported for ' . $language);
    }
  }

  /**
   * Import all events
   *
   * @param string $language
   * @param array $ids
   * @return void
   */
  private function importEvents($language = '', $ids = []) {
    $importer = new VisitEurope_Importer_Event();
    $eventIDs = [];

    if (!is_array($ids) || empty($ids)) {
      $events = $importer->getExistingEventIDs();
    } else {
      $importer->importExistingEvents($ids);

      // Get the next IDs to import
      $after = intval(end($ids));
      $events = $importer->getExistingEventIDs($after);
    }

    foreach ($events as $event) {
      $eventIDs[] = $event->id;
    }

    if (!empty($eventIDs)) {
      $url = get_site_url(null) . '/?' . http_build_query([
        'import' => true,
        'importFunction' => 'importEvents',
        'importIDs' => implode(',',  $eventIDs)
      ]);

      // Errors with too many redirects using header
      echo '<script>window.location = "' . $url . '";</script>'; exit;
    } else {
      die('All events imported.');
    }
  }

  /**
   * Import all galleries for the given language
   *
   * @param string $language
   * @param array $ids
   * @return void
   */
  private function importGalleries($language = 'en', $ids = []) {
    $importer = new VisitEurope_Importer_Gallery($language);
    $galleryIDs = [];

    if (!is_array($ids) || empty($ids)) {
      $galleries = $importer->getExistingGalleryIDs();
    } else {
      $importer->importExistingGalleries($ids);

      // Get the next IDs to import
      $after = intval(end($ids));
      $galleries = $importer->getExistingGalleryIDs($after);
    }

    foreach ($galleries as $gallery) {
      $galleryIDs[] = $gallery->id;
    }

    if (!empty($galleryIDs)) {
      $url = get_site_url(null) . '/?' . http_build_query([
        'import' => true,
        'importFunction' => 'importGalleries',
        'importIDs' => implode(',',  $galleryIDs),
        'importLanguage' => $language,
      ]);

      // Errors with too many redirects using header
      echo '<script>window.location = "' . $url . '";</script>'; exit;
    } else {
      die('All galleries imported for ' . $language);
    }
  }

  /**
   * Import all products
   *
   * @param string $language
   * @param array $ids
   * @return void
   */
  private function importProducts($language = '', $ids = []) {
    $importer = new VisitEurope_Importer_Product();
    $productIDs = [];

    if (!is_array($ids) || empty($ids)) {
      $products = $importer->getExistingProductIDs();
    } else {
      $importer->importExistingProducts($ids);

      // Get the next IDs to import
      $after = intval(end($ids));
      $products = $importer->getExistingProductIDs($after);
    }

    foreach ($products as $product) {
      $productIDs[] = $product->id;
    }

    if (!empty($productIDs)) {
      $url = get_site_url(null) . '/?' . http_build_query([
        'import' => true,
        'importFunction' => 'importProducts',
        'importIDs' => implode(',',  $productIDs)
      ]);

      // Errors with too many redirects using header
      echo '<script>window.location = "' . $url . '";</script>'; exit;
    } else {
      die('All products imported.');
    }
  }

  /**
   * Import all recipes
   *
   * @param string $language
   * @param array $ids
   * @return void
   */
  private function importRecipes($language = '', $ids = []) {
    $importer = new VisitEurope_Importer_Recipe();
    $recipeIDs = [];

    if (!is_array($ids) || empty($ids)) {
      $recipes = $importer->getExistingRecipeIDs();
    } else {
      $importer->importExistingRecipes($ids);

      // Get the next IDs to import
      $after = intval(end($ids));
      $recipes = $importer->getExistingRecipeIDs($after);
    }

    foreach ($recipes as $recipe) {
      $recipeIDs[] = $recipe->id;
    }

    if (!empty($recipeIDs)) {
      $url = get_site_url(null) . '/?' . http_build_query([
        'import' => true,
        'importFunction' => 'importRecipes',
        'importIDs' => implode(',',  $recipeIDs)
      ]);

      // Errors with too many redirects using header
      echo '<script>window.location = "' . $url . '";</script>'; exit;
    } else {
      die('All recipes imported.');
    }
  }

  /**
   * Import all regions for the given language
   *
   * @param string $language
   * @param array $ids
   * @return void
   */
  private function importRegions($language = 'en', $ids = []) {
    $importer = new VisitEurope_Importer_Region($language);
    $regionIDs = [];

    if (!is_array($ids) || empty($ids)) {
      $regions = $importer->getExistingRegionIDs();
    } else {
      $importer->importExistingRegions($ids);

      // Get the next IDs to import
      $after = intval(end($ids));
      $regions = $importer->getExistingRegionIDs($after);
    }

    foreach ($regions as $region) {
      $regionIDs[] = $region->id;
    }

    if (!empty($regionIDs)) {
      $url = get_site_url(null) . '/?' . http_build_query([
        'import' => true,
        'importFunction' => 'importRegions',
        'importIDs' => implode(',',  $regionIDs),
        'importLanguage' => $language,
      ]);

      // Errors with too many redirects using header
      echo '<script>window.location = "' . $url . '";</script>'; exit;
    } else {
      die('All regions imported for ' . $language);
    }
  }

  /**
   * Import all routes
   *
   * @param string $language
   * @param array $ids
   * @return void
   */
  private function importRoutes($language = '', $ids = []) {
    $importer = new VisitEurope_Importer_Route();
    $routeIDs = [];

    if (!is_array($ids) || empty($ids)) {
      $routes = $importer->getExistingRouteIDs();
    } else {
      $importer->importExistingRoutes($ids);

      // Get the next IDs to import
      $after = intval(end($ids));
      $routes = $importer->getExistingRouteIDs($after);
    }

    foreach ($routes as $route) {
      $routeIDs[] = $route->id;
    }

    if (!empty($routeIDs)) {
      $url = get_site_url(null) . '/?' . http_build_query([
        'import' => true,
        'importFunction' => 'importRoutes',
        'importIDs' => implode(',',  $routeIDs)
      ]);

      // Errors with too many redirects using header
      echo '<script>window.location = "' . $url . '";</script>'; exit;
    } else {
      die('All routes imported.');
    }
  }

  /**
   * Import all countries for the given language
   *
   * @param string $language
   * @param array $codes
   * @return void
   */
  private function importCountries($language = 'en', $codes = []) {
    $importer = new VisitEurope_Importer_Country($language);
    $countryCodes = [];

    if (!is_array($codes) || empty($codes)) {
      $countries = $importer->getExistingCountryCodes();
    } else {
      $importer->importExistingCountries($codes);

      // Get the next codes to import
      $after = end($codes);
      $countries = $importer->getExistingCountryCodes($after);
    }

    foreach ($countries as $country) {
      $countryCodes[] = $country->code;
    }

    if (!empty($countryCodes)) {
      $url = get_site_url(null) . '/?' . http_build_query([
        'import' => true,
        'importFunction' => 'importCountries',
        'importIDs' => implode(',',  $countryCodes),
        'importLanguage' => $language,
      ]);

      // Errors with too many redirects using header
      echo '<script>window.location = "' . $url . '";</script>'; exit;
    } else {
      die('All countries imported for ' . $language);
    }
  }

  /**
   * Import all trips for the given language
   *
   * @param string $language
   * @param array $ids
   * @return void
   */
  private function importTrips($language = 'en', $ids = []) {
    $importer = new VisitEurope_Importer_Trip($language);
    $tripIDs = [];

    if (!is_array($ids) || empty($ids)) {
      $trips = $importer->getExistingTripIDs();
    } else {
      $importer->importExistingTrips($ids);

      // Get the next IDs to import
      $after = intval(end($ids));
      $trips = $importer->getExistingTripIDs($after);
    }

    foreach ($trips as $trip) {
      $tripIDs[] = $trip->id;
    }

    if (!empty($tripIDs)) {
      $url = get_site_url(null) . '/?' . http_build_query([
        'import' => true,
        'importFunction' => 'importTrips',
        'importIDs' => implode(',',  $tripIDs),
        'importLanguage' => $language,
      ]);

      // Errors with too many redirects using header
      echo '<script>window.location = "' . $url . '";</script>'; exit;
      // echo $url; exit;
    } else {
      die('All trips imported for ' . $language);
    }
  }

  /**
   * Update post meta captions
   *
   *
   *
   */
  private function importImageCaptions() {
    global $wpdb;

    $items = $wpdb->get_results('
      SELECT *

      FROM `' . $wpdb->prefix . 'postmeta` `m`

      WHERE `meta_key` LIKE "experience_content_%_body"
        AND `meta_value` LIKE "%<img%"
    ');

    if (is_array($items) && !empty($items)) {
      foreach ($items as $item) {
        preg_match_all('/<img[^>]+>/i', $item->meta_value, $results);
        $metaValue = $item->meta_value;

        if (is_array($results) && !empty($results)) {
          foreach ($results[0] as $image) {
            preg_match_all('/(title)=("[^"]*")/i', $image, $imageAttributes);

            if (is_array($imageAttributes) && !empty($imageAttributes) && isset($imageAttributes[2])) {
              if (is_array($imageAttributes[2]) && !empty($imageAttributes[2])) {
                $title = trim(str_replace('"', '', $imageAttributes[2][0]));

                if (!empty($title)) {
                  $replace = '[caption id="' . uniqid() . '" align="alignnone" width="800"]' . $image . $title . '[/caption]';
                  $metaValue = str_replace($image, $replace, $metaValue);
                }
              }
            }
          }
        }

        update_post_meta($item->post_id, $item->meta_key, $metaValue);
      }
    }

    echo count($items) . ' updated';
    exit;
  }

  /**
   * Prepends the given string with the copyright symbol
   *
   * @param string $copyright
   * @return string
   */
  public static function getCopyrightString($copyright) {
    $string = trim(str_replace('©', '', $copyright));

    if (empty($string)) {
      $string = '© Europe';
    } else {
      $string = '© ' . $string;
    }

    return $string;
  }

  /**
   * Gets the original post id so we can link it up to an english translation
   *
   * @param WP_Post $post
   * @return void
   */
  public static function getOriginalIDForPost($post) {
    $originalID = 0;

    // Destinations use either ID or country code to connect
    if ($post->post_type == VisitEurope_CPT_Destination::POST_TYPE) {
      $typeOfDestination = VisitEurope_Destination::getTypeForPostID($post->ID);

      if ($typeOfDestination == VisitEurope_Destination::TYPE_COUNTRY) {
        $originalID = get_field('code', $post->ID);
      } else {
        $originalID = intval(get_post_meta($post->ID, VisitEurope_Importer::KEY_IMPORT_ID, true));
      }

    // Experience uses the slug to connect
    } else if ($post->post_type == VisitEurope_CPT_Experience::POST_TYPE) {
      $typeOfExperience = VisitEurope_Experience::getTypeForPostID($post->ID);
      $originalSlug = get_post_meta($post->ID, VisitEurope_Importer::KEY_IMPORT_SLUG, true);

      // Slugs use languageCode-, ie fr-, or en-
      if (strpos($originalSlug, '-') == 2) {
        $slug = substr($originalSlug, 3, strlen($originalSlug) - 3);

        if (!empty($slug)) {
          $posts = get_posts([
            'post_type' => VisitEurope_CPT_Experience::POST_TYPE,
            'meta_key' => self::KEY_IMPORT_SLUG,
            'meta_value' => 'en-' . $slug,
            'meta_compare' => 'LIKE',
          ]);

          if (!empty($posts)) {
            $originalID = intval(get_post_meta($posts[0]->ID, VisitEurope_Importer::KEY_IMPORT_ID, true));
          }
        }
      }
    }

    return $originalID;
  }

  /**
   * Connects a post with its English version
   * https://wpml.org/wpml-hook/wpml_set_element_language_details/
   *
   * @param WP_Post $post
   * @param string $language
   * @return void
   */
  public static function connectPostToEnglish($post, $language, $slug = null) {
    if ($language == 'en') {
      return;
    }

    $postType = $post->post_type;
    $wpmlElementType = 'post_' . apply_filters('wpml_element_type', $postType);
    $originalID = self::getOriginalIDForPost($post);

    if ($slug !== null) {
      $slugWithoutLanguage = substr($slug, 3);
      $posts = get_posts([
        'post_type' => $postType,
        'meta_query' => [
          [
            [
              'key' => VisitEurope_Importer::KEY_IMPORT_SLUG,
              'value' => 'en-' . $slugWithoutLanguage,
            ],
            [
              'key' => VisitEurope_Importer::KEY_IMPORT_LANGUAGE,
              'value' => 'en',
            ],
          ],
        ],
      ]);
    } else {
      $posts = get_posts([
        'post_type' => $postType,
        'meta_query' => [
          [
            [
              'key' => VisitEurope_Importer::KEY_IMPORT_ID,
              'value' => $originalID,
            ],
            [
              'key' => VisitEurope_Importer::KEY_IMPORT_LANGUAGE,
              'value' => 'en',
            ],
          ],
        ],
      ]);
    }

    if (!is_array($posts) || empty($posts)) {
      return;
    }

    $englishPost = $posts[0];
    $englishPostLanguageInfo = apply_filters('wpml_element_language_details', null, [
      'element_id' => $englishPost->ID,
      'element_type' => $wpmlElementType,
    ]);

    if (!$englishPostLanguageInfo) {
      do_action('wpml_set_element_language_details', [
        'element_id' => $englishPost->ID,
        'element_type' => $wpmlElementType,
        'trid' => false,
        'language_code' => 'en',
        'source_language_code' => null,
      ]);

      $englishPostLanguageInfo = apply_filters('wpml_element_language_details', null, [
        'element_id' => $englishPost->ID,
        'element_type' => $wpmlElementType,
      ]);
    }

    if ($englishPostLanguageInfo) {
      if ($language == 'pt') {
        $language = 'pt-pt';
      }

      $data = [
        'element_id' => $post->ID,
        'element_type' => $wpmlElementType,
        'trid' => intval($englishPostLanguageInfo->trid),
        'language_code' => $language,
        'source_language_code' => 'en',
      ];

      do_action('wpml_set_element_language_details', $data);
    }
  }

}
