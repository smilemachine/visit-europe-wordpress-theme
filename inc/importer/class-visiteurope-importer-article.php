<?php
if (!defined('ABSPATH')) exit;

/**
 * Content import for Visit Europe: Article
 *
 * @package Visit_Europe
 */
class VisitEurope_Importer_Article {

  private $language;

  public function __construct($language = 'en') {
    $this->language = $language;
  }

  /**
   * Import all articles for the given language
   *
   * @param array $ids
   * @return void
   */
  public function importExistingArticles($ids = []) {
    $existing = $this->getExistingArticles($ids);

    if (is_array($existing) && !empty($existing)) {
      foreach ($existing as $article) {
        $this->createArticleFromExistingArticle($article);
      }
    }
  }

  /**
   * Create a new article from an existing source
   *
   * @param stdClass $article
   * @return boolean
   */
  private function createArticleFromExistingArticle($article) {
    $data = [
      'post_date' => $article->published_date,
      'post_content' => '',
      'post_title' => $article->title,
      'post_excerpt' => '',
      'post_status' => 'publish',
      'post_type' => VisitEurope_CPT_Experience::POST_TYPE,
      'comment_status' => 'closed',
      'ping_status' => 'closed',
    ];

    // Check if this post already exists
    $posts = get_posts([
      'posts_per_page' => 1,
      'post_type' => VisitEurope_CPT_Experience::POST_TYPE,
      'meta_key' => VisitEurope_Importer::KEY_IMPORT_ID,
      'meta_query' => [
        [
          'key' => VisitEurope_Importer::KEY_IMPORT_ID,
          'value' => $article->id,
        ],
        [
          'key' => VisitEurope_Importer::KEY_IMPORT_LANGUAGE,
          'value' => $article->language,
        ],
      ],
    ]);

    // If it doesn't exist, create it and store the original ID
    if (empty($posts)) {
      $postID = wp_insert_post($data);
      if ($postID == 0) { die('Could not create article for #' . $article->id); }

      add_post_meta($postID, VisitEurope_Importer::KEY_IMPORT_LANGUAGE, $article->language);
      add_post_meta($postID, VisitEurope_Importer::KEY_IMPORT_ID, $article->id);
      add_post_meta($postID, VisitEurope_Importer::KEY_IMPORT_SLUG, $article->slug);

      $post = get_post($postID);
    } else {
      $post = $posts[0];

      add_post_meta($post->ID, VisitEurope_Importer::KEY_IMPORT_LANGUAGE, $article->language);
      add_post_meta($post->ID, VisitEurope_Importer::KEY_IMPORT_ID, $article->id);
      add_post_meta($post->ID, VisitEurope_Importer::KEY_IMPORT_SLUG, $article->slug);
    }

    if ($this->language != 'en') {
      VisitEurope_Importer::connectPostToEnglish($post, $this->language);
    }

    // Get the feature image and assign it to this Article
    VisitEurope_Importer_Media::setPostFeatureImageForExistingMediaID($post->ID, $article->image_id);

    // Save the meta data
    $this->saveArticleContent($post, $article);
  }

  /**
   * Save the content tabs for the given article
   *
   * @param WP_Post $post
   * @param stdClass $article
   * @return void
   */
  private function saveArticleContent($post, $article) {
    update_field('field_59a9639887cd0', 'Article', $post->ID);  // Type

    $destinationIDs = [];

    // Save interests
    $tags = [];
    $existingTags = $this->getExistingTagsForExistingArticle($article->id);

    if (is_array($existingTags) && !empty($existingTags)) {
      foreach ($existingTags as $existingTag) {
        // If a destination exists with this tag name, use the destination instead
        $existing = get_posts([
          'post_type' => VisitEurope_CPT_Destination::POST_TYPE,
          'title' => $existingTag->tag,
          'posts_per_page' => 1,
        ]);

        if (is_array($existing) && !empty($existing)) {
          $destinationIDs[] = $existing[0]->ID;
        } else if (is_array($existing) && empty($existing)) {
          $tags[] = $existingTag->tag;
        }
      }
    }

    wp_set_post_terms($post->ID, $tags, VisitEurope_Taxonomy_Interest::TAXONOMY, false);

    // Save destinations
    $existingRegions = $this->getExistingRegionIDsForExistingArticle($article->id);

    if (is_array($existingRegions) && !empty($existingRegions)) {
      foreach ($existingRegions as $existingRegion) {
        $existing = get_posts([
          'post_type' => VisitEurope_CPT_Destination::POST_TYPE,
          'meta_key' => VisitEurope_Importer::KEY_IMPORT_ID,
          'meta_value' => intval($existingRegion->id),
          'posts_per_page' => 1,
        ]);

        if (is_array($existing) && !empty($existing)) {
          $destinationIDs[] = $existing[0]->ID;
        }
      }
    }

    // If destinations are empty, try the log-way-around for the location heirarchy
    if (empty($destinationIDs) && isset($article->revision_id)) {
      $destination = VisitEurope_Importer_Drupal::getDestinationForExistingEntity($article->id, $article->revision_id);

      if ($destination) {
        $destinationIDs[] = $destination->ID;
      }
    }

    update_field('field_59b63b4e78192', $destinationIDs, $post->ID); // Article destinations

    // Save content
    update_field('field_59a96b2b07e5c', html_entity_decode(strip_tags($article->subtitle)), $post->ID);  // Subtitle
    update_field('field_59b669daa33ad', html_entity_decode(strip_tags($article->blurb)), $post->ID); // Blurb

    $body = VisitEurope_Importer_Media::storeAndReplaceImagesInHtmlString($article->content);
    $body = str_replace('&gt;&nbsp;&lt;', '&gt;&lt;', htmlentities($body));

    $content = [
      'acf_fc_layout' => 'content',
      'body' => html_entity_decode($body),
    ];

    update_field('field_59a9667138809', [$content], $post->ID);  // Experience content
  }

  /**
   * Get existing article IDs
   *
   * @param array $ids
   * @return array
   */
  public function getExistingArticleIDs($after = 0) {
    $db = VisitEurope_Importer::originalDatabaseConnection();

    return $db->get_results('
      SELECT
        `a`.`customdisplayable_ptr_id` AS `id`

      FROM `new_content_article` `a`

      WHERE `a`.`customdisplayable_ptr_id` > ' . intval($after) . '
        AND `a`.`language_id` = "' . $this->language . '"

      ORDER BY `a`.`customdisplayable_ptr_id` ASC

      LIMIT 3
    ');
  }

  /**
   * Get existing articles
   *
   * @param array $ids
   * @return array
   */
  public function getExistingArticles($ids = []) {
    $db = VisitEurope_Importer::originalDatabaseConnection();
    $idsSql = '';

    if (is_array($ids) && !empty($ids)) {
      $ids = array_map('intval', $ids);
      $idsSql = 'AND `a`.`customdisplayable_ptr_id` IN (' . implode(',', $ids) . ')';
    }

    return $db->get_results('
      SELECT
        `a`.`customdisplayable_ptr_id` AS `id`,
        `ad`.`title` AS `title`,
        `a`.`overview` AS `subtitle`,
        `a`.`content` AS `content`,
        `ad`.`publish_date` AS `published_date`,
        `ad`.`hero_id` AS `image_id`,
        `a`.`language_id` AS `language`,
        `ad`.`description` AS `blurb`,
        `ad`.`slug` AS `slug`

      FROM `new_content_article` `a`

      JOIN `new_content_customdisplayable` `ad`
        ON `ad`.`id` = `a`.`customdisplayable_ptr_id`

      WHERE
        `a`.`language_id` = "' . $this->language . '"
        ' . $idsSql . '
    ');
  }

  /**
   * Get an array of existing region ids for an existing article
   *
   * @param integer $articleID
   * @return array
   */
  private function getExistingRegionIDsForExistingArticle($articleID) {
    $db = VisitEurope_Importer::originalDatabaseConnection();

    return $db->get_results('
      SELECT
        `regionbasics_id` AS `id`

      FROM `new_content_article_regions`

      WHERE `article_id` = ' . intval($articleID) . '
    ');
  }

  /**
   * Get an array of existing tags for an existing article
   *
   * @param integer $articleID
   * @return array
   */
  private function getExistingTagsForExistingArticle($articleID) {
    $db = VisitEurope_Importer::originalDatabaseConnection();

    return $db->get_results('
      SELECT
        `t`.`english_name` AS `tag`

      FROM `new_content_article_tags` `at`

      JOIN `new_content_tag` `t`
        ON `at`.`tag_id` = `t`.`id`

      WHERE `at`.`article_id` = ' . intval($articleID) . '
    ');
  }

}
