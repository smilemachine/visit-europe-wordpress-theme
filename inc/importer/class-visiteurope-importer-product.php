<?php
if (!defined('ABSPATH')) exit;

/**
 * Content import for Visit Europe: Products
 *
 * @package Visit_Europe
 */
class VisitEurope_Importer_Product {

  public function __construct() {
    //
  }

  /**
   * Import all products
   *
   * @param array $ids
   * @return void
   */
  public function importExistingProducts($ids = []) {
    $existing = $this->getExistingProducts($ids);

    if (is_array($existing) && !empty($existing)) {
      foreach ($existing as $product) {
        $this->createProductFromExistingProduct($product);
      }
    }
  }

  /**
   * Create a new product from an existing source
   *
   * @param stdClass $product
   * @return boolean
   */
  private function createProductFromExistingProduct($product) {
    $data = [
      'post_date' => date('Y-m-d H:i:s', $product->published_date),
      'post_content' => '',
      'post_title' => $product->title,
      'post_excerpt' => '',
      'post_status' => 'publish',
      'post_type' => VisitEurope_CPT_Experience::POST_TYPE,
      'comment_status' => 'closed',
      'ping_status' => 'closed',
    ];

    // Check if this post already exists
    $posts = get_posts([
      'posts_per_page' => 1,
      'post_type' => VisitEurope_CPT_Experience::POST_TYPE,
      'meta_query' => [
        [
          'key' => VisitEurope_Importer::KEY_IMPORT_ID,
          'value' => $product->id,
        ],
        [
          'key' => VisitEurope_Importer::KEY_IMPORT_REVISION,
          'value' => $product->revision_id,
        ],
        [
          'key' => 'field_59a9639887cd0',
          'value' => 'Product',
        ]
      ]
    ]);

    // If it doesn't exist, create it and store the original ID
    if (empty($posts)) {
      $postID = wp_insert_post($data);
      if ($postID == 0) { die('Could not create product for #' . $product->id); }

      add_post_meta($postID, VisitEurope_Importer::KEY_IMPORT_REVISION, $product->revision_id);
      add_post_meta($postID, VisitEurope_Importer::KEY_IMPORT_ID, $product->id);

      $post = get_post($postID);
    } else {
      $post = $posts[0];
    }

    // Save the meta data
    $this->saveProductContent($post, $product);

    // Get the feature image and assign it to this Product
    if ($product->image_id) {
      VisitEurope_Importer_Media_Drupal::setPostFeatureImageForExistingMediaID($post->ID, $product->image_id);
    } else {
      $attachments = get_attached_media(null, $post->ID);

      if (is_array($attachments) && !empty($attachments)) {
        $first = array_shift($attachments);

        if ($first && isset($first->ID)) {
          set_post_thumbnail($post->ID, $first->ID);
        }
      }
    }
  }

  /**
   * Save the content tabs for the given product
   *
   * @param WP_Post $post
   * @param stdClass $product
   * @return void
   */
  private function saveProductContent($post, $product) {
    $destinationIDs = [];
    $destination = null;

    update_field('field_59a9639887cd0', 'Product', $post->ID); // Type
    update_field('field_59b669daa33ad', strip_tags($product->blurb), $post->ID); // Blurb

    // About
    $about = [];
    $links = VisitEurope_Importer_Drupal::getExistingLinksForExistingEntity($product->id, $product->revision_id);

    foreach ($links as $link) {
      $about[] = [
        'title' => str_replace('"', "'", $link->title),
        'url' => $link->url,
      ];
    }

    update_field('field_59a96a32deb6d', $about, $post->ID);  // Product About

    // Tags
    $tags = [];
    $existingTags = VisitEurope_Importer_Drupal::getExistingTagsForExistingEntity($product->id, $product->revision_id);

    if (is_array($existingTags) && !empty($existingTags)) {
      foreach ($existingTags as $existingTag) {
        // If a destination exists with this tag name, use the destination instead
        $existing = get_posts([
          'post_type' => VisitEurope_CPT_Destination::POST_TYPE,
          'title' => $existingTag->tag,
          'posts_per_page' => 1,
        ]);

        if (is_array($existing) && !empty($existing)) {
          $destinationIDs[] = $existing[0]->ID;

          if (is_null($destination)) {
            $destination = $existing[0];
          }
        } else if (is_array($existing) && empty($existing)) {
          $tags[] = $existingTag->tag;
        }
      }
    }

    wp_set_post_terms($post->ID, $tags, VisitEurope_Taxonomy_Interest::TAXONOMY, false);

    // If destinations are empty, try the log-way-around for the location heirarchy
    if (empty($destinationIDs)) {
      $destination = VisitEurope_Importer_Drupal::getDestinationForExistingEntity($product->id, $product->revision_id);

      if ($destination) {
        $destinationIDs[] = $destination->ID;
      }
    }

    // Destinations
    update_field('field_59ce5b4694abf', $destinationIDs, $post->ID); // Product destinations

    // Gallery
    $slides = [];
    $productMedia = VisitEurope_Importer_Drupal::getExistingGalleryForExistingEntity($product->id, $product->revision_id);

    if (is_array($productMedia) && !empty($productMedia)) {
      foreach ($productMedia as $productMediaItem) {
        $attachmentID = VisitEurope_Importer_Media::attachMediaToPostFromExistingMedia($post->ID, $productMediaItem);
        $slides[] = [
          'title' => $productMediaItem->title,
          'image' => $attachmentID,
        ];
      }
    }

    // Content
    $experienceContent = [];
    $experienceContent[] = [
      'acf_fc_layout' => 'content',
      'body' => $product->content,
    ];

    if (!empty($slides)) {
      $experienceContent[] = [
        'acf_fc_layout' => 'carousel',
        'slides' => $slides,
      ];
    }

    update_field('field_59a9667138809', $experienceContent, $post->ID);  // Experience content

    // Pointers
    $pointers = [];

    if (!is_null($destination)) {
      $pointers[] = [
        'type' => 'Where',
        'subtitle' => $destination->post_title,
      ];
    }

    update_field('field_59a96b0d07e5a', $pointers, $post->ID);  // Pointers
  }

  /**
   * Get existing product IDs
   *
   * @param array $ids
   * @return array
   */
  public function getExistingProductIDs($after = 0) {
    $db = VisitEurope_Importer::originalDatabaseConnection();

    return $db->get_results('
      SELECT
        `n`.`nid` AS `id`

      FROM `etc_node` `n`

      WHERE `n`.`nid` > ' . intval($after) . '
        AND `n`.`type` = "quality_labels"

      ORDER BY `n`.`nid` ASC

      LIMIT 3
    ');
  }

  /**
   * Get existing products
   *
   * @param array $ids
   * @return array
   */
  public function getExistingProducts($ids = []) {
    $db = VisitEurope_Importer::originalDatabaseConnection();
    $idsSql = '';

    if (is_array($ids) && !empty($ids)) {
      $ids = array_map('intval', $ids);
      $idsSql = 'AND `n`.`nid` IN (' . implode(',', $ids) . ')';
    }

    return $db->get_results('
      SELECT
        `n`.`nid` AS `id`,
        `n`.`vid` AS `revision_id`,
        `n`.`created` AS `published_date`,
        `n`.`title` AS `title`,
        `body`.`body_value` AS `content`,
        `blurb`.`field_brief_description_value` AS `blurb`,
        `image`.`field_main_picture_fid` AS `image_id`

      FROM `etc_node` `n`

      JOIN `etc_field_data_body` `body`
        ON `body`.`entity_id` = `n`.`nid`
        AND `body`.`revision_id` = `n`.`vid`

      JOIN `etc_field_data_field_brief_description` `blurb`
        ON `blurb`.`entity_id` = `n`.`nid`
        AND `blurb`.`revision_id` = `n`.`vid`

      LEFT JOIN `etc_field_data_field_main_picture` `image`
        ON `image`.`entity_id` = `n`.`nid`
        AND `image`.`revision_id` = `n`.`vid`

      WHERE `n`.`type` = "quality_labels"
        ' . $idsSql . '
    ');
  }

}
