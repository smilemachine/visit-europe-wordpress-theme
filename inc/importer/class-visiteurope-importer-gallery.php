<?php
if (!defined('ABSPATH')) exit;

/**
 * Content import for Visit Europe: Gallery
 *
 * @package Visit_Europe
 */
class VisitEurope_Importer_Gallery {

  private $language;

  public function __construct($language = 'en') {
    $this->language = $language;
  }

  /**
   * Import all galleries for the given language
   *
   * @param array $ids
   * @return void
   */
  public function importExistingGalleries($ids = []) {
    $existing = $this->getExistingGalleries($ids);

    if (is_array($existing) && !empty($existing)) {
      foreach ($existing as $gallery) {
        $this->createGalleryFromExistingGallery($gallery);
      }
    }
  }

  /**
   * Create a new gallery from an existing source
   *
   * @param stdClass $gallery
   * @return boolean
   */
  private function createGalleryFromExistingGallery($gallery) {
    $data = [
      'post_date' => $gallery->published_date,
      'post_content' => '',
      'post_title' => $gallery->title,
      'post_excerpt' => '',
      'post_status' => 'publish',
      'post_type' => VisitEurope_CPT_Experience::POST_TYPE,
      'comment_status' => 'closed',
      'ping_status' => 'closed',
    ];

    // Check if this post already exists
    $posts = get_posts([
      'posts_per_page' => 1,
      'post_type' => VisitEurope_CPT_Experience::POST_TYPE,
      'meta_query' => [
        [
          'key' => VisitEurope_Importer::KEY_IMPORT_ID,
          'value' => $gallery->id,
        ],
        [
          'key' => VisitEurope_Importer::KEY_IMPORT_LANGUAGE,
          'value' => $gallery->language,
        ],
      ],
    ]);

    // If it doesn't exist, create it and store the original ID
    if (empty($posts)) {
      $postID = wp_insert_post($data);
      if ($postID == 0) { die('Could not create gallery for #' . $gallery->id); }

      add_post_meta($postID, VisitEurope_Importer::KEY_IMPORT_LANGUAGE, $gallery->language);
      add_post_meta($postID, VisitEurope_Importer::KEY_IMPORT_ID, $gallery->id);
      add_post_meta($postID, VisitEurope_Importer::KEY_IMPORT_SLUG, $gallery->slug);

      $post = get_post($postID);
    } else {
      $post = $posts[0];

      add_post_meta($post->ID, VisitEurope_Importer::KEY_IMPORT_LANGUAGE, $gallery->language);
      add_post_meta($post->ID, VisitEurope_Importer::KEY_IMPORT_ID, $gallery->id);
      add_post_meta($post->ID, VisitEurope_Importer::KEY_IMPORT_SLUG, $gallery->slug);
    }

    if ($this->language != 'en') {
      VisitEurope_Importer::connectPostToEnglish($post, $this->language);
    }

    // Get the feature image and assign it to this Gallery
    VisitEurope_Importer_Media::setPostFeatureImageForExistingMediaID($post->ID, $gallery->image_id);

    // Save the meta data
    $this->saveGalleryContent($post, $gallery);
  }

  /**
   * Save the content tabs for the given gallery
   *
   * @param WP_Post $post
   * @param stdClass $gallery
   * @return void
   */
  private function saveGalleryContent($post, $gallery) {
    update_field('field_59a9639887cd0', 'Gallery', $post->ID);  // Type

    $destinationIDs = [];

    // Save interests
    $tags = [];
    $existingTags = $this->getExistingTagsForExistingGallery($gallery->id);

    if (is_array($existingTags) && !empty($existingTags)) {
      foreach ($existingTags as $existingTag) {
        // If a destination exists with this tag name, use the destination instead
        $existing = get_posts([
          'post_type' => VisitEurope_CPT_Destination::POST_TYPE,
          'title' => $existingTag->tag,
          'posts_per_page' => 1,
        ]);

        if (is_array($existing) && !empty($existing)) {
          $destinationIDs[] = $existing[0]->ID;
        } else if (is_array($existing) && empty($existing)) {
          $tags[] = $existingTag->tag;
        }
      }
    }

    wp_set_post_terms($post->ID, $tags, VisitEurope_Taxonomy_Interest::TAXONOMY, false);

    // If destinations are empty, try the log-way-around for the location heirarchy
    if (empty($destinationIDs) && isset($gallery->revision_id)) {
      $destination = VisitEurope_Importer_Drupal::getDestinationForExistingEntity($gallery->id, $gallery->revision_id);

      if ($destination) {
        $destinationIDs[] = $destination->ID;
      }
    }

    // Save destinations
    update_field('field_59b870483adde', $destinationIDs, $post->ID); // Gallery destinations

    // Save content
    update_field('field_59b669daa33ad', html_entity_decode(strip_tags($gallery->content)), $post->ID); // Blurb

    // Get the slides for the gallery
    $slides = [];
    $galleryMedia = $this->getExistingMediaForExistingGallery($gallery->id, $gallery->language);

    if (is_array($galleryMedia) && !empty($galleryMedia)) {
      foreach ($galleryMedia as $galleryMediaItem) {
        $attachmentID = VisitEurope_Importer_Media::attachMediaToPostFromExistingMedia($post->ID, $galleryMediaItem);
        $slides[] = [
          'title' => $galleryMediaItem->title,
          'lat' => (float) $galleryMediaItem->lat,
          'lng' => (float) $galleryMediaItem->lng,
          'body' => $galleryMediaItem->overview,
          'image' => $attachmentID,
        ];
      }
    }

    $carousel = [
      'acf_fc_layout' => 'carousel',
      'slides' => $slides
    ];

    update_field('field_59a9667138809', [$carousel], $post->ID);  // Experience content
  }

  /**
   * Get existing media items for an existing gallery
   *
   * @param integer $galleryID
   * @param string $galleryLanguage
   * @return array
   */
  private function getExistingMediaForExistingGallery($galleryID, $galleryLanguage) {
    $db = VisitEurope_Importer::originalDatabaseConnection();
    return $db->get_results('
      SELECT
        `mi`.`copyright` AS `copyright`,
        `mi`.`urlHDMax_id` AS `url`,
        `mi`.`city` AS `city`,
        `mi`.`region` AS `region`,
        `mi`.`landmark` AS `landmark`,
        `mt`.`overview` AS `overview`,
        `mt`.`title` AS `title`,
        `mm`.`lat` AS `lat`,
        `mm`.`lng` AS `lng`

      FROM `new_content_gallery` `g`

      JOIN `new_content_customdisplayable` `gd`
        ON `g`.`customdisplayable_ptr_id` = `gd`.`id`

      JOIN `new_content_mediawithtext` `gm`
        ON `gm`.`gallery_id` = `g`.`customdisplayable_ptr_id`

      JOIN `media_image` `mi`
        ON `gm`.`image_id` = `mi`.`media_ptr_id`

      JOIN `media_media` `mm`
        ON `mi`.`media_ptr_id` = `mm`.`id`

      JOIN `new_content_mediawithtext` `mt`
        ON `mt`.`image_id` = `gm`.`image_id`
        AND `mt`.`gallery_id` = `g`.`customdisplayable_ptr_id`

      WHERE `g`.`customdisplayable_ptr_id` = ' . intval($galleryID) . '
        AND `g`.`language_id` = "' . $galleryLanguage . '"
    ');
  }

  /**
   * Get existing gallery IDs
   *
   * @param array $ids
   * @return array
   */
  public function getExistingGalleryIDs($after = 0) {
    $db = VisitEurope_Importer::originalDatabaseConnection();

    return $db->get_results('
      SELECT
        `g`.`customdisplayable_ptr_id` AS `id`

      FROM `new_content_gallery` `g`

      WHERE `g`.`customdisplayable_ptr_id` > ' . intval($after) . '
        AND `g`.`language_id` = "' . $this->language . '"

      ORDER BY `g`.`customdisplayable_ptr_id` ASC

      LIMIT 3
    ');
  }

  /**
   * Get existing galleries
   *
   * @param array $ids
   * @return array
   */
  public function getExistingGalleries($ids = []) {
    $db = VisitEurope_Importer::originalDatabaseConnection();
    $idsSql = '';

    if (is_array($ids) && !empty($ids)) {
      $ids = array_map('intval', $ids);
      $idsSql = 'AND `g`.`customdisplayable_ptr_id` IN (' . implode(',', $ids) . ')';
    }

    return $db->get_results('
      SELECT
        `g`.`customdisplayable_ptr_id` AS `id`,
        `gd`.`title` AS `title`,
        `g`.`overview` AS `content`,
        `gd`.`publish_date` AS `published_date`,
        `gd`.`hero_id` AS `image_id`,
        `g`.`language_id` AS `language`,
        `gd`.`slug` AS `slug`

      FROM `new_content_gallery` `g`

      JOIN `new_content_customdisplayable` `gd`
        ON `gd`.`id` = `g`.`customdisplayable_ptr_id`

      WHERE
        `g`.`language_id` = "' . $this->language . '"
        ' . $idsSql . '
    ');
  }

  /**
   * Get an array of existing tags for an existing gallery
   *
   * @param integer $galleryID
   * @return array
   */
  private function getExistingTagsForExistingGallery($galleryID) {
    $db = VisitEurope_Importer::originalDatabaseConnection();

    return $db->get_results('
      SELECT
        `t`.`english_name` AS `tag`

      FROM `new_content_gallery_tags` `gt`

      JOIN `new_content_tag` `t`
        ON `gt`.`tag_id` = `t`.`id`

      WHERE `gt`.`gallery_id` = ' . intval($galleryID) . '
    ');
  }

}
