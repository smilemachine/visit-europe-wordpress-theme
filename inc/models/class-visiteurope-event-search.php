<?php
if (!defined('ABSPATH')) exit;

/**
 * Event Search model
 *
 * @package Visit_Europe
 */
class VisitEurope_Event_Search {

  private $term;
  private $destinationID;
  private $startDate;
  private $endDate;

  public function __construct() {
    //
  }

  /**
   * Populates the class with valid data from the given array
   *
   * @param array $data
   * @return void
   */
  public function fill($data = []) {
    if (!is_array($data)) {
      return;
    }

    // Search Term
    if (isset($data['term']) && !empty($data['term'])) {
      $this->term = str_replace('/', '', sanitize_text_field($data['term']));
    }

    // Destination
    if (isset($data['destination']) && !empty($data['destination'])) {
      $destinationID = str_replace('/', '', sanitize_text_field($data['destination']));

      if (!is_int($destinationID)) {
        $posts = get_posts([
          'post_type' => VisitEurope_CPT_Destination::POST_TYPE,
          'name' => $destinationID,
          'posts_per_page' => 1,
        ]);

        if (is_array($posts) && isset($posts[0])) {
          $destinationID = intval($posts[0]->ID);
        }
      }

      $this->destinationID = intval($destinationID);
    }

    // Start date
    if (isset($_GET['from']) && !empty($_GET['from'])) {
      if (date_parse_from_format('Y-m-d', str_replace('/', '', $_GET['from']))) {
        $this->startDate = $_GET['from'];
      }
    }

    // End date
    if (isset($_GET['to']) && !empty($_GET['to'])) {
      if (date_parse_from_format('Y-m-d', str_replace('/', '', $_GET['to']))) {
        $this->endDate = $_GET['to'];
      }
    }
  }

  /**
   * Get the results IDs
   *
   * @return array
   */
  public function getResultsIDs() {
    $results = $this->getResults();
    return VisitEurope_Helper::postsToIDs($results);
  }

  /**
   * Get the results
   *
   * @return array
   */
  public function getResults() {
    global $wpdb;

    $dateWhereSql = '';
    $destinationJoinSql = '';
    $destinationWhereSql = '';
    $termJoinSql = '';
    $termWhereSql = '';

    // Date sql
    if ($this->startDate && $this->endDate) {
      $dateWhereSql = ' AND (
        `start`.`meta_value` BETWEEN "' . $this->startDate . '" AND "' . $this->endDate . '"
        OR
        `end`.`meta_value` BETWEEN "' . $this->startDate . '" AND "' . $this->endDate . '"
      )';
    } else if ($this->startDate) {
      $dateWhereSql = ' AND `start`.`meta_value` >= "' . esc_sql($this->startDate) . '"';
    } else if ($this->endDate) {
      $dateWhereSql = ' AND `end`.`meta_value` <= "' . esc_sql($this->endDate) . '"';
    }

    // Destination sql
    if ($this->destinationID) {
      $destinationJoinSql = '
        JOIN `' . $wpdb->prefix . 'postmeta` `destination`
          ON `destination`.`post_id` = `p`.`ID`
          AND `destination`.`meta_key` = "event_destinations"
      ';
      $destinationWhereSql = ' AND `destination`.`meta_value` LIKE \'%"' . intval($this->destinationID) . '"%\' ';
    }

    // Term sql
    if ($this->term) {
      $termJoinSql = '
        JOIN `' . $wpdb->prefix . 'postmeta` `term`
          ON `term`.`post_id` = `p`.`ID`
          AND `term`.`meta_key` = "blurb"
      ';
      $termWhereSql = ' AND `term`.`meta_value` LIKE "%' . esc_sql($this->term) . '%" ';
    }

    $translationJoinSql = '
      JOIN `' . $wpdb->prefix . 'icl_translations` `wpml`
        ON `wpml`.`element_id` = `p`.`ID`
        AND `wpml`.`element_type` = "post_' . VisitEurope_CPT_Experience::POST_TYPE . '"
        AND `wpml`.`language_code` = "' . ICL_LANGUAGE_CODE . '"
    ';

    $sql = '
      SELECT
        `p`.*

      FROM `' . $wpdb->prefix . 'posts` `p`

      JOIN `' . $wpdb->prefix . 'postmeta` `type`
        ON `type`.`post_id` = `p`.`ID`
        AND `type`.`meta_key` = "type"
        AND `type`.`meta_value` = "Event"

      LEFT JOIN `' . $wpdb->prefix . 'postmeta` `start`
        ON `start`.`post_id` = `p`.`ID`
        AND `start`.`meta_key` = "start_date"

      LEFT JOIN `' . $wpdb->prefix . 'postmeta` `end`
        ON `end`.`post_id` = `p`.`ID`
        AND `end`.`meta_key` = "end_date"

      ' . $destinationJoinSql . '
      ' . $termJoinSql . '
      ' . $translationJoinSql . '

      WHERE `p`.`post_type` = "' . VisitEurope_CPT_Experience::POST_TYPE . '"
        AND `p`.`post_status` = "publish"
        ' . $dateWhereSql . '
        ' . $destinationWhereSql . '
        ' . $termWhereSql . '

      GROUP BY `p`.`ID`
      ORDER BY `p`.`ID` DESC
    ';

    return $wpdb->get_results($sql);
  }

}
