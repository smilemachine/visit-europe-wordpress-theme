<?php
if (!defined('ABSPATH')) exit;

/**
 * Destination model
 *
 * @package Visit_Europe
 */
class VisitEurope_Destination {

  const TYPE_ALL = 'DESTINATION';
  const TYPE_REGION = 'REGION';
  const TYPE_COUNTRY = 'COUNTRY';
  const WEATHER = 'WEATHER';
  const WEATHER_LOW = 'LOW';
  const WEATHER_HIGH = 'HIGH';

  private $post;

  /**
   * Setup
   *
   * @param WP_Post
   * @return void
   */
  public function __construct($post) {
    $this->post = $post;
  }

  /**
   * Returns an array of Destinations
   *
   * @param array $customParams
   * @param boolean $returnAsList
   * @return array
   */
  public static function get($customParams = [], $returnAsList = false) {
    $defaultParams = [
      'post_status' => 'publish',
      'post_type' => VisitEurope_CPT_Destination::POST_TYPE,
      'orderby' => 'rand',
    ];

    // Apply the cache if necessary
    $uniqueCacheData = wp_cache_get(VisitEurope_Content::CACHE_DESTINATION);

    if (is_array($uniqueCacheData) && !empty($uniqueCacheData)
      && !isset($customParams['post__not_in'])) {
      $defaultParams['post__not_in'] = $uniqueCacheData;
    }

    // If limit is set to 0, transform it to infinite
    if (isset($params['posts_per_page']) && intval($params['posts_per_page']) == 0) {
      $params['posts_per_page'] = -1;
    }

    $params = VisitEurope_Helper::paramsForQuery($defaultParams, $customParams);

    // Show the current language if not already set
    if (!isset($params['surpress_filters'])) {
      $params['suppress_filters'] = false;
    }

    $posts = get_posts($params);
    $postsList = VisitEurope_Helper::postsToList($posts);

    // Add to cache if necessary
    if (is_array($uniqueCacheData)) {
      $postIDs = array_keys($postsList);
      wp_cache_set(VisitEurope_Content::CACHE_DESTINATION, array_merge($uniqueCacheData, $postIDs), null);
    }

    if ($returnAsList !== true) {
      return $posts;
    }

    return $postsList;
  }

  /**
   * Returns an array of Destinations in an array of IDs
   *
   * @param array $rawIDs
   * @param array $customParams
   * @param boolean $returnAsList
   * @return array
   */
  public static function getWhereIn($rawIDs, $customParams = [], $returnAsList = false) {
    $postIDs = VisitEurope_Helper::validIDsFromArray($rawIDs);

    if (empty($postIDs)) {
      return [];
    }

    $defaultParams = [
      'post__in' => $postIDs,
    ];
    $params = VisitEurope_Helper::paramsForQuery($defaultParams, $customParams);

    return self::get($params, $returnAsList);
  }

  /**
   * Returns an array of Destinations by type
   *
   * @param string $type
   * @param array $customParams
   * @param boolean $returnAsList
   * @return array
   */
  public static function getWhereType($type, $customParams = [], $returnAsList = false) {
    $defaultParams = [
      'posts_per_page' => -1,
      'orderby', 'title',
    ];

    if (isset($customParams['meta_query']) && is_array($customParams['meta_query'])) {
      $customParams['meta_query'][] = [
        'key' => VisitEurope_CPT_Destination::POST_TYPE . '_TYPE',
        'value' => $type,
      ];
    } else {
      $defaultParams['meta_key'] = VisitEurope_CPT_Destination::POST_TYPE . '_TYPE';
      $defaultParams['meta_value'] = $type;
    }

    $params = VisitEurope_Helper::paramsForQuery($defaultParams, $customParams);

    return self::get($params, $returnAsList);
  }

  /**
   * Returns an array of Destinations by type for a given Post ID
   *
   * @param integer $postID
   * @param string $type
   * @param array $customParams
   * @param boolean $returnAsList
   * @return array
   */
  private static function getWhereTypeForPostID($postID, $type, $customParams = [], $returnAsList = false) {
    $key = VisitEurope_CPT_Destination::POST_TYPE . VisitEurope_Content::KEY_SEPARATOR . $type;
    $array = get_post_meta($postID, $key, false);
    $postIDs = VisitEurope_Helper::validIDsFromArray($array);

    if (empty($postIDs)) {
      return [];
    }

    $defaultParams = [
      'post__in' => $postIDs,
    ];
    $params = VisitEurope_Helper::paramsForQuery($defaultParams, $customParams);

    return self::getWhereType($type, $params, $returnAsList);
  }

  /**
   * Returns an array of Country Destinations
   *
   * @param array $customParams
   * @param boolean $returnAsList
   * @return array
   */
  public static function getCountries($customParams = [], $returnAsList = false) {
    return self::getWhereType(self::TYPE_COUNTRY, $customParams, $returnAsList);
  }

  /**
   * Returns an array of Country Destinations for a given Post ID
   *
   * @param integer $postID
   * @param array $customParams
   * @param boolean $returnAsList
   * @return array
   */
  public static function getCountriesForPostID($postID, $customParams = [], $returnAsList = false) {
    return self::getWhereTypeForPostID($postID, self::TYPE_COUNTRY, $customParams, $returnAsList);
  }

  /**
   * Returns an array of Region Destinations by type
   *
   * @param array $customParams
   * @param boolean $returnAsList
   * @return array
   */
  public static function getRegions($customParams = [], $returnAsList = false) {
    return self::getWhereType(self::TYPE_REGION, $customParams, $returnAsList);
  }

  /**
   * Returns an array of Experiences for a given Post ID
   *
   * A Destination could either be a Region or a Country,
   * when a Region, use all countries associated to it
   *
   * @param integer $postID
   * @param array $customParams
   * @param boolean $returnAsList
   * @return array
   */
  public static function getExperiencesForPostID($postID, $customParams = [], $returnAsList = false) {
    $type = self::getTypeForPostID($postID);

    if ($type == self::TYPE_REGION) {
      $postIDs = array_keys(self::getCountriesForPostID($postID, [], true));
    } else {
      $postIDs = [$postID];
    }

    return VisitEurope_Experience::getWhereIn($postIDs, $customParams, $returnAsList);
  }

  /**
   * Returns an array of Experiences by type for a given Post ID
   *
   * @param integer $postID
   * @param string $type
   * @param array $customParams
   * @param boolean $returnAsList
   * @return array
   */
  private static function getExperiencesByTypeForPostID($postID, $type, $customParams = [], $returnAsList = false) {
    $type = self::getTypeForPostID($postID);

    if ($type == self::TYPE_REGION) {
      $postIDs = array_keys(self::getCountriesForPostID($postID, [], true));
    } else {
      $postIDs = [$postID];
    }

    $defaultParams = [
      'post__in' => $postIDs,
    ];
    $params = VisitEurope_Helper::paramsForQuery($defaultParams, $customParams);

    return VisitEurope_Experience::getWhereType($type, $params, $returnAsList);
  }

  /**
   * Returns an array of Event Experiences for a given Post ID
   *
   * @param integer $postID
   * @param array $customParams
   * @param boolean $returnAsList
   * @return array
   */
  public static function getEventsForPostID($postID, $customParams = [], $returnAsList = false) {
    return self::getExperiencesByTypeForPostID($postID, VisitEurope_Experience::TYPE_EVENT, $customParams, $returnAsList);
  }

  /**
   * Returns an array of Gallery Experiences for a given Post ID
   *
   * @param integer $postID
   * @param array $customParams
   * @param boolean $returnAsList
   * @return array
   */
  public static function getGalleriesForPostID($postID, $returnAsList = false) {
    return self::getExperiencesByTypeForPostID($postID, VisitEurope_Experience::TYPE_GALLERY, $returnAsList);
  }

  /**
   * Returns an array of Recipe Experiences for a given Post ID
   *
   * @param integer $postID
   * @param array $customParams
   * @param boolean $returnAsList
   * @return array
   */
  public static function getRecipesForPostID($postID, $customParams = [], $returnAsList = false) {
    return self::getExperiencesByTypeForPostID($postID, VisitEurope_Experience::TYPE_RECIPE, $customParams, $returnAsList);
  }

  /**
   * Returns an array of Route Experiences for a given Post ID
   *
   * @param integer $postID
   * @param array $customParams
   * @param boolean $returnAsList
   * @return array
   */
  public static function getRoutesForPostID($postID, $customParams = [], $returnAsList = false) {
    return self::getExperiencesByTypeForPostID($postID, VisitEurope_Experience::TYPE_ROUTE, $customParams, $returnAsList);
  }

  /**
   * Returns an array of Destination types
   *
   * @return array
   */
  public static function getTypes() {
    return [
      self::TYPE_COUNTRY => _x('Country', 'Destination type: Country', VisitEurope_Theme::KEY),
      self::TYPE_REGION => _x('Region', 'Destination type: Region', VisitEurope_Theme::KEY),
    ];
  }

  /**
   * Returns the type of Destination for a given Post ID
   *
   * @param integer $postID
   * @return string
   */
  public static function getTypeForPostID($postID) {
    return get_post_meta($postID, VisitEurope_CPT_Destination::POST_TYPE . '_TYPE', true);
  }

  /**
   * Updates the Destination type for a given Post ID
   *
   * @param integer $postID
   * @param array $rawType
   * @return void
   */
  public static function updateTypeForPostID($postID, $rawType) {
    $type = trim($rawType);
    $types = VisitEurope_Destination::getTypes();

    if (!empty($type) && in_array($type, array_keys($types))) {
      delete_post_meta($postID, VisitEurope_CPT_Destination::POST_TYPE . '_TYPE');
      update_post_meta($postID, VisitEurope_CPT_Destination::POST_TYPE . '_TYPE', sanitize_text_field($type));
    }
  }

  /**
   * Updates the Country Destinations for a given Post ID
   *
   * @param integer $postID
   * @param array $rawCountries
   * @return void
   */
  public static function updateCountriesForPostID($postID, $rawCountries) {
    self::deleteAllCountriesForPostID($postID);

    if (is_array($rawCountries)) {
      $countries = array_map('intval', $_POST['countries']);

      if (!empty($countries)) {
        foreach ($countries as $country) {
          add_post_meta($postID, VisitEurope_CPT_Destination::POST_TYPE . VisitEurope_Content::KEY_SEPARATOR . self::TYPE_COUNTRY, $country);
        }

        return;
      }
    }
  }

  /**
   * Deletes all the Country Destinations for a given Post ID
   *
   * @param integer $postID
   * @return void
   */
  public static function deleteAllCountriesForPostID($postID) {
    delete_post_meta($postID, VisitEurope_CPT_Destination::POST_TYPE . VisitEurope_Content::KEY_SEPARATOR . self::TYPE_COUNTRY);
  }

  /**
   * Updates the weather for a given Post ID
   *
   * @param integer $postID
   * @param array $weather
   * @return void
   */
  public static function updateWeatherForPostID($postID, $weather) {
    self::deleteAllWeatherForPostID($postID);

    if (is_array($weather)) {
      $metaPreKey = VisitEurope_CPT_Destination::POST_TYPE . VisitEurope_Content::KEY_SEPARATOR . self::WEATHER . VisitEurope_Content::KEY_SEPARATOR;

      foreach ($weather as $season => $months) {
        $seasonLow = 999;
        $seasonHigh = -999;
        $seasonKey = strtoupper($season);

        if (!is_array($months) || empty($months)) {
          continue;
        }

        foreach ($months as $month => $temps) {
          if (!is_array($temps) || empty($temps) || !isset($temps['low']) || !isset($temps['high'])) {
            continue;
          }

          $low = intval($temps['low']);
          $high = intval($temps['high']);

          if ($seasonLow > $low) {
            $seasonLow = $low;
          }

          if ($seasonHigh < $high) {
            $seasonHigh = $high;
          }

          $monthKey = strtoupper($month);
          $metaMonthLowKey = $metaPreKey . $monthKey . VisitEurope_Content::KEY_SEPARATOR . self::WEATHER_LOW;
          $metaMonthHighKey = $metaPreKey . $monthKey . VisitEurope_Content::KEY_SEPARATOR . self::WEATHER_HIGH;

          add_post_meta($postID, $metaMonthLowKey, $low);
          add_post_meta($postID, $metaMonthHighKey, $high);
        }

        $metaSeasonLowKey = $metaPreKey . $seasonKey . VisitEurope_Content::KEY_SEPARATOR . self::WEATHER_LOW;
        $metaSeasonHighKey = $metaPreKey . $seasonKey . VisitEurope_Content::KEY_SEPARATOR . self::WEATHER_HIGH;

        if ($seasonLow != 999) {
          add_post_meta($postID, $metaSeasonLowKey, $seasonLow);
        }

        if ($seasonHigh != -999) {
          add_post_meta($postID, $metaSeasonHighKey, $seasonHigh);
        }
      }
    }
  }

  /**
   * Deletes all the weather information for a given Post ID
   *
   * @param integer $postID
   * @return void
   */
  public static function deleteAllWeatherForPostID($postID) {
    $keys = self::getWeatherKeys();

    foreach ($keys as $key) {
      delete_post_meta($postID, $key);
    }
  }

  /**
   * Present the given post into a format for the frontend
   *
   * @return array
   */
  public function presentPost() {
    return [
      'id' => $this->post->ID,
      'body' => VisitEurope_Helper::formatStringForFrontend($this->post->post_content),
      'filters' => [],
      'copyright' => VisitEurope_Helper::getPostThumbnailCopyright($this->post->ID),
      'copyrighturl' => VisitEurope_Helper::getPostThumbnailCopyrightUrl($this->post->ID),
      'imageinitial' => VisitEurope_Helper::getPostThumbnailUrl($this->post->ID, 'initial'),
      'imagefull' => VisitEurope_Helper::getPostThumbnailUrl($this->post->ID, 'medium'),
      'map' => [
        'icon' => get_template_directory_uri() . '/assets/img/google-map-marker.png',
        'location' => [
          'lat' => (float) get_field('map_lat', $this->post->ID),
          'lng' => (float) get_field('map_lng', $this->post->ID),
        ],
        'overlays' => $this->presentPostOverlays(),
      ],
      'save' => true,
      'title' => VisitEurope_Helper::formatStringForFrontend($this->post->post_title),
      'type' => self::getTypeForPostID($this->post->ID),
      'url' => get_permalink($this->post->ID),
    ];
  }

  /**
   * Returns the overlay for the given post id
   *
   * @param integer $countryID
   * @return array
   */
  private function presentPostOverlayForCountry($countryID) {
    $countryCode = get_field('code', $countryID);
    $overlayFilename = strtolower($countryCode) . '.svg';
    $overlayUrl = get_template_directory_uri() . '/assets/img/destinations/' . $overlayFilename;
    $overlayBounds = VisitEurope_Destination::getBoundsByCountryCode($countryCode);

    return [
      'imageurl' => $overlayUrl,
      'bounds' => $overlayBounds,
    ];
  }

  /**
   * Returns the overlays for the current destination
   *
   * @return array
   */
  public function presentPostOverlays() {
    $type = self::getTypeForPostID($this->post->ID);
    $overlays = [];

    if ($type == self::TYPE_COUNTRY) {
      $overlays[] = $this->presentPostOverlayForCountry($this->post->ID);
    } else {
      $countrys = [];
      $countryPosts = VisitEurope_Destination::getCountriesForPostID($this->post->ID);

      if (is_array($countryPosts) && !empty($countryPosts)) {
        foreach ($countryPosts as $countryPost) {
          $overlays[] = $this->presentPostOverlayForCountry($countryPost->ID);
        }
      }
    }

    return $overlays;
  }

  /**
   * Present the weather
   *
   * @return array
   */
  public function presentPostWeather() {
    $metaPreKey = VisitEurope_CPT_Destination::POST_TYPE . VisitEurope_Content::KEY_SEPARATOR . self::WEATHER . VisitEurope_Content::KEY_SEPARATOR;
    $metaPostLowKey = VisitEurope_Content::KEY_SEPARATOR . self::WEATHER_LOW;
    $metaPostHighKey = VisitEurope_Content::KEY_SEPARATOR . self::WEATHER_HIGH;

    $months = [
      'jan' => [
        'title' => VisitEurope_Content::getLocale('months.jan'),
        'key' => '01',
        'low' => VisitEurope_Helper::convertToIntOrNull(get_post_meta($this->post->ID, $metaPreKey . 'JAN' . $metaPostLowKey, true)),
        'high' => VisitEurope_Helper::convertToIntOrNull(get_post_meta($this->post->ID, $metaPreKey . 'JAN' . $metaPostHighKey, true)),
      ],
      'feb' => [
        'title' => VisitEurope_Content::getLocale('months.feb'),
        'key' => '02',
        'low' => VisitEurope_Helper::convertToIntOrNull(get_post_meta($this->post->ID, $metaPreKey . 'FEB' . $metaPostLowKey, true)),
        'high' => VisitEurope_Helper::convertToIntOrNull(get_post_meta($this->post->ID, $metaPreKey . 'FEB' . $metaPostHighKey, true)),
      ],
      'mar' => [
        'title' => VisitEurope_Content::getLocale('months.mar'),
        'key' => '03',
        'low' => VisitEurope_Helper::convertToIntOrNull(get_post_meta($this->post->ID, $metaPreKey . 'MAR' . $metaPostLowKey, true)),
        'high' => VisitEurope_Helper::convertToIntOrNull(get_post_meta($this->post->ID, $metaPreKey . 'MAR' . $metaPostHighKey, true)),
      ],
      'apr' => [
        'title' => VisitEurope_Content::getLocale('months.apr'),
        'key' => '04',
        'low' => VisitEurope_Helper::convertToIntOrNull(get_post_meta($this->post->ID, $metaPreKey . 'APR' . $metaPostLowKey, true)),
        'high' => VisitEurope_Helper::convertToIntOrNull(get_post_meta($this->post->ID, $metaPreKey . 'APR' . $metaPostHighKey, true)),
      ],
      'may' => [
        'title' => VisitEurope_Content::getLocale('months.may'),
        'key' => '05',
        'low' => VisitEurope_Helper::convertToIntOrNull(get_post_meta($this->post->ID, $metaPreKey . 'MAY' . $metaPostLowKey, true)),
        'high' => VisitEurope_Helper::convertToIntOrNull(get_post_meta($this->post->ID, $metaPreKey . 'MAY' . $metaPostHighKey, true)),
      ],
      'jun' => [
        'title' => VisitEurope_Content::getLocale('months.jun'),
        'key' => '06',
        'low' => VisitEurope_Helper::convertToIntOrNull(get_post_meta($this->post->ID, $metaPreKey . 'JUN' . $metaPostLowKey, true)),
        'high' => VisitEurope_Helper::convertToIntOrNull(get_post_meta($this->post->ID, $metaPreKey . 'JUN' . $metaPostHighKey, true)),
      ],
      'jul' => [
        'title' => VisitEurope_Content::getLocale('months.jul'),
        'key' => '07',
        'low' => VisitEurope_Helper::convertToIntOrNull(get_post_meta($this->post->ID, $metaPreKey . 'JUL' . $metaPostLowKey, true)),
        'high' => VisitEurope_Helper::convertToIntOrNull(get_post_meta($this->post->ID, $metaPreKey . 'JUL' . $metaPostHighKey, true)),
      ],
      'aug' => [
        'title' => VisitEurope_Content::getLocale('months.aug'),
        'key' => '08',
        'low' => VisitEurope_Helper::convertToIntOrNull(get_post_meta($this->post->ID, $metaPreKey . 'AUG' . $metaPostLowKey, true)),
        'high' => VisitEurope_Helper::convertToIntOrNull(get_post_meta($this->post->ID, $metaPreKey . 'AUG' . $metaPostHighKey, true)),
      ],
      'sep' => [
        'title' => VisitEurope_Content::getLocale('months.sep'),
        'key' => '09',
        'low' => VisitEurope_Helper::convertToIntOrNull(get_post_meta($this->post->ID, $metaPreKey . 'SEP' . $metaPostLowKey, true)),
        'high' => VisitEurope_Helper::convertToIntOrNull(get_post_meta($this->post->ID, $metaPreKey . 'SEP' . $metaPostHighKey, true)),
      ],
      'oct' => [
        'title' => VisitEurope_Content::getLocale('months.oct'),
        'key' => '10',
        'low' => VisitEurope_Helper::convertToIntOrNull(get_post_meta($this->post->ID, $metaPreKey . 'OCT' . $metaPostLowKey, true)),
        'high' => VisitEurope_Helper::convertToIntOrNull(get_post_meta($this->post->ID, $metaPreKey . 'OCT' . $metaPostHighKey, true)),
      ],
      'nov' => [
        'title' => VisitEurope_Content::getLocale('months.nov'),
        'key' => '11',
        'low' => VisitEurope_Helper::convertToIntOrNull(get_post_meta($this->post->ID, $metaPreKey . 'NOV' . $metaPostLowKey, true)),
        'high' => VisitEurope_Helper::convertToIntOrNull(get_post_meta($this->post->ID, $metaPreKey . 'NOV' . $metaPostHighKey, true)),
      ],
      'dec' => [
        'title' => VisitEurope_Content::getLocale('months.dec'),
        'key' => '12',
        'low' => VisitEurope_Helper::convertToIntOrNull(get_post_meta($this->post->ID, $metaPreKey . 'DEC' . $metaPostLowKey, true)),
        'high' => VisitEurope_Helper::convertToIntOrNull(get_post_meta($this->post->ID, $metaPreKey . 'DEC' . $metaPostHighKey, true)),
      ],
    ];

    // Return depending on the location in the hemisphere
    if (intval(get_field('map_lat', $this->post->ID)) < 0) {
      return [
        'seasons' => [
          'summer' => [
            'title' => VisitEurope_Content::getLocale('seasons.summer'),
            'key' => '01',
            'low' => VisitEurope_Helper::convertToIntOrNull(get_post_meta($this->post->ID, $metaPreKey . 'SUMMER' . $metaPostLowKey, true)),
            'high' => VisitEurope_Helper::convertToIntOrNull(get_post_meta($this->post->ID, $metaPreKey . 'SUMMER' . $metaPostHighKey, true)),
            'months' => [
              'dec' => $months['dec'],
              'jan' => $months['jan'],
              'feb' => $months['feb'],
            ],
          ],
          'autumn' => [
            'title' => VisitEurope_Content::getLocale('seasons.autumn'),
            'key' => '02',
            'low' => VisitEurope_Helper::convertToIntOrNull(get_post_meta($this->post->ID, $metaPreKey . 'AUTUMN' . $metaPostLowKey, true)),
            'high' => VisitEurope_Helper::convertToIntOrNull(get_post_meta($this->post->ID, $metaPreKey . 'AUTUMN' . $metaPostHighKey, true)),
            'months' => [
              'mar' => $months['mar'],
              'apr' => $months['apr'],
              'may' => $months['may'],
            ],
          ],
          'winter' => [
            'title' => VisitEurope_Content::getLocale('seasons.winter'),
            'key' => '03',
            'low' => VisitEurope_Helper::convertToIntOrNull(get_post_meta($this->post->ID, $metaPreKey . 'WINTER' . $metaPostLowKey, true)),
            'high' => VisitEurope_Helper::convertToIntOrNull(get_post_meta($this->post->ID, $metaPreKey . 'WINTER' . $metaPostHighKey, true)),
            'months' => [
              'jun' => $months['jun'],
              'jul' => $months['jul'],
              'aug' => $months['aug'],
            ],
          ],
          'spring' => [
            'title' => VisitEurope_Content::getLocale('seasons.spring'),
            'key' => '04',
            'low' => VisitEurope_Helper::convertToIntOrNull(get_post_meta($this->post->ID, $metaPreKey . 'SPRING' . $metaPostLowKey, true)),
            'high' => VisitEurope_Helper::convertToIntOrNull(get_post_meta($this->post->ID, $metaPreKey . 'SPRING' . $metaPostHighKey, true)),
            'months' => [
              'sep' => $months['sep'],
              'oct' => $months['oct'],
              'nov' => $months['nov'],
            ],
          ],
        ],
        'months' => $months,
      ];
    } else {
      return [
        'seasons' => [
          'summer' => [
            'title' => VisitEurope_Content::getLocale('seasons.summer'),
            'key' => '01',
            'low' => VisitEurope_Helper::convertToIntOrNull(get_post_meta($this->post->ID, $metaPreKey . 'SUMMER' . $metaPostLowKey, true)),
            'high' => VisitEurope_Helper::convertToIntOrNull(get_post_meta($this->post->ID, $metaPreKey . 'SUMMER' . $metaPostHighKey, true)),
            'months' => [
              'jun' => $months['jun'],
              'jul' => $months['jul'],
              'aug' => $months['aug'],
            ],
          ],
          'autumn' => [
            'title' => VisitEurope_Content::getLocale('seasons.autumn'),
            'key' => '02',
            'low' => VisitEurope_Helper::convertToIntOrNull(get_post_meta($this->post->ID, $metaPreKey . 'AUTUMN' . $metaPostLowKey, true)),
            'high' => VisitEurope_Helper::convertToIntOrNull(get_post_meta($this->post->ID, $metaPreKey . 'AUTUMN' . $metaPostHighKey, true)),
            'months' => [
              'sep' => $months['sep'],
              'oct' => $months['oct'],
              'nov' => $months['nov'],
            ],
          ],
          'winter' => [
            'title' => VisitEurope_Content::getLocale('seasons.winter'),
            'key' => '03',
            'low' => VisitEurope_Helper::convertToIntOrNull(get_post_meta($this->post->ID, $metaPreKey . 'WINTER' . $metaPostLowKey, true)),
            'high' => VisitEurope_Helper::convertToIntOrNull(get_post_meta($this->post->ID, $metaPreKey . 'WINTER' . $metaPostHighKey, true)),
            'months' => [
              'dec' => $months['dec'],
              'jan' => $months['jan'],
              'feb' => $months['feb'],
            ],
          ],
          'spring' => [
            'title' => VisitEurope_Content::getLocale('seasons.spring'),
            'key' => '04',
            'low' => VisitEurope_Helper::convertToIntOrNull(get_post_meta($this->post->ID, $metaPreKey . 'SPRING' . $metaPostLowKey, true)),
            'high' => VisitEurope_Helper::convertToIntOrNull(get_post_meta($this->post->ID, $metaPreKey . 'SPRING' . $metaPostHighKey, true)),
            'months' => [
              'mar' => $months['mar'],
              'apr' => $months['apr'],
              'may' => $months['may'],
            ],
          ],
        ],
        'months' => $months,
      ];
    }
  }

  /**
   * Present the given post's extras into a format for the frontend
   *
   * @return array
   */
  public function presentPostExtras() {
    $pointers = [];

    while (have_rows('pointers', $this->post->ID)) {
      the_row();
      $type = VisitEurope_ACF_FC::getPointerTypeFromRawType(get_sub_field('type', $this->post->ID));
      $title = VisitEurope_ACF_FC::getPointerTitleFromType($type);
      $subtitle = get_sub_field('subtitle', $this->post->ID);
      $url = get_sub_field('url', $this->post->ID);
      $pointers[] = [
        'type' => $type,
        'title' => VisitEurope_Helper::formatStringForFrontend($title),
        'subtitle' => VisitEurope_Helper::formatStringForFrontend($subtitle),
        'url' => $url ? $url : '#',
      ];
    }

    return [
      'map' => [
        'icon' => get_template_directory_uri() . '/assets/img/google-map-marker.png',
        'location' => [
          'lat' => (float) get_field('map_lat', $this->post->ID),
          'lng' => (float) get_field('map_lng', $this->post->ID),
        ],
      ],
      'pointers' => $pointers,
      'weather' => $this->presentPostWeather()
    ];
  }

  /**
   * Returns an array of all available weather keys
   *
   * @return array
   */
  public static function getWeatherKeys() {
    $metaPreKey = VisitEurope_CPT_Destination::POST_TYPE . VisitEurope_Content::KEY_SEPARATOR . self::WEATHER . VisitEurope_Content::KEY_SEPARATOR;

    return [
      $metaPreKey . 'SUMMER' . VisitEurope_Content::KEY_SEPARATOR . self::WEATHER_LOW,
      $metaPreKey . 'SUMMER' . VisitEurope_Content::KEY_SEPARATOR . self::WEATHER_HIGH,
      $metaPreKey . 'AUTUMN' . VisitEurope_Content::KEY_SEPARATOR . self::WEATHER_LOW,
      $metaPreKey . 'AUTUMN' . VisitEurope_Content::KEY_SEPARATOR . self::WEATHER_HIGH,
      $metaPreKey . 'WINTER' . VisitEurope_Content::KEY_SEPARATOR . self::WEATHER_LOW,
      $metaPreKey . 'WINTER' . VisitEurope_Content::KEY_SEPARATOR . self::WEATHER_HIGH,
      $metaPreKey . 'SPRING' . VisitEurope_Content::KEY_SEPARATOR . self::WEATHER_LOW,
      $metaPreKey . 'SPRING' . VisitEurope_Content::KEY_SEPARATOR . self::WEATHER_HIGH,
      $metaPreKey . 'JAN' . VisitEurope_Content::KEY_SEPARATOR . self::WEATHER_LOW,
      $metaPreKey . 'JAN' . VisitEurope_Content::KEY_SEPARATOR . self::WEATHER_HIGH,
      $metaPreKey . 'FEB' . VisitEurope_Content::KEY_SEPARATOR . self::WEATHER_LOW,
      $metaPreKey . 'FEB' . VisitEurope_Content::KEY_SEPARATOR . self::WEATHER_HIGH,
      $metaPreKey . 'MAR' . VisitEurope_Content::KEY_SEPARATOR . self::WEATHER_LOW,
      $metaPreKey . 'MAR' . VisitEurope_Content::KEY_SEPARATOR . self::WEATHER_HIGH,
      $metaPreKey . 'APR' . VisitEurope_Content::KEY_SEPARATOR . self::WEATHER_LOW,
      $metaPreKey . 'APR' . VisitEurope_Content::KEY_SEPARATOR . self::WEATHER_HIGH,
      $metaPreKey . 'MAY' . VisitEurope_Content::KEY_SEPARATOR . self::WEATHER_LOW,
      $metaPreKey . 'MAY' . VisitEurope_Content::KEY_SEPARATOR . self::WEATHER_HIGH,
      $metaPreKey . 'JUN' . VisitEurope_Content::KEY_SEPARATOR . self::WEATHER_LOW,
      $metaPreKey . 'JUN' . VisitEurope_Content::KEY_SEPARATOR . self::WEATHER_HIGH,
      $metaPreKey . 'JUL' . VisitEurope_Content::KEY_SEPARATOR . self::WEATHER_LOW,
      $metaPreKey . 'JUL' . VisitEurope_Content::KEY_SEPARATOR . self::WEATHER_HIGH,
      $metaPreKey . 'AUG' . VisitEurope_Content::KEY_SEPARATOR . self::WEATHER_LOW,
      $metaPreKey . 'AUG' . VisitEurope_Content::KEY_SEPARATOR . self::WEATHER_HIGH,
      $metaPreKey . 'SEP' . VisitEurope_Content::KEY_SEPARATOR . self::WEATHER_LOW,
      $metaPreKey . 'SEP' . VisitEurope_Content::KEY_SEPARATOR . self::WEATHER_HIGH,
      $metaPreKey . 'OCT' . VisitEurope_Content::KEY_SEPARATOR . self::WEATHER_LOW,
      $metaPreKey . 'OCT' . VisitEurope_Content::KEY_SEPARATOR . self::WEATHER_HIGH,
      $metaPreKey . 'NOV' . VisitEurope_Content::KEY_SEPARATOR . self::WEATHER_LOW,
      $metaPreKey . 'NOV' . VisitEurope_Content::KEY_SEPARATOR . self::WEATHER_HIGH,
      $metaPreKey . 'DEC' . VisitEurope_Content::KEY_SEPARATOR . self::WEATHER_LOW,
      $metaPreKey . 'DEC' . VisitEurope_Content::KEY_SEPARATOR . self::WEATHER_HIGH,
    ];
  }

  /**
   * Returns the bounds for a given country code
   *
   * @param string $code
   * @return array
   */
  public static function getBoundsByCountryCode($code = '') {
    $bounds = [
      'north' => 0,
      'south' => 0,
      'east' => 0,
      'west' => 0,
    ];

    switch ($code) {

      // Austria
      case 'AT':
        $bounds = [
          'north' => 49.06515002,
          'south' => 46.35500647,
          'east' => 17.26401367,
          'west' => 9.443720703,
        ];
        break;

      // Belgium
      case 'BE':
        $bounds = [
          'north' => 51.5238794,
          'south' => 49.44216043,
          'east' => 6.659399414,
          'west' => 2.543305664,
        ];
        break;

      // Bulgaria
      case 'BG':
        $bounds = [
          'north' => 44.61048887,
          'south' => 40.7509506,
          'east' => 28.62909668,
          'west' => 22.36032227,
        ];
        break;

      // Switzerland
      case 'CH':
        $bounds = [
          'north' => 47.93747096,
          'south' => 45.67492993,
          'east' => 10.58252258,
          'west' => 5.830297241,
        ];
        break;

      // Cyprus
      case 'CY':
        $bounds = [
          'north' => 35.99,
          'south' => 34.2535161,
          'east' => 34.6046875,
          'west' => 32.26166016,
        ];
        break;

      // Czech Republic
      case 'CZ':
        $bounds = [
          'north' => 51.39017607,
          'south' => 48.23998583,
          'east' => 18.92114258,
          'west' => 12.09878906,
        ];
        break;

      // Germany
      case 'DE':
        $bounds = [
          'north' => 55.10719924,
          'south' => 47.22243552,
          'east' => 15.05787109,
          'west' => 5.836835937,
        ];
        break;

      // Denmark
      case 'DK':
        $bounds = [
          'north' => 57.75362512,
          'south' => 54.52630682,
          'east' => 15.78442383,
          'west' => 6.152773437,
        ];
        break;

      // Estonia
      case 'EE':
        $bounds = [
          'north' => 59.74773307,
          'south' => 57.47678962,
          'east' => 28.27461914,
          'west' => 21.76461914,
        ];
        break;

      // Spain
      case 'ES':
        $bounds = [
          'north' => 43.77402999,
          'south' => 27.4918156,
          'east' => 7.31958313,
          'west' => -20.95986389,
        ];
        break;

      // Finland
      case 'FI':
        $bounds = [
          'north' => 70.53291706,
          'south' => 58.92885453,
          'east' => 31.81418945,
          'west' => 19.47762695,
        ];
        break;

      // France
      case 'FR':
        $bounds = [
          'north' => 51.13427489,
          'south' => 41.1412398,
          'east' => 10.9462207,
          'west' => -6.184462891,
        ];
        break;

      // UK
      case 'GB':
        $bounds = [
          'north' => 61.14784847,
          'south' => 48.69344719,
          'east' => 2.057131958,
          'west' => -10.80300354,
        ];
        break;

      // Greece
      case 'GR':
        $bounds = [
          'north' => 42.63909186,
          'south' => 33.73995015,
          'east' => 28.303710937,
          'west' => 19.34960937,
        ];
        break;

      // Croatia
      case 'HR':
        $bounds = [
          'north' => 46.55308401,
          'south' => 42.37862885,
          'east' => 19.41806152,
          'west' => 13.48405273,
        ];
        break;

      // Hungary
      case 'HU':
        $bounds = [
          'north' => 48.62719506,
          'south' => 45.65610859,
          'east' => 22.94835937,
          'west' => 16.10285156,
        ];
        break;

      // Ireland
      case 'IE':
        $bounds = [
          'north' => 55.67659616,
          'south' => 51.07441614,
          'east' => -5.364394531,
          'west' => -10.68378906,
        ];
        break;

      // Iceland
      case 'IS':
        $bounds = [
          'north' => 66.84311815,
          'south' => 62.98910183,
          'east' => -13.39540039,
          'west' => -24.57050781,
        ];
        break;

      // Italy
      case 'IT':
        $bounds = [
          'north' => 47.33453846,
          'south' => 35.0550214,
          'east' => 18.72171875,
          'west' => 6.634746094,
        ];
        break;

      // Lithuania
      case 'LT':
        $bounds = [
          'north' => 56.65393239,
          'south' => 53.63528955,
          'east' => 26.94047852,
          'west' => 20.9619043,
        ];
        break;

      // Latvia
      case 'LV':
        $bounds = [
          'north' => 58.31573086,
          'south' => 55.42765548,
          'east' => 28.34574219,
          'west' => 20.79612305,
        ];
        break;

      // Luxembourg
      case 'LU':
        $bounds = [
          'north' => 50.25342285,
          'south' => 49.39995363,
          'east' => 6.71008300,
          'west' => 5.54902099,
        ];
        break;

      // Monaco
      case 'MC':
        $bounds = [
          'north' => 43.75692688,
          'south' => 43.71939382,
          'east' => 7.44560408,
          'west' => 7.40570068,
        ];
        break;

      // Montenegro
      case 'ME':
        $bounds = [
          'north' => 43.65938849,
          'south' => 41.74136261,
          'east' => 20.40773315,
          'west' => 18.43985535,
        ];
        break;

      // Malta
      case 'MT':
        $bounds = [
          'north' => 36.11509213,
          'south' => 35.77303539,
          'east' => 14.64540405,
          'west' => 14.11124207,
        ];
        break;

      // Netherlands
      case 'NL':
        $bounds = [
          'north' => 53.56271198,
          'south' => 50.70408221,
          'east' => 7.575701904,
          'west' => 3.0699823,
        ];
        break;

      // Norway
      case 'NO':
        $bounds = [
          'north' => 71.27059325,
          'south' => 57.76738756,
          'east' => 32.58058472,
          'west' => 3.399572144,
        ];
        break;

      // Poland
      case 'PL':
        $bounds = [
          'north' => 55.58724068,
          'south' => 48.07130158,
          'east' => 24.27492065,
          'west' => 14.10025574,
        ];
        break;

      // Portugal
      case 'PT':
        $bounds = [
          'north' => 43.95670354,
          'south' => 30.35121138,
          'east' => -5.937481689,
          'west' => -31.33919739,
        ];
        break;

      // Romania
      case 'RO':
        $bounds = [
          'north' => 49.12013976,
          'south' => 42.76935233,
          'east' => 29.74611206,
          'west' => 20.27457214,
        ];
        break;

      // Serbia
      case 'RS':
        $bounds = [
          'north' => 46.19785668,
          'south' => 41.75066309,
          'east' => 23.17285461,
          'west' => 18.71176697,
        ];
        break;

      // Sweden
      case 'SE':
        $bounds = [
          'north' => 69.20304701,
          'south' => 55.0867781,
          'east' => 25.62280579,
          'west' => 9.801854858,
        ];
        break;

      // Slovenia
      case 'SI':
        $bounds = [
          'north' => 46.87799294,
          'south' => 43.92201342,
          'east' => 17.91040344,
          'west' => 13.05380798,
        ];
        break;

      // Slovakia
      case 'SK':
        $bounds = [
          'north' => 49.84768962,
          'south' => 47.48528948,
          'east' => 22.61255188,
          'west' => 16.84409119,
        ];
        break;

      // San Marino
      case 'SM':
        $bounds = [
          'north' => 43.99573976,
          'south' => 43.89169497,
          'east' => 12.53808899,
          'west' => 12.38364197,
        ];
        break;

      // Turkey
      case 'TR':
        $bounds = [
          'north' => 42.70561097,
          'south' => 35.05905657,
          'east' => 44.96972961,
          'west' => 25.67160583,
        ];
        break;

      // Default
      default:
        break;
    }

    return $bounds;
  }

  /**
   * Returns the markers for the given post
   *
   * @return array
   */
  public function getMarkers() {
    return $this->getMarkersForThings();
    // $type = self::getTypeForPostID($this->post->id);
    //
    // if ($type == self::TYPE_COUNTRY) {
    //   $country = $this->presentPost();
    //   $countries = [[
    //     'location' => $country['map']['location'],
    //   	'icon' => $country['map']['icon'],
    //     'metadata' => [
    //       'item' => $this->presentPost(),
    //     ],
    //   ]];
    //   $things = $this->getMarkersForThings();
    // } else {
    //   $countries = $this->getMarkersForCountries();
    //   $things = $this->getMarkersForThings();
    // }
    //
    // return array_merge($countries, $things);
  }

  /**
   * Gets the markers for a destination's countries
   *
   * @return array
   */
  private function getMarkersForCountries() {
    $markers = [];
    $countries = self::getCountriesForPostID($this->post->ID);

    if (is_array($countries) && !empty($countries)) {
    	foreach ($countries as $country) {
    		$vd = new self($country);
    		$countryPost = $vd->presentPost();
    		$markers[] = [
    			'icon' => $countryPost['map']['icon'],
    			'location' => $countryPost['map']['location'],
    			'metadata' => [
    				'item' => $countryPost,
    			],
    		];
    	}
    }

    return $markers;
  }

  /**
   * Gets the markers for a destination's things
   *
   * @return array
   */
  private function getMarkersForThings() {
    $markers = [];
    $things = get_field('top_things_gallery', $this->post->ID);

    if (is_array($things) && !empty($things)) {
    	foreach ($things as $thing) {
    		$mapIcon = get_template_directory_uri() . '/assets/img/google-map-marker.png';
    		$mapLocation = [
    			'lat' => (float) $thing['lat'],
    			'lng' => (float) $thing['lng'],
    		];

    		$markers[] = [
    			'icon' => $mapIcon,
    			'location' => $mapLocation,
    			'metadata' => [
    				'item' => [
    					'title' => $thing['title'],
    					'imageinitial' => $thing['image']['sizes']['initial'],
    					'imagefull' => $thing['image']['sizes']['thumbnail'],
    					'body' => $thing['body'],
    					'map' => [
    						'icon' => $mapIcon,
    						'location' => $mapLocation,
    					],
    				],
    			],
    		];
    	}
    }

    return $markers;
  }

}
