<?php
if (!defined('ABSPATH')) exit;

/**
 * Interest model
 *
 * @package Visit_Europe
 */
class VisitEurope_Interest {

  public function __construct() {
    //
  }

  /**
   * Returns a tax query for the given interests
   *
   * @param array $interests
   * @return array
   */
  public static function getTaxQuery($interests = []) {
    $taxQuery = [];

    if (is_array($interests) && !empty($interests)) {
      $taxQuery = ['relation' => 'OR'];

      foreach ($interests as $interest) {
        $taxQuery[] = [
          'taxonomy' => VisitEurope_Taxonomy_Interest::TAXONOMY,
          'field' => 'term_id',
          'terms' => intval($interest),
        ];
      }
    }

    return $taxQuery;
  }

  /**
   * Returns an array of taxonomy ids for the given interest group name
   *
   * @param string $name
   * @return array
   */
  public static function getTaxIDsByInterestGroupName($name) {
    $key = VisitEurope_Helper::getSlug($name);

    if ($key == 'all') {
      return;
    }

    $interestGroups = get_field('interest_groups', 'option');
    $interests = [];

    if (is_array($interestGroups) && !empty($interestGroups)) {
      foreach ($interestGroups as $interestGroup) {
        $interestGroupKey = VisitEurope_Helper::getSlug($interestGroup['title']);

        if ($interestGroupKey == $key) {
          $interests = $interestGroup['filters'];
          break;
        }
      }
    }

    return $interests;
  }

}
