<?php
if (!defined('ABSPATH')) exit;

/**
 * User model
 *
 * @package Visit_Europe
 */
class VisitEurope_User {

  const META_VERIFIED = 'AUTH_VERIFIED';
  const META_VERIFY_TOKEN = 'VERIFY_TOKEN';
  const META_FORGOT_TOKEN = 'FORGOT_TOKEN';

  /**
   * Returns an array of Users
   *
   * @param array $customParams
   * @param boolean $returnAsList
   * @return array
   */
  public static function get($customParams = [], $returnAsList = false) {
    $defaultParams = [
      'orderby' => 'display_name',
    ];
    $params = VisitEurope_Helper::paramsForQuery($defaultParams, $customParams);
    $users = get_users($params);

    if ($returnAsList !== true) {
      return $users;
    }

    $list = [];

    if (is_array($users) && !empty($users)) {
      foreach ($users as $item) {
        if (isset($item->ID) && isset($item->data->display_name)) {
          $list[$item->ID] = $item->data->display_name;
        }
      }
    }

    return $list;
  }

  /**
   * Checks to see if the current user is an administrator
   *
   * @return boolean
   */
  public static function isCurrentUserAdmin() {
    return current_user_can('administrator');
  }

  /**
   * Checks to see if the current user is a member
   *
   * @return boolean
   */
  public static function isCurrentUserMember() {
    return user_can(self::getCurrentUserID(), VisitEurope_Auth::ROLE_MEMBER);
  }

  /**
   * Returns the ID of the current user
   *
   * @return integer
   */
  public static function getCurrentUserID() {
    $userID1 = intval(get_current_user_id());
    $userID2 = intval(apply_filters('determine_current_user', false));

    return $userID1 != 0 ? $userID1 : $userID2;
  }

  /**
   * Determines if the viewer is currently logged in
   *
   * @return boolean
   */
  public static function isCurrentlyLoggedIn() {
    return intval(self::getCurrentUserID()) > 0;
  }

  /**
   * Checks to see if the current user is a member
   *
   * @return boolean
   */
  public static function currentUserIsVerified() {
    if (self::isCurrentlyLoggedIn()) {
      return self::isVerified(self::getCurrentUserID());
    }

    return false;
  }

  /**
   * Returns the currently logged in member
   *
   * @return mixed
   */
  public static function getCurrentMember() {
    $isLoggedIn = VisitEurope_User::isCurrentlyLoggedIn();
    $isVerified = VisitEurope_User::currentUserIsVerified();
    $isMember = VisitEurope_User::isCurrentUserMember();

    if ($isLoggedIn && $isVerified && $isMember) {
      return get_user_by('ID', self::getCurrentUserID());
    }

    return null;
  }


  /**
   * Determines if the current user is verified or not
   *
   * @param integer $userID
   * @return boolean
   */
  public static function isVerified($userID) {
    return get_user_meta($userID, self::META_VERIFIED, true) == self::META_VERIFIED;
  }

  /**
   * Log the user in with their ID
   *
   * @param integer $userID
   * @return boolean
   */
  public static function loginWithID($userID) {
    wp_set_current_user($userID);
    wp_set_auth_cookie($userID, false);

    return VisitEurope_User::isCurrentlyLoggedIn();
  }

  /**
   * Log the user in with their email address
   *
   * @param integer $userID
   * @return boolean
   */
  public static function loginWithEmail($email) {
    $user = get_user_by('email', $email);

    if ($user && self::isVerified($user->ID)) {
      return self::loginWithID($user->ID);
    }

    return false;
  }

}
