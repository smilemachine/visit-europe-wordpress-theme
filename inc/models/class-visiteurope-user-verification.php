<?php
if (!defined('ABSPATH')) exit;

/**
 * User model
 *
 * @package Visit_Europe
 */
class VisitEurope_User_Verification {

  const META_VERIFIED = 'AUTH_VERIFIED';
  const META_VERIFY_TOKEN = 'VERIFY_TOKEN';
  const META_FORGOT_TOKEN = 'FORGOT_TOKEN';

  /**
   * Sends a verification email for the given user
   *
   * @param WP_User
   * @return bool
   */
  public static function sendVerificationEmail($user) {
    if (!$user || !isset($user->data) || !isset($user->data->user_email)) {
      return false;
    }

    $token = self::setVerifyToken($user->ID);
    $url = get_site_url('', null) . '/' . ICL_LANGUAGE_CODE . '/verify/?' . http_build_query([
      'user' => $user->data->user_email,
      'token' => $token,
    ]);

    $subject = get_field('user_verification_register_email_subject', 'option');
    $body = get_field('user_verification_register_email_body', 'option');
    $body = str_replace(':link', $url, $body);

    return VisitEurope_Helper::sendHtmlEmail([$user->data->user_email], $subject, $body);
  }

  /**
   * Sends a forgot email for the given user
   *
   * @param WP_User
   * @return bool
   */
  public static function sendForgotEmail($user) {
    if (!$user || !isset($user->data) || !isset($user->data->user_email)) {
      return false;
    }

    $token = self::setForgotToken($user->ID);
    $url = get_site_url('', null) . '/' . ICL_LANGUAGE_CODE . '/?' . http_build_query([
      'forgot' => $user->data->user_email,
      'token' => $token,
    ]);

    $subject = get_field('user_verification_forgot_email_subject', 'option');
    $body = get_field('user_verification_forgot_email_body', 'option');
    $body = str_replace(':link', $url, $body);

    return VisitEurope_Helper::sendHtmlEmail([$user->data->user_email], $subject, $body);
  }

  /**
   * Sets the forgot token for a user
   *
   * @param integer $userID
   * @return string
   */
  private static function setForgotToken($userID) {
    $token = wp_generate_password(32, false, false);
    update_user_meta($userID, self::META_FORGOT_TOKEN, $token);

    return $token;
  }

  /**
   * Sets the forgot token for a user
   *
   * @param integer $userID
   * @return string
   */
  public static function clearForgotToken($userID) {
    delete_user_meta($userID, self::META_FORGOT_TOKEN);
  }

  /**
   * Sets the verify token for a user
   *
   * @param integer $userID
   * @return string
   */
  private static function setVerifyToken($userID) {
    if (intval($userID) <= 0) {
      return false;
    }

    $token = wp_generate_password(32, false, false);
    update_user_meta($userID, self::META_VERIFY_TOKEN, $token);

    return $token;
  }

  /**
   * Returns the user attempting verification
   *
   * @return mixed
   */
  public static function getUserAttemptingToVerify() {
    $email = VisitEurope_Helper::getAttributeFromGET('user');
    $token = VisitEurope_Helper::getAttributeFromGET('token');
    $user = get_user_by('email', $email);

    if ($user) {
      $userToken = get_user_meta($user->ID, self::META_VERIFY_TOKEN, true);

      if (!empty($userToken) && $token == $userToken) {
        return $user;
      }
    }

    return null;
  }

  /**
   * Returns the user attempting password reset
   *
   * @return mixed
   */
  public static function userAttemptingToReset() {
    $email = VisitEurope_Helper::getAttributeFromGET('forgot');
    $token = VisitEurope_Helper::getAttributeFromGET('token');
    $user = get_user_by('email', $email);

    if ($user) {
      $userToken = get_user_meta($user->ID, self::META_FORGOT_TOKEN, true);

      if (!empty($userToken) && $token == $userToken) {
        return $user;
      }
    }

    return null;
  }

  /**
   * Verify a user via email and token
   *
   * @return boolean
   */
  public static function verify() {
    if ($user = self::getUserAttemptingToVerify()) {
      self::markAsVerified($user->ID);
      VisitEurope_User::loginWithEmail($user->data->user_email);
      return true;
    }

    return false;
  }

  /**
   * Clear verify token
   *
   * @param integer $userID
   * @return void;
   */
  private static function clearVerifyToken($userID) {
    delete_user_meta($userID, self::META_VERIFY_TOKEN);
  }

  /**
   * Marks the given user id as verified
   *
   * @param integer $userID
   * @return void
   */
  public static function markAsVerified($userID) {
    update_user_meta($userID, self::META_VERIFIED, self::META_VERIFIED);
  }

  /**
   * Marks the given user id as verified
   *
   * @param integer $userID
   * @return void
   */
  public static function markAsUnverified($userID) {
    delete_user_meta($userID, self::META_VERIFIED);
  }

}
