<?php
if (!defined('ABSPATH')) exit;

/**
 * UserTrip model
 *
 * @package Visit_Europe
 */
class VisitEurope_UserTrip {

  const TYPE_CUSTOM = 'CUSTOM';
  const TYPE_PLANNED = 'PLANNED';

  private $post;

  /**
   * Setup
   *
   * @param WP_Post
   * @return void
   */
  public function __construct($post) {
    $this->post = $post;
  }

  /**
   * Returns an array of UserTrips that have items
   *
   * @param array $customParams
   * @return array
   */
  public static function get($customParams = []) {
    $defaultParams = [
      'post_status' => 'publish',
      'post_type' => VisitEurope_CPT_UserTrip::POST_TYPE,
      'orderby' => 'title',
      'order' => 'ASC',
      'posts_per_page' => -1
    ];

    // Force to the current user
    if (!isset($customParams['meta_query'])) {
      $customParams['meta_query'] = [];
    }

    $customParams['meta_query'][] = [
      'key' => 'user',
      'value' => VisitEurope_User::getCurrentUserID(),
    ];

    // If limit is set to 0, transform it to infinite
    if (isset($params['posts_per_page']) && intval($params['posts_per_page']) == 0) {
      $params['posts_per_page'] = -1;
    }

    $params = VisitEurope_Helper::paramsForQuery($defaultParams, $customParams);
    $posts = get_posts($params);

    return $posts;
  }

  /**
   * Returns an array of User Trips by type
   *
   * @param string $type
   * @param array $customParams
   * @param boolean $returnAsList
   * @return array
   */
  public static function getWhereType($type, $customParams = [], $returnAsList = false) {
    $defaultParams = [];
    $type = ucfirst(strtolower($type));

    if (isset($customParams['meta_query']) && is_array($customParams['meta_query'])) {
      $customParams['meta_query'][] = [
        'key' => 'type',
        'value' => $type,
      ];
    } else {
      $defaultParams['meta_query'] = [
        [
          'key' => 'type',
          'value' => $type,
        ]
      ];
    }

    $params = VisitEurope_Helper::paramsForQuery($defaultParams, $customParams);

    return self::get($params, $returnAsList);
  }

  /**
   * Returns a post for this user where trip is set to the given item id
   *
   * @param integer $itemID
   * @return WP_Post
   */
  public static function getByTripID($itemID) {
    return self::get([
      'posts_per_page' => 1,
      'meta_query' => [
        [
          'key' => 'type',
          'value' => ucfirst(strtolower(self::TYPE_PLANNED))
        ],
        [
          'key' => 'trip',
          'value' => intval($itemID),
        ],
      ],
    ]);
  }

  /**
   * Returns the type of UserTrip for a given Post ID
   *
   * @param integer $postID
   * @return string
   */
  public static function getTypeForPostID($postID) {
    return strtoupper(get_field('type', $postID));
  }

  /**
   * Get the item ids associated to this post
   *
   * @return array
   */
  public function getItemIDs() {
    $items = get_field('field_5a0435cfd169f', $this->post->ID);   // Items
    $itemIDs = [];

    if (is_array($items) && !empty($items)) {
      foreach ($items as $item) {
        if ($item['type'] == 'Destination' && isset($item['destination']->ID)) {
          $itemIDs[] = $item['destination']->ID;
        } else if ($item['type'] == 'Experience' && isset($item['experience']->ID)) {
          $itemIDs[] = $item['experience']->ID;
        }
      }
    }

    return $itemIDs;
  }

  /**
   * Returns the items associated to this UserTrip
   *
   * @return WP_Post
   */
  public function getItems() {
    $items = [];
    $postItems = get_field('field_5a0435cfd169f', $this->post->ID);   // Items

    if (is_array($postItems) && !empty($postItems)) {
      foreach ($postItems as $item) {
        if ($item['type'] == 'Destination' && isset($item['destination']->ID)) {
          $items[] = $item;
        } else if ($item['type'] == 'Experience' && isset($item['experience']->ID)) {
          $items[] = $item;
        }
      }
    }

    return $items;
  }

  /**
   * Returns a random item associated to this UserTrip
   *
   * @return WP_Post
   */
  public function presentItems() {
    $items = $this->getItems();
    $presentedItems = [];

    if (is_array($items) && !empty($items)) {
      foreach ($items as $item) {
        if (is_array($item)) {
          if ($item['type'] == 'Destination' && isset($item['destination']->ID)) {
            $ut = new VisitEurope_Destination($item['destination']);
          } else if ($item['type'] == 'Experience' && isset($item['experience']->ID)) {
            $ut = new VisitEurope_Experience($item['experience']);
          }

          $presentedItems[] = $ut->presentPost();
        }
      }
    }

    return $presentedItems;
  }

  /**
   * Returns a random item associated to this UserTrip
   *
   * @return WP_Post
   */
  public function getFirstItem() {
    $items = $this->getItems();

    if (is_array($items) && isset($items[0])) {
      if ($items[0]['type'] == 'Destination' && isset($items[0]['destination']->ID)) {
        return $items[0]['destination'];
      } else if ($items[0]['type'] == 'Experience' && isset($items[0]['experience']->ID)) {
        return $items[0]['experience'];
      }
    }

    return null;
  }

  /**
   * Returns a random item associated to this UserTrip
   *
   * @return WP_Post
   */
  public function getRandomItem() {
    $items = $this->getItems();

    if (is_array($items) && !empty($items)) {
      $index = rand(0, count($items) - 1);
      $item = $items[$index];

      if ($item['type'] == 'Destination' && isset($item['destination']->ID)) {
        return $item['destination'];
      } else if ($item['type'] == 'Experience' && isset($item['experience']->ID)) {
        return $item['experience'];
      }
    }

    return null;
  }

  /**
   * Determines if the user trip is planned
   *
   * @param integer $postID
   * @return boolean
   */
  public static function isPlanned($postID) {
    return strtoupper(get_field('type', intval($postID), true)) == self::TYPE_PLANNED;
  }

  /**
   * Determines if the user trip is custom
   *
   * @param integer $postID
   * @return boolean
   */
  public static function isCustom($postID) {
    return strtoupper(get_field('type', intval($postID), true)) == self::TYPE_CUSTOM;
  }

  /**
   * Present a UserTrip item
   *
   * @return array
   */
  public function presentPost() {
    $postIDForImage = $this->post->ID;
    $title = $this->post->post_title;

    if ($randomItem = $this->getFirstItem()) {
      $postIDForImage = $randomItem->ID;
    }

    if (self::isPlanned($this->post->ID)) {
      if ($trip = get_field('trip', $this->post->ID)) {
        $postIDForImage = $trip->ID;
        $title = $trip->post_title;
      }
    }

    return [
      'id' => intval($this->post->ID),
      'copyright' => VisitEurope_Helper::getPostThumbnailCopyright($postIDForImage),
      'copyrighturl' => VisitEurope_Helper::getPostThumbnailCopyrightUrl($postIDForImage),
      'imageinitial' => VisitEurope_Helper::getPostThumbnailUrl($postIDForImage, 'initial'),
      'imagefull' => VisitEurope_Helper::getPostThumbnailUrl($postIDForImage, 'medium'),
      'items' => $this->presentItems(),
      'title' => VisitEurope_Helper::formatStringForFrontend($title),
      'type' => self::getTypeForPostID($this->post->ID),
      'url' => get_permalink($this->post->ID),
    ];
  }

  /**
   * Adds a trip to the current user trip
   *
   * @param integer $itemID
   * @return boolean
   */
  public function addTrip($itemID) {
    update_field('trip', $itemID, $this->post->ID);
  }

  /**
   * Adds an item to the current user trip
   *
   * @param integer $itemID
   * @param string $itemType
   * @return boolean
   */
  public function addItem($itemID, $itemType) {
    $currentItems = $this->getItems();
    $updatedItems = [];
    $addItem = true;

    if (is_array($currentItems) && !empty($currentItems)) {
      $updatedItems = $currentItems;

      foreach ($currentItems as $currentItem) {

        // Destination item
        if (isset($currentItem['destination']) && $currentItem['destination']) {
          if ($currentItem['destination']->ID == $itemID) {
            $addItem = false;
            break;
          }
        }

        // Experience items
        if (isset($currentItem['experience']) && $currentItem['experience']) {
          if ($currentItem['experience']->ID == $itemID) {
            $addItem = false;
            break;
          }
        }
      }
    }

    // Already in the array
    if (!$addItem) {
      return true;
    }

    $destinationTypes = array_keys(VisitEurope_Destination::getTypes());

    if (in_array($itemType, $destinationTypes)) {
      $updatedItems[] = [
        'type' => 'Destination',
        'destination' => $itemID
      ];
    } else {
      $updatedItems[] = [
        'type' => 'Experience',
        'experience' => $itemID
      ];
    }

    update_field('field_5a0435cfd169f', $updatedItems, $this->post->ID);  // Items

    return true;
  }

  /**
   * Sets this user trip as a trip
   *
   * @return boolean;
   */
  public function setAsPlanned() {
    update_field('type', ucfirst(strtolower(self::TYPE_PLANNED)), $this->post->ID);
    return true;
  }

  /**
   * Sets this user trip as custom
   *
   * @return boolean;
   */
  public function setAsCustom() {
    update_field('type', ucfirst(strtolower(self::TYPE_CUSTOM)), $this->post->ID);
    return true;
  }

  /**
   * Assigns this user trip to the current user
   *
   * @return boolean
   */
  public function setUserAsCurrentUser() {
    $user = VisitEurope_User::getCurrentMember();
    return $this->setUser($user);
  }

  /**
   * Assigns this user trip to the given user
   *
   * @param WP_User $user
   * @return boolean
   */
  public function setUser($user) {
    if ($user && isset($user->ID)) {
      update_field('user', $user->ID, $this->post->ID);
    }

    return false;
  }

  /**
   * Update the order of items
   *
   * @param array $itemIDs
   * @return boolean
   */
  public function setItemOrder($itemIDs) {
    update_field('field_5a0435cfd169f', [], $this->post->ID);   // Items

    if (!is_array($itemIDs) || empty($itemIDs)) {
      return false;
    }

    $reorderedItems = [];

    foreach ($itemIDs as $itemID) {
      $post = get_post($itemID);

      if ($post) {
        switch ($post->post_type) {
          case VisitEurope_CPT_Destination::POST_TYPE:
            $reorderedItems[] = [
              'type' => 'Destination',
              'destination' => $itemID,
            ];
            break;
          case VisitEurope_CPT_Experience::POST_TYPE:
            $reorderedItems[] = [
              'type' => 'Experience',
              'experience' => $itemID,
            ];
            break;
          default:
            break;
        }
      }
    }

    update_field('field_5a0435cfd169f', $reorderedItems, $this->post->ID);  // Items

    return true;
  }

  /**
   * Returns a formatted item for the front-end
   *
   * @param integer $itemID
   * @param string $itemType
   * @return array
   */
  public static function getPresentedItem($itemID, $itemType) {
    $item = null;

    if (is_null($itemID) || empty($itemID) || $itemID == 0 || !is_string($itemType) || empty($itemType)) {
      return $item;
    }

    // Get the possible types
    $destinationTypes = array_keys(VisitEurope_Destination::getTypes());
    $experienceTypes = array_keys(VisitEurope_Experience::getTypes());

    // Destination
    if (in_array($itemType, $destinationTypes)) {
      $models = VisitEurope_Destination::getWhereType($itemType, [
        'post__in' => [$itemID],
      ]);

      if (is_array($models) && !empty($models) && isset($models[0]) && isset($models[0]->ID)) {
        $vd = new VisitEurope_Destination($models[0]);
        $item = $vd->presentPost();
      }

    // Experience
    } else if (in_array($itemType, $experienceTypes)) {
      $models = VisitEurope_Experience::getWhereType($itemType, [
        'post__in' => [$itemID],
      ]);

      if (is_array($models) && !empty($models) && isset($models[0]) && isset($models[0]->ID)) {
        $vd = new VisitEurope_Experience($models[0]);
        $item = $vd->presentPost();
      }

    // Trips
  } else if ($itemType == VisitEurope_Trip::TYPE_ALL) {
      $models = VisitEurope_Trip::get([
        'post__in' => [$itemID]
      ]);

      if (is_array($models) && !empty($models) && isset($models[0]) && isset($models[0]->ID)) {
        $vd = new VisitEurope_trip($models[0]);
        $item = $vd->presentPost();
      }
    }

    // We should have an item by now
    if (is_array($item)) {
      return $item;
    }

    return null;
  }

  /**
   * Determines whether or not this post is owned by the given user
   *
   * @param integer $userID
   * @return boolean
   */
  public function isOwnedBy($userID) {
    if (!isset($this->post->ID)) {
      return false;
    }

    $user = get_field('user', $this->post->ID, true);

    if (!is_array($user) || !isset($user['ID'])) {
      return false;
    }

    return intval($user['ID']) === intval($userID);
  }

}
