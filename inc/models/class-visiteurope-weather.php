<?php
if (!defined('ABSPATH')) exit;

/**
 * Weather model
 *
 * @package Visit_Europe
 */
class VisitEurope_Weather {

  const SEASON_AUTUMN = "AUTUMN";
  const SEASON_SPRING = "SPRING";
  const SEASON_SUMMER = "SUMMER";
  const SEASON_WINTER = "WINTER";

  const TEMP_COLD = "COLD";
  const TEMP_MILD = "MILD";
  const TEMP_WARM = "WARM";
  const TEMP_HOT = "HOT";

  /**
   * Returns an array of Weather seasons
   *
   * @return array
   */
  public static function getSeasons() {
    return [
      self::SEASON_SUMMER => VisitEurope_Content::getLocale('seasons.summer'),
      self::SEASON_AUTUMN => VisitEurope_Content::getLocale('seasons.autumn'),
      self::SEASON_WINTER => VisitEurope_Content::getLocale('seasons.winter'),
      self::SEASON_SPRING => VisitEurope_Content::getLocale('seasons.spring'),
    ];
  }

  /**
   * Returns an array of Weather temperatures
   *
   * @return array
   */
  public static function getTemperatures() {
    return [
      self::TEMP_HOT => VisitEurope_Content::getLocale('temperatures.hot'),
      self::TEMP_WARM => VisitEurope_Content::getLocale('temperatures.warm'),
      self::TEMP_MILD => VisitEurope_Content::getLocale('temperatures.mild'),
      self::TEMP_COLD => VisitEurope_Content::getLocale('temperatures.cold'),
    ];
  }

  /**
   * Get temperature min value for temperature
   *
   * @param string $temperature
   * @return integer
   */
  public static function getMinFromTemperatureForQuery($temperature) {
    $min = 10;

    switch ($temperature) {
      case self::TEMP_COLD:
        $min = -50;
        break;
      case self::TEMP_MILD:
        $min = 10;
        break;
      case self::TEMP_WARM:
        $min = 20;
        break;
      case self::TEMP_HOT:
        $min = 25;
        break;
    }

    return $min;
  }

  /**
   * Get temperature max value for temperature
   *
   * @param string $temperature
   * @return integer
   */
  public static function getMaxFromTemperatureForQuery($temperature) {
    $max = 25;

    switch ($temperature) {
      case self::TEMP_COLD:
        $max = 10;
        break;
      case self::TEMP_MILD:
        $max = 20;
        break;
      case self::TEMP_WARM:
        $max = 25;
        break;
      case self::TEMP_HOT:
        $max = 99;
        break;
    }

    return $max;
  }
}
