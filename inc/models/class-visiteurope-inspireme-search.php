<?php
if (!defined('ABSPATH')) exit;

/**
 * InspireMe Search model
 *
 * @package Visit_Europe
 */
class VisitEurope_InspireMe_Search {

  private $audiences;
  private $budgets;
  private $interests;
  private $season;
  private $temperature;

  public $selectedAudience;
  public $selectedBudget;
  public $selectedDesires;
  public $selectedSeason;
  public $selectedTemperature;

  public function __construct() {
    $this->audiences = null;
    $this->budgets = null;
    $this->interests = [];
    $this->season = null;
    $this->temperature = null;

    $this->selectedAudience = null;
    $this->selectedBudget = null;
    $this->selectedDesires = [];
    $this->selectedSeason = null;
    $this->selectedTemperature = null;
  }

  /**
   * Populates the class with valid data from the given array
   *
   * @param array $data
   * @return void
   */
  public function fill($data = []) {
    if (!is_array($data)) {
      return;
    }

    $im = new VisitEurope_InspireMe();
    $audiences = $im->getAudiences();
    $budgets = $im->getBudgets();
    $seasons = $im->getSeasons();
    $temperatures = $im->getTemperatures();

    // Audience
    if (is_array($audiences) && isset($data['audience']) && isset($audiences[$data['audience']])
      && isset($audiences[$data['audience']]['audiences'])) {
      $this->selectedAudience = $data['audience'];
      $this->audiences = $audiences[$this->selectedAudience]['audiences'];
    }

    // Budgets
    if (is_array($budgets) && isset($data['budget']) && isset($budgets[$data['budget']])
      && isset($budgets[$data['budget']]['budgets'])) {
      $this->selectedBudget = $data['budget'];
      $this->budgets = $budgets[$this->selectedBudget]['budgets'];
    }

    // Desires
    if (isset($data['desires'])) {
      if (is_array($data['desires'])) {
        $desires = array_map('intval', $data['desires']);
      } else {
        $desires = array_map('intval', explode(',', $data['desires']));
      }

      $this->selectedDesires = $desires;
      $this->interests = $this->getInterestsForDesires($desires);
    }

    // Seasons
    if (is_array($seasons) && isset($data['season']) && isset($seasons[$data['season']])
      && isset($seasons[$data['season']]['key'])) {
      $this->selectedSeason = $data['season'];
      $this->season = $seasons[$this->selectedSeason]['key'];
    }

    // Temperature
    if (is_array($temperatures) && isset($data['temperature']) && isset($temperatures[$data['temperature']])
      && isset($temperatures[$data['season']]['key'])) {
      $this->selectedTemperature = $data['temperature'];
      $this->temperature = $temperatures[$this->selectedTemperature]['key'];
    }
  }

  /**
   * Get the destinations that match the given search. Destinations take into consideration:
   *   - ACF Recommended Seasons
   *   - Weather postmeta
   *
   * @return array
   */
  public function getDestinations() {
    $items = [];
    $season = ucfirst(strtolower($this->season));
    $temperatureLow = intval(VisitEurope_Weather::getMinFromTemperatureForQuery($this->temperature));
    $temperatureHigh = intval(VisitEurope_Weather::getMaxFromTemperatureForQuery($this->temperature));
    $temperatureAverage = -99;

    if ($temperatureLow != 0 && $temperatureHigh != 0) {
      $temperatureAverage = ($temperatureLow + $temperatureHigh) / 2;
    }

    $temperatureKeyPrefix = VisitEurope_CPT_Destination::POST_TYPE . VisitEurope_Content::KEY_SEPARATOR . VisitEurope_Destination::WEATHER . VisitEurope_Content::KEY_SEPARATOR . $this->season . VisitEurope_Content::KEY_SEPARATOR;
    $temperatureLowKey = $temperatureKeyPrefix . VisitEurope_Destination::WEATHER_LOW;
    $temperatureHighKey = $temperatureKeyPrefix . VisitEurope_Destination::WEATHER_HIGH;

    $destinations = VisitEurope_Destination::get([
      'meta_query' => [
        'relation' => 'AND',
        [
          'key' => 'recommended_seasons',
          'value' => '"' . $season . '"',
          'compare' => 'LIKE',
        ],
        [
          'key' => $temperatureLowKey,
          'value' => $temperatureAverage,
          'compare' => '<=',
        ],
        [
          'key' => $temperatureHighKey,
          'value' => $temperatureAverage,
          'compare' => '>=',
        ]
      ],
    ]);

    if (is_array($destinations) && !empty($destinations)) {
      foreach ($destinations as $destination) {
        $vd = new VisitEurope_Destination($destination);
        $items[] = $vd->presentPost();
      }
    }

    return $items;
  }

  /**
   * Get the trips that match the given search. Trips take into consideration:
   *   - Audience taxonomy
   *   - ACF Recommended Season
   *   - ACF Recommended Temperature
   *   - Budget taxonomy
   *   - Interests / Interest Groups
   *
   * @return array
   */
  public function getTrips() {
    $items = [];
    $audiences = $this->audiences;
    $budgets = $this->budgets;
    $interests = $this->interests;
    $season = ucfirst(strtolower($this->season));
    $temperature = ucfirst(strtolower($this->temperature));

    $metaQuery = [
      'relation' => 'AND',
      [
        'key' => 'recommended_seasons',
        'value' => '"' . $season . '"',
        'compare' => 'LIKE',
      ],
      [
        'key' => 'recommended_temperature',
        'value' => '"' . $temperature . '"',
        'compare' => 'LIKE',
      ]
    ];

    $taxQuery = [
      'relation' => 'AND',
      [
        'taxonomy' => VisitEurope_Taxonomy_Audience::TAXONOMY,
        'field' => 'term_id',
        'terms' => $audiences,
      ],
      [
        'taxonomy' => VisitEurope_Taxonomy_Budget::TAXONOMY,
        'field' => 'term_id',
        'terms' => $budgets,
      ],
      [
        'taxonomy' => VisitEurope_Taxonomy_Interest::TAXONOMY,
        'field' => 'term_id',
        'terms' => $interests,
      ]
    ];

    $trips = VisitEurope_Trip::get([
      'meta_query' => $metaQuery,
      'tax_query' => $taxQuery,
    ]);

    if (is_array($trips) && !empty($trips)) {
      foreach ($trips as $trip) {
        $vt = new VisitEurope_Trip($trip);
        $items[] = $vt->presentPost();
      }
    }

    return $items;
  }

  /**
   * Returns the interests for the desires
   *
   * @param array $desires
   * @return array
   */
  private function getInterestsForDesires($desires) {
    $interests = [];

    if (is_array($desires) && !empty($desires)) {
      foreach ($desires as $index) {
        $desireInterests = $this->getInterestsForDesireAtIndex($index);
        $interests = array_merge($interests, $desireInterests);
      }
    }

    return $interests;
  }

  /**
   * Get Interests for Desire
   *
   * @param integer $index
   * @return array
   */
  private function getInterestsForDesireAtIndex($index) {
    $interests = [];
    $im = new VisitEurope_InspireMe();
    $interestGroups = VisitEurope_Settings::getInterestGroups(0);
    $desires = $im->getDesires();

    if (is_array($interestGroups) && !empty($interestGroups) && is_array($desires)
      && isset($desires[$index]) && is_array($desires[$index]) && isset($desires[$index]['key'])) {
      foreach ($interestGroups as $interestGroup) {
        if (is_array($interestGroup) && isset($interestGroup['title']) && isset($interestGroup['filters'])) {
          if ($interestGroup['title'] == $desires[$index]['key']) {
            $interests = $interestGroup['filters'];
          }
        }
      }
    }

    return $interests;
  }

}
