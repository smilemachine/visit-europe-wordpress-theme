<?php
if (!defined('ABSPATH')) exit;

/**
 * Interest model
 *
 * @package Visit_Europe
 */
class VisitEurope_InspireMe {

  public function __construct() {
    //
  }

  /**
   * Set the audiences
   *
   * @return void
   */
  private function setAudiences() {

  }

  /**
   * Returns the audiences
   *
   * @return array
   */
  public function getAudiences() {
    $audiences = [];
    $items = get_field('inspire_me_audiences', 'option');

    if (is_array($items) && !empty($items)) {
      foreach ($items as $item) {
        $audienceIDs = [];

        if (is_array($item['audiences']) && !empty($item['audiences'])) {
          $audienceIDs = $item['audiences'];
        }

        $audiences[] = [
          'title' => $item['title'],
          'audiences' => $audienceIDs,
          'imageinitial' => $item['image']['sizes']['initial'],
          'imagefull' => $item['image']['sizes']['thumbnail'],
        ];
      }
    }

    return $audiences;
  }

  /**
   * Returns the budgets
   *
   * @return array
   */
  public function getBudgets() {
    $budgets = [];
    $items = get_field('inspire_me_budgets', 'option');

    if (is_array($items) && !empty($items)) {
      foreach ($items as $item) {
        $budgetIDs = [];

        if (is_array($item['budgets']) && !empty($item['budgets'])) {
          $budgetIDs = $item['budgets'];
        }

        $budgets[] = [
          'title' => $item['title'],
          'budgets' => $budgetIDs,
          'imageinitial' => $item['image']['sizes']['initial'],
          'imagefull' => $item['image']['sizes']['thumbnail'],
        ];
      }
    }

    return $budgets;
  }

  /**
   * Returns the desires
   *
   * @return array
   */
  public function getDesires() {
    $desires = [];
    $items = get_field('inspire_me_desires', 'option');
    $interestGroups = VisitEurope_Settings::getInterestGroups(0, true);

    if (is_array($items) && !empty($items)) {
      foreach ($items as $item) {
        $title = $item['inspire_me_interest_group'];
        $key = $item['inspire_me_interest_group'];

        if (is_array($interestGroups) && isset($interestGroups[$title])) {
          $title = $interestGroups[$title];
        }

        $desires[] = [
          'title' => $title,
          'key' => $key,
          'imageinitial' => $item['image']['sizes']['initial'],
          'imagefull' => $item['image']['sizes']['thumbnail'],
          'weather' => [
            VisitEurope_Weather::SEASON_SUMMER => is_array($item['summer']) ? $item['summer'] : [],
            VisitEurope_Weather::SEASON_AUTUMN => is_array($item['autumn']) ? $item['autumn'] : [],
            VisitEurope_Weather::SEASON_WINTER => is_array($item['winter']) ? $item['winter'] : [],
            VisitEurope_Weather::SEASON_SPRING => is_array($item['spring']) ? $item['spring'] : [],
          ],
        ];
      }
    }

    return $desires;
  }

  /**
   * Returns the seasons
   *
   * @return array
   */
  public function getSeasons() {
    $seasons = [];
    $items = VisitEurope_Weather::getSeasons();

    if (is_array($items) && !empty($items)) {
      foreach ($items as $key => $value) {
        $image = get_field('season_' . strtolower($key), 'option');

        if (is_array($image) && !empty($image)) {
          $seasons[] = [
            'title' => $value,
            'key' => $key,
            'imageinitial' => $image['sizes']['initial'],
            'imagefull' => $image['sizes']['thumbnail'],
          ];
        }
      }
    }

    return $seasons;
  }

  /**
   * Returns the temperatures
   *
   * @return array
   */
  public function getTemperatures() {
    $temperatures = [];
    $items = VisitEurope_Weather::getTemperatures();

    if (is_array($items) && !empty($items)) {
      foreach ($items as $key => $value) {
        $image = get_field('temperature_' . strtolower($key), 'option');

        if (is_array($image) && !empty($image)) {
          $temperatures[] = [
            'title' => $value,
            'key' => $key,
            'imageinitial' => $image['sizes']['initial'],
            'imagefull' => $image['sizes']['thumbnail'],
          ];
        }
      }
    }

    return $temperatures;
  }

  /**
   * Get the background image
   *
   * @param string $size
   * @return string
   */
  private function getBackgroundImage($size = '') {
    $image = null;
    $backgroundImage = get_field('inspire_me_background_image', 'option');

    if (is_array($backgroundImage) && !empty($backgroundImage)) {
      $image = $backgroundImage['sizes'][$size];
    }

    return $image;
  }

  /**
   * Get the background image
   *
   * @return string
   */
  public function getBackgroundImageInitial() {
    $image = '';
    $backgroundImage = $this->getBackgroundImage('initial');

    if (!empty($backgroundImage)) {
      $image = $backgroundImage;
    }

    return $image;
  }

  /**
   * Get the background image
   *
   * @return string
   */
  public function getBackgroundImageFull() {
    $image = '';
    $backgroundImage = $this->getBackgroundImage('large');

    if (!empty($backgroundImage)) {
      $image = $backgroundImage;
    }

    return $image;
  }

}
