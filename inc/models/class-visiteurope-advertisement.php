<?php
if (!defined('ABSPATH')) exit;

/**
 * Advertisement model
 *
 * @package Visit_Europe
 */
class VisitEurope_Advertisement {

  const TYPE_FEATURE = 'FEATURE';

  private $post;

  /**
   * Setup
   *
   * @param WP_Post
   * @return void
   */
  public function __construct($post) {
    $this->post = $post;
  }

  /**
   * Returns an array of Experiences
   *
   * @param array $customParams
   * @param boolean $returnAsList
   * @return array
   */
  public static function get($customParams = [], $returnAsList = false) {
    $defaultParams = [
      'post_status' => 'publish',
      'post_type' => VisitEurope_CPT_Advertisement::POST_TYPE,
      'orderby' => 'rand',
    ];

    // Apply the cache if necessary
    $uniqueCacheData = wp_cache_get(VisitEurope_Content::CACHE_ADVERTISEMENT);

    if (is_array($uniqueCacheData) && !empty($uniqueCacheData)
      && !isset($customParams['post__not_in'])) {
      $defaultParams['post__not_in'] = $uniqueCacheData;
    }

    $params = VisitEurope_Helper::paramsForQuery($defaultParams, $customParams);

    // Show the current language if not already set
    if (!isset($params['surpress_filters'])) {
      $params['suppress_filters'] = false;
    }

    $posts = get_posts($params);
    $postsList = VisitEurope_Helper::postsToList($posts);

    // Add to cache if necessary
    if (is_array($uniqueCacheData)) {
      $postIDs = array_keys($postsList);
      wp_cache_set(VisitEurope_Content::CACHE_ADVERTISEMENT, array_merge($uniqueCacheData, $postIDs), null);
    }

    if ($returnAsList !== true) {
      return $posts;
    }

    return $postsList;
  }

  /**
   * Returns an array of Advertisements in an array of IDs
   *
   * @param array $rawIDs
   * @param array $customParams
   * @param boolean $returnAsList
   * @return array
   */
  public static function getWhereIn($rawIDs, $customParams = [], $returnAsList = false) {
    $postIDs = VisitEurope_Helper::validIDsFromArray($rawIDs);

    if (empty($postIDs)) {
      return [];
    }

    $defaultParams = [
      'post__in' => $postIDs
    ];
    $params = VisitEurope_Helper::paramsForQuery($defaultParams, $customParams);

    return self::get($params, $returnAsList);
  }

  /**
   * Returns an array of Experiences by type
   *
   * @param string $type
   * @param array $customParams
   * @param boolean $returnAsList
   * @return array
   */
  public static function getWhereType($type, $customParams = [], $returnAsList = false) {
    $defaultParams = [
      'meta_key' => 'type',
      'meta_value' => $type,
    ];
    $params = VisitEurope_Helper::paramsForQuery($defaultParams, $customParams);

    return self::get($params, $returnAsList);
  }

  /**
   * Returns an array of Experiences by type for a given Post ID
   *
   * @param integer $postID
   * @param string $type
   * @param array $customParams
   * @param boolean $returnAsList
   * @return array
   */
  public static function getWhereTypeForPostID($postID, $type, $customParams = [], $returnAsList = false) {
    $key = VisitEurope_CPT_Experience::POST_TYPE . VisitEurope_Content::KEY_SEPARATOR . $type;
    $array = get_post_meta($postID, $key, false);
    $postIDs = VisitEurope_Helper::validIDsFromArray($array);

    if (empty($postIDs)) {
      return [];
    }

    $defaultParams = [
      'post__in' => $postIDs,
    ];
    $params = VisitEurope_Helper::paramsForQuery($defaultParams, $customParams);

    return self::getWhereType($type, $params, $returnAsList);
  }

  /**
   * Returns an array of Feature Advertisements
   *
   * @param array $customParams
   * @param boolean $returnAsList
   * @return array
   */
  public static function getFeature($customParams = [], $returnAsList = false) {
    $type = ucfirst(strtolower(self::TYPE_FEATURE));

    return self::getWhereType($type, $customParams, $returnAsList);
  }

  /**
   * Returns the type of Experience for a given Post ID
   *
   * @param integer $postID
   * @return string
   */
  public static function getTypeForPostID($postID) {
    return strtoupper(get_field('type', $postID));
  }

  /**
   * Presents the given post into a format for the frontend
   *
   * @return array
   */
  public function presentPost() {
    $data = null;
    $type = self::getTypeForPostID($this->post->ID);

    switch ($type) {
      case self::TYPE_FEATURE:
        $data = $this->presentFeature();
        break;
      default:
        break;
    }

    return $data;
  }

  /**
   * Present an article item
   *
   * @return array
   */
  private function presentFeature() {
    return [
      'id' => intval($this->post->ID),
      'body' => get_field('body', $this->post->ID),
      'imageinitial' => VisitEurope_Helper::getPostThumbnailUrl($this->post->ID, 'initial'),
      'imagefull' => VisitEurope_Helper::getPostThumbnailUrl($this->post->ID, 'medium'),
      'title' => VisitEurope_Helper::formatStringForFrontend($this->post->post_title),
      'type' => self::getTypeForPostID($this->post->ID),
      'button' => get_field('button', $this->post->ID),
      'url' => get_field('url', $this->post->ID),
    ];
  }

}
