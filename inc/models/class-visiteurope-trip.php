<?php
if (!defined('ABSPATH')) exit;

/**
 * Destination model
 *
 * @package Visit_Europe
 */
class VisitEurope_Trip {

  const TYPE_ALL = 'TRIP';

  private $post;

  /**
   * Setup
   *
   * @param WP_Post
   * @return void
   */
  public function __construct($post) {
    $this->post = $post;
  }

  /**
   * Returns an array of Experiences
   *
   * @param array $customParams
   * @param boolean $returnAsList
   * @return array
   */
  public static function get($customParams = [], $returnAsList = false) {
    $defaultParams = [
      'post_status' => 'published',
      'post_type' => VisitEurope_CPT_Trip::POST_TYPE,
      'orderby' => 'rand',
    ];

    // Apply the cache if necessary
    $uniqueCacheData = wp_cache_get(VisitEurope_Content::CACHE_TRIP);

    if (is_array($uniqueCacheData) && !empty($uniqueCacheData)
      && !isset($customParams['post__not_in'])) {
      $defaultParams['post__not_in'] = $uniqueCacheData;
    }

    // If limit is set to 0, transform it to infinite
    if (isset($params['posts_per_page']) && intval($params['posts_per_page']) == 0) {
      $params['posts_per_page'] = -1;
    }

    $params = VisitEurope_Helper::paramsForQuery($defaultParams, $customParams);

    // Show the current language if not already set
    if (!isset($params['surpress_filters'])) {
      $params['suppress_filters'] = false;
    }

    $posts = get_posts($params);
    $postsList = VisitEurope_Helper::postsToList($posts);

    // Add to cache if necessary
    if (is_array($uniqueCacheData)) {
      $postIDs = array_keys($postsList);
      wp_cache_set(VisitEurope_Content::CACHE_TRIP, array_merge($uniqueCacheData, $postIDs), null);
    }

    if ($returnAsList !== true) {
      return $posts;
    }

    return $postsList;
  }

  /**
   * Presents the given post into a format for the frontend
   *
   * @return array
   */
  public function presentPost() {
    $filters = [];
    $terms = get_the_terms($this->post->ID, VisitEurope_Taxonomy_TripType::TAXONOMY);

    if (is_array($terms) && !empty($terms)) {
      foreach ($terms as $term) {
        $filters[] = $term->term_id;
      }
    }

    $overlays = [];
    $countrys = [];
    $destinationPosts = get_field('destinations', $this->post->ID);

    if (is_array($destinationPosts) && !empty($destinationPosts)) {
      foreach ($destinationPosts as $destinationPost) {
        $vd = new VisitEurope_Destination($destinationPost);
        $overlays = array_merge($overlays, $vd->presentPostOverlays());
      }
    }

    return [
      'id' => intval($this->post->ID),
      'body' => VisitEurope_Helper::formatStringForFrontend(get_field('blurb', $this->post->ID)),
      'filters' => $filters,
      'copyright' => VisitEurope_Helper::getPostThumbnailCopyright($this->post->ID),
      'copyrighturl' => VisitEurope_Helper::getPostThumbnailCopyrightUrl($this->post->ID),
      'imageinitial' => VisitEurope_Helper::getPostThumbnailUrl($this->post->ID, 'initial'),
      'imagefull' => VisitEurope_Helper::getPostThumbnailUrl($this->post->ID, 'medium'),
      'map' => [
        'icon' => get_template_directory_uri() . '/assets/img/google-map-marker.png',
        'location' => [
          'lat' => (float) get_field('map_lat', $this->post->ID),
          'lng' => (float) get_field('map_lng', $this->post->ID),
        ],
        'overlays' => $overlays,
      ],
      'save' => true,
      'title' => $this->post->post_title,
      'type' => 'TRIP',
      'url' => get_permalink($this->post->ID),
    ];
  }

}
