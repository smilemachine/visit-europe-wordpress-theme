<?php
if (!defined('ABSPATH')) exit;

require_once dirname(__FILE__) . '/class-visiteurope-experience-item.php';

/**
 * Experience model
 *
 * @package Visit_Europe
 */
class VisitEurope_Experience_Route extends VisitEurope_Experience_Item {

  /**
   * Sets the initial data
   *
   * @param WP_Post $post
   * @return void
   */
  public function __construct ($post) {
    $this->setPost($post);
    $this->setDestination(get_field('route_destinations', $this->post->ID));
  }

  /**
   * Gets the markers available for the map
   *
   * @return array
   */
  public function getMapMarkers() {
    $locations = get_field('route_locations', $this->post->ID);
    $markers = [];

    if (is_array($locations) && !empty($locations)) {
    	foreach ($locations as $key => $location) {
        $markerMap = [
          'location' => [
            'lat' => (float) $location['lat'],
            'lng' => (float) $location['lng'],
          ],
          'icon' => get_template_directory_uri() . '/assets/img/google-map-marker.png',
        ];

        $markerImageInitialUrl = '';
        $markerImageFullUrl = '';

        if (isset($location['image']) && is_array($location['image'])) {
          $markerImageInitialUrl = $location['image']['sizes']['initial'];
          $markerImageFullUrl = $location['image']['sizes']['thumbnail'];
        }

        $markerMetadata = [
          'item' => [
            'id' => intval($key),
            'body' => '',
            'filters' => [],
            'copyright' => VisitEurope_Helper::getPostThumbnailCopyright($this->post->ID),
            'copyrighturl' => VisitEurope_Helper::getPostThumbnailCopyrightUrl($this->post->ID),
            'imageinitial' => $markerImageInitialUrl,
            'imagefull' => $markerImageFullUrl,
            'map' => $markerMap,
            'save' => false,
            'title' => $location['title'],
            'type' => '',
            'url' => '#',
          ]
        ];

        $markers[] = array_merge($markerMap, ['metadata' => $markerMetadata]);
    	}
    }

    return $markers;
  }

  /**
   * Returns the location for the map
   *
   * @return array
   */
  public function getMapLocation() {
    $markers = $this->getMapMarkers();

    if (count($markers) >= 1) {
      return [
        'lat' => (float) $markers[0]['location']['lat'],
        'lng' => (float) $markers[0]['location']['lng'],
      ];
    }
  }

}
