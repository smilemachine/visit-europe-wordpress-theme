<?php
if (!defined('ABSPATH')) exit;

/**
 * Experience model
 *
 * @package Visit_Europe
 */
class VisitEurope_Experience {

  const TYPE_ALL = 'EXPERIENCE';
  const TYPE_ARTICLE = 'ARTICLE';
  const TYPE_EVENT = 'EVENT';
  const TYPE_EVENT_PRIVATE = 'EVENT_PRIVATE';
  const TYPE_GALLERY = 'GALLERY';
  const TYPE_PRODUCT = 'PRODUCT';
  const TYPE_RECIPE = 'RECIPE';
  const TYPE_ROUTE = 'ROUTE';

  private $post;

  /**
   * Setup
   *
   * @param WP_Post
   * @return void
   */
  public function __construct($post) {
    $this->post = $post;
  }

  /**
   * Returns an array of Experiences
   *
   * @param array $customParams
   * @param boolean $returnAsList
   * @return array
   */
  public static function get($customParams = [], $returnAsList = false) {
    $defaultParams = [
      'post_status' => 'publish',
      'post_type' => VisitEurope_CPT_Experience::POST_TYPE,
      'orderby' => 'rand',
    ];

    // Apply the cache if necessary
    $uniqueCacheData = wp_cache_get(VisitEurope_Content::CACHE_EXPERIENCE);

    if (is_array($uniqueCacheData) && !empty($uniqueCacheData)
      && !isset($customParams['post__not_in'])) {
      $defaultParams['post__not_in'] = $uniqueCacheData;
    }

    // If limit is set to 0, transform it to infinite
    if (isset($params['posts_per_page']) && intval($params['posts_per_page']) == 0) {
      $params['posts_per_page'] = -1;
    }

    $params = VisitEurope_Helper::paramsForQuery($defaultParams, $customParams);

    // Show the current language if not already set
    if (!isset($params['surpress_filters'])) {
      $params['suppress_filters'] = false;
    }

    $posts = get_posts($params);
    $postsList = VisitEurope_Helper::postsToList($posts);

    // Add to cache if necessary
    if (is_array($uniqueCacheData)) {
      $postIDs = array_keys($postsList);
      wp_cache_set(VisitEurope_Content::CACHE_EXPERIENCE, array_merge($uniqueCacheData, $postIDs), null);
    }

    if ($returnAsList !== true) {
      return $posts;
    }

    return $postsList;
  }

  /**
   * Returns an array of Experiences in an array of IDs
   *
   * @param array $rawIDs
   * @param array $customParams
   * @param boolean $returnAsList
   * @return array
   */
  public static function getWhereIn($rawIDs, $customParams = [], $returnAsList = false) {
    $postIDs = VisitEurope_Helper::validIDsFromArray($rawIDs);

    if (empty($postIDs)) {
      return [];
    }

    $defaultParams = [
      'post__in' => $postIDs
    ];
    $params = VisitEurope_Helper::paramsForQuery($defaultParams, $customParams);

    return self::get($params, $returnAsList);
  }

  /**
   * Returns an array of Experiences by type
   *
   * @param string $type
   * @param array $customParams
   * @param boolean $returnAsList
   * @return array
   */
  public static function getWhereType($type, $customParams = [], $returnAsList = false) {
    $defaultParams = [];

    if (isset($customParams['meta_query']) && is_array($customParams['meta_query'])) {
      $customParams['meta_query'][] = [
        'key' => 'type',
        'value' => $type,
      ];
    } else {
      $defaultParams['meta_key'] = 'type';
      $defaultParams['meta_value'] = $type;
    }

    $params = VisitEurope_Helper::paramsForQuery($defaultParams, $customParams);

    return self::get($params, $returnAsList);
  }

  /**
   * Returns an array of Experiences by type for a given Post ID
   *
   * @param integer $postID
   * @param string $type
   * @param array $customParams
   * @param boolean $returnAsList
   * @return array
   */
  public static function getWhereTypeForPostID($postID, $type, $customParams = [], $returnAsList = false) {
    $key = VisitEurope_CPT_Experience::POST_TYPE . VisitEurope_Content::KEY_SEPARATOR . $type;
    $array = get_post_meta($postID, $key, false);
    $postIDs = VisitEurope_Helper::validIDsFromArray($array);

    if (empty($postIDs)) {
      return [];
    }

    $defaultParams = [
      'post__in' => $postIDs,
    ];
    $params = VisitEurope_Helper::paramsForQuery($defaultParams, $customParams);

    return self::getWhereType($type, $params, $returnAsList);
  }

  /**
   * Returns an array of Article Experiences
   *
   * @param array $customParams
   * @param boolean $returnAsList
   * @return array
   */
  public static function getArticles($customParams = [], $returnAsList = false) {
    $type = ucfirst(strtolower(self::TYPE_ARTICLE));
    return self::getWhereType($type, $customParams, $returnAsList);
  }

  /**
   * Returns an array of Event Experiences
   *
   * @param array $customParams
   * @param boolean $returnAsList
   * @return array
   */
  public static function getEvents($customParams = [], $returnAsList = false) {
    $type = ucfirst(strtolower(self::TYPE_EVENT));
    return self::getWhereType($type, $customParams, $returnAsList);
  }

  /**
   * Returns an array of Gallery Experiences
   *
   * @param array $customParams
   * @param boolean $returnAsList
   * @return array
   */
  public static function getGalleries($customParams = [], $returnAsList = false) {
    $type = ucfirst(strtolower(self::TYPE_GALLERY));
    return self::getWhereType($type, $customParams, $returnAsList);
  }

  /**
   * Returns an array of Product Experiences
   *
   * @param array $customParams
   * @param boolean $returnAsList
   * @return array
   */
  public static function getProducts($customParams = [], $returnAsList = false) {
    $type = ucfirst(strtolower(self::TYPE_PRODUCT));
    return self::getWhereType($type, $customParams, $returnAsList);
  }

  /**
   * Returns an array of Recipe Experiences
   *
   * @param array $customParams
   * @param boolean $returnAsList
   * @return array
   */
  public static function getRecipes($customParams = [], $returnAsList = false) {
    $type = ucfirst(strtolower(self::TYPE_RECIPE));
    return self::getWhereType($type, $customParams, $returnAsList);
  }

  /**
   * Returns an array of Route Experiences
   *
   * @param array $customParams
   * @param boolean $returnAsList
   * @return array
   */
  public static function getRoutes($customParams = [], $returnAsList = false) {
    $type = ucfirst(strtolower(self::TYPE_ROUTE));
    return self::getWhereType($type, $customParams, $returnAsList);
  }

  /**
   * Returns an array of Destinations by type for a given Post ID
   *
   * @param integer $postID
   * @param string $type
   * @param array $customParams
   * @param boolean $returnAsList
   * @return array
   */
  private static function getDestinationsByTypeForPostID($postID, $type, $customParams = [], $returnAsList = false) {
    $key = VisitEurope_CPT_Experience::POST_TYPE . VisitEurope_Content::KEY_SEPARATOR . $type;
    $array = get_field('destinations', $postID, false);
    $postIDs = VisitEurope_Helper::validIDsFromArray($array);

    if (empty($postIDs)) {
      return [];
    }

    $customParams['meta_key'] = VisitEurope_CPT_Destination::POST_TYPE . '_TYPE';
    $customParams['meta_value'] = $type;

    return VisitEurope_Destination::getWhereIn($postIDs, $customParams, $returnAsList);
  }

  /**
   * Returns an array of Country Destinations for a given Post ID
   *
   * @param integer $postID
   * @param array $customParams
   * @param boolean $returnAsList
   * @return array
   */
  public static function getCountriesForPostID($postID, $customParams = [], $returnAsList = false) {
    return self::getDestinationsByTypeForPostID($postID, VisitEurope_Destination::TYPE_COUNTRY, $customParams, $returnAsList);
  }

  /**
   * Returns an array of Region Destinations for a given Post ID
   *
   * @param integer $postID
   * @param array $customParams
   * @param boolean $returnAsList
   * @return array
   */
  public static function getRegionsForPostID($postID, $customParams, $returnAsList = false) {
    return self::getDestinationsByTypeForPostID($postID, VisitEurope_Destination::TYPE_REGION, $customParams, $returnAsList);
  }

  /**
   * Returns an array of Experience types
   *
   * @return array
   */
  public static function getTypes() {
    return [
      self::TYPE_ARTICLE => VisitEurope_Content::getLocale('experiences.types.article'),
      self::TYPE_EVENT => VisitEurope_Content::getLocale('experiences.types.event'),
      self::TYPE_GALLERY => VisitEurope_Content::getLocale('experiences.types.gallery'),
      self::TYPE_PRODUCT => VisitEurope_Content::getLocale('experiences.types.product'),
      self::TYPE_RECIPE => VisitEurope_Content::getLocale('experiences.types.recipe'),
      self::TYPE_ROUTE => VisitEurope_Content::getLocale('experiences.types.route'),
    ];
  }

  /**
   * Returns the type of Experience for a given Post ID
   *
   * @param integer $postID
   * @return string
   */
  public static function getTypeForPostID($postID) {
    return strtoupper(get_field('type', $postID));
  }

  /**
   * Presents the given post into a format for the frontend
   *
   * @return array
   */
  public function presentPost() {
    $data = null;
    $type = self::getTypeForPostID($this->post->ID);

    switch ($type) {
      case self::TYPE_ARTICLE:
        $data = $this->presentArticle();
        break;
      case self::TYPE_EVENT:
        $data = $this->presentEvent();
        break;
      case str_replace('_', ' ', self::TYPE_EVENT_PRIVATE):
        $data = $this->presentEvent();
        break;
      case self::TYPE_GALLERY:
        $data = $this->presentGallery();
        break;
      case self::TYPE_PRODUCT:
        $data = $this->presentProduct();
        break;
      case self::TYPE_RECIPE:
        $data = $this->presentRecipe();
        break;
      case self::TYPE_ROUTE:
        $data = $this->presentRoute();
        break;
      default:
        break;
    }

    return $data;
  }

  /**
   * Presents the given post into a format for the frontend
   *
   * @return array
   */
  public function presentPostPreview() {
    return $this->presentExperiencePreview();
  }

  /**
   * Present a default experience item
   *
   * @return array
   */
  private function presentExperience() {
    $filters = [];
    $terms = get_the_terms($this->post->ID, VisitEurope_Taxonomy_Interest::TAXONOMY);

    if (is_array($terms) && !empty($terms)) {
      foreach ($terms as $term) {
        $filters[] = $term->term_id;
      }
    }

    return [
      'id' => intval($this->post->ID),
      'body' => VisitEurope_Helper::formatStringForFrontend(get_field('blurb', $this->post->ID)),
      'filters' => $filters,
      'copyright' => VisitEurope_Helper::getPostThumbnailCopyright($this->post->ID),
      'copyrighturl' => VisitEurope_Helper::getPostThumbnailCopyrightUrl($this->post->ID),
      'imageinitial' => VisitEurope_Helper::getPostThumbnailUrl($this->post->ID, 'initial'),
      'imagefull' => VisitEurope_Helper::getPostThumbnailUrl($this->post->ID, 'medium'),
      'save' => true,
      'title' => VisitEurope_Helper::formatStringForFrontend($this->post->post_title),
      'type' => self::getTypeForPostID($this->post->ID),
      'url' => get_permalink($this->post->ID),
    ];
  }

  /**
   * Present a default experience item for preview
   *
   * @return array
   */
  private function presentExperiencePreview() {
    return [
      'id' => intval($this->post->ID),
      'body' => $this->presentExperiencePreviewContent(),
      'copyright' => VisitEurope_Helper::getPostThumbnailCopyright($this->post->ID),
      'copyrighturl' => VisitEurope_Helper::getPostThumbnailCopyrightUrl($this->post->ID),
      'imageinitial' => VisitEurope_Helper::getPostThumbnailUrl($this->post->ID, 'initial'),
      'imagefull' => VisitEurope_Helper::getPostThumbnailUrl($this->post->ID, 'medium'),
      'title' => VisitEurope_Helper::formatStringForFrontend($this->post->post_title),
      'type' => self::getTypeForPostID($this->post->ID),
      'url' => get_permalink($this->post->ID),
      'date' => VisitEurope_Helper::getLocalisedPublishedDateString($this->post->post_date),
      'pointers' => $this->presentExperiencePreviewPointers()
    ];
  }

  /**
   * Checks whether or not this post has a single destination attached
   *
   * @return boolean
   */
  private function hasSingleDestination() {
    $type = self::getTypeForPostID($this->post->ID);
    $destination = null;
    $destinations = [];

    switch ($type) {
      case self::TYPE_ARTICLE:
        $destinations = get_field('article_destinations', $this->post->ID);
        break;
      case self::TYPE_EVENT:
        $destinations = get_field('event_destinations', $this->post->ID);
        break;
      case self::TYPE_GALLERY:
        $destinations = get_field('gallery_destinations', $this->post->ID);
        break;
      case self::TYPE_PRODUCT:
        $destinations = get_field('product_destinations', $this->post->ID);
        break;
      case self::TYPE_RECIPE:
        $destinations = get_field('recipe_destinations', $this->post->ID);
        break;
      case self::TYPE_ROUTE:
        $destinations = get_field('route_destinations', $this->post->ID);
        break;
      default:
        break;
    }

    if (is_array($destinations) && count($destinations) == 1) {
      $destination = $destinations[0];
    }

    return $destination;
  }

  /**
   * Checks whether or not this post has a single destination attached
   *
   * @param WP_Post $destination
   * @return boolean
   */
  private function presentExperienceDestination($destination) {
    return [
      'icon' => get_template_directory_uri() . '/assets/img/google-map-marker.png',
      'location' => [
        'lat' => (float) get_field('map_lat', $destination->ID),
        'lng' => (float) get_field('map_lng', $destination->ID),
      ],
    ];
  }

  /**
   * Presents the content for an experience preview
   *
   * @return boolean
   */
  private function presentExperiencePreviewContent() {
    $content = get_field('experience_content', $this->post->ID);
    $body = [];

    if (!is_array($content) || empty($content)) {
      return [];
    }

    foreach ($content as $item) {
      if ($item['acf_fc_layout'] == 'content') {
        $body[] = VisitEurope_Helper::formatStringForFrontend(strip_tags($item['body']));
      }
    }

    return implode('<br/>', $body);
  }

  /**
   * Presents the pointers for an experience preview
   *
   * @return boolean
   */
  private function presentExperiencePreviewPointers() {
    while (have_rows('pointers', $this->post->ID)) {
      the_row();
      $type = VisitEurope_ACF_FC::getPointerTypeFromRawType(get_sub_field('type', $this->post->ID));
      $title = VisitEurope_ACF_FC::getPointerTitleFromType($type);
      $subtitle = get_sub_field('subtitle', $this->post->ID);
      $url = get_sub_field('url', $this->post->ID);
      $pointers[] = [
        'type' => $type,
        'title' => VisitEurope_Helper::formatStringForFrontend($title),
        'subtitle' => VisitEurope_Helper::formatStringForFrontend($subtitle),
        'url' => $url ? $url : '#',
      ];
    }

    return $pointers;
  }


  /**
   * Present an article item
   *
   * @return array
   */
  private function presentArticle() {
    $data = $this->presentExperience();

    if ($destination = $this->hasSingleDestination()) {
      $data['map'] = $this->presentExperienceDestination($destination);
    }

    return $data;
  }

  /**
   * Present an event item
   *
   * @return array
   */
  private function presentEvent() {
    $data = $this->presentExperience();
    $data['map'] = [
      'icon' => get_template_directory_uri() . '/assets/img/google-map-marker.png',
      'location' => [
        'lat' => (float) get_field('event_map_lat', $this->post->ID),
        'lng' => (float) get_field('event_map_lng', $this->post->ID),
      ],
      'overlays' => [],
    ];

    $date = date('Y');

    if ($startDate = get_field('start_date', $this->post->ID)) {
      $date = date('F Y', strtotime($startDate));
    }

    $data['date'] = $date;

    return $data;
  }

  /**
   * Present a product item
   *
   * @return array
   */
  private function presentProduct() {
    $data = $this->presentExperience();

    if ($destination = $this->hasSingleDestination()) {
      $data['map'] = $this->presentExperienceDestination($destination);
    }

    return $data;
  }

  /**
   * Present a recipe item
   *
   * @return array
   */
  private function presentRecipe() {
    $data = $this->presentExperience();

    if ($destination = $this->hasSingleDestination()) {
      $data['map'] = $this->presentExperienceDestination($destination);
    }

    return $data;
  }

  /**
   * Present a route item
   *
   * @return array
   */
  private function presentRoute() {
    $data = $this->presentExperience();

    if ($destination = $this->hasSingleDestination()) {
      $data['map'] = $this->presentExperienceDestination($destination);
    }

    return $data;
  }

  /**
   * Present a gallery item
   *
   * @return array
   */
  private function presentGallery() {
    $items = [];
    $content = get_field('experience_content', $this->post->ID);

    if (is_array($content) && !empty($content)) {
      foreach ($content as $item) {
        if ($item['acf_fc_layout'] == 'carousel') {
          $items = VisitEurope_ACF_FC::presentExperienceCarousel($item['slides']);
          break;
        }
      }
    }

    if ($destination = $this->hasSingleDestination()) {
      $data['map'] = $this->presentExperienceDestination($destination);
    }

    $data = $this->presentExperience();
    $data['items'] = $items;

    return $data;
  }

  /**
   * Returns the meta query necessary for destinations
   *
   * @param integer $postID
   * @return array
   */
  public static function getMetaQueryForDestinationID($postID) {
    $postType = VisitEurope_Destination::getTypeForPostID($postID);
    $destinationIDs = [];
    $metaQuery = ['relation' => 'OR'];

    if (empty($postID) || intval($postID) == 0) {
      return [];
    }

    // Get posts from this region, and all countries
    if ($postType == VisitEurope_Destination::TYPE_REGION) {
      $params = ['posts_per_page' => -1];
      $destinations = VisitEurope_Destination::getCountriesForPostID($postID, $params, true);
      $destinationIDs = array_keys($destinations);

    // Get posts from this country
    } else {
      $destinationIDs[] = $postID;
    }

    foreach ($destinationIDs as $destinationID) {
      $englishDestinationID = icl_object_id($postID, VisitEurope_CPT_Experience::POST_TYPE, false, 'en');

      // Article
      $metaQuery[] = self::getMetaQueryItemForPostID('article_destinations', $destinationID);
      $metaQuery[] = self::getMetaQueryItemForPostID('event_destinations', $destinationID);
      $metaQuery[] = self::getMetaQueryItemForPostID('gallery_destinations', $destinationID);
      $metaQuery[] = self::getMetaQueryItemForPostID('product_destinations', $destinationID);
      $metaQuery[] = self::getMetaQueryItemForPostID('route_destinations', $destinationID);

      if (intval($destinationID) != intval($englishDestinationID)) {
        $metaQuery[] = self::getMetaQueryItemForPostID('article_destinations', $englishDestinationID);
        $metaQuery[] = self::getMetaQueryItemForPostID('article_destinations', $englishDestinationID);
        $metaQuery[] = self::getMetaQueryItemForPostID('event_destinations', $englishDestinationID);
        $metaQuery[] = self::getMetaQueryItemForPostID('gallery_destinations', $englishDestinationID);
        $metaQuery[] = self::getMetaQueryItemForPostID('product_destinations', $englishDestinationID);
        $metaQuery[] = self::getMetaQueryItemForPostID('route_destinations', $englishDestinationID);
      }
    }

    return $metaQuery;
  }

  /**
   * Returns a meta query array item
   *
   * @param string $key
   * @param integer $destinationID
   * @return array
   */
  private static function getMetaQueryItemForPostID($key, $destinationID) {
    return [
      'key' => $key,
      'value' => '"' . $destinationID . '"',
      'compare' => 'LIKE',
    ];
  }

  /**
   * Returns the meta query for event restriction
   *
   * @param integer $postID
   * @return array
   */
  public static function getMetaQueryForEventDateRestriction($postID) {
    $restrictTo = get_sub_field('events_restrict_to', $postID);

    if ($restrictTo == "Upcoming") {
      return [
        'key' => 'start_date',
        'value' => date('Y-m-d'),
        'compare' => '>=',
        'type' => 'DATE',
      ];
    } else if ($restrictTo == "Past") {
      return [
        'key' => 'end_date',
        'value' => date('Y-m-d'),
        'compare' => '<',
        'type' => 'DATE',
      ];
    }
  }

  /**
   * If we're interacting with the map, we want to only show those items that
   * actually have a valid location. Those items are:
   *  - Posts with a valid map location
   *  - Posts with a single destination
   *
   * @param integer $postID
   * @param integer $limit
   * @param string $type
   * @param boolean $forceHasInterests
   * @return array
   */
  public static function getWithValidLocation($postID, $limit, $type = null, $forceHasInterests = false) {
    global $wpdb;

    $destinationString = 'a:1:{i:0;s:' . strlen((string) $postID) . ':"' . $postID . '";}';
    $destinationRestriction = [];

    $destinationRestrictionArticle = '(`pm`.`meta_key` = "article_destinations"
      AND `pm`.`meta_value` = \'' . $destinationString . '\')';

    $destinationRestrictionEvent = '(`pm`.`meta_key` = "event_destinations"
      AND `pm`.`meta_value` = \'' . $destinationString . '\')';

    $destinationRestrictionEventLocation = '(`pm`.`meta_key` = "event_map_lat" AND `pm`.`meta_value` != 0)
      AND (`pm`.`meta_key` = "event_map_lng" AND `pm`.`meta_value` != 0)';

    $destinationRestrictionGallery = '(`pm`.`meta_key` = "gallery_destinations"
      AND `pm`.`meta_value` = \'' . $destinationString . '\')';

    $destinationRestrictionProduct = '(`pm`.`meta_key` = "product_destinations"
      AND `pm`.`meta_value` = \'' . $destinationString . '\')';

    $destinationRestrictionRecipe = '(`pm`.`meta_key` = "recipe_destinations"
      AND `pm`.`meta_value` = \'' . $destinationString . '\')';

    $destinationRestrictionRoute = '(`pm`.`meta_key` = "route_destinations"
      AND `pm`.`meta_value` = \'' . $destinationString . '\')';

    switch ($type) {
      case self::TYPE_ALL:
        $destinationRestriction[] = $destinationRestrictionArticle;
        $destinationRestriction[] = $destinationRestrictionEvent;
        $destinationRestriction[] = $destinationRestrictionEventLocation;
        $destinationRestriction[] = $destinationRestrictionGallery;
        $destinationRestriction[] = $destinationRestrictionProduct;
        $destinationRestriction[] = $destinationRestrictionRecipe;
        $destinationRestriction[] = $destinationRestrictionRoute;
        break;
      case self::TYPE_ARTICLE:
        $destinationRestriction[] = $destinationRestrictionArticle;
        break;
      case self::TYPE_EVENT:
        $destinationRestriction[] = $destinationRestrictionEventLocation;
        break;
      case self::TYPE_GALLERY:
        $destinationRestriction[] = $destinationRestrictionGallery;
        break;
      case self::TYPE_PRODUCT:
        $destinationRestriction[] = $destinationRestrictionProduct;
        break;
      case self::TYPE_RECIPE:
        $destinationRestriction[] = $destinationRestrictionRecipe;
        break;
      case self::TYPE_ROUTE:
        $destinationRestriction[] = $destinationRestrictionRoute;
        break;
      default:
        break;
    }

    // Prepare the destination restriction query
    $destinationRestrictionSql = '';

    if (!empty($destinationRestriction)) {
      $destinationRestrictionSql = ' AND (' . implode(' OR ', $destinationRestriction) . ') ';
    }

    // Prepare the unique IDs query
    $uniqueSql = '';
    $uniqueCacheData = wp_cache_get(VisitEurope_Content::CACHE_EXPERIENCE);

    if (is_array($uniqueCacheData) && !empty($uniqueCacheData)) {
      $uniqueSql = ' AND `p`.`ID` NOT IN (' . implode(',', $uniqueCacheData) . ')';
    }

    // Force interests if necessary
    $interestRestrictionSql = '';

    if ($forceHasInterests == true) {
      $interestRestrictionSql = 'JOIN `' . $wpdb->prefix . 'term_relationships` `tr`
        ON `tr`.`object_id` = `p`.`ID`';
    }

    // Get the posts
    $posts = $wpdb->get_results('
      SELECT `p`.*

      FROM `' . $wpdb->prefix . 'posts` `p`

      JOIN `' . $wpdb->prefix . 'postmeta` `pm`
        ON `pm`.`post_id` = `p`.`ID`
        ' . $destinationRestrictionSql . '

      ' . $interestRestrictionSql . '

      WHERE `p`.`post_type` = "' . VisitEurope_CPT_Experience::POST_TYPE . '"
        AND `p`.`post_status` = "publish"
        ' . $uniqueSql . '

      GROUP BY `p`.`ID`

      ORDER BY RAND()

      LIMIT ' . $limit . '
    ');

    // Add to cache if necessary
    if (is_array($uniqueCacheData)) {
      $postIDs = array_keys(VisitEurope_Helper::postsToList($posts));
      wp_cache_set(VisitEurope_Content::CACHE_EXPERIENCE, array_merge($uniqueCacheData, $postIDs), null);
    }

    return $posts;
  }

}
