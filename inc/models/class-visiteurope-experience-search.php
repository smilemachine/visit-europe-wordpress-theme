<?php
if (!defined('ABSPATH')) exit;

/**
 * Experience Search model
 *
 * @package Visit_Europe
 */
class VisitEurope_Experience_Search {

  private $type;
  private $destinationID;
  private $interestIDs;

  public function __construct() {
    //
  }

  /**
   * Populates the class with valid data from the given array
   *
   * @param array $data
   * @return void
   */
  public function fill($data = []) {
    if (!is_array($data)) {
      return;
    }

    // Interest
    if (isset($data['interest'])) {
      $interestGroupName = str_replace('/', '', sanitize_text_field($data['interest']));
      $interestIDs = VisitEurope_Interest::getTaxIDsByInterestGroupName($interestGroupName);

      if (is_array($interestIDs) && !empty($interestIDs)) {
        $this->interestIDs = $interestIDs;
      }
    }

    // Destination
    if (isset($data['destination']) && !empty($data['destination'])) {
      $destinationID = str_replace('/', '', sanitize_text_field($data['destination']));

      if (!is_int($destinationID) && intval($destinationID) == 0) {
        $posts = get_posts([
          'post_type' => VisitEurope_CPT_Destination::POST_TYPE,
          'name' => $destinationID,
          'posts_per_page' => 1,
        ]);

        if (is_array($posts) && isset($posts[0])) {
          $destinationID = $posts[0]->ID;
        }
      }

      $this->destinationID = $destinationID;
    }

    // Type
    if (isset($data['type'])) {
      $this->type = $this->getTypeFromRaw(str_replace('/', '', $data['type']));
    }
  }

  /**
   * Returns the type
   *
   * @return string
   */
  public function getType() {
    return $this->type;
  }

  /**
   * Get the type from the raw type
   *
   * @param string $rawType
   * @return string
   */
  private function getTypeFromRaw($rawType) {
    $type = $this->type;

    switch (strtolower($rawType)) {
      case 'article':
        $type = VisitEurope_Experience::TYPE_ARTICLE;
        break;
      case 'event':
        $type = VisitEurope_Experience::TYPE_EVENT;
        break;
      case 'gallery':
        $type = VisitEurope_Experience::TYPE_GALLERY;
        break;
      case 'product':
        $type = VisitEurope_Experience::TYPE_PRODUCT;
        break;
      case 'recipe':
        $type = VisitEurope_Experience::TYPE_RECIPE;
        break;
      case 'route':
        $type = VisitEurope_Experience::TYPE_ROUTE;
        break;
      default:
        break;
    }

    return $type;
  }

  /**
   * Get the results IDs
   *
   * @return array
   */
  public function getResultsIDs() {
    $results = $this->getResults();
    return VisitEurope_Helper::postsToIDs($results);
  }

  /**
   * Get the results
   *
   * @return array
   */
  public function getResults() {
    global $wpdb;

    $typeJoinSql = '';
    $destinationJoinSql = '';
    $destinationWhereSql = '';
    $interestJoinSql = '';
    $interestWhereSql = '';

    // Type
    if ($this->type) {
      $typeJoinSql = '
        JOIN `' . $wpdb->prefix . 'postmeta` `type`
          ON `type`.`post_id` = `p`.`ID`
          AND `type`.`meta_key` = "type"
          AND `type`.`meta_value` = "' . $this->type . '"
      ';
    }

    // Destination sql
    if ($this->destinationID) {
      $destinationType = strtolower($this->type);
      $destinationJoinSql = '
        LEFT JOIN `' . $wpdb->prefix . 'postmeta` `destination`
          ON `destination`.`post_id` = `p`.`ID`
          AND `destination`.`meta_key` LIKE "%_destinations"
          AND `destination`.`meta_value` LIKE "a:%"
      ';
      $destinationWhereSql = ' AND `destination`.`meta_value` LIKE \'%"' . intval($this->destinationID) . '"%\' ';
    }

    // Interests
    if (is_array($this->interestIDs) && !empty($this->interestIDs)) {
      $interestJoinSql = '
        JOIN `' . $wpdb->prefix . 'term_relationships` `tr`
          ON `tr`.`object_id` = `p`.`ID`

        JOIN `' . $wpdb->prefix . 'term_taxonomy` `tt`
          ON `tt`.`term_taxonomy_id` = `tr`.`term_taxonomy_id`
          AND `tt`.`taxonomy` = "' . VisitEurope_Taxonomy_Interest::TAXONOMY . '"

        JOIN `' . $wpdb->prefix . 'terms` `t`
          ON `t`.`term_id` = `tt`.`term_id`
      ';
      $interestWhereSql = ' AND `t`.`term_id` IN (' . implode(',', $this->interestIDs) . ') ';
    }

    $translationJoinSql = '
      JOIN `' . $wpdb->prefix . 'icl_translations` `wpml`
        ON `wpml`.`element_id` = `p`.`ID`
        AND `wpml`.`element_type` = "post_' . VisitEurope_CPT_Experience::POST_TYPE . '"
        AND `wpml`.`language_code` = "' . ICL_LANGUAGE_CODE . '"
    ';

    $sql = '
      SELECT
        `p`.*

      FROM `' . $wpdb->prefix . 'posts` `p`
      ' . $typeJoinSql . '
      ' . $destinationJoinSql . '
      ' . $interestJoinSql . '
      ' . $translationJoinSql . '

      WHERE `p`.`post_type` = "' . VisitEurope_CPT_Experience::POST_TYPE . '"
        AND `p`.`post_status` = "publish"
        ' . $destinationWhereSql . '
        ' . $interestWhereSql . '

      GROUP BY `p`.`ID`
      ORDER BY `p`.`ID` DESC
    ';

    return $wpdb->get_results($sql);
  }

}
