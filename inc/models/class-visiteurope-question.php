<?php
if (!defined('ABSPATH')) exit;

/**
 * Question model
 *
 * @package Visit_Europe
 */
class VisitEurope_Question {

  const TYPE_ALL = 'QUESTION';

  /**
   * Returns an array of Questions
   *
   * @param array $customParams
   * @param boolean $returnAsList
   * @return array
   */
  public static function get($customParams = [], $returnAsList = false) {
    $defaultParams = [
      'post_status' => 'published',
      'post_type' => VisitEurope_CPT_Question::POST_TYPE,
      'posts_per_page' => -1.
      // 'orderby' => 'rand',
    ];

    $params = VisitEurope_Helper::paramsForQuery($defaultParams, $customParams);
    $posts = get_posts($params);

    if ($returnAsList !== true) {
      return $posts;
    }

    return VisitEurope_Helper::postsToList($posts);
  }

  /**
   * Returns an array of Questions in an array of IDs
   *
   * @param array $rawIDs
   * @param array $customParams
   * @param boolean $returnAsList
   * @return array
   */
  public static function getWhereIn($rawIDs, $customParams = [], $returnAsList = false) {
    $postIDs = VisitEurope_Helper::validIDsFromArray($rawIDs);

    if (empty($postIDs)) {
      return [];
    }

    $defaultParams = [
      'post__in' => $postIDs
    ];
    $params = VisitEurope_Helper::paramsForQuery($defaultParams, $customParams);

    return self::get($params, $returnAsList);
  }

  /**
   * Returns an array of Questions by type
   *
   * @param string $type
   * @param array $customParams
   * @param boolean $returnAsList
   * @return array
   */
  public static function getWhereType($type, $customParams = [], $returnAsList = false) {
    $defaultParams = [
      'meta_key' => 'type',
      'meta_value' => $type,
    ];
    $params = VisitEurope_Helper::paramsForQuery($defaultParams, $customParams);

    return self::get($params, $returnAsList);
  }

}
