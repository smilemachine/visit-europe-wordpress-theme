<?php
if (!defined('ABSPATH')) exit;

/**
 * Custom post type interface
 *
 * @package Visit_Europe
 */
class VisitEurope_Experience_Item {

  public $post = null;
  private $destination = null;

  /**
   * Set the post for this item
   *
   * @param WP_Post $post
   * @return void
   */
  public function setPost($post) {
    $this->post = $post;
  }

  /**
   * Set the destination for this item
   *
   * @param array $destinations
   * @return void
   */
  public function setDestination($destinations = []) {
    $randomDestination = null;

    if (is_array($destinations) && !empty($destinations)) {
    	$randomDestination = $destinations[array_rand($destinations)];
    }

    if ($randomDestination) {
    	$this->destination = $randomDestination;
    }
  }

  /**
   * Returns the destination for this route
   *
   * @return bool
   */
  public function hasValidDestination() {
    return $this->destination ? true : false;
  }

  /**
   * Returns the destination ID
   *
   * @return mixed;
   */
  public function getDestinationID() {
    if ($this->destination) {
      return intval($this->destination->ID);
    }

    return null;
  }

  /**
   * Returns the destination URL
   *
   * @return mixed;
   */
  public function getDestinationUrl() {
    if ($this->destination) {
      return get_permalink($this->destination->ID);
    }

    return null;
  }

  /**
   * Returns the destination Type
   *
   * @return mixed;
   */
  public function getDestinationType() {
    if ($this->destination) {
      return VisitEurope_Destination::getTypeForPostID($this->destination->ID);
    }

    return null;
  }

  /**
   * Returns the destination Type
   *
   * @return mixed;
   */
  public function getDestinationTitle() {
    if ($this->destination) {
      return $this->destination->post_title;
    }

    return null;
  }

  /**
   * Returns the image url for the given size
   *
   * @param string $size
   * @return string
   */
  public function getDestinationImageUrl($size = 'full') {
    if ($this->destination) {
      return VisitEurope_Helper::getPostThumbnailUrl($this->destination->ID, $size);
    }

    return null;
  }

  /**
   * Returns the location for the map
   *
   * @param string $prefix
   * @return array
   */
  public function getMapLocation($prefix = '') {
    if (!empty($prefix)) {
      $lat = (float) get_field($prefix . '_map_lat', $this->post->ID);
      $lng = (float) get_field($prefix . '_map_lng', $this->post->ID);

      if ($lat != 0 && $lng != 0) {
        return [
          'lat' => $lat,
          'lng' => $lng,
        ];
      }
    }

    if ($this->destination) {
      return [
      	'lat' => (float) get_field('map_lat', $this->destination->ID),
      	'lng' => (float) get_field('map_lng', $this->destination->ID),
      ];
    }

    return [
      'lat' => 0,
      'lng' => 0,
    ];
  }

  /**
   * Gets the header image for the given size. Defaults to destination image if none found.
   *
   * @param string $size
   * @return string
   */
  public function getHeaderImageUrl($size = 'full') {
    $imageUrl = VisitEurope_Helper::getPostThumbnailUrl($this->post->ID, $size);

    if ($imageUrl && !empty($imageUrl)) {
      return $imageUrl;
    }

    if (empty($imageUrl) && $this->destination) {
      return VisitEurope_Helper::getPostThumbnailUrl($this->destination->ID, $size);
    }

    return null;
  }
}
