<?php
if (!defined('ABSPATH')) exit;

require_once dirname(__FILE__) . '/class-visiteurope-trip-item.php';

/**
 * Experience model
 *
 * @package Visit_Europe
 */
class VisitEurope_Trip_Public extends VisitEurope_Trip_Item {

  /**
   * Sets the initial data
   *
   * @param WP_Post $post
   * @return void
   */
  public function __construct ($post) {
    $this->setPost($post);
    $this->setDestination(get_field('destinations', $this->post->ID));
  }

  /**
   * Gets the markers available for the map
   *
   * @param array $segment
   * @return array
   */
  public function getMapMarkersForSegment($segment) {
    $markers = [];

    if (!is_array($segment) || !isset($segment['locations']) || !is_array($segment['locations'])
      || empty($segment['locations'])) {
      return [];
    }

    foreach ($segment['locations'] as $location) {
      $imageInitialUrl = '';
      $imageFullUrl = '';

      if ($location['image']) {
        $imageInitialUrl = $location['image']['sizes']['initial'];
        $imageFullUrl = $location['image']['sizes']['thumbnail'];
      }

      $mapIcon = get_template_directory_uri() . '/assets/img/google-map-marker.png';
      $mapLocation = [
        'lat' => (float) $location['latitude'],
        'lng' => (float) $location['longitude'],
      ];

      $markers[] = [
        'icon' => $mapIcon,
        'location' => $mapLocation,
        'metadata' => [
          'item' => [
            'imageinitial' => $imageInitialUrl,
            'imagefull' => $imageFullUrl,
            'title' => $location['title'],
            'body' => '',
            'map' => [
              'icon' => $mapIcon,
              'location' => $mapLocation,
            ]
          ],
        ],
      ];
    }

    return $markers;
  }



  /**
   * Gets the polygon available for the map
   *
   * @param array $segment
   * @return array
   */
  public function getMapPolygonForSegment($segment) {
    $points = [];

    if (!is_array($segment) || !isset($segment['locations']) || !is_array($segment['locations'])
      || empty($segment['locations'])) {
      return [];
    }

    foreach ($segment['locations'] as $location) {
      $points[] = [
        'lat' => (float) $location['latitude'],
        'lng' => (float) $location['longitude'],
      ];
    }

    return $points;
  }

  /**
   * Returns the location for the map
   *
   * @param array $segment
   * @return array
   */
  public function getMapLocationForSegment($segment) {
    $markers = $this->getMapMarkersForSegment($segment);

    if (is_array($markers) && !empty($markers)) {
      return [
      	'lat' => (float) get_field('latitude', $markers[0]['location']['lat']),
      	'lng' => (float) get_field('longitude', $markers[0]['location']['lng']),
      ];
    }

    return [
      'lat' => 0,
      'lng' => 0,
    ];
  }

}
