<?php
if (!defined('ABSPATH')) exit;

require_once dirname(__FILE__) . '/class-visiteurope-experience-item.php';

/**
 * Experience model
 *
 * @package Visit_Europe
 */
class VisitEurope_Experience_Recipe extends VisitEurope_Experience_Item {

  /**
   * Sets the initial data
   *
   * @param WP_Post $post
   * @return void
   */
  public function __construct ($post) {
    $this->setPost($post);
    $this->setDestination(get_field('recipe_destinations', $this->post->ID));
  }

  /**
   * Gets the markers available for the map
   *
   * @return array
   */
  public function getMapMarkers() {
    return [
      'location' => $this->getMapLocation(),
      'icon' => get_template_directory_uri() . '/assets/img/google-map-marker.png'
    ];
  }

  /**
   * Gets the zoom for the map
   *
   * @return array
   */
  public function getMapZoom() {
    $zoom = intval(get_field('recipe_map_zoom', $this->post->ID));
    return $zoom == 0 ? 6 : $zoom;
  }

}
