<?php
if (!defined('ABSPATH')) exit;

require_once dirname(__FILE__) . '/class-visiteurope-experience-item.php';

/**
 * Experience model
 *
 * @package Visit_Europe
 */
class VisitEurope_Experience_Event extends VisitEurope_Experience_Item {

  /**
   * Sets the initial data
   *
   * @param WP_Post $post
   * @return void
   */
  public function __construct ($post) {
    $this->setPost($post);
    $this->setDestination(get_field('event_destinations', $this->post->ID));
  }

  /**
   * Gets the zoom for the map
   *
   * @return array
   */
  public function getMapZoom() {
    $zoom = intval(get_field('event_map_zoom', $this->post->ID));
    return $zoom == 0 ? 6 : $zoom;
  }

  /**
   * Gets the markers available for the map
   *
   * @param string $prefix
   * @return array
   */
  public function getMapMarkers($prefix = '') {
    return [[
      'location' => $this->getMapLocation($prefix),
      'icon' => get_template_directory_uri() . '/assets/img/google-map-marker.png'
    ]];
  }

  /**
   * Gets the pointers
   *
   * @return array
   */
  public function getPointers() {
    $startDate = get_field('start_date', $this->post->ID);
    $startTime = get_field('start_time', $this->post->ID);
    $endDate = get_field('end_date', $this->post->ID);
    $endTime = get_field('end_time', $this->post->ID);

    if (!$startTime || empty($startTime)) {
      $startTime = '00:00';
    }

    if (!$endTime || empty($endTime)) {
      $endTime = '00:00';
    }

    $startTimestamp = strtotime($startDate . ' ' . $startTime);
    $endTimestamp = strtotime($endDate . ' ' . $endTime);
    $pointers = get_field('pointers', $this->post->ID);
    $dateFormat = VisitEurope_Content::getLocale('date.event.format');
    $string = date($dateFormat, $startTimestamp) . ' to<br/>' . date($dateFormat, $endTimestamp);

    array_unshift($pointers, [
      'type' => 'When',
      'subtitle' => $string,
      'url' => ''
    ]);

    return $pointers;
  }

}
