<?php
if (!defined('ABSPATH')) exit;

require_once dirname(__FILE__) . '/interface-visiteurope-taxonomy.php';

/**
 * Interest custom taxonomy
 *
 * @package Visit_Europe
 */
class VisitEurope_Taxonomy_Interest implements VisitEurope_Taxonomy {

  const TAXONOMY = 've_taxonomy_interest';

  /**
   * Registers Interest as a custom taxonomy
   *
   * @return void
   */
  public function setup() {
    $params = [
      'labels' => [
        'name' => 'Interests',
        'singular_name' => 'Interest',
        'search_items' => 'Search Interest',
    		'popular_items' => 'Popular Interest',
    		'all_items' => 'All Interest',
    		'parent_item' => null,
    		'parent_item_colon' => null,
    		'edit_item' => 'Edit Interest',
    		'update_item' => 'Update Interest',
    		'add_new_item' => 'Add New Interest',
    		'new_item_name' => 'New Interest Name',
    		'separate_items_with_commas' => 'Separate interests with commas',
    		'add_or_remove_items' => 'Add or remove interests',
    		'choose_from_most_used' => 'Choose from the most used interests',
    		'not_found' => 'No interests found.',
    		'menu_name' => 'Interests',
      ],
      'hierarchical' => false,
      'show_ui' => true,
      'show_admin_column' => true,
      // 'update_count_callback' => '',
      'query_var' => true,
      'rewrite' => [
        'slug' => 'interest',
        'with_front' => false,
        'hierarchical' => false,
      ],
    ];

    $postTypes = [
      VisitEurope_CPT_Experience::POST_TYPE,
      VisitEurope_CPT_Trip::POST_TYPE,
    ];

    register_taxonomy(self::TAXONOMY, $postTypes, $params);
  }

}
