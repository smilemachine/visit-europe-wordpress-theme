<?php
if (!defined('ABSPATH')) exit;

/**
 * Taxonomy interface
 *
 * @package Visit_Europe
 */
interface VisitEurope_Taxonomy {
  public function setup();
}
