<?php
if (!defined('ABSPATH')) exit;

require_once dirname(__FILE__) . '/interface-visiteurope-taxonomy.php';

/**
 * Audience custom taxonomy
 *
 * @package Visit_Europe
 */
class VisitEurope_Taxonomy_Audience implements VisitEurope_Taxonomy {

  const TAXONOMY = 've_taxonomy_audience';

  /**
   * Registers Audience as a custom taxonomy
   *
   * @return void
   */
  public function setup() {
    $params = [
      'labels' => [
        'name' => 'Audiences',
        'singular_name' => 'Audience',
        'search_items' => 'Search Audience',
    		'popular_items' => 'Popular Audience',
    		'all_items' => 'All Audience',
    		'parent_item' => null,
    		'parent_item_colon' => null,
    		'edit_item' => 'Edit Audience',
    		'update_item' => 'Update Audience',
    		'add_new_item' => 'Add New Audience',
    		'new_item_name' => 'New Audience Name',
    		'separate_items_with_commas' => 'Separate audiences with commas',
    		'add_or_remove_items' => 'Add or remove audiences',
    		'choose_from_most_used' => 'Choose from the most used audiences',
    		'not_found' => 'No audiences found.',
    		'menu_name' => 'Audiences',
      ],
      'hierarchical' => false,
      'show_ui' => true,
      'show_admin_column' => true,
      // 'update_count_callback' => '',
      'query_var' => true,
      'rewrite' => [
        'slug' => 'audience',
        'with_front' => false,
        'hierarchical' => false,
      ],
    ];

    $postTypes = [
      VisitEurope_CPT_Trip::POST_TYPE,
    ];

    register_taxonomy(self::TAXONOMY, $postTypes, $params);
  }

}
