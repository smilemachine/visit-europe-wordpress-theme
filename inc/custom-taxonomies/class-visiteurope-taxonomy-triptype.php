<?php
if (!defined('ABSPATH')) exit;

require_once dirname(__FILE__) . '/interface-visiteurope-taxonomy.php';

/**
 * TripType custom taxonomy
 *
 * @package Visit_Europe
 */
class VisitEurope_Taxonomy_TripType implements VisitEurope_Taxonomy {

  const TAXONOMY = 've_taxonomy_triptype';

  /**
   * Registers TripType as a custom taxonomy
   *
   * @return void
   */
  public function setup() {
    $params = [
      'labels' => [
        'name' => 'Trip Types',
        'singular_name' => 'Trip Type',
        'search_items' => 'Search Trip Types',
    		'popular_items' => 'Popular Trip Types',
    		'all_items' => 'All Trip Types',
    		'parent_item' => null,
    		'parent_item_colon' => null,
    		'edit_item' => 'Edit Trip Type',
    		'update_item' => 'Update Trip Type',
    		'add_new_item' => 'Add New Trip Type',
    		'new_item_name' => 'New Trip Type Name',
    		'separate_items_with_commas' => 'Separate trip types with commas',
    		'add_or_remove_items' => 'Add or remove trip types',
    		'choose_from_most_used' => 'Choose from the most used trip types',
    		'not_found' => 'No trip types found.',
    		'menu_name' => 'Trip Types',
      ],
      'hierarchical' => false,
      'show_ui' => true,
      'show_admin_column' => true,
      // 'update_count_callback' => '',
      'query_var' => true,
      'rewrite' => [
        'slug' => 'trip-type',
        'with_front' => false,
        'hierarchical' => false,
      ],
    ];

    $postTypes = [
      VisitEurope_CPT_Trip::POST_TYPE,
    ];

    register_taxonomy(self::TAXONOMY, $postTypes, $params);
  }

}
