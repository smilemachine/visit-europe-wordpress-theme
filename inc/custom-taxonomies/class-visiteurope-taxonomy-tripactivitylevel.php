<?php
if (!defined('ABSPATH')) exit;

require_once dirname(__FILE__) . '/interface-visiteurope-taxonomy.php';

/**
 * TripActivityLevel custom taxonomy
 *
 * @package Visit_Europe
 */
class VisitEurope_Taxonomy_TripActivityLevel implements VisitEurope_Taxonomy {

  const TAXONOMY = 've_taxonomy_tripactivitylevel';

  /**
   * Registers TripActivityLevel as a custom taxonomy
   *
   * @return void
   */
  public function setup() {
    $params = [
      'labels' => [
        'name' => 'Trip Activity Levels',
        'singular_name' => 'Trip Activity Level',
        'search_items' => 'Search Trip Activity Levels',
    		'popular_items' => 'Popular Trip Activity Levels',
    		'all_items' => 'All Trip Activity Levels',
    		'parent_item' => null,
    		'parent_item_colon' => null,
    		'edit_item' => 'Edit Trip Activity Level',
    		'update_item' => 'Update Trip Activity Level',
    		'add_new_item' => 'Add New Trip Activity Level',
    		'new_item_name' => 'New Trip Activity Level Name',
    		'separate_items_with_commas' => 'Separate trip activity levels with commas',
    		'add_or_remove_items' => 'Add or remove trip activity levels',
    		'choose_from_most_used' => 'Choose from the most used trip activity levels',
    		'not_found' => 'No trip activity levels found.',
    		'menu_name' => 'Trip Activity Levels',
      ],
      'hierarchical' => false,
      'show_ui' => true,
      'show_admin_column' => true,
      // 'update_count_callback' => '',
      'query_var' => true,
      'rewrite' => [
        'slug' => 'trip-activity-levels',
        'with_front' => false,
        'hierarchical' => false,
      ],
    ];

    $postActivities = [
      VisitEurope_CPT_Trip::POST_TYPE,
    ];

    register_taxonomy(self::TAXONOMY, $postActivities, $params);
  }

}
