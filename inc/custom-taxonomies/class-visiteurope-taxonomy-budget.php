<?php
if (!defined('ABSPATH')) exit;

require_once dirname(__FILE__) . '/interface-visiteurope-taxonomy.php';

/**
 * Budget custom taxonomy
 *
 * @package Visit_Europe
 */
class VisitEurope_Taxonomy_Budget implements VisitEurope_Taxonomy {

  const TAXONOMY = 've_taxonomy_budget';

  /**
   * Registers Budget as a custom taxonomy
   *
   * @return void
   */
  public function setup() {
    $params = [
      'labels' => [
        'name' => 'Budgets',
        'singular_name' => 'Budget',
        'search_items' => 'Search Budget',
    		'popular_items' => 'Popular Budget',
    		'all_items' => 'All Budget',
    		'parent_item' => null,
    		'parent_item_colon' => null,
    		'edit_item' => 'Edit Budget',
    		'update_item' => 'Update Budget',
    		'add_new_item' => 'Add New Budget',
    		'new_item_name' => 'New Budget Name',
    		'separate_items_with_commas' => 'Separate budgets with commas',
    		'add_or_remove_items' => 'Add or remove budgets',
    		'choose_from_most_used' => 'Choose from the most used budgets',
    		'not_found' => 'No budgets found.',
    		'menu_name' => 'Budgets',
      ],
      'hierarchical' => false,
      'show_ui' => true,
      'show_admin_column' => true,
      // 'update_count_callback' => '',
      'query_var' => true,
      'rewrite' => [
        'slug' => 'budget',
        'with_front' => false,
        'hierarchical' => false,
      ],
    ];

    $postTypes = [
      VisitEurope_CPT_Trip::POST_TYPE,
    ];

    register_taxonomy(self::TAXONOMY, $postTypes, $params);
  }

}
