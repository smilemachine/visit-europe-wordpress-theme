<?php
if (!defined('ABSPATH')) exit;

/**
 * Setup for Visit Europe
 *
 * @package Visit_Europe
 */
class VisitEurope_Settings {

  /**
   * Returns the Essential Information from settings
   *
   * @param integer $limit
   * @return array
   */
  public static function getEssentialInformation($limit = 0) {
    $items = get_field('essential_information', 'option');
    $return = [];

    if (!is_array($items) || empty($items)) {
      return [];
    }

    if (intval($limit) >= 0) {
      $limit = intval(count($items) > $limit ? $limit : count($items));
      shuffle($items);

      $items = array_slice($items, 0, $limit);
    }

    // Title and Image are optional, so if they're not provided - give them ones associated to the post
    foreach ($items as $item) {
      $post = $item['url'];
      $title = $post->post_title;
      $imageInitial = VisitEurope_Helper::getPostThumbnailUrl($post->ID, 'initial');
      $imageFull = VisitEurope_Helper::getPostThumbnailUrl($post->ID, 'medium');

      if ($item['title'] && !empty($item['title'])) {
        $title = $item['title'];
      }

      if ($item['image'] && is_array($item['image'])) {
        $imageInitial = $item['image']['sizes']['initial'];
        $imageFull = $item['image']['sizes']['thumbnail'];
      }

      $return[] = [
        'title' => $title,
        'imageInitial' => $imageInitial,
        'imageFull' => $imageFull,
        'url' => get_the_permalink($post->ID)
      ];
    }

    return $return;
  }

  /**
   * Returns the Interest Groups from settings
   *
   * @param integer $limit
   * @param boolean $returnAsList
   * @return array
   */
  public static function getInterestGroups($limit = 0, $returnAsList = false) {
    $items = get_field('interest_groups', 'option');

    if (!is_array($items) || empty($items)) {
      return [];
    }

    if (intval($limit) > 0) {
      $limit = intval(count($items) > $limit ? $limit : count($items));
      shuffle($items);

      $items = array_slice($items, 0, $limit);
    }

    if (!$returnAsList) {
      return $items;
    }

    return VisitEurope_Helper::arrayToList($items, 'name', 'title');
  }

  /**
   * Returns the Interest Groups from settings for a filter grid
   *
   * @return array
   */
  public static function getInterestGroupsForFilter() {
    $items = [];
    $groups = self::getInterestGroups();

    foreach ($groups as $group) {
      if (!isset($group['title']) || !isset($group['filters']) || !is_array($group['filters']) || empty($group['filters'])) {
        continue;
      }

      $items[] = [
        'title' => $group['title'],
        'filters' => $group['filters']
      ];
    }

    return $items;
  }

  /**
   * Returns the Interest Groups from settings for a dropdown with icons
   *
   * @return array
   */
  public static function getInterestGroupsForDropdown() {
    $items = [];
    $groups = self::getInterestGroups();

    foreach ($groups as $group) {
      if (!isset($group['title']) || !isset($group['icon']) || !is_array($group['icon']) || empty($group['icon'])) {
        continue;
      }

      $items[] = [
        'icon' => $group['icon']['url'],
        'key' => VisitEurope_Helper::getSlug($group['title']),
        'title' => $group['name'],
        'types' => array_map('strtoupper', $group['search']),
      ];
    }

    return $items;
  }

  /**
   * Returns the Trip Types from settings
   *
   * @param integer $limit
   * @param boolean $returnAsList
   * @return array
   */
  public static function getTripTypes($limit = 0, $returnAsList = false) {
    $items = get_field('trip_types', 'option');

    if (!is_array($items) || empty($items)) {
      return [];
    }

    if (intval($limit) > 0) {
      $limit = intval(count($items) > $limit ? $limit : count($items));
      shuffle($items);

      $items = array_slice($items, 0, $limit);
    }

    if (!$returnAsList) {
      return $items;
    }

    return VisitEurope_Helper::arrayToList($items, 'name', 'title');
  }

  /**
   * Returns the Trip Types from settings for a filter grid
   *
   * @return array
   */
  public static function getTripTypesForFilter() {
    $items = [];
    $groups = self::getTripTypes();

    foreach ($groups as $group) {
      if (!isset($group['title']) || !isset($group['filters']) || !is_array($group['filters']) || empty($group['filters'])) {
        continue;
      }

      $items[] = [
        'title' => $group['title'],
        'filters' => $group['filters']
      ];
    }

    return $items;
  }

  /**
   * Returns the social accounts
   *
   * @return array
   */
  public static function getSocialAccounts() {
    $accounts = [];
    $items = get_field('social_accounts', 'option');

    if (!is_array($items) || empty($items)) {
      return [];
    }

    foreach ($items as $item) {
      $accounts[strtolower($item['type'])] = $item['url'];
    }

    return $accounts;
  }

}
