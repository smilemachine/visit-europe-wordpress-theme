<?php
if (!defined('ABSPATH')) exit;

/**
 * API functions for Visit Europe
 *
 * @package Visit_Europe
 */
class VisitEurope_API {

  const VERSION = 'v1';
  const NAMESPACE = 'visit-europe/' . self::VERSION;

  public function __construct() {
    //
  }

  /**
   * Sets up all of the API routes
   *
   * @return void
   */
  public function setup() {
    $routes = ['Destination', 'Experience', 'Google', 'InspireMe', 'Newsletter', 'Search', 'Trip', 'User', 'UserTrip'];

    foreach ($routes as $route) {
      $className = 'VisitEurope_API_' . $route . '_Controller';
      $controller = new $className();
      $controller->setup();
    }
  }

}
