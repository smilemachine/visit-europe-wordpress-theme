<?php
if (!defined('ABSPATH')) exit;

/**
 * Feature functions for Visit Europe
 *
 * @package Visit_Europe
 */
class VisitEurope_Feature {

  const INSPIRE_ME = 'INSPIRE_ME';
  const USER_ACCOUNTS = 'USER_ACCOUNTS';
  const MY_TRIPS = 'MY_TRIPS';

  /**
   * Determines if the given feature is active
   *
   * @param string $feature
   * @return boolean
   */
  public static function isActive($string) {
    $isActive = false;

    switch ($string) {
      case self::INSPIRE_ME:
        $isActive = self::isInspireMeActive();
        break;
      case self::USER_ACCOUNTS:
        $isActive = self::isUserAccountsActive();
        break;
      case self::MY_TRIPS:
        $isActive = self::isMyTripsActive();
        break;
      default:
        break;
    }

    return $isActive;
  }

  /**
   * Determines if inspire me is active
   *
   * @return boolean
   */
  private static function isInspireMeActive() {
    $isActive = get_field('inspire_me_is_enabled', 'option');

    // If it's not available for the public yet, enable it for admins
    if ($isActive !== true) {
      $isActive = VisitEurope_User::isCurrentUserAdmin();
    }

    return $isActive === true;
  }

  /**
   * Determines if user accounts is active
   *
   * @return boolean
   */
  private static function isUserAccountsActive() {
    $isActive = get_field('user_accounts_is_enabled', 'option');

    // Ensure we have the required Facebook and Google keys
    $facebookAppID = get_field('facebook_app_id', 'option');
    $googleAppID = get_field('google_app_id', 'option');

    if ($isActive === true) {
      return !empty($facebookAppID) && !empty($googleAppID);
    }

    return false;
  }

  /**
   * Determines if my trips is active
   *
   * @return boolean
   */
  private static function isMyTripsActive() {
    return self::isUserAccountsActive() && get_field('my_trips_is_enabled', 'option') === true;
  }

}
