<?php
if (!defined('ABSPATH')) exit;

/**
 * Auth functions for Visit Europe
 *
 * @package Visit_Europe
 */
class VisitEurope_Auth {

  const ROLE_MEMBER = 'VE_MEMBER';

  public function __construct() {
    //
  }

  /**
   * Setup the auth
   *
   * @return void
   */
  public function setup() {
    $this->setupCustomUserRoles();

    add_filter('authenticate', [$this, 'setupUserAuthentication'], 10, 2);
    add_filter('login_redirect', [$this, 'setupLoginRedirect'], 10, 2);
    add_filter('nonce_life', [$this, 'setupNonceLifetime']);

    $this->restrictAdminArea();
    $this->returnWpNonce();
  }

  /**
   * Setup extra user authentication
   *
   * @param WP_User $user
   * @param string $username
   * @param string $password
   * @return mixed
   */
  public function setupUserAuthentication($user, $username) {
    if (empty($username)) {
      return null;
    }

    $existingUser = get_user_by('login', $username);

    // Ensure the user has verified their account
    if ($existingUser) {
      $isVerified = VisitEurope_User::isVerified($existingUser->ID);

      if ($isVerified || user_can($existingUser->ID, 'administrator')) {
        return null;
      }
    }

    remove_action('authenticate', 'wp_authenticate_username_password', 20);
    remove_action('authenticate', 'wp_authenticate_email_password', 20);

    return new WP_Error('unauthorized', VisitEurope_Content::getLocale('userVerification.login.error.unverified'));
  }

  /**
   * Restrict admin area to admins only
   *
   * @return void
   */
  public function restrictAdminArea() {
    show_admin_bar(false);

    $isAdmin = current_user_can('administrator');
    $isEditor = current_user_can('editor');
    $isAuthor = current_user_can('author');

    if (is_admin()) {
      if (!$isAdmin && !$isEditor && !$isAuthor) {
        wp_redirect(get_site_url('', null) . '/' . ICL_LANGUAGE_CODE . '/');
        exit;
      }
    }
  }

  /**
   * We're going to always redirect to admin. We protect the admin area with self::restrictAdminArea
   *
   * @return string
   */
  public function setupLoginRedirect() {
    return get_site_url('', null) . '/wp-admin/';
  }

  /**
   * Modifies the lifetime of nonces in terms of hours
   *
   * @return integer
   */
  public function setupNonceLifetime() {
    return 60 * 60 * 4;
  }

  /**
   * Setup custom user roles
   *
   * @return void
   */
  public function setupCustomUserRoles() {
    add_role(VisitEurope_Auth::ROLE_MEMBER, 'Member', [
      'read'
    ]);
  }

  public function returnWpNonce() {
    if (isset($_GET['ve_api']) && $_GET['ve_api'] == 1) {
      header('Content-type: application/json');
      echo json_encode([
        'veRestToken' => VisitEurope_Content::getUrl('_wpnonce')
      ]);
      exit;
    }
  }

}
