<?php
if (!defined('ABSPATH')) exit;

/**
 * Helper functions for Visit Europe
 *
 * @package Visit_Europe
 */
class VisitEurope_Media {

  const DIR_USERS = 'users';
  const DIR_TEMP = 'temp';

  /**
   * Uploads a file and adds it to media
   *
   * @param array $file
   * @return array
   */
  public static function uploadUserImage($file = []) {
    if (!is_array($file) || empty($file)) {
      return false;
    }

    require_once(ABSPATH . 'wp-admin/includes/admin.php');
    add_filter('upload_dir', [self::class, 'setUserTempUploadDirectory']);

    $result = wp_handle_upload($file, [
      'test_form' => false,
      'mimes' => [
        'jpg' => 'image/jpg',
        'jpeg' => 'image/jpeg',
        'png' => 'image/png',
        'gif' => 'image/gif',
      ],
    ]);

    remove_filter('upload_dir', [self::class, 'setUserTempUploadDirectory']);

    return $result;
  }

  /**
   * Overrides upload directory settings
   *
   * @param array $directory
   * @return array
   */
  public static function setUserTempUploadDirectory($directory) {
    return [
        'path' => $directory['basedir'] . '/' . self::DIR_USERS . '/' . self::DIR_TEMP,
        'url' => $directory['baseurl'] . '/' . self::DIR_USERS . '/' . self::DIR_TEMP,
        'subdir' => '/' . self::DIR_USERS . '/' . self::DIR_TEMP,
    ] + $directory;
  }

  /**
   * Returns a clean filename
   *
   * @param string
   * @return string
   */
  public static function getCleanFilename($filename) {
    $slugFilename = VisitEurope_Helper::getSlug($filename);
    $slugFilenameParts = explode('-', $slugFilename);
    $slugExtension = array_pop($slugFilenameParts);
    $cleanFilename = implode('-', $slugFilenameParts) . '.' . $slugExtension;

    return preg_replace('/-+/', '-', str_replace('_', '-', strtolower($cleanFilename)));
  }

  /**
   * Fetch and store media from the given url
   * https://wordpress.stackexchange.com/questions/256830/programmatically-adding-images-to-media-library
   *
   * @param string $url
   * @return string
   */
  public static function storeUserTempMediaFromURL($url) {
    $filename = basename($url);
    $cleanFilename = self::getCleanFilename($filename);
    $uploadDir = wp_upload_dir();
    $usersTempDir = $uploadDir['basedir'] . '/' . self::DIR_USERS . '/' . self::DIR_TEMP . '/';

    // If a filename exists with this url, don't fetch it - simply return the file path
    if (file_exists($usersTempDir . $cleanFilename)) {
      return $usersTempDir . $cleanFilename;
    }

    // Image doesn't exist yet, go get it
    $imageData = @file_get_contents($url);

    if (!$imageData || empty($imageData)) {
      return false;
    }

    if (wp_mkdir_p($uploadDir['path'])) {
      $file = $uploadDir['path'] . '/' . self::DIR_USERS . '/' . $cleanFilename;
    } else {
      $file = $uploadDir['basedir'] . '/' . self::DIR_USERS . '/' . $cleanFilename;
    }

    $stored = file_put_contents($file, $imageData);

    if (!$stored) {
      return false;
    }

    return $file;
  }

  /**
   * Attach an existing media object to the given post
   *
   * @param integer $postID
   * @param string $url
   * @return integer
   */
  public static function attachUserMediaToPost($postID, $url) {
    $file = self::storeUserTempMediaFromURL($url);

    if (empty($file)) {
      return 0;
    }

    $attachmentID = self::attachLocalFileToPostID($file, $postID);

    if (!$attachmentID) {
      return 0;
    }

    return intval($attachmentID);
  }

  /**
   * Attach a local file to the given id
   *
   * @param string $file
   * @param integer $postID
   * @return boolean
   */
  public static function attachLocalFileToPostID($file, $postID) {
    // If the file is already attached to the post, simply return it's attachment ID
    $filename = basename($file);
    $wpFiletype = wp_check_filetype($filename, null);
    $attachmentID = null;

    // If the file is already attached, don't re-attach it
    $attachedMedia = get_attached_media('image', $postID);

    if (is_array($attachedMedia) && !empty($attachedMedia)) {
      foreach ($attachedMedia as $attachedMediaItem) {
        $existingAttachmentURL = wp_get_attachment_url($attachedMediaItem->ID);

        if ($filename == basename($existingAttachmentURL)) {
          $attachmentID = $attachedMediaItem->ID;
          break;
        }
      }
    }

    if ($attachmentID) {
      return $attachmentID;
    }

    $attachment = [
      'post_mime_type' => $wpFiletype['type'],
      'post_title' => sanitize_file_name($filename),
      'post_content' => '',
      'post_status' => 'inherit',
      'post_title' => '',
      'post_content' => '',
      'post_excerpt' => '',
    ];

    $attachmentID = wp_insert_attachment($attachment, $file, $postID);

    require_once(ABSPATH . 'wp-admin/includes/image.php');
    $attachmentData = wp_generate_attachment_metadata($attachmentID, $file);
    wp_update_attachment_metadata($attachmentID, $attachmentData);

    return $attachmentID;
  }

}
