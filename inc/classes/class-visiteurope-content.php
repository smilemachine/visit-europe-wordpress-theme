<?php
if (!defined('ABSPATH')) exit;

/**
 * Feature functions for Visit Europe
 *
 * @package Visit_Europe
 */
class VisitEurope_Content {

  const CACHE_UNIQUE = 'VE_CACHE_UNIQUE';
  const CACHE_ADVERTISEMENT = 'VE_CACHE_ADVERTISEMENT';
  const CACHE_DESTINATION = 'VE_CACHE_DESTINATION';
  const CACHE_EXPERIENCE = 'VE_CACHE_EXPERIENCE';
  const CACHE_TRIP = 'VE_CACHE_TRIP';
  const KEY_SEPARATOR = '_';

  public function __construct() {
    //
  }

  /**
   * Sets up the Content
   *
   * @return void
   */
  public function setup() {
    // $this->importContent();
    add_filter('wpseo_opengraph_desc', [$this, 'overwriteYoastDesc']);
    add_action('wp_mail_failed', [$this, 'actionWPMailFailed'], 10, 1);
  }

  /**
   * Populate Yoast description with the blurb if it's currently empty
   *
   * @param string $desc
   * @return string
   */
  public function overwriteYoastDesc($desc) {
    if (empty($desc)) {
      $desc = get_field('blurb');

      if (!empty($desc)) {
        return $desc;
      }
    }

    return $desc;
  }

  /**
   * Log email errors
   *
   * @param WP_Error $wpError
   * @return
   */
  public function actionWPMailFailed($wpError) {
    return error_log(print_r($wpError, true));
  }

  /**
   * Adds unique cache keys which will be used when rendering a modular page.
   * For example, if 3 modules on a single page use Destinations, then we
   * may only want Destination A to appear in a single module.
   *
   * @param string $key
   * @return void
   */
  public static function addUniqueCache($key = '') {
    $expiry = 20;

    if (empty($key)) {
      $unique = self::CACHE_UNIQUE;
    }

    $existingCache = wp_cache_get($key);

    if (!$existingCache) {
      wp_cache_add($key, [], null, $expiry);
    }
  }

  /**
   * Deletes the unique cache
   *
   * @param string $key
   * @return void
   */
  public static function clearUniqueCache($key = '') {
    if (empty($key)) {
      $unique = self::CACHE_UNIQUE;
    }

    wp_cache_delete($key);
  }

  /**
   * Temporary content import functionality to be used in development only
   */
  private function importContent() {
    if (!defined('VE_IMPORT_CONTENT') || VE_IMPORT_CONTENT !== true) {
      return;
    }

    $importer = new VisitEurope_Importer();
    $importer->setup();
  }

  /**
   * Get locale from dot.notation
   * https://selvinortiz.com/blog/traversing-arrays-using-dot-notation
   *
   * @param string $path
   * @param string $default
   * @return string
   */
  private static function getArrayValueFromPath($data = [], $key = '', $default = '') {
    if (!is_string($key) || empty($key) || empty($data)) {
      return $default;
    }

    if (strpos($key, '.') !== false) {
      $keys = explode('.', $key);

      foreach ($keys as $innerKey) {
        if (!array_key_exists($innerKey, $data)) {
          return $default;
        }

        $data = $data[$innerKey];
      }

      return $data;
    }

    // @fallback returning value of $key in $data or $default value
    return array_key_exists($key, $data) ? $data[$key] : $default;
  }

  /**
   * Get locale from dot.notation
   *
   * @param string $path
   * @param string $default
   * @return string
   */
  public static function getLocale($key = '', $default = '') {
    return self::getArrayValueFromPath(self::getLocales(), $key, $default);
  }

  /**
   * Get url from dot.notation
   *
   * @param string $path
   * @param string $default
   * @return string
   */
  public static function getUrl($key = '', $default = '') {
    return self::getArrayValueFromPath(self::getUrls(), $key, $default);
  }

  /**
   * Returns an array of translated strings for the frontend
   *
   * @return array
   */
  public static function getLocales() {
    return [
      'key' => ICL_LANGUAGE_CODE,
      'account' => [
        'details' => [
          'email' => _x('Email address', 'User account: Email label', VisitEurope_Theme::KEY),
          'password' => [
            'title' => _x('Change your password', 'User account: Password title', VisitEurope_Theme::KEY),
            'password' => _x('New Password', 'User account: New password label', VisitEurope_Theme::KEY),
            'confirmPassword' => _x('Confirm new password', 'User account: Confirm new password label', VisitEurope_Theme::KEY),
          ],
          'button' => _x('Update', 'User account: Submit button', VisitEurope_Theme::KEY),
          'success' => [
            'title' => _x('Thank you', 'Update details: Title', VisitEurope_Theme::KEY),
            'body' => _x('Your details have been updated.', 'Update details: Body', VisitEurope_Theme::KEY),
            'trips' => _x('Go to your trips', 'Update details: Trips button', VisitEurope_Theme::KEY),
            'home' => _x('Go to homepage', 'Update details: Home button', VisitEurope_Theme::KEY),
          ],
          'error' => [
            'unique' => _x('A user already exists with that email address.', 'Update details: Error unique', VisitEurope_Theme::KEY),
          ],
        ],
        'trips' => [

        ],
      ],
      'carousel' => [
        'readMore' => _x('Read More', 'Carousel: Read More', VisitEurope_Theme::KEY),
        'readLess' => _x('Read Less', 'Carousel: Read Less', VisitEurope_Theme::KEY),
        'things' => [
          'title' => _x('Top things to do', 'Carousel: Top things to do', VisitEurope_Theme::KEY),
        ],
        'places' => [
          'title' => _x('Top places to go', 'Carousel: Top places to go', VisitEurope_Theme::KEY),
        ],
      ],
      'cta' => [
        'eventSearchBanner' => [
          'buttonExternal' => _x('See all Events', 'CTA Event Search Banner: External button', VisitEurope_Theme::KEY),
          'buttonSeparator' => _x('or', 'CTA Event Search Banner: Button separator', VisitEurope_Theme::KEY),
          'buttonSubmit' => _x('Search', 'CTA Event Search Banner: Submit button', VisitEurope_Theme::KEY),
          'dateFromPlaceholder' => _x('From', 'CTA Event Search Banner: Date from placeholder', VisitEurope_Theme::KEY),
          'dateToPlaceholder' => _x('To', 'CTA Event Search Banner: Date to placeholder', VisitEurope_Theme::KEY),
          'destinationPlaceholder' => _x('Country', 'CTA Event Search Banner: Destination placeholder', VisitEurope_Theme::KEY),
          'subtitle' => _x('What are you looking for?', 'CTA Event Search Banner: Subtitle', VisitEurope_Theme::KEY),
          'searchTermPlaceholder' => _x('e.g. wine festival', 'CTA Event Search Banner: Search term', VisitEurope_Theme::KEY),
          'title' => _x('Looking for an event?', 'CTA Event Search Banner: Title', VisitEurope_Theme::KEY),
        ],
        'experienceSearchBanner' => [
          'button' => _x('Search', 'CTA Experience Search Banner: Button', VisitEurope_Theme::KEY),
          'destinationPlaceholder' => _x('Country or region', 'CTA Experience Search Banner: Destination placeholder', VisitEurope_Theme::KEY),
          'destinationGroupCountry' => _x('Country', 'CTA Experience Search Destination Group: Country', VisitEurope_Theme::KEY),
          'destinationGroupRegion' => _x('Region', 'CTA Experience Search Destination Group: Region', VisitEurope_Theme::KEY),
          'interestPlaceholder' => _x('Interests', 'CTA Experience Search Banner: Interests placeholder', VisitEurope_Theme::KEY),
          'title' => _x('Looking for an experience?', 'CTA Experience Search Banner: Title', VisitEurope_Theme::KEY),
          'typePlaceholder' => _x('Type of experience', 'CTA Experience Search Banner: Type placeholder', VisitEurope_Theme::KEY),
        ],
        'experienceSubmitBanner' => [
          'title' => _x('Want to feature your experience on Europe?', 'CTA Experience Submit Banner: Title', VisitEurope_Theme::KEY),
          'button' => _x('Submit an experience', 'CTA Experience Submit Banner: Button', VisitEurope_Theme::KEY),
        ],
        'newsletterBanner' => [
          'buttonExternal' => _x('See the Magazine', 'CTA Newsletter Banner: External button', VisitEurope_Theme::KEY),
          'buttonSeparator' => _x('or', 'CTA Newsletter Banner: Button separator', VisitEurope_Theme::KEY),
          'buttonSubmit' => _x('Register', 'CTA Newsletter Banner: Submit button', VisitEurope_Theme::KEY),
          'placeholder' => _x('Email address*', 'CTA Newsletter Banner: Input Placeholder', VisitEurope_Theme::KEY),
          'subtitle' => _x('Register to the newsletter here:', 'CTA Newsletter Banner: Subtitle', VisitEurope_Theme::KEY),
          'title' => _x('Want to know more about Europe?', 'CTA Newsletter Banner: Title', VisitEurope_Theme::KEY),
        ],
        'newsletterStrip' => [
          'buttonSubmit' => _x('Register', 'CTA Newsletter Strip: Submit button', VisitEurope_Theme::KEY),
          'placeholder' => _x('Email address*', 'CTA Newsletter Strip: Input Placeholder', VisitEurope_Theme::KEY),
          'subtitle' => _x('Register to the newsletter here:', 'CTA Newsletter Strip: Subtitle', VisitEurope_Theme::KEY),
          'title' => _x('Want to know more about Europe?', 'CTA Newsletter Strip: Title', VisitEurope_Theme::KEY),
          'confirmationTitle' => _x('THANK YOU', 'CTA Newsletter Strip: Confirmation title', VisitEurope_Theme::KEY),
          'confirmationMessage' => _x('for signing up to the Europe newsletter!', 'CTA Newsletter Strip: Confirmation message', VisitEurope_Theme::KEY),
        ]
      ],
      'date' => [
        'published' => [
          'format' => _x('D, d-F-Y', 'Published date format', VisitEurope_Theme::KEY), // Fri, 06-July-2014
          'string' => _x('Submitted on {date}', 'Published date string', VisitEurope_Theme::KEY),
        ],
        'event' => [
          'format' => _x('D, d M Y - H:i', 'Published date format', VisitEurope_Theme::KEY), // Fri, 06 Jul 2014, 22:00
        ]
      ],
      'destination' => [
        'carousel' => [
          'title' => _x('Top things to do in', 'Destination: Carousel title', VisitEurope_Theme::KEY),
        ],
        'information' => [
          'title' => _x('General information', 'Destination: General Information', VisitEurope_Theme::KEY),
        ],
        'questions' => [
          'back' => _x('Back to Essential information', 'Destination questions back', VisitEurope_Theme::KEY),
        ],
        'search' => [
          'tabs' => [
            'country' => _x('Countries', 'Destination search tab: Countries', VisitEurope_Theme::KEY),
            'region' => _x('Regions', 'Destination search tab: Regions', VisitEurope_Theme::KEY),
          ],
        ],
        'subtitle' => _x('Discover', 'Destination: Subtitle', VisitEurope_Theme::KEY),
        'tabs' => [
          'tab1' => _x('Overview', 'Destination tab 1 title', VisitEurope_Theme::KEY),
          'tab2' => _x('Places to go', 'Destination tab 2 title', VisitEurope_Theme::KEY),
          'tab3' => _x('Things to do', 'Destination tab 3 title', VisitEurope_Theme::KEY),
          'tab4' => _x('Essential Information', 'Destination tab 4 title', VisitEurope_Theme::KEY),
        ],
      ],
      'destinations' => [
        'countries' => [
          'title' => _x('By country', 'Destinations: Countries title', VisitEurope_Theme::KEY),
        ],
        'regions' => [
          'title' => _x('By region', 'Destinations: Regions title', VisitEurope_Theme::KEY),
        ],
        'subtitle' => _x('Discover', 'Destinations: Subtitle', VisitEurope_Theme::KEY),
        'title' => _x('Europe', 'Destinations: Title', VisitEurope_Theme::KEY),
        'recommended' => [
          'title' => _x('Other places you might like', 'Destinations: Recommended title', VisitEurope_Theme::KEY),
        ],
        'search' => [
          'all' => _x('All destinations', 'Destinations search: All', VisitEurope_Theme::KEY),
        ],
      ],
      'error' => [
        'api' => [
          'unknown' => _x('An unknown error has occurred.', 'Error: Unknown', VisitEurope_Theme::KEY),
          'unauthorized' => _x('You are unauthorized to complete this action.', 'Error: Unauthorized', VisitEurope_Theme::KEY),
          '404' => _x('Page not found.', 'Error: 404', VisitEurope_Theme::KEY),
          '401' => _x('Access denied.', 'Error: 401', VisitEurope_Theme::KEY),
          'mailchimp' => [
            'alreadySubscribed' => _x('You have already subscribed to our newsletter.', 'Errors Mailchimp: Already subscribed', VisitEurope_Theme::KEY),
            'unknown' => _x('Sorry an error occurred. Please try again.', 'Errors Mailchimp: Unknown', VisitEurope_Theme::KEY),
          ],
          'crowdriff' => [
            'unknown' => _x('Sorry an error occurred. Please try again.', 'Errors Crowdriff: Unknown', VisitEurope_Theme::KEY),
          ],
        ],
        '404' => [
          'title' => _x('Seems like you went too far...', '404: Title', VisitEurope_Theme::KEY),
          'subtitle' => _x('To go back to Europe click here', '404: Subtitle', VisitEurope_Theme::KEY),
          'button' => _x('Homepage', '404: Button text', VisitEurope_Theme::KEY),
        ],
      ],
      'experiences' => [
        'event' => [
          'create' => [
            'city' => _x('City', 'Experience Event Create: City label', VisitEurope_Theme::KEY),
            'country' => _x('Country', 'Experience Event Create: Country label', VisitEurope_Theme::KEY),
            'countryPlaceholder' => _x('Select', 'Experience Event Create: Country placeholder', VisitEurope_Theme::KEY),
            'dateFrom' => _x('Starting date', 'Experience Event Create: Date from label', VisitEurope_Theme::KEY),
            'dateTo' => _x('Ending date', 'Experience Event Create: Date to label', VisitEurope_Theme::KEY),
            'description' => _x('Description', 'Experience Event Create: Description label', VisitEurope_Theme::KEY),
            'images' => _x('Pictures', 'Experience Event Create: Images label', VisitEurope_Theme::KEY),
            'imagesDescription' => _x('JPG, GIF or PNG. Shots are to be at least 800x600 pixels. If your image is not the right dimensions, we will resize it.', 'Experience Event Create: Images description', VisitEurope_Theme::KEY),
            'interest' => _x('Interest', 'Experience Event Create: Interest label', VisitEurope_Theme::KEY),
            'interestPlaceholder' => _x('Select', 'Experience Event Create: Interest placeholder', VisitEurope_Theme::KEY),
            'links' => _x('Links', 'Experience Event Create: Links label', VisitEurope_Theme::KEY),
            'name' => _x('Name', 'Experience Event Create: Name label', VisitEurope_Theme::KEY),
            'submit' => _x('Submit', 'Experience Event Create: Submit button', VisitEurope_Theme::KEY),
            'upload' => _x('Upload', 'Experience Event Create: Upload', VisitEurope_Theme::KEY),
            'uploading' => _x('Uploading', 'Experience Event Create: Uploading', VisitEurope_Theme::KEY),
            'modal' => [
              'title' => _x('Thanks for submitting your experience.', 'Experience Event Create: Title', VisitEurope_Theme::KEY),
              'body' => get_field('submit_experience_success_message', 'option'),
              'experiences' => _x('Back to Experiences', 'Experience Event Create: Experiences button', VisitEurope_Theme::KEY),
              'home' => _x('Back to Homepage', 'Experience Event Create: Back button', VisitEurope_Theme::KEY),
            ],
          ],
        ],
        'types' => [
          'article' => _x('Article', 'Experience type: Article', VisitEurope_Theme::KEY),
          'event' => _x('Event', 'Experience type: Event', VisitEurope_Theme::KEY),
          'gallery' => _x('Gallery', 'Experience type: Gallery', VisitEurope_Theme::KEY),
          'product' => _x('Product', 'Experience type: Product', VisitEurope_Theme::KEY),
          'recipe' => _x('Recipe', 'Experience type: Recipe', VisitEurope_Theme::KEY),
          'route' => _x('Route', 'Experience type: Route', VisitEurope_Theme::KEY),
        ],
        'recipe' => [
          'ingredients' => [
            'cta' => _x('See Ingredients', 'Experience Recipe: Ingredients CTA', VisitEurope_Theme::KEY),
            'heading' => _x('Ingredients', 'Experience Recipe: Ingredients heading', VisitEurope_Theme::KEY),
          ],
        ],
      ],
      'interactiveMap' => [
        'label' => _x('Interactive Map', 'Interactive Map', VisitEurope_Theme::KEY),
        'backTo' => _x('back to', 'Interactive Map: Back to', VisitEurope_Theme::KEY),
        'viewFull' => _x('view full article', 'Interactive Map: View full article', VisitEurope_Theme::KEY),
      ],
      'filters' => [
        'all' => _x('All', 'Interests Filter', VisitEurope_Theme::KEY),
      ],
      'flexibleContent' => [
        'interestsGrid' => [
          'title' => _x('Choose your interests', 'Flexible Content: Interests title', VisitEurope_Theme::KEY),
        ],
        'pointers' => [
          'capitalCity' => _x('Capital City', 'Pointer title: Capital city', VisitEurope_Theme::KEY),
          'city' => _x('City', 'Pointer title: City', VisitEurope_Theme::KEY),
          'currency' => _x('Currency', 'Pointer title: Currency', VisitEurope_Theme::KEY),
          'language' => _x('Language', 'Pointer title: Language', VisitEurope_Theme::KEY),
          'moreInfo' => _x('Find out more', 'Pointer title: More info', VisitEurope_Theme::KEY),
          'timezone' => _x('Timezone', 'Pointer title: Timezone', VisitEurope_Theme::KEY),
          'where' => _x('Where is it from?', 'Pointer title: Where is it from', VisitEurope_Theme::KEY),
          'when' => _x('When is it?', 'Pointer title: When', VisitEurope_Theme::KEY),
        ],
        'about' => _x('About', 'About box: Title', VisitEurope_Theme::KEY),
      ],
      'inspireMe' => [
        'modal' => [
          'title' => _x("Can't decide where to go in Europe?", 'Inspire me modal: Title', VisitEurope_Theme::KEY),
          'body' => _x("Let us help you pick your ideal holiday destination. Give us an idea of what you like and we'll do the rest.", 'Inspire me modal: Body', VisitEurope_Theme::KEY),
          'required' => [
            'select1' => _x('Please select 1', 'Inspire me modal: Select 1', VisitEurope_Theme::KEY),
            'select2' => _x('Please select 2', 'Inspire me modal: Select 2', VisitEurope_Theme::KEY),
          ],
          'audiences' => [
            'title' => _x('I am travelling with...', 'Inspire me modal: Audiences title', VisitEurope_Theme::KEY),
          ],
          'seasons' => [
            'title' => _x('I want to go in...', 'Inspire me modal: Seasons title', VisitEurope_Theme::KEY),
          ],
          'temperatures' => [
            'title' => _x("I prefer when it's...", 'Inspire me modal: Temperature title', VisitEurope_Theme::KEY),
          ],
          'budgets' => [
            'title' => _x('My budget is...', 'Inspire me modal: Budget title', VisitEurope_Theme::KEY),
          ],
          'desires' => [
            'title' => _x('I would like to', 'Inspire me modal: Desires title', VisitEurope_Theme::KEY),
            'subtitle' => _x('Please select 2', 'Inspire me modal: Desires subtitle', VisitEurope_Theme::KEY),
          ],
          'button' => _x('Find my perfect trip', 'Inspire me modal: Button', VisitEurope_Theme::KEY),
          'processing' => [
            'title' => _x('Finding the ideal holidays for you', 'Inspire me modal: Processing title', VisitEurope_Theme::KEY),
            'subtitle' => _x('Loading', 'Inspire my modal: Processing subtitle', VisitEurope_Theme::KEY),
          ],
        ],
        'interactiveMap' => [
          'title' => _x('Let us inspire you', 'Inspire Me Interactive Map: title', VisitEurope_Theme::KEY),
          'subtitle' => _x('Discover Europe', 'Inspire Me Interactive Map: subtitle', VisitEurope_Theme::KEY),
        ],
        'search' => [
          'titleNoResults' => _x('Let us inspire you', 'Inspire Me results: title no results', VisitEurope_Theme::KEY),
          'title' => _x('Here is what we suggest', 'Inspire Me results: title', VisitEurope_Theme::KEY),
          'subtitle' => _x('We have :number recommendations for you', 'Inspire Me results: subtitle', VisitEurope_Theme::KEY),
          'subtitleNoResults' => _x('Discover Europe', 'Inspire Me results: subtitle no results', VisitEurope_Theme::KEY),
          'destinations' => _x('Destinations', 'Inspire Me results: Destinations header', VisitEurope_Theme::KEY),
          'trips' => _x('Trips', 'Inspire Me results: Trips header', VisitEurope_Theme::KEY),
          'sentence' => _x('I want to travel with :audience in :season, where it is :temperature with a :budget budget to :desire1 and :desire2', 'Inspire Me results: Sentence', VisitEurope_Theme::KEY),
          'noResults' => _x('Sorry, but nothing matched your search. Please try again with some different options.', 'Inspire Me results: Nothing found', VisitEurope_Theme::KEY),
          'redoQuiz' => _x('Redo Quiz', 'Inspire Me results: Redo quiz', VisitEurope_Theme::KEY),
        ],
      ],
      'months' => [
        'jan' => _x('January', 'Months: Jan', VisitEurope_Theme::KEY),
        'feb' => _x('February', 'Months: Feb', VisitEurope_Theme::KEY),
        'mar' => _x('March', 'Months: Mar', VisitEurope_Theme::KEY),
        'apr' => _x('April', 'Months: Apr', VisitEurope_Theme::KEY),
        'may' => _x('May', 'Months: May', VisitEurope_Theme::KEY),
        'jun' => _x('June', 'Months: Jun', VisitEurope_Theme::KEY),
        'jul' => _x('July', 'Months: Jul', VisitEurope_Theme::KEY),
        'aug' => _x('August', 'Months: Aug', VisitEurope_Theme::KEY),
        'sep' => _x('September', 'Months: Sep', VisitEurope_Theme::KEY),
        'oct' => _x('October', 'Months: Oct', VisitEurope_Theme::KEY),
        'nov' => _x('November', 'Months: Nov', VisitEurope_Theme::KEY),
        'dec' => _x('December', 'Months: Dec', VisitEurope_Theme::KEY),
      ],
      'myTrips' => [
        'header' => [
          'back' => _x('back to my trips', 'My Trips Header: Back', VisitEurope_Theme::KEY),
          'name' => _x('Name', 'My Trips Header: Name label', VisitEurope_Theme::KEY),
          'save' => _x('Save', 'My Trips Header: Name label', VisitEurope_Theme::KEY),
        ],
        'preview' => [
          'create' => _x('Create trip', 'My Trips: Create trip', VisitEurope_Theme::KEY),
          'empty' => _x('You can now save articles in this trip', 'My Trips: Empty trip', VisitEurope_Theme::KEY),
          'saved' => _x('saved', 'My Trips: Saved', VisitEurope_Theme::KEY),
        ],
        'plannedTripsTitle' => _x('My Europe Trips', 'My Trips: Planned trips title', VisitEurope_Theme::KEY),
        'suggestedTripsTitle' => _x('Suggested Trips', 'My Trips: Suggested trips title', VisitEurope_Theme::KEY),
        'items' => [
          'addEvent' => _x('Add your own event', 'My Trips Items: Add event', VisitEurope_Theme::KEY),
          'tabs' => [
            'all' => _x('All', 'My Trips Items Tabs: All', VisitEurope_Theme::KEY),
            'places' => _x('Places to go', 'My Trips Items Tabs: Places', VisitEurope_Theme::KEY),
            'things' => _x('Things to do', 'My Trips Items Tabs: Things', VisitEurope_Theme::KEY),
          ],
          'title' => _x('Saved places', 'My Trips Items: Title', VisitEurope_Theme::KEY),
        ],
        'modal' => [
          'close' => _x('Go back to page', 'My Trips Modal: Close', VisitEurope_Theme::KEY),
          'create' => [
            'cancel' => _x('Cancel', 'My Trips Modal Create: Cancel button', VisitEurope_Theme::KEY),
            'create' => _x('Create', 'My Trips Modal Create: Create button', VisitEurope_Theme::KEY),
            'name' => _x('Name', 'My Trips Modal Create: Name label', VisitEurope_Theme::KEY),
            'title' => _x('Create your own trip', 'My Trips Modal Create: Title', VisitEurope_Theme::KEY),
          ],
          'save' => [
            'title' => _x('Saved to', 'My Trips Modal Save: Title', VisitEurope_Theme::KEY),
            'trips' => _x('Go to my trips', 'My Trips Modal Save: Trips button', VisitEurope_Theme::KEY),
          ],
          'select' => [
            'allTrips' => _x('All Trips', 'My Trips Modal Select: All trips', VisitEurope_Theme::KEY),
            'create' => _x('Create a new trip', 'My Trips Modal Select: Create button', VisitEurope_Theme::KEY),
            'empty' => _x('Please create a trip to continue.', 'My Trips Modal Select: No trips', VisitEurope_Theme::KEY),
            'emptySearch' => _x('No trips match your search.', 'My Trips Modal Select: Empty search', VisitEurope_Theme::KEY),
            'title' => _x('Choose trip', 'My Trips Modal Select: Title', VisitEurope_Theme::KEY),
            'search' => _x('Search', 'My Trips Modal Select: Search', VisitEurope_Theme::KEY),
          ],
        ],
      ],
      'navbar' => [
        'inspireMe' => _x('Inspire me', 'Navbar: Inspire me', VisitEurope_Theme::KEY),
        'logout' => _x('Log out', 'Navbar: Log out', VisitEurope_Theme::KEY),
        'myAccount' => _x('My Account', 'Navbar: My Account', VisitEurope_Theme::KEY),
        'myTrips' => _x('My Trips', 'Navbar: My trips', VisitEurope_Theme::KEY),
        'search' => _x('Search', 'Navbar: Search', VisitEurope_Theme::KEY),
      ],
      'search' => [
        'results' => _x('Results', 'Search subtitle', VisitEurope_Theme::KEY),
        'placeholder' => _x('Search', 'Search: Placeholder', VisitEurope_Theme::KEY),
        'destinations' => _x('Destinations', 'Search: Destinations header', VisitEurope_Theme::KEY),
        'experiences' => _x('Experiences', 'Search: Experiences header', VisitEurope_Theme::KEY),
        'trips' => _x('Trips', 'Search: Trips header', VisitEurope_Theme::KEY),
        'noResults' => _x('Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'Search: Search nothing found', VisitEurope_Theme::KEY),
        'eventsNoResults' => _x('Sorry, no matches found.  Please adjust your search terms and try again.', 'Search: Events nothing found', VisitEurope_Theme::KEY),
        'experiencesNoResults' => _x('Sorry, no matches found.  Please adjust your search terms and try again.', 'Search: Experiences nothing found', VisitEurope_Theme::KEY),
      ],
      'seasons' => [
        'summer' => _x('Summer', 'Seasons: Summer', VisitEurope_Theme::KEY),
        'autumn' => _x('Autumn', 'Seasons: Autumn', VisitEurope_Theme::KEY),
        'winter' => _x('Winter', 'Seasons: Winter', VisitEurope_Theme::KEY),
        'spring' => _x('Spring', 'Seasons: Spring', VisitEurope_Theme::KEY),
      ],
      'sectionHeader' => [
        'link'=> _x('View all', 'CTA Section header link', VisitEurope_Theme::KEY)
      ],
      'social' => [
        'share' => _x('Share', 'Social: Share', VisitEurope_Theme::KEY),
        'facebook' => _x('Like us on Facebook', 'Social: Facebook', VisitEurope_Theme::KEY),
        'googleplus' => _x('Follow us on GooglePlus', 'Social: Google Plus', VisitEurope_Theme::KEY),
        'instagram' => _x('Follow us on Instagram', 'Social: Instagram', VisitEurope_Theme::KEY),
        'pinterest' => _x('Follow us on Pinterest', 'Social: Instagram', VisitEurope_Theme::KEY),
        'twitter' => _x('Follow us on Twitter', 'Social: Twitter', VisitEurope_Theme::KEY),
        'youtube' => _x('Watch us on YouTube', 'Social: YouTube', VisitEurope_Theme::KEY),
      ],
      'temperatures' => [
        'cold' => _x('Cold (0-10°C)', 'Weather temp: Cold', VisitEurope_Theme::KEY),
        'mild' => _x('Mild (1-20°C)', 'Weather temp: Mild', VisitEurope_Theme::KEY),
        'warm' => _x('Warm (20-25°C)', 'Weather temp: Warm', VisitEurope_Theme::KEY),
        'hot' => _x('Hot (25°C+)', 'Weather temp: Hot', VisitEurope_Theme::KEY),
      ],
      'trip' => [
        'tag' => _x('Trip', 'Trip: Tag label', VisitEurope_Theme::KEY),
        'day' => _x('Day', 'Trip: Day label', VisitEurope_Theme::KEY),
        'distance' => [
          'label' => _x('Distance', 'Trip: Distance label', VisitEurope_Theme::KEY),
          'km' => _x('km', 'Trip: Kilometers label', VisitEurope_Theme::KEY),
          'mi' => _x('mi', 'Trip: Miles label', VisitEurope_Theme::KEY),
        ],
        'onTheWay' => _x('On the way', 'Trip: On the way label', VisitEurope_Theme::KEY),
        'beforeYouGo' => _x('Before you go', 'Trip: Before you go label', VisitEurope_Theme::KEY),
      ],
      'trips' => [
        'title' => _x('Trips', 'Trips interactive map: Title', VisitEurope_Theme::KEY),
        'subtitle' => _x('Discover', 'Trips interactive map: Subtitle', VisitEurope_Theme::KEY),
      ],
      'userVerification' => [
        'login' => [
          'title' => _x('Sign In', 'User Verification Login: Title', VisitEurope_Theme::KEY),
          'email' => _x('Email address', 'User Verification Login: Email label', VisitEurope_Theme::KEY),
          'password' => _x('Password', 'User Verification Login: Password label', VisitEurope_Theme::KEY),
          'forgot' => _x('Forgot your password? :link', 'User Verification Login: Forgot Label', VisitEurope_Theme::KEY),
          'forgotLink' => _x('Click here', 'User Verification Login: Forgot Link', VisitEurope_Theme::KEY),
          'login' => _x('Sign In', 'User Verification Login: Login button', VisitEurope_Theme::KEY),
          'optionsSeparator' => _x('or', 'User Verification Login: Options separator', VisitEurope_Theme::KEY),
          'facebook' => _x('Sign in with', 'User Verification Login: Social button', VisitEurope_Theme::KEY),
          'google' => _x('Sign in with', 'User Verification Login: Social button', VisitEurope_Theme::KEY),
          'register' => _x('Not registered yet? :link', 'User Verification Login: Register label', VisitEurope_Theme::KEY),
          'registerLink' => _x('Sign up here', 'User Verification Login: Register Link', VisitEurope_Theme::KEY),
          'error' => [
            'invalid' => _x('Invalid credentials. Please try again.', 'User Verification: Error invalid credentials', VisitEurope_Theme::KEY),
            'noAccount' => _x("It looks like you don't have an account with us yet. Please sign up to continue.", 'User Verification: Error no account', VisitEurope_Theme::KEY),
            'unverified' => _x('Your account has not been verified. Please check your email.', 'User Verification: Error unverified account', VisitEurope_Theme::KEY),
          ],
        ],
        'register' => [
          'title' => _x('Sign Up', 'User Verification Register: Title', VisitEurope_Theme::KEY),
          'body' => get_field('user_verification_register_body', 'option'),
          'email' => _x('Email address', 'User Verification Register: Email label', VisitEurope_Theme::KEY),
          'password' => _x('Password', 'User Verification Register: Password label', VisitEurope_Theme::KEY),
          'confirmPassword' => _x('Confirm password', 'User Verification Register: Confirm password label', VisitEurope_Theme::KEY),
          'register' => _x('Sign Up', 'User Verification Register: Register button', VisitEurope_Theme::KEY),
          'success' => [
            'body' => get_field('user_verification_register_success_message', 'option'),
          ],
          'error' => [
            'unknown' => _x('Your account could not be created. Please try again.', 'User Verification Register: Error unknown', VisitEurope_Theme::KEY),
            'unique' => _x('A user already exists with that email address.', 'User Verification Register: Error unique', VisitEurope_Theme::KEY)
          ],
        ],
        'verified' => [
          'body' => get_field('user_verification_verified_body', 'option'),
          'homeButton' => _x('Go to homepage', 'User Verification Verified: Home button', VisitEurope_Theme::KEY),
          'title' => _x('Thank you for your registration.', 'User Verification Verified: Title', VisitEurope_Theme::KEY),
          'tripsButton' => _x('Go to your trips', 'User Verification Verified: Trips button', VisitEurope_Theme::KEY),
        ],
        'forgot' => [
          'title' => _x('Forgot Your Password', 'User Verification Forgot: Title', VisitEurope_Theme::KEY),
          'body' => get_field('user_verification_forgot_body', 'option'),
          'email' => _x('Email address', 'User Verification Forgot: Email label', VisitEurope_Theme::KEY),
          'button' => _x('Get New Password', 'User Verification Forgot: Submit button', VisitEurope_Theme::KEY),
          'success' => [
            'body' => get_field('user_verification_forgot_success_message', 'option'),
          ],
          'error' => [
            'unknown' => _x('There was an error sending you a password reset email. Please try again.', 'User Verification Forgot: Error unknown', VisitEurope_Theme::KEY),
          ],
        ],
        'reset' => [
          'title' => _x('Create a new password', 'User Verification Reset: Title', VisitEurope_Theme::KEY),
          'body' => get_field('user_verification_reset_body', 'option'),
          'password' => _x('New password', 'User Verification Reset: Password label', VisitEurope_Theme::KEY),
          'confirmPassword' => _x('Confirm new password', 'User Verification Reset: Confirm new password label', VisitEurope_Theme::KEY),
          'button' => _x('Create Password', 'User Verification Reset: Submit button', VisitEurope_Theme::KEY),
          'success' => [
            'title' => _x('Thank you.', 'User Verification Reset: Success title', VisitEurope_Theme::KEY),
            'body' => get_field('user_verification_reset_success_message', 'option'),
            'button' => _x('Sign In', 'User Verification Reset: Success sign-in button', VisitEurope_Theme::KEY),
          ],
        ],
      ],
      'validation' => [
        'between' => _x('The :field field must be between :min and :max.', 'Validation: Between', VisitEurope_Theme::KEY),
        'email' => _x('The :field field must be a valid email address.', 'Validation: Email', VisitEurope_Theme::KEY),
        'generic' => _x('The :field field is invalid.', 'Validation: Generic', VisitEurope_Theme::KEY),
        'maxLength' => _x('The :field field must be less than :max characters.', 'Validation: Max length', VisitEurope_Theme::KEY),
        'minLength' => _x('The :field field must be more than :min characters.', 'Validation: Min length', VisitEurope_Theme::KEY),
        'numeric' => _x('The :field field must be a valid number.', 'Validation: Numeric', VisitEurope_Theme::KEY),
        'required' => _x('The :field field is required.', 'Validation: Required', VisitEurope_Theme::KEY),
        'sameAs' => _x('The :field field does not match :same.', 'Validation: Same as', VisitEurope_Theme::KEY),
        'fields' => [
          'password' => _x('password', 'Form field: Password', VisitEurope_Theme::KEY),
          'confirmPassword' => _x('confirm password', 'Form field: Confirm password', VisitEurope_Theme::KEY),
          'email' => _x('email', 'Form field: Email', VisitEurope_Theme::KEY),
          'dateFrom' => _x('date from', 'Form field: Date from', VisitEurope_Theme::KEY),
          'dateTo' => _x('date to', 'Form field: Date to', VisitEurope_Theme::KEY),
          'description' => _x('description', 'Form field: Description', VisitEurope_Theme::KEY),
          'destination' => _x('destination', 'Form field: Destination', VisitEurope_Theme::KEY),
          'searchTerm' => _x('search term', 'Form field: Search term', VisitEurope_Theme::KEY),
          'interest' => _x('interest', 'Form field: Interest', VisitEurope_Theme::KEY),
          'type' => _x('type', 'Form field: Type', VisitEurope_Theme::KEY),
        ],
      ],
      'weather' => [
        'heading' => _x('Climate', 'Weather: Heading', VisitEurope_Theme::KEY),
        'tabs' => [
          'months' => _x('Monthly', 'Weather: Nav monthly', VisitEurope_Theme::KEY),
          'seasons' => _x('Seasonally', 'Weather: Nav seasonally', VisitEurope_Theme::KEY),
        ],
      ],
    ];
  }

  /**
   * Returns an array of urls used for JS
   *
   * @return array
   */
  public static function getUrls() {
    $urlBase = get_site_url('', null) . '/' . ICL_LANGUAGE_CODE . '/';

    return [
      '_wpnonce' => wp_create_nonce('wp_rest'),
      'base' => $urlBase,
      'api' => $urlBase . 'wp-json/' . VisitEurope_API::NAMESPACE . '/',
      'theme' => get_template_directory_uri(),
      'pages' => [
        'account' => $urlBase . 'account',
        'accountTrips' => $urlBase . 'account/trips',
        'accountTripsCreate' => $urlBase . '/account/trips/create',
        'destinations' => $urlBase . 'destinations',
        'destinationsCountries' => $urlBase . 'destinations/countries',
        'destinationsRegions' => $urlBase . 'destinations/regions',
        'essentialInformation' => $urlBase . 'essential-information',
        'events' => $urlBase . 'events',
        'eventsSearch' => $urlBase . 'events/search',
        'experiences' => $urlBase . 'experiences',
        'experiencesSearch' => $urlBase . 'experiences/search',
        'experiencesEventCreate' => $urlBase . 'events/create',
        'inspireMeSearch' => $urlBase . 'inspire-me',
        'interests' => $urlBase . 'interests',
        'trips' => $urlBase . 'trips',
        'search' => $urlBase . 'search',
        'magazine' => 'https://visiteurope.cld.bz/#tq19tTu',
      ],
      'services' => [
        'facebook' => [
          'appID' => get_field('facebook_app_id', 'option'),
        ],
        'google' => [
          'appID' => get_field('google_app_id', 'option'),
        ],
      ],
    ];
  }

}
