<?php
if (!defined('ABSPATH')) exit;

/**
 * Helper functions for Visit Europe
 *
 * @package Visit_Europe
 */
class VisitEurope_Helper {

  /**
   * Converts an array of posts to an array of ID => post_title
   *
   * @param array $posts
   * @return array
   */
  public static function postsToList($posts) {
    $list = [];

    if (is_array($posts) && !empty($posts)) {
      foreach ($posts as $item) {
        if (isset($item->ID) && isset($item->post_title)) {
          $list[$item->ID] = $item->post_title;
        }
      }
    }

    return $list;
  }

  /**
   * Converts an array of posts to an array of IDs
   *
   * @param array $posts
   * @return array
   */
  public static function postsToIDs($posts) {
    $list = [];

    if (is_array($posts) && !empty($posts)) {
      $list = wp_list_pluck($posts, 'ID');
    }

    return $list;
  }

  /**
   * Converts an array of posts to an array of ID => post_title
   *
   * @param array $items
   * @param string $valueKey
   * @param string $keyKey
   * @return array
   */
  public static function arrayToList($items, $valueKey, $keyKey = null) {
    $list = [];

    if (is_array($items) && !empty($items)) {
      foreach ($items as $item) {
        if (!isset($item[$valueKey])) {
          continue;
        }

        if (!is_null($keyKey) && isset($item[$keyKey])) {
          $list[$item[$keyKey]] = $item[$valueKey];
        } else {
          $list[] = $item[$valueKey];
        }
      }
    }

    return $list;
  }

  /**
   * Returns an array of valid IDs, aka an integer > 0
   *
   * @param array $array
   * @return array
   */
  public static function validIDsFromArray($array) {
    $postIDs = [];

    if (!is_array($array) || empty($array)) {
      return $postIDs;
    }

    foreach ($array as $item) {
      $id = intval($item);

      if ($id > 0) {
        $postIDs[] = $id;
      }
    }

    return $postIDs;
  }

  /**
   * Returns a merged array of parameters for a query
   *
   * @param array $defaultParams
   * @param array $customParams
   * @return array
   */
  public static function paramsForQuery($defaultParams = [], $customParams = []) {
    if (!is_array($defaultParams) || !is_array($customParams)) {
      return [];
    }

    return array_merge($defaultParams, $customParams);
  }

  /**
   * Transforms an array into an object ready for echoing to JS
   *
   * @param array $array
   * @return string
   */
  public static function arrayToObjectString($array) {
    if (!is_array($array)) {
      return '';
    }

    return str_replace("'", "\u2019", json_encode($array));
  }

  /**
   * Returns a formatted function name from the given string
   *
   * @param string $name
   * @return string
   */
  public static function getFunctionNameFromString($name) {
    return lcfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', $name))));
  }

  /**
   * Get the type of post
   *
   * @param WP_Post $post
   * @return string
   */
  public static function getTypeFromPost($post) {
    $type = '';

    switch ($post->post_type) {
      case VisitEurope_CPT_Destination::POST_TYPE:
        $type = VisitEurope_Destination::getTypeForPostID($post->ID);
        break;
      case VisitEurope_CPT_Experience::POST_TYPE:
        $type = VisitEurope_Experience::getTypeForPostID($post->ID);
        break;
      default:
        break;
    }

    return $type;
  }

  /**
   * Returns a rendered template part with local variables
   *
   * @param string $template
   * @param array $data
   * @return void;
   */
  public static function getTemplatePart($template, $data = []) {
    $filename = 'template-parts/' . $template . '.php';

    if (empty(locate_template($filename)) || !is_array($data)) {
      return '';
    }

    extract($data);

    ob_start();
    include(locate_template($filename));
    return ob_get_clean();
  }

  /**
   * Returns the url for a posts image at a given size
   *
   * @param integer $postID
   * @param string $size
   * @return string
   */
  public static function getPostThumbnailUrl($postID, $size) {
    $thumbnailID = get_post_thumbnail_id(intval($postID));
    $imageArray = wp_get_attachment_image_src($thumbnailID, $size);

    if (!is_array($imageArray) || !isset($imageArray[0])) {
      return '';
    }

    return $imageArray[0];
  }

  /**
   * Converts a string to an integer, or null if empty
   *
   * @param string $value
   * @return mixed
   */
  public static function convertToIntOrNull($value) {
    if (empty($value) && $value != '0') {
      return null;
    }

    return intval($value);
  }

  /**
   * Returns the published date in the localised format
   *
   * @param string $date
   * @return string
   */
  public static function getLocalisedPublishedDateString($date) {
    $string = VisitEurope_Content::getLocale('date.published.string');
    $dateFormat = VisitEurope_Content::getLocale('date.published.format');

    return self::getLocalisedDateString($string, $date, $dateFormat);
  }

  /**
   * Return a date in the localised format
   *
   * @param string $string
   * @param string $dateString
   * @param string $dateFormat
   * @return string
   */
  public static function getLocalisedDateString($string, $dateString, $dateFormat) {
    $timestamp = strtotime($dateString);
    $date = date($dateFormat, $timestamp);

    if ($timestamp && $date) {
      return str_replace('{date}', $date, $string);
    }

    return null;
  }

  /**
   * Replace the apostrophie with a html entity for use with JS
   *
   * @param string $string
   * @return string
   */
  public static function formatStringForFrontend($string) {
    $find = ["'", '<br />', 'class='];
    $replace = ["&#700;", '', 'data-class='];

    return str_replace($find, $replace, html_entity_decode($string, ENT_QUOTES));
  }

  /**
   * Returns the slug for the given string
   *
   * @param string $string
   * @return void
   */
  public static function getSlug($string) {
    return sanitize_title($string);
  }

  /**
   * Force throw a 404 error
   * https://richjenks.com/wordpress-throw-404/
   *
   * @return void
   */
  public static function throw404() {
    global $wp_query;
  	$wp_query->set_404();

  	add_action('wp_title', function () {
  			return _x('Not found', '404 header', VisitEurope_Theme::KEY);
  	}, 9999);

  	status_header(404);
  	nocache_headers();

  	require get_404_template();
  	exit;
  }

  /**
   * Returns the View All url for feature / standard grids with the following differences
   *  - URLs for Pages or Destinations (if destination, read more on destination)
   *  - Apply optional interest groups
   *
   * @param WP_Post $post
   * @param string $type
   * @param string $interestGroupName
   * @return string
   */
  public static function getViewAllUrl($post, $type, $interestGroupName = null) {
    $url = null;
    $params = [];

    // Add destination if necessary
    if ($post->post_type === VisitEurope_CPT_Destination::POST_TYPE) {
      $params['destination'] = $post->post_name;
    }

    // Add interest if necessary
    if (!empty($interestGroupName) && $interestGroupName != 'All') {
      $params['interest'] = self::getSlug($interestGroupName);
    }

    // Get the base url
    switch ($type) {
      case VisitEurope_Destination::TYPE_COUNTRY:
        $url = VisitEurope_Content::getUrl('pages.destinationsCountries');
        break;
      case VisitEurope_Destination::TYPE_REGION:
        $url = VisitEurope_Content::getUrl('pages.destinationsRegions');
        break;
      case VisitEurope_Experience::TYPE_ALL:
        if (isset($params['destination']) || isset($params['interest'])) {
          $url = VisitEurope_Content::getUrl('pages.experiencesSearch');
        } else {
          $url = VisitEurope_Content::getUrl('pages.experiences');
        }
        break;
      case VisitEurope_Experience::TYPE_ARTICLE:
        $url = VisitEurope_Content::getUrl('pages.experiencesSearch');
        $params['type'] = 'article';
        break;
      case VisitEurope_Experience::TYPE_EVENT:
        $url = VisitEurope_Content::getUrl('pages.experiencesSearch');
        $params['type'] = 'event';
        break;
      case VisitEurope_Experience::TYPE_GALLERY:
        $url = VisitEurope_Content::getUrl('pages.experiencesSearch');
        $params['type'] = 'gallery';
        break;
      case VisitEurope_Experience::TYPE_RECIPE:
        $url = VisitEurope_Content::getUrl('pages.experiencesSearch');
        $params['type'] = 'recipe';
        break;
      case VisitEurope_Experience::TYPE_ROUTE:
        $url = VisitEurope_Content::getUrl('pages.experiencesSearch');
        $params['type'] = 'route';
        break;
      case VisitEurope_Trip::TYPE_ALL:
        $url = VisitEurope_Content::getUrl('pages.trips');
        break;
      default:
        break;
    }

    return $url . (empty($params) ? null : '?' . http_build_query($params));
  }

  /**
   * Returns the copyright for the given post thumbnail
   *
   * @param integer $postID
   * @return string
   */
  public static function getPostThumbnailCopyright($postID) {
    $thumbnailID = get_post_thumbnail_id(intval($postID));
    return self::getAttachmentCopyright($thumbnailID);
  }

  /**
   * Returns the copyright url for the given post thumbnail
   *
   * @param integer $postID
   * @return string
   */
  public static function getPostThumbnailCopyrightUrl($postID) {
    $thumbnailID = get_post_thumbnail_id(intval($postID));
    return self::getAttachmentCopyrightUrl($thumbnailID);
  }

  /**
   * Returns the copyright for the given post thumbnail
   *
   * @param integer $postID
   * @return string
   */
  public static function getAttachmentCopyright($postID) {
    $copyright = '';

    if ($postID && intval($postID) > 0) {
      $postMeta = get_post_meta($postID, VisitEurope_Theme::KEY_MEDIA_COPYRIGHT, true);

      if ($postMeta && !empty($postMeta)) {
        $copyright = '&copy; ' . $postMeta;
      }
    }

    return $copyright;
  }

  /**
   * Returns the copyright url for the given post thumbnail
   *
   * @param integer $postID
   * @return string
   */
  public static function getAttachmentCopyrightUrl($postID) {
    $copyrightUrl = '#';

    if ($postID && intval($postID) > 0) {
      $postMeta = get_post_meta($postID, VisitEurope_Theme::KEY_MEDIA_COPYRIGHT_URL, true);

      if ($postMeta && !empty($postMeta)) {
        $copyrightUrl = $postMeta;
      }
    }

    return $copyrightUrl;
  }

  /**
   * Send text email
   *
   * @param array $emails
   * @param string $subject
   * @param string $message
   * @param array $headers
   * @param array $attachments
   * @return boolean
   */
  public static function sendTextEmail($emails, $subject, $message, $headers = [], $attachments = []) {
    // Ensure we have actual data
    if (!is_array($emails) || empty($emails) || !is_string($subject) || empty($subject)
      || !is_string($message) || empty($message) || !is_array($headers) || !is_array($attachments)) {
      return false;
    }

    return wp_mail($emails, $subject, $message, $headers, $attachments);
  }

  /**
   * Send text email
   *
   * @param array $emails
   * @param string $subject
   * @param string $message
   * @param array $headers
   * @param array $attachments
   * @return boolean
   */
  public static function sendHtmlEmail($emails, $subject, $message, $headers = [], $attachments = []) {
    // Ensure we have actual data
    if (!is_array($emails) || empty($emails) || !is_string($subject) || empty($subject)
      || !is_string($message) || empty($message) || !is_array($headers) || !is_array($attachments)) {
      return false;
    }

    add_filter('wp_mail_content_type', [self::class, 'htmlEmailContentType']);
    $sent = wp_mail($emails, $subject, $message, $headers, $attachments);
    remove_filter('wp_mail_content_type', [self::class, 'htmlEmailContentType']);

    return $sent;
  }

  /**
   * Return the content type for html email
   *
   * @return string
   */
  public static function htmlEmailContentType() {
    return 'text/html';
  }

  /**
   * Returns a specific verification attribute
   *
   * @param string $attribute
   * @return string
   */
  public static function getAttributeFromGET($attribute) {
    if (isset($_GET[$attribute]) && !empty(trim($_GET[$attribute]))) {
      return esc_sql(trim($_GET[$attribute]));
    }

    return '';
  }

  /**
   * Determines whether or not the server is https
   *
   * @return bool
   */
  public static function isSecure() {
    return (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off')
      || intval($_SERVER['SERVER_PORT']) == 443;
  }
}
