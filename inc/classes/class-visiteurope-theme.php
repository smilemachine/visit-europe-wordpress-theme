<?php
if (!defined('ABSPATH')) exit;

/**
 * Theme functions for Visit Europe
 *
 * @package Visit_Europe
 */
class VisitEurope_Theme {

  const KEY = 'visit-europe-wordpress-theme';
  const KEY_MEDIA_COPYRIGHT = 've_media_copyright';
  const KEY_MEDIA_COPYRIGHT_URL = 've_media_copyright_url';

  protected $version;

  public function __construct() {
    if ($theme = wp_get_theme()) {
      $this->version = $theme->get('Version');
    }
  }

  /**
   * Sets up the Theme
   *
   * @return void
   */
  public function setup() {
    // Setup
    $this->setupRequirements();
    $this->setupCustomImageSizes();
    $this->setupCustomPostTypes();
    $this->setupCustomShortcodes();
    $this->setupCustomTaxonomies();
    $this->setupOptions();

    add_action('after_setup_theme', [$this, 'afterSetup']);

    // Clean up admin
    add_action('admin_menu', [$this, 'removeMenuItems']);

    // Styles
    add_action('wp_enqueue_scripts', [$this, 'setupPublicStyles']);
    add_action('admin_enqueue_scripts', [$this, 'setupAdminStyles']);

    // Scripts
    add_action('wp_enqueue_scripts', [$this, 'setupPublicScripts']);
    add_action('admin_enqueue_scripts', [$this, 'setupAdminScripts']);

    // Advanced Custom Fields
    add_filter('acf/settings/save_json', [$this, 'setupACFLocalJson']);
    add_filter('acf/fields/post_object/query', [$this, 'acfPostObjectQuery'], 10, 3);
    add_filter('acf/load_field/name=inspire_me_interest_group', [$this, 'acfLoadFieldInspireMeInterestGroups'], 10, 3);
    add_filter('acf/load_field/name=crowdriff_albums', [$this, 'acfLoadFieldCrowdriffAlbums'], 10, 3);

    // Extra user meta data
    add_action('show_user_profile', [$this, 'renderExtraUserProfileFields'], 10, 2);
    add_action('edit_user_profile', [$this, 'renderExtraUserProfileFields'], 10, 2);
    add_action('edit_user_profile_update', [$this, 'saveExtraUserProfileFields'], 10, 2);

    // Extra attachment meta data
    add_filter('attachment_fields_to_edit', [$this, 'setupAttachmentFieldsToEdit'], 10, 2);
    add_filter('attachment_fields_to_save', [$this, 'setupAttachmentFieldsToSave'], 10, 2);
    add_filter('edit_attachment', [$this, 'setupEditAttachment'], 10, 2);

    // Rendering
    add_filter('oembed_dataparse', [$this, 'setupResponsiveVideos'], 10, 2);
    add_filter('body_class', [$this, 'setupBodyClasses']);
    add_filter('admin_body_class', [$this, 'setupAdminBodyClasses']);
    add_action('wp_head', [$this, 'addCustomHeaderCode']);

    // Custom mailer
    add_action('phpmailer_init', [$this, 'setupCustomMailer']);
  }

  /**
   * Setup the theme requirements
   *
   * @return void
   */
  private function setupRequirements() {
    if (!is_admin()) {
      if (!function_exists('icl_get_languages')) {
        die('This theme requires WPML');
      }

      if (!function_exists('get_field')) {
        die('This theme requires Advanced Custom Fields');
      }
    }
  }

  /**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 *
   * @return void
   */
  public function afterSetup() {
		load_theme_textdomain(VisitEurope_Theme::KEY, get_template_directory() . '/languages');
		add_theme_support('automatic-feed-links');
		add_theme_support('title-tag');
		add_theme_support('post-thumbnails');

		register_nav_menus([
			'menu-header' => esc_html_x('Header', 'Header menu name', VisitEurope_Theme::KEY),
			'menu-footer' => esc_html_x('Footer', 'Footer menu name', VisitEurope_Theme::KEY),
		]);

		add_theme_support('html5', [
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		]);
  }

  /**
   * Setup the public scripts
   *
   * @return void
   */
  public function setupPublicScripts() {
    wp_enqueue_script('visit-europe-vendor-scripts', get_template_directory_uri() . '/assets/js/vendor.js?theme=' . $this->version, ['jquery', 'jquery-ui-core'], null, true);
    wp_enqueue_script('visit-europe-app-scripts', get_template_directory_uri() . '/assets/js/app.js?theme=' . $this->version, ['visit-europe-vendor-scripts'], null, true);
    wp_localize_script('visit-europe-app-scripts', 'VisitEuropeLocales', VisitEurope_Content::getLocales());
    wp_localize_script('visit-europe-app-scripts', 'VisitEuropeUrls', VisitEurope_Content::getUrls());
  }

  /**
   * Setup the public styles
   *
   * @return void
   */
  public function setupPublicStyles() {
    wp_enqueue_style('visit-europe-public-style', get_stylesheet_uri());
  	wp_enqueue_style('visit-europe-public-styles', get_template_directory_uri() . '/assets/css/app.css?theme=' . $this->version);
  }

  /**
   * Setup the admin scripts
   *
   * @return void
   */
  public function setupAdminScripts() {
    wp_enqueue_script('vuejs-min', '//cdnjs.cloudflare.com/ajax/libs/vue/2.5.13/vue.min.js', [], null, false);
    wp_enqueue_script('vue-resource-min', '//cdnjs.cloudflare.com/ajax/libs/vue-resource/1.3.4/vue-resource.min.js', [], null, false);
    wp_enqueue_script('visit-europe-admin-scripts', get_template_directory_uri() . '/assets/js/admin.js?theme=' . $this->version, [], null, false);
    wp_localize_script('visit-europe-admin-scripts', 'VisitEuropeUrls', VisitEurope_Content::getUrls());
  }

  /**
   * Setup the admin styles
   *
   * @return void
   */
  public function setupAdminStyles() {
    wp_enqueue_style('visit-europe-admin-styles', get_template_directory_uri() . '/assets/css/admin.css?theme=' . $this->version, [], null, false);
  }

  /**
   * Return the path for ACF Local Json
   *
   * @return void
   */
  public function setupACFLocalJson() {
    return get_stylesheet_directory() . '/acf-json';
  }

  /**
   * Force post object queries to return English values
   *
   * @param array $args
   * @param array $field
   * @param integer $postID
   * @return array
   */
  public function acfPostObjectQuery($args, $field, $postID) {
    $forceEnglish = false;

	  if (is_array($field) && isset($field['name'])) {
      if (strpos($field['name'], 'destination') !== false
        || $field['name'] == 'experience') {
        $forceEnglish = true;
      }
    }

    // Add the filter if necessary
    if ($forceEnglish) {
      global $sitepress;

      $lang = 'en';
      $sitepress->switch_lang($lang);

      $args['suppress_filter'] = true;
    }

    return $args;
  }

  /**
   * Makes the Interest Groups available for selection for Inspire Me desires
   *
   * @param array $field
   * @return array
   */
  public function acfLoadFieldInspireMeInterestGroups($field) {
    if (!is_array($field)) {
      return;
    }

    $field['choices'] = [];
    $interestGroups = VisitEurope_Settings::getInterestGroups(0, true);

    if (is_array($interestGroups) && !empty($interestGroups)) {
      $field['choices'] = $interestGroups;
    }

    return $field;
  }

  /**
   * Returns a list of available Crowdriff albums for selection
   *
   * @return array
   */
  public function acfLoadFieldCrowdriffAlbums($field) {
    if (!is_array($field)) {
      return;
    }

    $field['choices'] = [];
    $crowdriff = new VisitEurope_Service_CrowdRiff();
    $albums = $crowdriff->getAlbums();

    if (is_array($albums) && !empty($albums)) {
      $field['choices'] = $albums;
    }

    return $field;
  }

  /**
   * Sets up ACF theme options
   *
   * @return boid
   */
  private function setupOptions() {
    if (function_exists('acf_add_options_page')) {
      $parent = acf_add_options_page([
        'page_title' => 'Visit Europe Config: General',
        'menu_title' => 'Visit Europe',
        'menu_slug' => 've-config-general',
        'capability' => 'edit_posts',
        'redirect' => false
      ]);

      acf_add_options_page([
        'page_title' => 'Visit Europe Config: Submit Experience',
        'menu_title' => 'Submit Experience',
        'menu_slug' => 've-config-experience-submit',
        'parent_slug' => $parent['menu_slug'],
        'capability' => 'edit_posts',
        'redirect' => false
      ]);

      acf_add_options_page([
        'page_title' => 'Visit Europe Config: Inspire Me',
        'menu_title' => 'Inspire Me',
        'menu_slug' => 've-config-inspire-me',
        'parent_slug' => $parent['menu_slug'],
        'capability' => 'edit_posts',
        'redirect' => false
      ]);

      acf_add_options_page([
        'page_title' => 'Visit Europe Config: Integrations',
        'menu_title' => 'Integrations',
        'menu_slug' => 've-config-integration',
        'parent_slug' => $parent['menu_slug'],
        'capability' => 'edit_posts',
        'redirect' => false
      ]);

      acf_add_options_page([
        'page_title' => 'Visit Europe Config: Taxonomy Mapping',
        'menu_title' => 'Taxonomy Mapping',
        'menu_slug' => 've-config-taxonomy-mapping',
        'parent_slug' => $parent['menu_slug'],
        'capability' => 'edit_posts',
        'redirect' => false
      ]);

      acf_add_options_page([
        'page_title' => 'Visit Europe Config: User Verification',
        'menu_title' => 'User Verification',
        'menu_slug' => 've-config-user-verification',
        'parent_slug' => $parent['menu_slug'],
        'capability' => 'edit_posts',
        'redirect' => false
      ]);
    }
  }

  /**
   * Removes menu items
   *
   * @return void
   */
  public function removeMenuItems() {
    remove_menu_page('edit-comments.php');
  }

  /**
   * Renders the extra user profile field
   *
   * @return void
   */
  public function renderExtraUserProfileFields($user) {
    if (!is_admin()) {
      return false;
    }

    $isVerified = VisitEurope_User::isVerified($user->ID);
    ob_start(); ?>
    <h3>Extra Information</h3>
<table class="form-table">
  <tr>
    <th><label for="verified">Is Verified</label></th>
    <td>
      <select name="<?php echo VisitEurope_User::META_VERIFIED; ?>">
        <option value="NO">No</option>
        <option value="YES" <?php echo $isVerified ? 'selected="selected"' : null; ?>>Yes</option>
      </select>
      <p class="description">Whether or not the user has verified their account</p>
    </td>
  </tr>
</table>
<?php
    echo ob_get_clean();
  }

  /**
   * Save extra user profile information
   *
   * @return void
   */
  public function saveExtraUserProfileFields($userID) {
    if (!is_admin() || !current_user_can('edit_user', $userID)) {
      return false;
    }

    if (isset($_POST[VisitEurope_User::META_VERIFIED])) {
      if ($_POST[VisitEurope_User::META_VERIFIED] == "YES") {
        update_user_meta($userID, VisitEurope_User::META_VERIFIED, VisitEurope_User::META_VERIFIED);
      } else {
        delete_user_meta($userID, VisitEurope_User::META_VERIFIED);
      }
    }
  }

  /**
   * Adds code to the 'header'
   *
   * @return void
   */
  public function addCustomHeaderCode() {
    $facebookPixelCode = get_field('facebook_pixel_code', 'option');
    $googleAnalyticsID = get_field('google_analytics_id', 'option');

    ob_start(); ?>
<?php if (!empty($facebookPixelCode)) { ?>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '<?php echo $facebookPixelCode; ?>');
fbq('track', 'PageView');
</script>
<noscript>
<img height="1" width="1"
src="https://www.facebook.com/tr?id=<?php echo $facebookPixelCode; ?>&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
<?php } ?>

<?php if (!empty($googleAnalyticsID)) { ?>
<script>
// Google Analytics
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create','<?php echo $googleAnalyticsID; ?>');
ga('send', 'pageview');
ga('require', 'ec');
</script>
<?php } ?>
    <?php echo ob_get_clean();
  }

  /**
   * Appends the type of destination or experience to the body classes
   *
   * @param array $classes
   * @return array
   */
  public function setupBodyClasses($classes = []) {
    global $post;

    if ($post) {
      if ($post->post_type == VisitEurope_CPT_Destination::POST_TYPE) {
        $type = VisitEurope_Destination::getTypeForPostID($post->ID);
        $classes[] = 'type-' . strtolower($type);
      } else if ($post->post_type == VisitEurope_CPT_Experience::POST_TYPE) {
        $type = VisitEurope_Experience::getTypeForPostID($post->ID);
        $classes[] = 'type-' . strtolower($type);
      }
    }

    if (VisitEurope_Feature::isActive(VisitEurope_Feature::MY_TRIPS)) {
      $classes[] = 'feature-my-trips';
    }

    if (VisitEurope_Feature::isActive(VisitEurope_Feature::INSPIRE_ME)) {
      $classes[] = 'feature-inspire-me';
    }

    if (VisitEurope_Feature::isActive(VisitEurope_Feature::USER_ACCOUNTS)) {
      $classes[] = 'feature-user-accounts';
    }

    return $classes;
  }

  /**
   * Admin body classes
   *
   * @param array $classes
   * @return array
   */
  public function setupAdminBodyClasses($classes) {
    if ($current_user = wp_get_current_user()) {
      if (strpos($current_user->user_email, 'smilemachine') !== false || strpos($current_user->user_email, 'digitalinnovationgroup') !== false) {

        if (is_array($classes)) {
          $classes[] = 'superadmin';
        } else if (is_string($classes)) {
          $classes = $classes . ' superadmin';
        }
      }
    }

    return $classes;
  }

  /**
   * Add copyright data to media uploader
   *
   * @param array $formFields
   * @param WP_Post $post
   * @return array
   */
  public function setupAttachmentFieldsToEdit($formFields, $post) {
    $formFields[self::KEY_MEDIA_COPYRIGHT] = [
      'label' => 'Copyright',
      'input' => 'text',
      'value' => get_post_meta($post->ID, self::KEY_MEDIA_COPYRIGHT, true),
    ];

    $formFields[self::KEY_MEDIA_COPYRIGHT_URL] = [
      'label' => 'Copyright URL',
      'input' => 'text',
      'value' => get_post_meta($post->ID, self::KEY_MEDIA_COPYRIGHT_URL, true),
    ];

    return $formFields;
  }

  /**
   * Save copyright data in media uploader
   *
   * @param WP_Post $post
   * @param array $attachment
   * @return WP_Post
   */
  public function setupAttachmentFieldsToSave($post, $attachment) {
    if (isset($attachment[self::KEY_MEDIA_COPYRIGHT])) {
      update_post_meta($post['ID'], self::KEY_MEDIA_COPYRIGHT, $attachment[self::KEY_MEDIA_COPYRIGHT]);
    }

    if (isset($attachment[self::KEY_MEDIA_COPYRIGHT_URL])) {
      update_post_meta($post['ID'], self::KEY_MEDIA_COPYRIGHT_URL, $attachment[self::KEY_MEDIA_COPYRIGHT_URL]);
    }

    return $post;
  }

  /**
   * Save copyright data in edit media
   *
   * @param WP_Post $post
   * @param integer $attachmentID
   * @return WP_Post
   */
  public function setupEditAttachment($attachmentID) {
    if (isset($_REQUEST['attachments'][$attachmentID][self::KEY_MEDIA_COPYRIGHT])) {
      $value = $_REQUEST['attachments'][$attachmentID][self::KEY_MEDIA_COPYRIGHT];
      update_post_meta($attachmentID, self::KEY_MEDIA_COPYRIGHT, $value);
    }

    if (isset($_REQUEST['attachments'][$attachmentID][self::KEY_MEDIA_COPYRIGHT_URL])) {
      $value = $_REQUEST['attachments'][$attachmentID][self::KEY_MEDIA_COPYRIGHT_URL];
      update_post_meta($attachmentID, self::KEY_MEDIA_COPYRIGHT_URL, $value);
    }

    return $post;
  }

  /**
   * Registers all the custom post types
   *
   * @return void
   */
  private function setupCustomPostTypes() {
    $types = ['Advertisement', 'Destination', 'Experience', 'Question', 'Trip', 'UserTrip'];

    foreach ($types as $type) {
      $className = 'VisitEurope_CPT_' . $type;
      $cpt = new $className();
      $cpt->setup();
    }
  }

  /**
   * Registers all the custom taxonomies
   *
   * @return void
   */
  private function setupCustomTaxonomies() {
    $types = ['Audience', 'Budget', 'Interest', 'TripActivityLevel', 'TripType'];

    foreach ($types as $type) {
      $className = 'VisitEurope_Taxonomy_' . $type;
      $taxonomy = new $className();
      $taxonomy->setup();
    }
  }

  /**
   * Registers all the custom shortcodes
   *
   * @return void
   */
  private function setupCustomShortcodes() {
    $types = ['CTA_Imagelink', 'Preview', 'Section_Header'];

    foreach ($types as $type) {
      $className = 'VisitEurope_Shortcode_' . $type;
      $shortcode = new $className();
      $shortcode->setup();
    }
  }

  /**
   * Add custom image sizes, and update the actual wordpress ones
   *
   * @return void
   */
  private function setupCustomImageSizes() {

    // Thumbnail for grid: 500x500
    update_option('map_size_w', 100);
    update_option('map_size_h', 100);
    update_option('map_crop', 0);

    // Thumbnail for grid: 500x500
    update_option('thumbnail_size_w', 500);
    update_option('thumbnail_size_h', 500);
    update_option('thumbnail_crop', 0);

    // Medium for gallery: 1000x1000
    update_option('medium_size_w', 1000);
    update_option('medium_size_h', 1000);
    update_option('medium_crop', 0);

    // Large for full page: 1440x1440
    update_option('large_size_w', 1440);
    update_option('large_size_h', 1440);
    update_option('large_crop', 0);

    // Header banner
    add_image_size('page_header', 1440, 655, true);

    // Initial image, for quick page load
    add_image_size('initial', 10, 10, true);

    // Gallery images
    add_image_size('gallery_thumbnail', 500, 288, true);
    add_image_size('gallery_large', 1000, 575, true);
  }

  /**
   * Add a wrapper around youtube embedded videos
   * https://lorut.no/responsive-vimeo-youtube-embed-wordpress/
   *
   * @param string $return
   * @param object $data
   * @param string $url
   * @return string
   */
  public function setupResponsiveVideos($html, $data) {
    if (!is_object($data) || empty($data->type)) {
      return $html;
    }

    // Verify that it is a video
    if (!($data->type == 'video')) {
      return $html;
    }

    $aspectRatio = $data->width / $data->height;
    $ratio4x3 = abs($aspectRatio - (4/3));
    $ratio16x9 = abs($aspectRatio - (16/9));
    $aspectRatioClass = ( $ratio4x3 < $ratio16x9 ? 'embed-responsive-4by3' : 'embed-responsive-16by9');
    $html = preg_replace( '/(width|height)="\d*"\s/', "", $html );

    return '<div class="embed-responsive ' . $aspectRatioClass . '">' . $html . '</div>';
  }

  /**
   * Returns whether or not the user has accepted cookies
   *
   * @return boolean
   */
  public static function hasAcceptedCookies() {
    return (isset($_COOKIE['hasAcceptedCookies']) && $_COOKIE['hasAcceptedCookies'] == 'true');
  }

  /**
   * Setup custom php mailer settings
   *
   * @return void
   */
  public function setupCustomMailer($phpmailer) {
    $port = intval(get_field('mail_port', 'option'));
    $smtpSecure = false;

    if (VisitEurope_Helper::isSecure()) {
        $smtpSecure = ($port == 587 ? 'tls' : true);
    }

    $phpmailer->isSMTP();
    $phpmailer->Host = get_field('mail_host', 'option');
    $phpmailer->SMTPAuth = true;
    $phpmailer->SMTPSecure = $smtpSecure;
    $phpmailer->Port = $port;
    $phpmailer->Username = get_field('mail_username', 'option');
    $phpmailer->Password = get_field('mail_password', 'option');
    $phpmailer->From = get_field('mail_from_email', 'option');
    $phpmailer->FromName = get_field('mail_from_name', 'option');
    $phpmailer->IsHTML(true);
  }


}
