<?php
if (!defined('ABSPATH')) exit;

/**
 * API Controller
 *
 * @package Visit_Europe
 */
class VisitEurope_API_Search_Controller extends WP_REST_Controller {

  public function __construct() {
    $this->namespace = VisitEurope_API::NAMESPACE;
    $this->resource_name = '/search';
  }

  public function setup() {
    add_action('rest_api_init', function () {
      register_rest_route($this->namespace, $this->resource_name, [
        'methods' => 'GET',
        'callback' => [self::class, 'index'],
      ]);
    });
  }

  /**
   * Returns results for a search request
   *
   * @param WP_REST_Request $request
   * @return WP_REST_Response
   */
  public static function index(WP_REST_Request $request) {
    $searchTerm = sanitize_text_field($request->get_param('searchTerm'));
    $items = [];

    // Get the posts
    $wpQuery = new WP_Query([
      'post_type' => [
        VisitEurope_CPT_Destination::POST_TYPE,
        VisitEurope_CPT_Experience::POST_TYPE,
        VisitEurope_CPT_Trip::POST_TYPE,
      ],
      's' => $searchTerm,
      'post_status' => 'publish',
      'posts_per_page' => 3,
      'meta_query' => [
        'relation' => 'OR',
        [
          'key' => 'type',
          'value' => 'Private',
          'compare' => 'NOT LIKE',
        ],
        [
          'key' => 'type',
          'value' => '',
          'compare' => 'NOT EXISTS',
        ]
      ],
    ]);
    $posts = $wpQuery->get_posts();

    // Format the results
    if (is_array($posts) && !empty($posts)) {
      foreach ($posts as $post) {
        if ($post->post_type == VisitEurope_CPT_Destination::POST_TYPE) {
          $tag = VisitEurope_Destination::getTypeForPostID($post->ID);
        } else if ($post->post_type == VisitEurope_CPT_Experience::POST_TYPE) {
          $tag = VisitEurope_Experience::getTypeForPostID($post->ID);
        } else {
          $tag = _x('Trip', 'Trips CTP: singular name', VisitEurope_Theme::KEY);
        }

        $items[] = [
          'title' => $post->post_title,
          'tag' => $tag,
          'url' => get_the_permalink($post->ID),
        ];
      }
    }

    $response = new WP_REST_Response($items);
    $response->set_status(200);

    return $response;
  }

}
