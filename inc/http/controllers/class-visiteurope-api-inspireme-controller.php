<?php
if (!defined('ABSPATH')) exit;

/**
 * API Controller
 *
 * @package Visit_Europe
 */
class VisitEurope_API_InspireMe_Controller extends WP_REST_Controller {

  public function __construct() {
    $this->namespace = VisitEurope_API::NAMESPACE;
    $this->resource_name = '/inspire-me';
  }

  public function setup() {
    add_action('rest_api_init', function () {
      register_rest_route($this->namespace, $this->resource_name, [
        'methods' => 'GET',
        'callback' => [self::class, 'index'],
      ]);
    });

    add_action('rest_api_init', function () {
      register_rest_route($this->namespace, $this->resource_name . '/search', [
        'methods' => 'GET',
        'callback' => [self::class, 'search'],
      ]);
    });
  }

  /**
   * Returns information for the inspire me modal
   *
   * @param WP_REST_Request $request
   * @return WP_REST_Response
   */
  public static function index(WP_REST_Request $request) {
    $im = new VisitEurope_InspireMe();

    $response = new WP_REST_Response([
      'audiences' => $im->getAudiences(),
      'budgets' => $im->getBudgets(),
      'desires' => $im->getDesires(),
      'imageinitial' => $im->getBackgroundImageInitial(),
      'imagefull' => $im->getBackgroundImageFull(),
      'seasons' => $im->getSeasons(),
      'temperatures' => $im->getTemperatures(),
    ]);
    $response->set_status(200);

    return $response;
  }

  /**
   * Returns results for a search request
   *
   * @param WP_REST_Request $request
   * @return WP_REST_Response
   */
  public static function search(WP_REST_Request $request) {
    $search = new VisitEurope_InspireMe_Search();
    $search->fill($request->get_params());
    $destinations = $search->getDestinations();
    $trips = $search->getTrips();

    $response = new WP_REST_Response([
      'destinations' => $destinations,
      'trips' => $trips,
    ]);
    $response->set_status(200);

    return $response;
  }

}
