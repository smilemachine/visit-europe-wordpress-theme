<?php
if (!defined('ABSPATH')) exit;

require_once dirname(__FILE__) . '/class-visiteurope-api-base-controller.php';

/**
 * API Controller
 *
 * @package Visit_Europe
 */
class VisitEurope_API_Experience_Controller extends VisitEurope_API_Base_Controller {

  public function __construct() {
    $this->namespace = VisitEurope_API::NAMESPACE;
    $this->resource_name = '/experiences';
  }

  public function setup() {
    add_action('rest_api_init', function () {
      register_rest_route($this->namespace, $this->resource_name . '/(?P<id>\d+)', [
        'methods' => 'GET',
        'callback' => [self::class, 'show'],
        'args' => [
          'id' => [
            'validate_callback' => function($param, $request, $key) {
              $post = get_post(intval($param));

              if (!$post) {
                return false;
              }

              return $post->post_type == VisitEurope_CPT_Experience::POST_TYPE;
            }
          ],
        ],
      ]);

      register_rest_route($this->namespace, $this->resource_name . '/preview/(?P<id>\d+)', [
        'methods' => 'GET',
        'callback' => [self::class, 'showPreview'],
        'args' => [
          'id' => [
            'validate_callback' => function($param, $request, $key) {
              $post = get_post(intval($param));

              if (!$post) {
                return false;
              }

              return $post->post_type == VisitEurope_CPT_Experience::POST_TYPE;
            }
          ],
        ],
      ]);

      register_rest_route($this->namespace, $this->resource_name . '/event/create', [
        'methods' => 'POST',
        'callback' => [self::class, 'eventCreate'],
        'permission_callback' => [self::class, 'permissionCurrentUserIsMember'],
      ]);

      register_rest_route($this->namespace, $this->resource_name . '/event/create/upload', [
        'methods' => 'POST',
        'callback' => [self::class, 'eventCreateUpload'],
        'permission_callback' => [self::class, 'permissionCurrentUserIsMember'],
      ]);
    });
  }

  /**
   * Returns a single Experience
   *
   * @param WP_REST_Request $request
   * @return WP_REST_Response
   */
  public static function show(WP_REST_Request $request) {
    $post = get_post(intval($request->get_param('id')));
    $type = VisitEurope_Experience::getTypeForPostID($post->ID);
    $veExperience = new VisitEurope_Experience($post);
    $data = $veExperience->presentPost();

    $response = new WP_REST_Response($data);
    $response->set_status(200);

    return $response;
  }

  /**
   * Returns a single Experience
   *
   * @param WP_REST_Request $request
   * @return WP_REST_Response
   */
  public static function showPreview(WP_REST_Request $request) {
    $post = get_post(intval($request->get_param('id')));
    $type = VisitEurope_Experience::getTypeForPostID($post->ID);
    $veExperience = new VisitEurope_Experience($post);
    $data = $veExperience->presentPostPreview();

    $response = new WP_REST_Response($data);
    $response->set_status(200);

    return $response;
  }

  /**
   * Creates a new event for the given user
   * This should really be a LOT cleaner than it is #rushedfortime
   *
   * @param WP_REST_Request $request
   * @return WP_REST_Response
   */
  public static function eventCreate(WP_REST_Request $request) {
    $city = self::getValueFromRequestParams($request, 'city');
    $country = self::getValueFromRequestParams($request, 'country');
    $dateFrom = self::getValueFromRequestParams($request, 'dateFrom');
    $dateTo = self::getValueFromRequestParams($request, 'dateTo');
    $description = self::getValueFromRequestParams($request, 'description');
    $interest = self::getValueFromRequestParams($request, 'interest');
    $images = self::getValueFromRequestParams($request, 'images');
    $latitude = self::getValueFromRequestParams($request, 'latitude');
    $links = self::getValueFromRequestParams($request, 'links');
    $longitude = self::getValueFromRequestParams($request, 'longitude');
    $name = self::getValueFromRequestParams($request, 'name');
    $private = self::getValueFromRequestParams($request, 'private');
    $tripID = self::getValueFromRequestParams($request, 'tripID');
    $zoom = self::getValueFromRequestParams($request, 'zoom');
    $type = ($private === true ? 'Event Private' : 'Event');
    $userTrip = null;

    if (empty($city) || empty($country) || empty($dateTo) || empty($dateFrom) || empty($description)
      || empty($interest) || empty($latitude) || empty($longitude) || empty($name) || empty($zoom)) {
      return self::unknownResponse();
    }

    // If private, ensure the user owns this trip
    if ($private === true) {
      if (empty($tripID)) {
        return self::unknownResponse();
      }

      $userTrip = new VisitEurope_UserTrip(get_post($tripID));

      if (!$userTrip || !$userTrip->isOwnedBy(VisitEurope_User::getCurrentUserID())) {
        return self::unauthorizedResponse();
      }
    }

    // Create a new event
    $result = wp_insert_post([
      'post_content' => '',
      'post_title' => $name,
      'post_excerpt' => '',
      'post_status' => ($private === true ? 'publish' : 'draft'),
      'post_type' => VisitEurope_CPT_Experience::POST_TYPE,
      'comment_status' => 'closed',
      'ping_status' => 'closed',
    ]);

    if (is_wp_error($result)) {
      $response = new WP_REST_Response([
        'errors' => $result->get_error_messages(),
      ]);
      $response->set_status($result->get_error_code());

      return $response;
    }

    // Get the post
    $userEvent = get_post($result);

    if (!$userEvent) {
      return self::unknownResponse();
    }

    // Save the meta data
    $about = [];
    $content = [];
    $destinationIDs = [];
    $pointers = [];
    $slides = [];

    update_field('field_5a1603c059b7d', VisitEurope_User::getCurrentUserID(), $userEvent->ID);  // User
    update_field('field_59afbbdff80f0', $latitude, $userEvent->ID);  // Map lat
    update_field('field_59afbbb9f80ef', $longitude, $userEvent->ID);  // Map lng
    update_field('field_59bad778ee7d6', intval($zoom), $userEvent->ID);  // Map zoom
    update_field('field_59a9639887cd0', $type, $userEvent->ID); // Type
    update_field('field_59b669daa33ad', strip_tags($description), $userEvent->ID); // Blurb
    update_field('field_59bad795ee7d7', date('Y-m-d', strtotime($dateFrom)), $userEvent->ID); // Start date
    update_field('field_59bad824ee7d8', date('Y-m-d', strtotime($dateTo)), $userEvent->ID); // End date

    // Pointers
    $pointers[] = [
      'type' => 'City',
      'subtitle' => $city
    ];
    update_field('field_59a96b0d07e5a', $pointers, $userEvent->ID);

    // About
    if (is_array($links) && !empty($links)) {
      foreach ($links as $link) {
        if (strpos($link, 'http' === false)) {
          $link = 'http://' . $link;
        }

        $about[] = [
          'title' => $link,
          'url' => $link,
        ];
      }
    }

    update_field('field_59a96a32deb6d', $about, $userEvent->ID);

    // Destinations
    $destinationIDs[] = intval($country);
    update_field('field_59bae5504316c', $destinationIDs, $userEvent->ID);

    // Content
    $content[] = [
      'acf_fc_layout' => 'content',
      'body' => $description,
    ];

    if (is_array($images) && !empty($images)) {
      foreach ($images as $key => $value) {
        $attachmentID = VisitEurope_Media::attachUserMediaToPost($userEvent->ID, $value);

        if (intval($attachmentID) > 0) {
          if ($key === 0) {
            // Set the feature image
            set_post_thumbnail($userEvent->ID, $attachmentID);
          }

          $slides[] = [
            'title' => '',
            'image' => $attachmentID,
          ];
        }
      }

      $content[] = [
        'acf_fc_layout' => 'carousel',
        'slides' => $slides,
      ];
    }

    update_field('field_59a9667138809', $content, $userEvent->ID);

    // Interests
    $interests = [];
    $interestGroups = VisitEurope_Settings::getInterestGroups();

    if (is_array($interestGroups) && !empty($interestGroups)) {
      foreach ($interestGroups as $interestGroup) {
        if (is_array($interestGroup) && isset($interestGroup['title'])
          && strtolower($interestGroup['title']) == strtolower($interest)
          && isset($interestGroup['filters']) && is_array($interestGroup['filters'])) {
          $interests = array_map('intval', $interestGroup['filters']);
        }
      }
    }

    wp_set_post_terms($userEvent->ID, $interests, VisitEurope_Taxonomy_Interest::TAXONOMY, false);

    // Add to the trip
    if (!is_null($userTrip)) {
      $userTrip->addItem($userEvent->ID, 'Experience');
    }

    // Alert the admins
    if (!$private) {
      $emails = explode(',', get_field('submit_experience_admin_emails', 'option'));

      if (is_array($emails) && !empty($emails)) {
        $emails = array_map('trim', $emails);
        $subject = 'New event submission on Europe';
        $message = "A user has submitted a new event on Europe. You can view it at " . get_site_url('', null) . '/wp-admin/post.php?post=' . $userEvent->ID . '&action=edit';
        VisitEurope_Helper::sendTextEmail($emails, $subject, $message);
      }
    }

    // Return the response
    $response = new WP_REST_Response([
      'id' => $userEvent->ID,
      'url' => get_the_permalink($userEvent->ID),
    ]);
    $response->set_status(201);

    return $response;
  }

  /**
   * Uploads user images
   *
   * @param WP_REST_Request $request
   * @return WP_REST_Response
   */
  public static function eventCreateUpload(WP_REST_Request $request) {
    $files = $_FILES;
    $uploadedFiles = [];

    if (!is_array($files) || empty($files)) {
      return self::unknownResponse();
    }

    foreach ($files as $file) {
      $success = VisitEurope_Media::uploadUserImage($file);

      if (is_array($success) && isset($success['url'])) {
        $uploadedFiles[] = $success['url'];
      }
    }

    $response = new WP_REST_Response($uploadedFiles);
    $response->set_status(200);

    return $response;
  }

}
