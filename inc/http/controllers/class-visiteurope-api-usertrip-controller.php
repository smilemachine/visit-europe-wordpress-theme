<?php
if (!defined('ABSPATH')) exit;

require_once dirname(__FILE__) . '/class-visiteurope-api-base-controller.php';

/**
 * API Controller
 *
 * @package Visit_Europe
 */
class VisitEurope_API_UserTrip_Controller extends VisitEurope_API_Base_Controller {

  public function __construct() {
    $this->namespace = VisitEurope_API::NAMESPACE;
    $this->resource_name = '/user/trips';
  }

  public function setup() {
    add_action('rest_api_init', function () {
      $argsForID = [
        'id' => [
          'validate_callback' => function($param, $request, $key) {
            $post = get_post(intval($param));

            if (!$post) {
              return false;
            }

            return $post->post_type == VisitEurope_CPT_UserTrip::POST_TYPE;
          }
        ],
      ];

      register_rest_route($this->namespace, $this->resource_name . '/modal', [
        'methods' => 'POST',
        'callback' => [self::class, 'modal'],
        'permission_callback' => [self::class, 'permissionCurrentUserIsMember'],
      ]);

      register_rest_route($this->namespace, $this->resource_name . '/create', [
        'methods' => 'POST',
        'callback' => [self::class, 'create'],
        'permission_callback' => [self::class, 'permissionCurrentUserIsMember'],
      ]);

      register_rest_route($this->namespace, $this->resource_name . '/(?P<id>\d+)/details', [
        'methods' => 'POST',
        'callback' => [self::class, 'details'],
        'permission_callback' => [self::class, 'permissionCurrentUserIsMember'],
        'args' => $argsForID,
      ]);

      register_rest_route($this->namespace, $this->resource_name . '/(?P<id>\d+)/map', [
        'methods' => 'GET',
        'callback' => [self::class, 'map'],
        'permission_callback' => [self::class, 'permissionCurrentUserIsMember'],
        'args' => $argsForID,
      ]);

      register_rest_route($this->namespace, $this->resource_name . '/(?P<id>\d+)/reorder', [
        'methods' => 'POST',
        'callback' => [self::class, 'reorder'],
        'permission_callback' => [self::class, 'permissionCurrentUserIsMember'],
        'args' => $argsForID,
      ]);

      register_rest_route($this->namespace, $this->resource_name . '/(?P<id>\d+)/save-item', [
        'methods' => 'POST',
        'callback' => [self::class, 'saveItem'],
        'permission_callback' => [self::class, 'permissionCurrentUserIsMember'],
        'args' => $argsForID,
      ]);
    });
  }

  /**
   * Creates a new user trip
   *
   * @param WP_REST_Request $request
   * @return WP_REST_Response
   */
  public static function create(WP_REST_Request $request) {
    $itemID = intval(self::getValueFromRequestParams($request, 'id'));
    $itemType = strtoupper(self::getValueFromRequestParams($request, 'type'));
    $item = VisitEurope_UserTrip::getPresentedItem($itemID, $itemType);
    $name = self::getValueFromRequestParams($request, 'name');

    // Ensure we have a valid item
    if ($item) {
      if (!isset($item['type']) || (empty($name) && $item['type'] != VisitEurope_Trip::TYPE_ALL)) {
        return self::unknownResponse();
      }
    }

    $isTrip = $item['type'] == VisitEurope_Trip::TYPE_ALL;

    // If we're working with a trip and it exists already, return it
    if ($isTrip) {
      $existingUserTrips = VisitEurope_UserTrip::getByTripID($itemID);

      if (is_array($existingUserTrips) && isset($existingUserTrips[0])) {
        $ut = new VisitEurope_UserTrip($existingUserTrips[0]);
        $response = new WP_REST_Response($ut->presentPost());
        $response->set_status(200);

        return $response;
      }
    }

    // Create new trip
    $result = wp_insert_post([
      'post_title' => !$isTrip ? $name : $item['title'],
      'post_status' => 'publish',
      'post_type' => VisitEurope_CPT_UserTrip::POST_TYPE,
      'comment_status' => 'closed',
      'ping_status' => 'closed',
    ]);

    if (is_wp_error($result)) {
      $response = new WP_REST_Response([
        'errors' => $result->get_error_messages(),
      ]);
      $response->set_status($result->get_error_code());

      return $response;
    }

    // Get the post
    $userTrip = get_post($result);

    if (!$userTrip) {
      return self::unknownResponse();
    }

    // Setup the user trip
    $ut = new VisitEurope_UserTrip($userTrip);
    $ut->setUserAsCurrentUser();

    // If we have an item, save it
    if ($item) {
      if ($isTrip) {
        $ut->setAsPlanned();
        $ut->addTrip($itemID);
      } else {
        $ut->setAsCustom();
        $ut->addItem($itemID, $itemType);
      }
    } else {
      $ut->setAsCustom();
    }

    // Return the response
    $response = new WP_REST_Response($ut->presentPost());
    $response->set_status(201);

    return $response;
  }

  /**
   * Updates the details of a user trip
   *
   * @param WP_REST_Request $request
   * @return WP_REST_Response
   */
  public static function details(WP_REST_Request $request) {
    $userTrip = get_post(intval($request->get_param('id')));
    $name = self::getValueFromRequestParams($request, 'name');

    // Ensure we have valid data
    if (!$userTrip || empty($name)) {
      return self::unknownResponse();
    }

    // Ensure the current user owns this user trip
    $user = get_field('user', $userTrip->ID);

    if (!is_array($user) || $user['ID'] != VisitEurope_User::getCurrentUserID()) {
      return self::unauthorizedResponse();
    }

    $result = wp_update_post([
      'ID' => $userTrip->ID,
      'post_title' => esc_html($name),
      'post_name' => sanitize_title($name),
    ]);

    if (is_wp_error($result)) {
      $response = new WP_REST_Response([
        'errors' => $result->get_error_messages(),
      ]);
      $response->set_status($result->get_error_code());

      return $response;
    }

    // Return the response
    $response = new WP_REST_Response();
    $response->set_status(200);

    return $response;
  }

  /**
   * Returns information for the interactive map
   *
   * @param WP_REST_Request $request
   * @return WP_REST_Response
   */
  public static function map(WP_REST_Request $request) {
    $userTrip = get_post(intval($request->get_param('id')));

    // Ensure we have valid data
    if (!$userTrip) {
      return self::unknownResponse();
    }

    // Ensure the current user owns this user trip
    $user = get_field('user', $userTrip->ID);

    if (!is_array($user) || $user['ID'] != VisitEurope_User::getCurrentUserID()) {
      return self::unauthorizedResponse();
    }

    $ut = new VisitEurope_UserTrip($userTrip);

    // Return the response
    $response = new WP_REST_Response($ut->presentPost());
    $response->set_status(200);

    return $response;
  }

  /**
   * Returns the trips and preview item information
   *
   * @param WP_REST_Request $request
   * @return WP_REST_Response
   */
  public static function modal(WP_REST_Request $request) {
    $itemID = intval(self::getValueFromRequestParams($request, 'id'));
    $itemType = strtoupper(self::getValueFromRequestParams($request, 'type'));
    $item = VisitEurope_UserTrip::getPresentedItem($itemID, $itemType);

    // Ensure we have a valid item
    if (!$item) {
      return self::unknownResponse();
    }

    // Get all user trips that are an items type
    $models = VisitEurope_UserTrip::getWhereType(VisitEurope_UserTrip::TYPE_CUSTOM);
    $trips = [];

    if (is_array($models) && !empty($models)) {
      foreach ($models as $model) {
        $ut = new VisitEurope_UserTrip($model);
        $trips[] = $ut->presentPost();
      }
    }

    // Return the response
    $response = new WP_REST_Response([
      'item' => $item,
      'userTrips' => $trips,
    ]);
    $response->set_status(200);

    return $response;
  }

  /**
   * Reorder the items within a trip
   *
   * @param WP_REST_Request $request
   * @return WP_REST_Response
   */
  public static function reorder(WP_REST_Request $request) {
    $userTrip = get_post(intval($request->get_param('id')));
    $ids = self::getValueFromRequestParams($request, 'ids');

    // Ensure we have valid data
    if (!$userTrip || !is_array($ids) || empty($ids)) {
      return self::unknownResponse();
    }

    // Ensure the current user owns this user trip
    $user = get_field('user', $userTrip->ID);

    if (!is_array($user) || $user['ID'] != VisitEurope_User::getCurrentUserID()) {
      return self::unauthorizedResponse();
    }

    $ut = new VisitEurope_UserTrip($userTrip);
    $ut->setItemOrder($ids);

    // Return the response
    $response = new WP_REST_Response();
    $response->set_status(200);

    return $response;
  }

  /**
   * Saves an item to a user's trip
   *
   * @param WP_REST_Request $request
   * @return WP_REST_Response
   */
  public static function saveItem(WP_REST_Request $request) {
    $userTrip = get_post(intval($request->get_param('id')));
    $itemID = intval(self::getValueFromRequestParams($request, 'id'));
    $itemType = strtoupper(self::getValueFromRequestParams($request, 'type'));
    $item = VisitEurope_UserTrip::getPresentedItem($itemID, $itemType);

    // Ensure we have a valid item
    if (!$userTrip || !$item) {
      return self::unknownResponse();
    }

    // Ensure the current user owns this user trip
    $user = get_field('user', $userTrip->ID);

    if (!is_array($user) || $user['ID'] != VisitEurope_User::getCurrentUserID()) {
      return self::unauthorizedResponse();
    }

    // Ensure this userTrip belongs to this user
    $assignedUser = get_field('user', $userTrip, true);
    $currentUserID = VisitEurope_User::getCurrentUserID();

    if (!is_array($assignedUser) || !$currentUserID || !isset($assignedUser['ID'])
      || $assignedUser['ID'] != $currentUserID) {
      return self::unauthorizedResponse();
    }

    // Assign the item
    $ut = new VisitEurope_UserTrip($userTrip);

    if ($itemType == 'trip') {
      $ut->addTrip($itemID, $itemType);
      $response = new WP_REST_Response();
      $response->set_status(201);

      return $response;
    } else {
      $ut->addItem($itemID, $itemType);
      $response = new WP_REST_Response();
      $response->set_status(200);

      return $response;
    }
  }

}
