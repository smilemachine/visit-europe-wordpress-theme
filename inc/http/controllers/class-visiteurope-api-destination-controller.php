<?php
if (!defined('ABSPATH')) exit;

/**
 * API Controller
 *
 * @package Visit_Europe
 */
class VisitEurope_API_Destination_Controller extends WP_REST_Controller {

  public function __construct() {
    $this->namespace = VisitEurope_API::NAMESPACE;
    $this->resource_name = '/destinations';
  }

  public function setup() {
    add_action('rest_api_init', function () {
      register_rest_route($this->namespace, $this->resource_name, [
        'methods' => 'GET',
        'callback' => [self::class, 'index'],
      ]);

      register_rest_route($this->namespace, $this->resource_name . '/(?P<id>\d+)', [
        'methods' => 'GET',
        'callback' => [self::class, 'show'],
        'args' => [
          'id' => [
            'validate_callback' => function($param, $request, $key) {
              $post = get_post(intval($param));

              if (!$post) {
                return false;
              }

              return $post->post_type == VisitEurope_CPT_Destination::POST_TYPE;
            }
          ],
        ],
      ]);

      register_rest_route($this->namespace, $this->resource_name . '/weather', [
        'methods' => 'POST',
        'callback' => [self::class, 'weather'],
      ]);
    });
  }

  /**
   * Returns a list of Destinations
   *
   * @param WP_REST_Request $request
   * @return WP_REST_Response
   */
  public static function index(WP_REST_Request $request) {
    $countries = [];
    $countryPosts = VisitEurope_Destination::getCountries([
      'orderby' => 'title',
      'order' => 'ASC'
    ]);

    $regions = [];
    $regionPosts = VisitEurope_Destination::getRegions([
      'orderby' => 'title',
      'order' => 'ASC'
    ]);

    if (is_array($countryPosts) && !empty($countryPosts)) {
      foreach ($countryPosts as $country) {
        $vd = new VisitEurope_Destination($country);
        $countries[] = $vd->presentPost();
      }
    }

    if (is_array($regionPosts) && !empty($regionPosts)) {
      foreach ($regionPosts as $region) {
        $vd = new VisitEurope_Destination($region);
        $regions[] = $vd->presentPost();
      }
    }

    $data = [
      'countries' => $countries,
      'regions' => $regions
    ];

    $response = new WP_REST_Response($data);
    $response->set_status(200);

    return $response;
  }

  /**
   * Returns a single Destinations
   *
   * @param WP_REST_Request $request
   * @return WP_REST_Response
   */
  public static function show(WP_REST_Request $request) {
    VisitEurope_Content::addUniqueCache(VisitEurope_Content::CACHE_DESTINATION);
    VisitEurope_Content::addUniqueCache(VisitEurope_Content::CACHE_EXPERIENCE);

    $post = get_post(intval($request->get_param('id')));
    $veDestination = new VisitEurope_Destination($post);
    $veFC = new VisitEurope_ACF_FC($post);

    $data = $veDestination->presentPost();
    $data['extras'] = $veDestination->presentPostExtras();
    $data['modules'] = $veFC->presentPost();

    VisitEurope_Content::clearUniqueCache(VisitEurope_Content::CACHE_DESTINATION);
    VisitEurope_Content::clearUniqueCache(VisitEurope_Content::CACHE_EXPERIENCE);

    $response = new WP_REST_Response($data);
    $response->set_status(200);

    return $response;
  }

  /**
   * Gets the weather for a given lat/lng
   *
   * @param WP_REST_Request $request
   * @return WP_REST_Response
   */
  public static function weather(WP_REST_Request $request) {
    $data = $request->get_params();

    // Ensure we have valid data
    if (!is_array($data) || !isset($data['params']) || !is_array($data['params']) || !isset($data['params']['endDay']) || !isset($data['params']['startDay']) || !isset($data['params']['lat']) || !isset($data['params']['lng'])) {
      $response = new WP_REST_Response([
        'code' => 400,
        'message' => 'Invalid request data.',
      ]);
      $response->set_status(400);

      return $response;
    }

    $safeTimestamp = strtotime('NOW - 1 minute');
    $nowTimestamp = strtotime('NOW');
    $weatherRequests = [];
    $recentlyRequested = 0;

    if (isset($_SESSION['ve_weather_requests'])) {
      $weatherRequests = $_SESSION['ve_weather_requests'];
      $reachedLimit = false;

      // Check that we haven't used more than 1 in the past minute
      foreach ($weatherRequests as $weatherRequest) {
        if ($recentlyRequested > 1) {
          $reachedLimit = true;
          break;
        }

        if ($weatherRequest > $safeTimestamp) {
          $recentlyRequested++;
        }
      }

      // If we've reached the limit, return an error
      if ($reachedLimit) {
        $seconds = $weatherRequest - $safeTimestamp;
        $response = new WP_REST_Response([
          'code' => 403,
          'message' => 'Reached free api limit. Please try again in ' . $seconds . 's.',
        ]);
        $response->set_status(403);

        return $response;
      }
    }

    // Add the current timestamp to the array
    $weatherRequests[] = $nowTimestamp;
    $_SESSION['ve_weather_requests'] = $weatherRequests;

    // Attempt to get the weather data
    $apiKey = '3a28d7660059f473';
    $apiBaseUrl = 'http://api.wunderground.com/api/' . $apiKey . '/';
    $apiFeature = 'planner_' . $data['params']['startDay'] . $data['params']['endDay'];
    $apiQuery = $data['params']['lat'] . ',' . $data['params']['lng'] . '.json';
    $weatherRequest = wp_remote_get($apiBaseUrl . $apiFeature . '/q/' . $apiQuery);

    if (is_wp_error($weatherRequest)) {
      $response = new WP_REST_Response([
        'code' => 403,
        'message' => 'Could not retrieve weather.',
      ]);
      $response->set_status(403);

      return $response;
    }

    // Return a successfull response
    $weatherData = json_decode(wp_remote_retrieve_body($weatherRequest));
    $response = new WP_REST_Response($weatherData);
    $response->set_status(200);

    return $response;
  }

}
