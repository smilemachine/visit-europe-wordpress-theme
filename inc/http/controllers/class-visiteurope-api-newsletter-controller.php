<?php
if (!defined('ABSPATH')) exit;

/**
 * API Controller
 *
 * @package Visit_Europe
 */
class VisitEurope_API_Newsletter_Controller extends WP_REST_Controller {

  public function __construct() {
    $this->namespace = VisitEurope_API::NAMESPACE;
    $this->resource_name = '/newsletter';
  }

  public function setup() {
    add_action('rest_api_init', function () {
      register_rest_route($this->namespace, $this->resource_name, [
        'methods' => 'POST',
        'callback' => [self::class, 'subscribe'],
      ]);
    });
  }

  /**
   * Subscribes a user to the newsletter
   *
   * @param WP_REST_Request $request
   * @return WP_REST_Response
   */
  public static function subscribe(WP_REST_Request $request) {
    $data = $request->get_params();
    $email = null;

    if (is_array($data) && !empty($data) && isset($data['params']) && is_array($data['params']) && isset($data['params']['email'])) {
      $email = $data['params']['email'];
    }

    $listID = get_field('mailchimp_newsletter_list_id', 'option');
    $mailchimp = new VisitEurope_Service_Mailchimp();
    $result = $mailchimp->subscribeEmailToList($listID, $email);

    if ($result) {
      $response = new WP_REST_Response();
      $response->set_status(200);

      return $response;
    } else {
      $response = new WP_REST_Response([
        'errors' => $mailchimp->getErrors(),
      ]);
      $response->set_status(400);

      return $response;
    }
  }

}
