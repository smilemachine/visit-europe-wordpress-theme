<?php
if (!defined('ABSPATH')) exit;

/**
 * Base API Controller
 *
 * @package Visit_Europe
 */
class VisitEurope_API_Base_Controller extends WP_REST_Controller {

  /**
   * Returns the value (or null) for a request parameter
   *
   * @param WP_REST_Request $request
   * @param string $key
   * @return mixed
   */
  public static function getValueFromRequestParams($request, $key) {
    $data = $request->get_params();
    $param = null;

    if (is_array($data) && !empty($data) && isset($data['params']) && is_array($data['params'])
      && isset($data['params'][$key]) && !empty($data['params'][$key])) {
      $param = $data['params'][$key];
    }

    return $param;
  }

  /**
   * Returns an unauthorized response
   *
   * @return WP_Rest_Response
   */
  public static function unauthorizedResponse() {
    $response = new WP_REST_Response([
      'errors' => [VisitEurope_Content::getLocale('error.api.unauthorized')],
    ]);
    $response->set_status(401);

    return $response;
  }

  /**
   * Returns an known response
   *
   * @return WP_Rest_Response
   */
  public static function unknownResponse() {
    $response = new WP_REST_Response([
      'errors' => [VisitEurope_Content::getLocale('error.api.unknown')],
    ]);
    $response->set_status(400);

    return $response;
  }

  /**
   * Determines whether or not the current user is a member
   *
   * @return boolean
   */
  public static function permissionCurrentUserIsMember() {
    return !is_null(VisitEurope_User::getCurrentMember());
  }

}
