<?php
if (!defined('ABSPATH')) exit;

/**
 * API Controller
 *
 * @package Visit_Europe
 */
class VisitEurope_API_Trip_Controller extends WP_REST_Controller {

  public function __construct() {
    $this->namespace = VisitEurope_API::NAMESPACE;
    $this->resource_name = '/trips';
  }

  public function setup() {
    add_action('rest_api_init', function () {
      register_rest_route($this->namespace, $this->resource_name, [
        'methods' => 'GET',
        'callback' => [self::class, 'index'],
      ]);

      register_rest_route($this->namespace, $this->resource_name . '/(?P<id>\d+)', [
        'methods' => 'GET',
        'callback' => [self::class, 'show'],
        'args' => [
          'id' => [
            'validate_callback' => function($param, $request, $key) {
              $post = get_post(intval($param));

              if (!$post) {
                return false;
              }

              return $post->post_type == VisitEurope_CPT_Trip::POST_TYPE;
            }
          ],
        ],
      ]);

      register_rest_route($this->namespace, $this->resource_name . '/weather', [
        'methods' => 'POST',
        'callback' => [self::class, 'weather'],
      ]);
    });
  }

  /**
   * Returns a list of Trips
   *
   * @param WP_REST_Request $request
   * @return WP_REST_Response
   */
  public static function index(WP_REST_Request $request) {
    $trips = [];
    $tripPosts = VisitEurope_Trip::get([
      'orderby' => 'title',
      'order' => 'ASC',
      'posts_per_page' => -1
    ]);

    if (is_array($tripPosts) && !empty($tripPosts)) {
      foreach ($tripPosts as $trip) {
        $vt = new VisitEurope_Trip($trip);
        $trips[] = $vt->presentPost();
      }
    }

    $response = new WP_REST_Response($trips);
    $response->set_status(200);

    return $response;
  }

}
