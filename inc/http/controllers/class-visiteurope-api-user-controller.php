<?php
if (!defined('ABSPATH')) exit;

require_once dirname(__FILE__) . '/class-visiteurope-api-base-controller.php';

/**
 * API Controller
 *
 * @package Visit_Europe
 */
class VisitEurope_API_User_Controller extends VisitEurope_API_Base_Controller {

  public function __construct() {
    $this->namespace = VisitEurope_API::NAMESPACE;
    $this->resource_name = '/user';
  }

  public function setup() {
    add_action('rest_api_init', function () {
      register_rest_route($this->namespace, $this->resource_name . '/forgot', [
        'methods' => 'POST',
        'callback' => [self::class, 'forgot'],
      ]);

      register_rest_route($this->namespace, $this->resource_name . '/login/facebook', [
        'methods' => 'POST',
        'callback' => [self::class, 'loginWithFacebook'],
      ]);

      register_rest_route($this->namespace, $this->resource_name . '/login/google', [
        'methods' => 'POST',
        'callback' => [self::class, 'loginWithGoogle'],
      ]);

      register_rest_route($this->namespace, $this->resource_name . '/login', [
        'methods' => 'POST',
        'callback' => [self::class, 'login'],
      ]);

      register_rest_route($this->namespace, $this->resource_name . '/register', [
        'methods' => 'POST',
        'callback' => [self::class, 'register'],
      ]);

      register_rest_route($this->namespace, $this->resource_name . '/reset', [
        'methods' => 'POST',
        'callback' => [self::class, 'reset'],
      ]);

      register_rest_route($this->namespace, $this->resource_name . '/account', [
        'methods' => 'POST',
        'callback' => [self::class, 'account'],
        'permission_callback' => [self::class, 'permissionCurrentUserIsMember'],
      ]);
    });
  }

  /**
   * Sends a password reset request
   *
   * @param WP_REST_Request $request
   * @return WP_REST_Response
   */
  public static function forgot(WP_REST_Request $request) {
    $email = self::getValueFromRequestParams($request, 'email');

    // Ensure the user's validated their account
    if (is_null($email) || empty($email)) {
      return self::unauthorizedResponse();
    }

    // Send the password reset request
    $user = get_user_by('email', $email);

    if ($user) {
      VisitEurope_User_Verification::sendForgotEmail($user);
    }

    $response = new WP_REST_Response();
    $response->set_status(200);

    return $response;
  }

  /**
   * Updates a user's account
   *
   * @param WP_REST_Request $request
   * @return WP_REST_Response
   */
  public static function account(WP_REST_Request $request) {
    $id = self::getValueFromRequestParams($request, 'id');
    $email = self::getValueFromRequestParams($request, 'email');
    $password = self::getValueFromRequestParams($request, 'password');
    $user = VisitEurope_User::getCurrentMember();
    $userFromEmail = null;

    // Ensure we have at least an id and email
    if (!$user || !$id || !$email) {
      return self::unauthorizedResponse();
    }

    // Ensure the current user is the one that's being edited
    if ($user->ID != $id) {
      return self::unauthorizedResponse();
    }

    // Ensure this email address is still available
    $userFromEmail = get_user_by('email', $email);
    $userFromLogin = get_user_by('login', $email);

    if (($userFromLogin && $user->ID != $userFromLogin->ID) || ($userFromEmail && $user->ID != $userFromEmail->ID)) {
      $response = new WP_REST_Response([
        'errors' => [VisitEurope_Content::getLocale('account.details.error.unique')],
      ]);
      $response->set_status(403);

      return $response;
    }

    // If the user is changing their password, update
    if (!empty($password)) {
      $result = wp_set_password($password, $user->ID);

      if (is_wp_error($result)) {
        $response = new WP_REST_Response([
          'errors' => $result->get_error_messages(),
        ]);
        $response->set_status($result->get_error_code());

        return $response;
      }
    }

    // If the user is changing their email, update
    if ($email != $user->data->user_email) {
      $result = wp_update_user([
        'ID' => $user->ID,
        'user_email' => $email,
      ]);

      if (is_wp_error($result)) {
        $response = new WP_REST_Response([
          'errors' => $result->get_error_messages(),
        ]);
        $response->set_status($result->get_error_code());

        return $response;
      }
    }

    // Log the user back in as their password has changed
    wp_signon([
      'user_login' => $email,
      'user_password' => $password,
      'remember' => false,
    ]);

    // Return a response
    $response = new WP_REST_Response();
    $response->set_status(200);

    return $response;
  }

  /**
   * Logs a user in
   *
   * @param WP_REST_Request $request
   * @return WP_REST_Response
   */
  public static function login(WP_REST_Request $request) {
    $email = self::getValueFromRequestParams($request, 'email');
    $password = self::getValueFromRequestParams($request, 'password');

    // Ensure the user's validated their account
    if ($user = get_user_by('email', $email)) {
      if (!VisitEurope_User::isVerified($user->ID)) {
        return self::unverifiedAccountResponse();
      }
    } else {
      return self::noAccountOnLoginResponse($email);
    }

    // Attempt to log the user in
    $result = wp_signon([
      'user_login' => $email,
      'user_password' => $password,
      'remember' => false,
    ]);

    // Seems to have been an error
    if (is_wp_error($result)) {
      $response = new WP_REST_Response([
        'errors' => [VisitEurope_Content::getLocale('userVerification.login.error.invalid')],
      ]);
      $response->set_status(403);

      return $response;
    }

    $response = new WP_REST_Response();
    $response->set_status(200);

    return $response;
  }

  /**
   * Logs a user in with Facebook
   *
   * @param WP_REST_Request $request
   * @return WP_REST_Response
   */
  public static function loginWithFacebook(WP_REST_Request $request) {
    $userID = self::getValueFromRequestParams($request, 'id');
    $accessToken = self::getValueFromRequestParams($request, 'token');

    // Get the user's details from Facebook
    $facebook = new VisitEurope_Service_Facebook();
    $userDetails = $facebook->getBasicDetailsByID($userID, $accessToken);

    if (!$userDetails || !isset($userDetails->email)) {
      return self::unauthorizedResponse();
    }

    // Ensure the user's validated their account
    if ($user = get_user_by('email', $userDetails->email)) {
      if (!VisitEurope_User::isVerified($user->ID)) {
        return self::unverifiedAccountResponse();
      }
    } else {
      return self::noAccountOnLoginResponse($userDetails->email);
    }

    // Log the user in with email
    $loginSuccess = VisitEurope_User::loginWithEmail($userDetails->email);

    if (!$loginSuccess) {
      return self::noAccountOnLoginResponse($userDetails->email);
    }

    // Return success
    $response = new WP_REST_Response();
    $response->set_status(200);

    return $response;
  }

  /**
   * Logs a user in with Google
   *
   * @param WP_REST_Request $request
   * @return WP_REST_Response
   */
  public static function loginWithGoogle(WP_REST_Request $request) {
    $accessToken = self::getValueFromRequestParams($request, 'token');

    // Get the user's details from Google
    $google = new VisitEurope_Service_Google();
    $userDetails = $google->getBasicDetailsByID($accessToken);

    if (!$userDetails || !isset($userDetails->email)) {
      return self::unauthorizedResponse();
    }

    // Ensure the user's validated their account
    if ($user = get_user_by('email', $userDetails->email)) {
      if (!VisitEurope_User::isVerified($user->ID)) {
        return self::unverifiedAccountResponse();
      }
    } else {
      return self::noAccountOnLoginResponse($userDetails->email);
    }

    // Log the user in with email
    $loginSuccess = VisitEurope_User::loginWithEmail($userDetails->email);

    if (!$loginSuccess) {
      return self::noAccountOnLoginResponse($userDetails->email);
    }

    // Return success
    $response = new WP_REST_Response();
    $response->set_status(200);

    return $response;
  }

  /**
   * Registers a new account
   *
   * @param WP_REST_Request $request
   * @return WP_Rest_Response
   */
  public static function register(WP_REST_Request $request) {
    $email = self::getValueFromRequestParams($request, 'email');
    $password = self::getValueFromRequestParams($request, 'password');

    // Check if a user already exists w this email
    if (username_exists($email) || get_user_by('email', $email)) {
      $response = new WP_REST_Response([
        'errors' => [VisitEurope_Content::getLocale('userVerification.register.error.unique')],
      ]);
      $response->set_status(403);

      return $response;
    }

    // Create the new user
    $result = wp_insert_user([
      'user_pass' => $password,
      'user_login' => $email,
      'user_email' => $email,
      'role' => VisitEurope_Auth::ROLE_MEMBER,
    ]);

    $user = get_user_by('email', $email);

    // Seems to have been an error
    if (is_wp_error($result) || !$user) {
      $response = new WP_REST_Response([
        'errors' => $result->get_error_messages(),
      ]);
      $response->set_status($result->get_error_code());

      return $response;
    }

    // User has been successfully created
    VisitEurope_User_Verification::markAsUnverified($user->ID);
    $sent = VisitEurope_User_Verification::sendVerificationEmail($user);

    $response = new WP_REST_Response([
      'sent' => $sent
    ]);
    $response->set_status(201);

    return $response;
  }

  /**
   * Resets a user's password
   *
   * @param WP_REST_Request $request
   * @return WP_Rest_Response
   */
  public static function reset(WP_REST_Request $request) {
    $email = self::getValueFromRequestParams($request, 'email');
    $password = self::getValueFromRequestParams($request, 'password');
    $token = self::getValueFromRequestParams($request, 'token');
    $user = get_user_by('email', $email);

    // Ensure the user exists
    if (!$user) {
      return self::unauthorizedResponse();
    }

    // Ensure the token matches
    if ($token != get_user_meta($user->ID, VisitEurope_User_Verification::META_FORGOT_TOKEN, true)) {
      return self::unauthorizedResponse();
    }

    // Update their password
    $result = wp_set_password($password, $user->ID);

    // Seems to have been an error
    if (is_wp_error($result) || !$user) {
      $response = new WP_REST_Response([
        'errors' => $result->get_error_messages(),
      ]);
      $response->set_status($result->get_error_code());

      return $response;
    }

    // User has successfully reset their password
    VisitEurope_User_Verification::clearForgotToken($user->ID);

    $response = new WP_REST_Response();
    $response->set_status(200);

    return $response;
  }

  /**
   * Returns a no-account message on login
   *
   * @return WP_Rest_Response
   */
  private static function noAccountOnLoginResponse($email) {
    $response = new WP_REST_Response([
      'errors' => [VisitEurope_Content::getLocale('userVerification.login.error.noAccount')],
      'email' => (string) $email,
    ]);
    $response->set_status(403);

    return $response;
  }

  /**
   * Returns an unverified account response
   *
   * @return WP_Rest_Response
   */
  private static function unverifiedAccountResponse() {
    $response = new WP_REST_Response([
      'errors' => [VisitEurope_Content::getLocale('userVerification.login.error.unverified')],
    ]);
    $response->set_status(401);

    return $response;
  }

}
