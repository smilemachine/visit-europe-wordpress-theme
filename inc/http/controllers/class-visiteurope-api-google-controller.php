<?php
if (!defined('ABSPATH')) exit;

require_once dirname(__FILE__) . '/class-visiteurope-api-base-controller.php';

/**
 * API Controller
 *
 * @package Visit_Europe
 */
class VisitEurope_API_Google_Controller extends VisitEurope_API_Base_Controller {

  public function __construct() {
    $this->namespace = VisitEurope_API::NAMESPACE;
    $this->resource_name = '/google';
  }

  public function setup() {
    add_action('rest_api_init', function () {
      register_rest_route($this->namespace, $this->resource_name . '/geocode', [
        'methods' => 'POST',
        'callback' => [self::class, 'geocode'],
        'permission_callback' => [self::class, 'permissionCurrentUserIsMember'],
      ]);
    });
  }

  /**
   * Returns latitude / longitude
   *
   * @param WP_REST_Request $request
   * @return WP_REST_Response
   */
  public static function geocode(WP_REST_Request $request) {
    $city = strtoupper(self::getValueFromRequestParams($request, 'city'));
    $country = strtoupper(self::getValueFromRequestParams($request, 'country'));

    // Ensure we have a valid item
    if (empty($city) || empty($country)) {
      return self::unknownResponse();
    }

    // Get the lat/lng from Google
    $google = new VisitEurope_Service_Google();

    $google = new VisitEurope_Service_Google();
    $location = $google->getGeocode($city . ',' . $country);

    if (!$location || !isset($location->lat) || !isset($location->lng)) {
      return self::unknownResponse();
    }

    // Return the response
    $response = new WP_REST_Response($location);
    $response->set_status(200);

    return $response;
  }

}
