<?php
if (!defined('ABSPATH')) exit;

require_once dirname(__FILE__) . '/interface-visiteurope-service.php';

/**
 * Facebook service model
 *
 * @package Visit_Europe
 */
class VisitEurope_Service_Facebook implements VisitEurope_Service {

  private $errors;

  public function __construct() {
    $this->errors = [];
  }

  /**
   * Gets the user's basic details
   *
   * @param string $userID
   * @param string $accessToken
   * @return array
   */
  public function getBasicDetailsByID($userID, $accessToken) {
    $url = 'https://graph.facebook.com/' . $userID . '?' . http_build_query([
      'fields' => 'name,email',
      'access_token' => $accessToken,
    ]);
    $response = wp_remote_get($url);

    try {
      return json_decode($response['body']);
    } catch (Exception $e) {
      return [];
    }
  }

  /**
   * Returns an array of error messages
   *
   * @return array
   */
  public function getErrors() {
    return $this->errors;
  }

}
