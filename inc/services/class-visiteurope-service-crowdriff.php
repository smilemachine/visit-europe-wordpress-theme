<?php
if (!defined('ABSPATH')) exit;

require_once dirname(__FILE__) . '/interface-visiteurope-service.php';

/**
 * CrowdRiff service model
 *
 * @package Visit_Europe
 */
class VisitEurope_Service_CrowdRiff implements VisitEurope_Service {

  private $apiKey;
  private $baseUrl = 'https://api.crowdriff.com/v2';
  private $errors;

  public function __construct() {
    $apiKey = get_field('crowdriff_api_key', 'option');

    if ($apiKey && !empty($apiKey)) {
      $this->apiKey = $apiKey;
    }

    $this->errors = [];
  }

  /**
   * Get the headers for requests
   *
   * @retur array
   */
  private function getHeaders() {
    return [
      'Authorization' => 'Bearer ' . $this->apiKey,
      'Content-Type' => 'application/json',
    ];
  }

  /**
   * Returns an array of error messages
   *
   * @return array
   */
  public function getErrors() {
    return $this->errors;
  }

  /**
   * Returns a list of albums
   *
   * @return array
   */
  public function getAlbums() {
    $url = $this->baseUrl . '/albums';
    $params = [
      'method' => 'GET',
      'headers' => $this->getHeaders(),
    ];

    $response = wp_remote_get($url, $params);

    if (is_wp_error($response)) {
      $this->errors = $response->get_error_messages();
      return [];
    }

    try {
      $body = json_decode($response['body']);
      $albums = [];

      if (isset($body->data) && is_array($body->data) && !empty($body->data)) {
        foreach ($body->data as $album) {
          $albums[$album->id] = $album->label;
        }
      }

      return $albums;
    } catch (Exception $e) {
      $this->errors = $locales['error']['crowdriff']['unknown'];
      return [];
    }
  }

  /**
   * Returns search results
   *
   * @param string $term
   * @param array $albums
   * @param integer limit
   * @return array
   */
  public function search($term = '', $albums = [], $limit = 24) {
    $url = $this->baseUrl . '/search';

    if (!is_array($albums)) {
      $albums = [];
    }

    $params = [
      'method' => 'POST',
      'headers' => $this->getHeaders(),
      'body' => json_encode([
        'search_term' => (string) $term,
        'search_filter' => [
          'albums' => array_map('strval', $albums),
        ],
        'order' => [
          'field' => 'added_at',
          'direction' => 'asc'
        ],
        'count' => intval($limit),
      ]),
    ];

    $response = wp_remote_post($url, $params);

    try {
      $body = json_decode($response['body']);
      $results = [];

      if (isset($body->data) && is_array($body->data->assets) && !empty($body->data->assets)) {
        foreach ($body->data->assets as $asset) {
          $results[] = [
            'imageinitial' => $asset->image_gallery->url,
            'imagefull' => $asset->image_fullscreen->url,
            'source' => $asset->source,
            'title' => !empty($asset->location_label) ? $asset->location_label : $asset->source,
            'url' => $asset->native_url,
          ];
        }
      }

      return $results;
    } catch (Exception $e) {
      $this->errors = $locales['error']['crowdriff']['unknown'];
      return [];
    }
  }

}
