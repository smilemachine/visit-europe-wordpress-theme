<?php
if (!defined('ABSPATH')) exit;

/**
 * Service interface interface
 *
 * @package Visit_Europe
 */
interface VisitEurope_Service {
  public function getErrors();
}
