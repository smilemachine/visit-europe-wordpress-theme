<?php
if (!defined('ABSPATH')) exit;

require_once dirname(__FILE__) . '/interface-visiteurope-service.php';

/**
 * Mailchimp service model
 *
 * @package Visit_Europe
 */
class VisitEurope_Service_Mailchimp implements VisitEurope_Service {

  private $apiKey;
  private $baseUrl = 'https://us13.api.mailchimp.com/3.0';
  private $errors;

  public function __construct() {
    $apiKey = get_field('mailchimp_api_key', 'option');

    if ($apiKey && !empty($apiKey)) {
      $this->apiKey = $apiKey;
    }

    $this->errors = [];
  }

  /**
   * Get the headers for requests
   *
   * @retur array
   */
  private function getHeaders() {
    return [
      'Authorization' => 'Basic ' . base64_encode('user:'. $this->apiKey),
      'Content-Type' => 'application/json',
    ];
  }

  /**
   * Adds the given email address to the given user list
   *
   * @param string $list
   * @param string $email
   * @return boolean
   */
  public function subscribeEmailToList($list, $email) {
    $url = $this->baseUrl . '/lists/' . $list . '/members';
    $params = [
      'method' => 'POST',
      'headers' => $this->getHeaders(),
      'body' => json_encode([
        'email_address' => $email,
        'status' => 'subscribed',
      ]),
    ];

    $response = wp_remote_post($url, $params);

    try {
      $body = json_decode($response['body']);

      if ($body && isset($body->status) && $body->status == 'subscribed') {
        return true;
      }

      if (isset($body->title) && $body->title == 'Member Exists') {
        $this->errors[] = VisitEurope_Content::getLocale('error.api.mailchimp.alreadySubscribed');
      } else if (isset($body->detail)) {
        $this->errors[] = $body->detail;
      }

      return false;
    } catch (Exception $e) {
      $this->errors[] = VisitEurope_Content::getLocale('error.api.mailchimp.unknown');
      return false;
    }
  }

  /**
   * Returns an array of error messages
   *
   * @return array
   */
  public function getErrors() {
    return $this->errors;
  }

}
