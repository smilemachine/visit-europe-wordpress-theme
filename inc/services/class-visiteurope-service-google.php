<?php
if (!defined('ABSPATH')) exit;

require_once dirname(__FILE__) . '/interface-visiteurope-service.php';

/**
 * Facebook service model
 *
 * @package Visit_Europe
 */
class VisitEurope_Service_Google implements VisitEurope_Service {

  private $errors;

  public function __construct() {
    $this->errors = [];
  }

  /**
   * Gets the user's basic details
   *
   * @param string $accessToken
   * @return array
   */
  public function getBasicDetailsByID($accessToken) {
    $url = 'https://www.googleapis.com/oauth2/v1/userinfo/?' . http_build_query([
      'access_token' => $accessToken,
    ]);
    $response = wp_remote_get($url);

    try {
      return json_decode($response['body']);
    } catch (Exception $e) {
      return [];
    }
  }

  /**
   * Geocodes an address
   *
   * @param string $address
   * @return array
   */
  public function getGeocode($address) {
    $url = 'https://maps.googleapis.com/maps/api/geocode/json?' . http_build_query([
      'key' => get_field('google_geocode_api_key', 'option'),
      'address' => $address,
    ]);
    $response = wp_remote_get($url);

    try {
      $body = json_decode($response['body']);

      if ($body && isset($body->results) && isset($body->results[0]) && isset($body->results[0]->geometry)
        && isset($body->results[0]->geometry->location) && isset($body->results[0]->geometry->location->lat)
        && isset($body->results[0]->geometry->location->lng)) {
        return $body->results[0]->geometry->location;
      }
      return null;
    } catch (Exception $e) {
      return null;
    }
  }

  /**
   * Returns an array of error messages
   *
   * @return array
   */
  public function getErrors() {
    return $this->errors;
  }

}
