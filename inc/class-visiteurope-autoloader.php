<?php
if (!defined('ABSPATH')) exit;

/**
 * Autoloads VisitEurope classes using WordPress convention.
 *
 * @package VisitEurope
 * @author https://carlalexander.ca/organizing-files-object-oriented-wordpress-plugin
 */
class VisitEurope_Autoloader {

  /**
   * Setup Autoloader as an SPL autoloader
   *
   * @param boolean $prepend
   */
  public function setup($prepend = false) {
    spl_autoload_register([new self, 'autoloadACFContent'], true, $prepend);
    spl_autoload_register([new self, 'autoloadClasses'], true, $prepend);
    spl_autoload_register([new self, 'autoloadCustomPostTypes'], true, $prepend);
    spl_autoload_register([new self, 'autoloadCustomTaxonomies'], true, $prepend);
    spl_autoload_register([new self, 'autoloadCustomShortcodes'], true, $prepend);
    spl_autoload_register([new self, 'autoloadFlexibleContent'], true, $prepend);
    spl_autoload_register([new self, 'autoloadHttpControllers'], true, $prepend);
    spl_autoload_register([new self, 'autoloadImporter'], true, $prepend);
    spl_autoload_register([new self, 'autoloadServices'], true, $prepend);
    spl_autoload_register([new self, 'autoloadModels'], true, $prepend);
  }

  /**
   * Handles autoloading of VisitEurope files
   *
   * @param string $folder
   * @param string $class
   * @return void
   */
  private function autoloadByFolder($folder, $class) {
    if (0 !== strpos($class, 'VisitEurope')) {
      return;
    }

    $path = dirname(__FILE__) . '/' . $folder . '/';
    $filename = 'class-' . strtolower(str_replace(array('_', "\0"), array('-', ''), $class)) . '.php';

    if (is_file($file = $path . $filename)) {
      require_once $file;
    }
  }

  /**
   * Handles autoloading of VisitEurope classes
   *
   * @param string $class
   * @return void
   */
  public function autoloadClasses($class) {
    self::autoloadByFolder('classes', $class);
  }

  /**
   * Handles autoloading of VisitEurope models
   *
   * @param string $class
   * @return void
   */
  public function autoloadModels($class) {
    self::autoloadByFolder('models', $class);
  }

  /**
   * Handles autoloading of VisitEurope models
   *
   * @param string $class
   * @return void
   */
  public function autoloadCustomPostTypes($class) {
    self::autoloadByFolder('custom-post-types/classes', $class);
  }

  /**
   * Handles autoloading of VisitEurope taxonomies
   *
   * @param string $class
   * @return void
   */
  public function autoloadCustomTaxonomies($class) {
    self::autoloadByFolder('custom-taxonomies', $class);
  }

  /**
   * Handles autoloading of VisitEurope shortcodes
   *
   * @param string $class
   * @return void
   */
  public function autoloadCustomShortcodes($class) {
    self::autoloadByFolder('custom-shortcodes/classes', $class);
  }

  /**
   * Handles autoloading of VisitEurope acf content
   *
   * @param string $class
   * @return void
   */
  public function autoloadACFContent($class) {
    self::autoloadByFolder('acf', $class);
  }

  /**
   * Handles autoloading of VisitEurope flexible content
   *
   * @param string $class
   * @return void
   */
  public function autoloadFlexibleContent($class) {
    self::autoloadByFolder('acf/flexible-content', $class);
  }

  /**
   * Handles autoloading of VisitEurope controllers
   *
   * @param string $class
   * @return void
   */
  public function autoloadHttpControllers($class) {
    self::autoloadByFolder('http/controllers', $class);
  }

  /**
   * Handles autoloading of VisitEurope importer
   *
   * @param string $class
   * @return void
   */
  public function autoloadImporter($class) {
    if (defined('VE_IMPORT_CONTENT') && VE_IMPORT_CONTENT === true) {
      self::autoloadByFolder('importer', $class);
    }
  }

  /**
   * Handles autoloading of VisitEurope services
   *
   * @param string $class
   * @return void
   */
  public function autoloadServices($class) {
    self::autoloadByFolder('services', $class);
  }
}
