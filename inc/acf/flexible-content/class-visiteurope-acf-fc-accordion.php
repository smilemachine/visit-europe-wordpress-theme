<?php
if (!defined('ABSPATH')) exit;

/**
 * Setup for Visit Europe FlexibleContent: Accordion
 *
 * @package Visit_Europe
 */
class VisitEurope_ACF_FC_Accordion {

  protected $post;

  /**
   * Setup
   *
   * @param WP_Post
   */
  public function __construct($post) {
      $this->post = $post;
  }

  /**
   * Present Accordion
   *
   * @return array
   */
  public function presentLayout() {
    $items = get_sub_field('items', $post->ID);
    $onMap = get_sub_field('on_map', $post->ID);
    $title = get_sub_field('title', $post->ID);
    $url = get_sub_field('url', $post->ID);

    if (!is_array($items) || empty($items) || $onMap !== true) {
      return [];
    }

    return [
      'content' => $items,
      'title' => $title ? $title : null,
      'url' => $url ? $url : null,
    ];
  }

}
