<?php
if (!defined('ABSPATH')) exit;

/**
 * Setup for Visit Europe FlexibleContent: StandardGrid
 *
 * @package Visit_Europe
 */
class VisitEurope_ACF_FC_StandardGrid {

  protected $items;
  protected $type;
  protected $limit;
  protected $post;
  protected $interests;
  protected $itemIDs;
  protected $restrictToValidLocation;

  /**
   * Setup
   *
   * @param WP_Post $post
   * @param string $type
   * @param integer $limit
   * @return void
   */
  public function __construct($post, $type = '', $limit = 4) {
    $this->post = $post;
    $this->items = [];
    $this->itemIDs = [];
    $this->type = null;
    $this->limit = $this->setLimit($limit);
    $this->restrictToValidLocation = false;

    if ($this->isValidType($type)) {
      $this->type = $type;
      $this->items = $this->getItems();
    }
  }

  /**
   * Sets the type from a raw value
   *
   * @param string $rawType
   * @return void
   */
  public function setTypeFromRaw($rawType) {
    if ($this->isValidType($rawType)) {
      $this->type = $rawType;
    } else {
      $type = self::getTypeFromRaw($rawType);

      if ($this->isValidType($type)) {
        $this->type = $type;
      }
    }
  }

  /**
   * Sets the type from a raw value
   *
   * @param integer $limit
   * @return void
   */
  public function setLimit($limit = 4) {
    if (intval($limit) == 0) {
      $this->limit = -1;
    } else {
      $this->limit = intval($limit);
    }
  }

  /**
   * Apply IDs to restrict the query to
   *
   * @param mixed $itemIDs
   * @return void
   */
  public function restrictToItemIDs($itemIDs = null) {
    if (is_array($itemIDs) && !is_null($itemIDs)) {
      $this->itemIDs = $itemIDs;
    } else {
      $this->itemIDs = null;
    }
  }

  /**
   * Returns the View All url for this standard grid
   *
   * @param string $interestGroupName
   * @return string
   */
  public function getViewAllUrl($interestGroupName = null) {
    return VisitEurope_Helper::getViewAllUrl($this->post, $this->type, $interestGroupName);
  }

  /**
   * Determines if this type is currently supported
   *
   * @param string $type
   * @return boolean
   */
  private function isValidType($type) {
    $types = [
      VisitEurope_Destination::TYPE_ALL,
      VisitEurope_Destination::TYPE_COUNTRY,
      VisitEurope_Destination::TYPE_REGION,
      VisitEurope_Experience::TYPE_ALL,
      VisitEurope_Experience::TYPE_ARTICLE,
      VisitEurope_Experience::TYPE_EVENT,
      VisitEurope_Experience::TYPE_GALLERY,
      VisitEurope_Experience::TYPE_PRODUCT,
      VisitEurope_Experience::TYPE_RECIPE,
      VisitEurope_Experience::TYPE_ROUTE,
      VisitEurope_Trip::TYPE_ALL
    ];

    return in_array($type, $types);
  }

  /**
   * Gets items for the current type
   *
   * @return array
   */
  private function getItems() {
    $items = [];

    if (!$this->type) {
      return $items;
    }

    switch ($this->type) {
      case VisitEurope_Destination::TYPE_ALL:
        $items = $this->getItemsForDestination();
        break;
      case VisitEurope_Destination::TYPE_COUNTRY:
        $items = $this->getItemsForCountry();
        break;
      case VisitEurope_Destination::TYPE_REGION:
        $items = $this->getItemsForRegion();
        break;
      case VisitEurope_Experience::TYPE_ALL:
        $items = $this->getItemsForExperience();
        break;
      case VisitEurope_Experience::TYPE_ARTICLE:
        $items = $this->getItemsForArticles();
        break;
      case VisitEurope_Experience::TYPE_EVENT:
        $items = $this->getItemsForEvents();
        break;
      case VisitEurope_Experience::TYPE_GALLERY:
        $items = $this->getItemsForGalleries();
        break;
      case VisitEurope_Experience::TYPE_PRODUCT:
        $items = $this->getItemsForProducts();
        break;
      case VisitEurope_Experience::TYPE_RECIPE:
        $items = $this->getItemsForRecipes();
        break;
      case VisitEurope_Experience::TYPE_ROUTE:
        $items = $this->getItemsForRoutes();
        break;
      case VisitEurope_Trip::TYPE_ALL:
        $items = $this->getItemsForTrip();
        break;
      default:
        break;
    }

    return $items;
  }

  /**
   * Get items for Articles
   *
   * @return array
   */
  private function getItemsForArticles() {
    if ($this->restrictToValidLocation) {
      $items = VisitEurope_Experience::getWithValidLocation($this->post->ID, $this->limit, $this->type);
    } else {
      $taxQuery = VisitEurope_Interest::getTaxQuery($this->interests);
      $metaQuery = [];

      if ($this->post->post_type == VisitEurope_CPT_Destination::POST_TYPE) {
        $metaQuery = VisitEurope_Experience::getMetaQueryForDestinationID($this->post->ID);
      }

      $params = [
        'posts_per_page' => $this->limit,
        'tax_query' => $taxQuery,
        'meta_query' => $metaQuery,
      ];

      if (is_array($this->itemIDs)) {
        if (!empty($this->itemIDs)) {
          $params['post__in'] = $this->itemIDs;
        } else {
          $params['post__in'] = [-999];
        }
      }

      $items = VisitEurope_Experience::getArticles($params);
    }

    return $this->presentExperienceItems($items);
  }

  /**
   * Get items for Country
   *
   * If the post type is a destination, and we're showing a grid of countries,
   * limit them to countries of that region.
   *
   * @return array
   */
  private function getItemsForCountry() {
    if ($this->post->post_type == VisitEurope_CPT_Destination::POST_TYPE
        && VisitEurope_Destination::getTypeForPostID($this->post->ID) == VisitEurope_Destination::TYPE_REGION) {
        $items = VisitEurope_Destination::getCountriesForPostID($this->post->ID, [
          'posts_per_page' => -1,
          'orderby' => 'title',
          'order' => 'ASC',
        ]);
    } else {
      $items = VisitEurope_Destination::getCountries(['posts_per_page' => $this->limit]);
    }

    return $this->presentDestinationItems($items);
  }

  /**
   * Get items for Country
   *
   * @return array
   */
  private function getItemsForDestination() {
    $params = ['posts_per_page' => $this->limit];

    if (is_array($this->itemIDs)) {
      if (!empty($this->itemIDs)) {
        $params['post__in'] = $this->itemIDs;
      } else {
        $params['post__in'] = [-999];
      }
    }

    $items = VisitEurope_Destination::get($params);
    return $this->presentDestinationItems($items);
  }

  /**
   * Get items for Experiences
   *
   * @return array
   */
  private function getItemsForExperience() {
    if ($this->restrictToValidLocation) {
      $items = VisitEurope_Experience::getWithValidLocation($this->post->ID, $this->limit, $this->type);
    } else {
      $taxQuery = VisitEurope_Interest::getTaxQuery($this->interests);
      $metaQuery = [];

      if ($this->post->post_type == VisitEurope_CPT_Destination::POST_TYPE) {
        $metaQuery = VisitEurope_Experience::getMetaQueryForDestinationID($this->post->ID);
      }

      $params = [
        'posts_per_page' => $this->limit,
        'tax_query' => $taxQuery,
        'meta_query' => $metaQuery,
      ];

      if (is_array($this->itemIDs)) {
        if (!empty($this->itemIDs)) {
          $params['post__in'] = $this->itemIDs;
        } else {
          $params['post__in'] = [-999];
        }
      }

      $items = VisitEurope_Experience::get($params);
    }

    return $this->presentExperienceItems($items);
  }

  /**
   * Get items for Events
   *
   * @return array
   */
  private function getItemsForEvents() {
    if ($this->restrictToValidLocation) {
      $items = VisitEurope_Experience::getWithValidLocation($this->post->ID, $this->limit, $this->type);
    } else {
      $taxQuery = VisitEurope_Interest::getTaxQuery($this->interests);
      $metaQuery = [];

      if ($this->post->post_type == VisitEurope_CPT_Destination::POST_TYPE) {
        $metaQuery = VisitEurope_Experience::getMetaQueryForDestinationID($this->post->ID);
      }

      // Add date restriction if necessary
      $metaQueryDates = VisitEurope_Experience::getMetaQueryForEventDateRestriction($this->post->ID);

      if (!empty($metaQueryDates) && is_array($metaQueryDates)) {
        $metaQuery[] = $metaQueryDates;
      }

      $params = [
        'posts_per_page' => $this->limit,
        'tax_query' => $taxQuery,
        'meta_query' => $metaQuery,
      ];

      if (is_array($this->itemIDs)) {
        if (!empty($this->itemIDs)) {
          $params['post__in'] = $this->itemIDs;
        } else {
          $params['post__in'] = [-999];
        }
      }

      $items = VisitEurope_Experience::getEvents($params);
    }

    return $this->presentExperienceItems($items);
  }

  /**
   * Get items for Galleries
   *
   * @return array
   */
  private function getItemsForGalleries() {
    if ($this->restrictToValidLocation) {
      $items = VisitEurope_Experience::getWithValidLocation($this->post->ID, $this->limit, $this->type);
    } else {
      $taxQuery = VisitEurope_Interest::getTaxQuery($this->interests);
      $metaQuery = [];

      if ($this->post->post_type == VisitEurope_CPT_Destination::POST_TYPE) {
        $metaQuery = VisitEurope_Experience::getMetaQueryForDestinationID($this->post->ID);
      }

      $params = [
        'posts_per_page' => $this->limit,
        'tax_query' => $taxQuery,
        'meta_query' => $metaQuery,
      ];

      if (is_array($this->itemIDs)) {
        if (!empty($this->itemIDs)) {
          $params['post__in'] = $this->itemIDs;
        } else {
          $params['post__in'] = [-999];
        }
      }

      $items = VisitEurope_Experience::getGalleries($params);
    }

    return $this->presentExperienceItems($items);
  }

  /**
   * Get items for Products
   *
   * @return array
   */
  private function getItemsForProducts() {
    if ($this->restrictToValidLocation) {
      $items = VisitEurope_Experience::getWithValidLocation($this->post->ID, $this->limit, $this->type);
    } else {
      $taxQuery = VisitEurope_Interest::getTaxQuery($this->interests);
      $metaQuery = [];

      if ($this->post->post_type == VisitEurope_CPT_Destination::POST_TYPE) {
        $metaQuery = VisitEurope_Experience::getMetaQueryForDestinationID($this->post->ID);
      }

      $params = [
        'posts_per_page' => $this->limit,
        'tax_query' => $taxQuery,
        'meta_query' => $metaQuery,
      ];

      if (is_array($this->itemIDs)) {
        if (!empty($this->itemIDs)) {
          $params['post__in'] = $this->itemIDs;
        } else {
          $params['post__in'] = [-999];
        }
      }

      $items = VisitEurope_Experience::getProducts($params);
    }

    return $this->presentExperienceItems($items);
  }

  /**
   * Get items for Recipes
   *
   * @return array
   */
  private function getItemsForRecipes() {
    if ($this->restrictToValidLocation) {
      $items = VisitEurope_Experience::getWithValidLocation($this->post->ID, $this->limit, $this->type);
    } else {
      $taxQuery = VisitEurope_Interest::getTaxQuery($this->interests);
      $metaQuery = [];

      if ($this->post->post_type == VisitEurope_CPT_Destination::POST_TYPE) {
        $metaQuery = VisitEurope_Experience::getMetaQueryForDestinationID($this->post->ID);
      }

      $params = [
        'posts_per_page' => $this->limit,
        'tax_query' => $taxQuery,
        'meta_query' => $metaQuery,
      ];

      if (is_array($this->itemIDs)) {
        if (!empty($this->itemIDs)) {
          $params['post__in'] = $this->itemIDs;
        } else {
          $params['post__in'] = [-999];
        }
      }

      $items = VisitEurope_Experience::getRecipes($params);
    }

    return $this->presentExperienceItems($items);
  }

  /**
   * Get items for Region
   *
   * @return array
   */
  private function getItemsForRegion() {
    $items = VisitEurope_Destination::getRegions(['posts_per_page' => $this->limit]);
    return $this->presentDestinationItems($items);
  }

  /**
   * Get items for Routes
   *
   * @return array
   */
  private function getItemsForRoutes() {
    if ($this->restrictToValidLocation) {
      $items = VisitEurope_Experience::getWithValidLocation($this->post->ID, $this->limit, $this->type);
    } else {
      $taxQuery = VisitEurope_Interest::getTaxQuery($this->interests);
      $metaQuery = [];

      if ($this->post->post_type == VisitEurope_CPT_Destination::POST_TYPE) {
        $metaQuery = VisitEurope_Experience::getMetaQueryForDestinationID($this->post->ID);
      }

      $params = [
        'posts_per_page' => $this->limit,
        'tax_query' => $taxQuery,
        'meta_query' => $metaQuery,
      ];

      if (is_array($this->itemIDs)) {
        if (!empty($this->itemIDs)) {
          $params['post__in'] = $this->itemIDs;
        } else {
          $params['post__in'] = [-999];
        }
      }

      $items = VisitEurope_Experience::getRoutes($params);
    }

    return $this->presentExperienceItems($items);
  }

  /**
   * Get items for Trips
   *
   * @return array
   */
  private function getItemsForTrip() {
    $taxQuery = VisitEurope_Interest::getTaxQuery($this->interests);

    $params = [
      'posts_per_page' => $this->limit,
      'tax_query' => $taxQuery
    ];

    if (is_array($this->itemIDs)) {
      if (!empty($this->itemIDs)) {
        $params['post__in'] = $this->itemIDs;
      } else {
        $params['post__in'] = [-999];
      }
    }

    $items = VisitEurope_Trip::get($params);

    return $this->presentTripItems($items);
  }

  /**
   * Present Country items
   *
   * @param array $items
   * @return array
   */
  private function presentDestinationItems($items) {
    $return = [];

    foreach ($items as $key => $item) {
      $veDestination = new VisitEurope_Destination($item);
      $return[] = $veDestination->presentPost();
    }

    return $return;
  }

  /**
   * Present Experience items
   *
   * @param array $items
   * @return array
   */
  private function presentExperienceItems($items) {
    $return = [];

    foreach ($items as $key => $item) {
      $veExperience = new VisitEurope_Experience($item);
      $return[] = $veExperience->presentPost();
    }

    return $return;
  }

  /**
   * Present Trip items
   *
   * @param array $items
   * @return array
   */
  private function presentTripItems($items) {
    $return = [];

    foreach ($items as $key => $item) {
      $veTrip = new VisitEurope_Trip($item);
      $return[] = $veTrip->presentPost();
    }

    return $return;
  }

  /**
   * Get the standard items
   *
   * @return array
   */
  public function getStandardItems() {
    if (!empty($this->items)) {
      return $this->items;
    }

    $this->items = $this->getItems();

    return $this->items;
  }

  /**
   * Present StandardGrid
   *
   * @return array
   */
  public function presentLayout() {
    $onMap = get_sub_field('on_map', $this->post->ID);
    $title = get_sub_field('title', $this->post->ID);
    $type = self::getTypeFromRaw(get_sub_field('type', $this->post->ID));
    $limit = intval(get_sub_field('limit', $this->post->ID));
    $url = get_sub_field('url', $this->post->ID);

    $this->limit = intval($limit);
    $this->restrictToValidLocation = true;

    $standard = $this->getStandardItems();

    if (!is_array($standard) || empty($standard) || $onMap !== true) {
      return [];
    }

    return [
      'standard' => $standard,
      'title' => $title ? $title : null,
      'url' => $url ? $url : null,
    ];
  }

  /**
   * Get the type from the raw type
   *
   * @param string $rawType
   * @return string
   */
  private static function getTypeFromRaw($rawType) {
    $type = null;

    switch ($rawType) {
      case 'Articles':
        $type = VisitEurope_Experience::TYPE_ARTICLE;
        break;
      case 'article':
        $type = VisitEurope_Experience::TYPE_ARTICLE;
        break;
      case 'Countries':
        $type = VisitEurope_Destination::TYPE_COUNTRY;
        break;
      case 'Destinations':
        $type = VisitEurope_Destination::TYPE_ALL;
        break;
      case 'Events':
        $type = VisitEurope_Experience::TYPE_EVENT;
        break;
      case 'event':
        $type = VisitEurope_Experience::TYPE_EVENT;
        break;
      case 'Experiences';
        $type = VisitEurope_Experience::TYPE_ALL;
        break;
      case 'Galleries':
        $type = VisitEurope_Experience::TYPE_GALLERY;
        break;
      case 'gallery':
        $type = VisitEurope_Experience::TYPE_GALLERY;
        break;
      case 'Regions':
        $type = VisitEurope_Destination::TYPE_REGION;
        break;
      case 'Products':
        $type = VisitEurope_Experience::TYPE_PRODUCT;
        break;
      case 'product':
        $type = VisitEurope_Experience::TYPE_PRODUCT;
        break;
      case 'Recipes':
        $type = VisitEurope_Experience::TYPE_RECIPE;
        break;
      case 'recipe':
        $type = VisitEurope_Experience::TYPE_RECIPE;
        break;
      case 'Routes':
        $type = VisitEurope_Experience::TYPE_ROUTE;
        break;
      case 'route':
        $type = VisitEurope_Experience::TYPE_ROUTE;
        break;
      case 'Trips':
        $type = VisitEurope_Trip::TYPE_ALL;
        break;
      default:
        break;
    }

    return $type;
  }

  /**
   * Sets the interest group
   *
   * @param string $interestGroup
   * @return void
   */
  public function setInterestsByGroupName($name) {
    $interests = VisitEurope_Interest::getTaxIDsByInterestGroupName($name);

    if (is_array($interests) && !empty($interests)) {
      $this->interests = $interests;
    }
  }

}
