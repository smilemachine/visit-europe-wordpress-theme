<?php
if (!defined('ABSPATH')) exit;

/**
 * Setup for Visit Europe FlexibleContent: Carousel
 *
 * @package Visit_Europe
 */
class VisitEurope_ACF_FC_Carousel {

  protected $items;
  protected $title;
  protected $type;
  protected $limit;
  protected $post;

  const TYPE_CUSTOM = 'CUSTOM';
  const TYPE_PLACES = 'PLACES';
  const TYPE_THINGS = 'THINGS';

  /**
   * Setup
   *
   * @param WP_Post
   * @param string $type
   * @param bool $isHalfWidth
   * @return void
   */
  public function __construct($post, $type = '', $limit = 0) {
    $this->post = $post;
    $this->items = [];
    $this->limit = intval($limit);
    $this->type = null;

    if ($this->isValidType($type)) {
      $this->type = $type;
      $this->items = $this->getItems();
    }
  }

  /**
   * Sets the type from a raw value
   *
   * @param string $rawType
   * @return void
   */
  public function setTypeFromRaw($rawType) {
    if ($this->isValidType($rawType)) {
      $this->type = $rawType;
    } else {
      $type = self::getTypeFromRaw($rawType);

      if ($this->isValidType($type)) {
        $this->type = $type;
      }
    }
  }

  /**
   * Sets the title from a raw value
   *
   * @param string $rawTitle
   * @return void
   */
  public function setTitleFromRaw($rawTitle = '') {
    $this->title = '';

    if (!empty($rawTitle) && is_string($rawTitle)) {
      $this->title = $rawTitle;
    } else {
      switch ($this->type) {
        case VisitEurope_Destination::TYPE_COUNTRY:
          $this->title = VisitEurope_Content::getLocale('carousel.places.title');
          break;
        case self::TYPE_PLACES:
          $this->title = VisitEurope_Content::getLocale('carousel.places.title');
          break;
        case self::TYPE_THINGS:
          $this->title = VisitEurope_Content::getLocale('carousel.things.title');
          break;
        default:
          break;
      }
    }
  }

  /**
   * Returns the title
   *
   * @return string
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * Determines if this type is currently supported
   *
   * @param string $type
   * @return boolean
   */
  private function isValidType($type) {
    $types = [
      VisitEurope_Destination::TYPE_COUNTRY,
      self::TYPE_PLACES,
      self::TYPE_THINGS,
    ];

    return in_array($type, $types);
  }/**
   * Gets items for the current type
   *
   * @return array
   */
  private function getItems() {
    $items = [];

    if (!$this->type) {
      return $items;
    }

    switch ($this->type) {
      case VisitEurope_Destination::TYPE_COUNTRY:
        $items = $this->getItemsForCountry();
        break;
      case VisitEurope_Destination::TYPE_ALL:
        $items = $this->getItemsForDestination();
        break;
      case VisitEurope_Experience::TYPE_ALL:
        $items = $this->getItemsForExperience();
        break;
      case VisitEurope_Destination::TYPE_REGION:
        $items = $this->getItemsForRegion();
        break;
      case self::TYPE_CUSTOM:
        $items = $this->getItemsForCustom();
        break;
      case self::TYPE_PLACES:
        $items = $this->getItemsForPlaces();
        break;
      case self::TYPE_THINGS:
        $items = $this->getItemsForThings();
        break;
      default:
        break;
    }

    return $items;
  }

  /**
   * Get items for Destinations
   *
   * @return array
   */
  private function getItemsForDestination() {
    $items = VisitEurope_Destination::get(['posts_per_page' => $this->limit]);
    return $this->presentDestinationItems($items);
  }

  /**
   * Get items for Countries
   *
   * @return array
   */
  private function getItemsForCountry() {
    if ($this->post->post_type == VisitEurope_CPT_Destination::POST_TYPE
        && VisitEurope_Destination::getTypeForPostID($this->post->ID) == VisitEurope_Destination::TYPE_REGION) {
        $items = VisitEurope_Destination::getCountriesForPostID($this->post->ID, [
          'posts_per_page' => -1,
          'orderby' => 'title',
          'order' => 'ASC',
        ]);
    } else {
      $items = VisitEurope_Destination::getCountries(['posts_per_page' => $this->limit]);
    }

    return $this->presentDestinationItems($items);
  }

  /**
   * Get items for Experiences
   *
   * @return array
   */
  private function getItemsForExperience() {
    $items = VisitEurope_Experience::get(['posts_per_page' => $this->limit]);
    return $this->presentExperienceItems($items);
  }

  /**
   * Get items for Regions
   *
   * @return array
   */
  private function getItemsForRegion() {
    $items = VisitEurope_Destination::getRegions(['posts_per_page' => $this->limit]);
    return $this->presentDestinationItems($items);
  }

  /**
   * Get items for Custom
   *
   * @return array
   */
  private function getItemsForCustom() {
    return $this->presentCustomItems([]);
  }

  /**
   * Get items for Places. Invalidate the map data as we don't want them plotted
   *
   * @return array
   */
  private function getItemsForPlaces() {
    $countries = $this->getItemsForCountry();
    $places = [];

    if (is_array($countries) && !empty($countries)) {
      foreach ($countries as $country) {
        $country['map']['location']['lat'] = 0;
        $country['map']['location']['lng'] = 0;

        $places[] = $country;
      }
    }

    return $places;
  }

  /**
   * Get items for Custom
   *
   * @return array
   */
  private function getItemsForThings() {
    $items = get_field('top_things_gallery', $this->post->ID);
    return $this->presentThingsItems($items);
  }

  /**
   * Present Places items
   *
   * @param array $items
   * @return array
   */
  private function presentDestinationItems($items) {
    $return = [];

    foreach ($items as $key => $item) {
      $fc = new VisitEurope_Destination($item);
      $return[] = $fc->presentPost();
    }

    return $return;
  }

  /**
   * Present Thing items
   *
   * @param array $items
   * @return array
   */
  private function presentExperienceItems($items) {
    $return = [];

    foreach ($items as $key => $item) {
      $fc = new VisitEurope_Experience($item);
      $return[] = $fc->presentPost();
    }

    return $return;
  }

  /**
   * Present Custom items
   *
   * @param array $items
   * @return array
   */
  private function presentCustomItems($items) {
    return [];
  }

  /**
   * Present Things items
   *
   * @param array $items
   * @return array
   */
  private function presentThingsItems($items = []) {
    $return = [];

    if (is_array($items) && !empty($items)) {
      foreach ($items as $key => $item) {
        $return[] = [
          'id' => $key,
          'body' => VisitEurope_Helper::formatStringForFrontend($item['body']),
          'filters' => [],
          'copyright' => VisitEurope_Helper::getAttachmentCopyright($item['image']['ID']),
          'copyrighturl' => VisitEurope_Helper::getAttachmentCopyrightUrl($item['image']['ID']),
          'imageinitial' => $item['image']['sizes']['initial'],
          'imagefull' => $item['image']['sizes']['medium'],
          'map' => [
            'icon' => get_template_directory_uri() . '/assets/img/google-map-marker.png',
            'location' => [
              'lat' => (float) $item['lat'],
              'lng' => (float) $item['lng'],
            ],
          ],
          'save' => true,
          'title' => VisitEurope_Helper::formatStringForFrontend($item['title']),
          'type' => '',
          'url' => '#',
        ];
      }
    }

    return $return;
  }

  /**
   * Present Accordion
   *
   * @return array
   */
  public function presentLayout() {
    $limit = intval(get_sub_field('limit', $this->post->ID));
    $onMap = get_sub_field('on_map', $this->post->ID);
    $rawType = get_sub_field('type', $this->post->ID);
    $rawTitle = get_sub_field('title', $this->post->ID);
    $url = get_sub_field('url', $this->post->ID);

    $this->setTypeFromRaw($rawType);
    $this->setTitleFromRaw($rawTitle);

    $items = $this->getItems();
    $title = $this->getTitle();

    if (!is_array($items) || empty($items) || $onMap !== true) {
      return [];
    }

    return [
      'content' => $items,
      'title' => $title ? $title : null,
      'url' => $url ? $url : null,
    ];
  }

  /**
   * Get the type from the raw type
   *
   * @param string $rawType
   * @return string
   */
  private static function getTypeFromRaw($rawType) {
    $type = null;

    switch ($rawType) {
      case 'Countries':
        $type = VisitEurope_Destination::TYPE_COUNTRY;
        break;
      case 'Regions':
        $type = VisitEurope_Destination::TYPE_REGION;
        break;
      case 'Destinations';
        $type = VisitEurope_Destination::TYPE_ALL;
        break;
      case 'Experiences':
        $type = VisitEurope_Experience::TYPE_ALL;
        break;
      case 'Custom':
        $type = self::TYPE_CUSTOM;
        break;
      case 'Places':
        $type = self::TYPE_PLACES;
        break;
      case 'Things':
        $type = self::TYPE_THINGS;
        break;
      default:
        break;
    }

    return $type;
  }

}
