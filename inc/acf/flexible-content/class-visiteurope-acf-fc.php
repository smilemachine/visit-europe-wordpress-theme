<?php
if (!defined('ABSPATH')) exit;

/**
 * Setup for Visit Europe
 *
 * @package Visit_Europe
 */
class VisitEurope_ACF_FC {

  private $post;

  /**
   * Setup
   *
   * @param WP_Post $post
   * @return void
   */
  public function __construct($post = null) {
    $this->post = $post;
  }

  /**
   * Get the content to render ACF flexible content: carousel
   *
   * @param array $item
   * @return string
   */
  public static function getParamsFromItem($item) {
    $params = [];

    if (is_array($item) && !empty($item)) {
      foreach ($item as $key => $value) {
        if ($value === true) {
          $value = 'true';
        } else if ($value === false) {
          $value = 'false';
        } else if (is_array($value)) {
          $value = '';
        }

        $params[] = $key . '="' . $value . '"';
      }
    }

    return implode(' ', $params);
  }

  /**
   * Present the modules for a given post
   *
   * @return array
   */
  public function presentPost() {
    $items = [];

    for ($i=1; $i<=4; $i++) {
      while (have_rows('content_tab' . $i, $this->post->ID)) {
        the_row();
        $layout = $this->presentLayout(get_row_layout(), get_sub_field('type'));

        if (!isset($items[$i])) {
          $items[$i] = [];
        }

        if (is_array($layout) && !empty($layout) && isset($layout['content']) && is_array($layout['content']) && !empty($layout['content'])) {
          $items['tab' . $i][] = $layout;
        }
      }
    }

    // Remove items with empty content
    foreach ($items as $key => $value) {
      if (empty($value)) {
        unset($items[$key]);
      }
    }

    return $items;
  }

  /**
   * Present the given layout
   *
   * @param string $layout
   * @param string $type
   * @return array
   */
  private function presentLayout($layout, $type) {
    $content = null;

    switch ($layout) {
      case 'accordion':
        $fc = new VisitEurope_ACF_FC_Accordion($this->post);
        $fcContent = $fc->presentLayout();

        if (is_array($fcContent) && !empty($fcContent)) {
          $content = $fcContent;
        }
        break;
      case 'carousel':
        $fc = new VisitEurope_ACF_FC_Carousel($this->post);
        $fc->setTypeFromRaw($type);
        $fcContent = $fc->presentLayout();

        if (is_array($fcContent) && !empty($fcContent)) {
          $content = $fcContent;
        }
        break;
      case 'feature_grid':
        $fc = new VisitEurope_ACF_FC_FeatureGrid($this->post);
        $fc->setTypeFromRaw($type);
        $fcContent = $fc->presentLayout();

        if (is_array($fcContent) && !empty($fcContent)) {
          $content = $fcContent;
        }
        break;
      case 'interests_grid':
        $fc = new VisitEurope_ACF_FC_InterestsGrid($this->post);
        $fc->setTypeFromRaw($type);
        $fcContent = $fc->presentLayout();

        if (is_array($fcContent) && !empty($fcContent)) {
          $content = $fcContent;
        }
        break;
      case 'standard_grid':
        $fc = new VisitEurope_ACF_FC_StandardGrid($this->post);
        $fc->setTypeFromRaw($type);
        $fcContent = $fc->presentLayout();

        if (is_array($fcContent) && !empty($fcContent)) {
          $content = $fcContent;
        }
        break;
      default:
        break;
    }

    return [
      'layout' => $layout,
      'content' => $content,
    ];
  }

  /**
   * Returns the title for the pointer from the given type
   *
   * @param string $type
   * @return string
   */
  public static function getPointerTypeFromRawType($rawType) {
    $type = '';

    switch ($rawType) {
      case 'Airport':
        $type = 'airport';
        break;
      case 'Capital City':
        $type = 'capitalCity';
        break;
      case 'City':
        $type = 'city';
        break;
      case 'Currency':
        $type = 'currency';
        break;
      case 'Language':
        $type = 'language';
        break;
      case 'More Info':
        $type = 'moreInfo';
        break;
      case 'Timezone':
        $type = 'timezone';
        break;
      case 'Where':
        $type = 'where';
        break;
      case 'When':
        $type = 'when';
        break;
      default:
        break;
    }

    return $type;
  }

  /**
   * Returns the appropriate html class for the given type
   *
   * @param string
   * @return string
   */
  public static function getPointerHtmlClassFromType($type) {
    $options = [
      'airport' => 'airport',
      'capitalCity' => 'capital-city',
      'city' => 'city',
      'currency' => 'currency',
      'language' => 'language',
      'moreInfo' => 'more-info',
      'timezone' => 'timezone',
      'where' => 'where',
      'when' => 'when',
    ];

    if (!isset($options[$type])) {
      return '';
    }

    return $options[$type];
  }

  /**
   * Returns the title for the pointer from the given type
   *
   * @param string $type
   * @return string
   */
  public static function getPointerTitleFromType($type) {
    return VisitEurope_Content::getLocale('flexibleContent.pointers.' . $type);
  }

  /**
   * Present an Experience Carousel item
   *
   * @param array $slides
   * @return array
   */
  public static function presentExperienceCarousel($slides = []) {
    $items = [];

    if (is_array($slides) && !empty($slides)) {
      foreach ($slides as $key => $slide) {
        $items[] = [
          'body' => VisitEurope_Helper::formatStringForFrontend($slide['body']),
          'id' => $key,
          'imageinitial' => $slide['image']['sizes']['gallery_thumbnail'],
          'imagefull' => $slide['image']['sizes']['gallery_large'],
          'map' => [
            'icon' => get_template_directory_uri() . '/assets/img/google-map-marker.png',
            'location' => [
              'lat' => (float) $slide['lat'],
              'lng' => (float) $slide['lng'],
            ],
          ],
          'title' => VisitEurope_Helper::formatStringForFrontend($slide['title']),
        ];
      }
    }

    return $items;
  }
}
