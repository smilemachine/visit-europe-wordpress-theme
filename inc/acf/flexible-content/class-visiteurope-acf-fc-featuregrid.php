<?php
if (!defined('ABSPATH')) exit;

/**
 * Setup for Visit Europe FlexibleContent: FeatureGrid
 *
 * @package Visit_Europe
 */
class VisitEurope_ACF_FC_FeatureGrid {

  const ORIENTATION_LEFT = 'LEFT';
  const ORIENTATION_RIGHT = 'RIGHT';

  protected $items;
  protected $type;
  protected $isHalfWidth;
  protected $post;
  protected $orderBy;
  protected $order;
  protected $orientation;
  protected $includeAdvert;
  protected $restrictToValidLocation;

  /**
   * Setup
   *
   * @param WP_Post $post
   * @param string $type
   * @param bool $isHalfWidth
   * @param string $orderBy
   * @return void
   */
  public function __construct($post, $type = '', $isHalfWidth = false) {
    $this->post = $post;
    $this->items = [];
    $this->type = null;
    $this->orderBy = 'rand';
    $this->isHalfWidth = $isHalfWidth === true;
    $this->includeAdvert = true;
    $this->restrictToValidLocation = false;

    if ($this->isValidType($type)) {
      $this->type = $type;
      $this->items = $this->getItems();
    }
  }

  /**
   * Sets the type from a raw value
   *
   * @param string $rawType
   * @return void
   */
  public function setTypeFromRaw($rawType) {
    if ($this->isValidType($rawType)) {
      $this->type = $rawType;
    } else {
      $type = self::getTypeFromRaw($rawType);

      if ($this->isValidType($type)) {
        $this->type = $type;
      }
    }
  }

  /**
   * Sets the order by value
   *
   * @param string $orderBy
   * @return void
   */
  public function setOrderBy($orderBy) {
    $this->orderBy = $orderBy;
    $this->order = $orderBy == 'title' ? 'ASC' : 'DESC';
  }

  /**
   * Sets the half width to true
   *
   * @return void
   */
  public function setHalfWidth() {
    $this->isHalfWidth = true;
  }

  /**
   * Returns the View All url for this feature grid
   *
   * @return string
   */
  public function getViewAllUrl() {
    return VisitEurope_Helper::getViewAllUrl($this->post, $this->type);
  }

  /**
   * Sets the orientation
   *
   * @param string $orientation
   * @return void
   */
  public function setOrientation($orientation) {
    if ($orientation != self::ORIENTATION_LEFT) {
      $this->orientation = self::ORIENTATION_RIGHT;
    } else {
      $this->orientation = self::ORIENTATION_LEFT;
    }
  }

  /**
   * Determines if this type is currently supported
   *
   * @param string $type
   * @return boolean
   */
  private function isValidType($type) {
    $types = [
      VisitEurope_Destination::TYPE_COUNTRY,
      VisitEurope_Destination::TYPE_REGION,
      VisitEurope_Experience::TYPE_ALL,
      VisitEurope_Experience::TYPE_ARTICLE,
      VisitEurope_Experience::TYPE_EVENT,
      VisitEurope_Experience::TYPE_GALLERY,
      VisitEurope_Experience::TYPE_PRODUCT,
      VisitEurope_Experience::TYPE_RECIPE,
      VisitEurope_Experience::TYPE_ROUTE,
      VisitEurope_Trip::TYPE_ALL
    ];

    return in_array($type, $types);
  }

  /**
   * Gets items for the current type
   *
   * @return array
   */
  private function getItems() {
    $items = [];

    if (!$this->type) {
      return $items;
    }

    switch ($this->type) {
      case VisitEurope_Destination::TYPE_COUNTRY:
        $items = $this->getItemsForCountry();
        break;
      case VisitEurope_Destination::TYPE_REGION:
        $items = $this->getItemsForRegion();
        break;
      case VisitEurope_Experience::TYPE_ALL:
        $items = $this->getItemsForExperience();
        break;
      case VisitEurope_Experience::TYPE_ARTICLE:
        $items = $this->getItemsForArticles();
        break;
      case VisitEurope_Experience::TYPE_EVENT:
        $items = $this->getItemsForEvents();
        break;
      case VisitEurope_Experience::TYPE_GALLERY:
        $items = $this->getItemsForGalleries();
        break;
      case VisitEurope_Experience::TYPE_PRODUCT:
        $items = $this->getItemsForProducts();
        break;
      case VisitEurope_Experience::TYPE_RECIPE:
        $items = $this->getItemsForRecipes();
        break;
      case VisitEurope_Experience::TYPE_ROUTE:
        $items = $this->getItemsForRoutes();
        break;
      case VisitEurope_Trip::TYPE_ALL:
        $items = $this->getItemsForTrip();
        break;
      default:
        break;
    }

    return $items;
  }

  /**
   * Get items for Articles
   *
   * @return array
   */
  private function getItemsForArticles() {
    $featuredLimit = $this->getFeaturedLimit();
    $standardLimit = $this->getStandardLimit();

    if ($this->restrictToValidLocation) {
      $featuredItems = VisitEurope_Experience::getWithValidLocation($this->post->ID, $featuredLimit, $this->type);
      $standardItems = VisitEurope_Experience::getWithValidLocation($this->post->ID, $standardLimit, $this->type);
    } else {
      $metaQuery = [];

      if ($this->post->post_type == VisitEurope_CPT_Destination::POST_TYPE) {
        $metaQuery = VisitEurope_Experience::getMetaQueryForDestinationID($this->post->ID);
      }

      $featuredItems = VisitEurope_Experience::getArticles([
        'posts_per_page' => $featuredLimit,
        'orderby' => $this->orderBy,
        'order' => $this->order,
        'meta_query' => $metaQuery,
      ]);

      $standardItems = VisitEurope_Experience::getArticles([
        'posts_per_page' => $standardLimit,
        'orderby' => $this->orderBy,
        'order' => $this->order,
        'meta_query' => $metaQuery,
      ]);
    }

    $featured = $this->presentExperienceItems($featuredItems);
    $standard = $this->presentExperienceItems($standardItems);

    return array_merge($featured, $standard);
  }

  /**
   * Get items for Country
   *
   * @return array
   */
  private function getItemsForCountry() {
    $featuredLimit = $this->getFeaturedLimit();
    $standardLimit = $this->getStandardLimit();

    $featuredItems = VisitEurope_Destination::getCountries([
      'posts_per_page' => $featuredLimit,
      'orderby' => $this->orderBy,
      'order' => $this->order,
    ]);

    $standardItems = VisitEurope_Destination::getCountries([
      'posts_per_page' => $standardLimit,
      'orderby' => $this->orderBy,
      'order' => $this->order,
    ]);

    $featured = $this->presentCountryItems($featuredItems);
    $standard = $this->presentCountryItems($standardItems);

    return array_merge($featured, $standard);
  }

  /**
   * Get items for Events
   *
   * @return array
   */
  private function getItemsForEvents() {
    $featuredLimit = $this->getFeaturedLimit();
    $standardLimit = $this->getStandardLimit();

    if ($this->restrictToValidLocation) {
      $featuredItems = VisitEurope_Experience::getWithValidLocation($this->post->ID, $featuredLimit, $this->type);
      $standardItems = VisitEurope_Experience::getWithValidLocation($this->post->ID, $standardLimit, $this->type);
    } else {
      $metaQuery = [];

      if ($this->post->post_type == VisitEurope_CPT_Destination::POST_TYPE) {
        $metaQuery = VisitEurope_Experience::getMetaQueryForDestinationID($this->post->ID);
      }

      // Add date restriction if necessary
      $metaQueryDates = VisitEurope_Experience::getMetaQueryForEventDateRestriction($this->post->ID);

      if (!empty($metaQueryDates) && is_array($metaQueryDates)) {
        $metaQuery[] = $metaQueryDates;
      }

      $featuredItems = VisitEurope_Experience::getEvents([
        'posts_per_page' => $featuredLimit,
        'orderby' => $this->orderBy,
        'order' => $this->order,
        'meta_query' => $metaQuery,
      ]);

      $standardItems = VisitEurope_Experience::getEvents([
        'posts_per_page' => $standardLimit,
        'orderby' => $this->orderBy,
        'order' => $this->order,
        'meta_query' => $metaQuery,
      ]);
    }

    $featured = $this->presentExperienceItems($featuredItems);
    $standard = $this->presentExperienceItems($standardItems);

    return array_merge($featured, $standard);
  }

  /**
   * Get items for Galleries
   *
   * @return array
   */
  private function getItemsForGalleries() {
    $featuredLimit = $this->getFeaturedLimit();
    $standardLimit = $this->getStandardLimit();

    if ($this->restrictToValidLocation) {
      $featuredItems = VisitEurope_Experience::getWithValidLocation($this->post->ID, $featuredLimit, $this->type);
      $standardItems = VisitEurope_Experience::getWithValidLocation($this->post->ID, $standardLimit, $this->type);
    } else {
      $metaQuery = [];

      if ($this->post->post_type == VisitEurope_CPT_Destination::POST_TYPE) {
        $metaQuery = VisitEurope_Experience::getMetaQueryForDestinationID($this->post->ID);
      }

      $featuredItems = VisitEurope_Experience::getGalleries([
        'posts_per_page' => $featuredLimit,
        'orderby' => $this->orderBy,
        'order' => $this->order,
        'meta_query' => $metaQuery,
      ]);

      $standardItems = VisitEurope_Experience::getGalleries([
        'posts_per_page' => $standardLimit,
        'orderby' => $this->orderBy,
        'order' => $this->order,
        'meta_query' => $metaQuery,
      ]);
    }


    $featured = $this->presentExperienceItems($featuredItems);
    $standard = $this->presentExperienceItems($standardItems);

    return array_merge($featured, $standard);
  }

  /**
   * Get items for Experiences
   *
   * @return array
   */
  private function getItemsForExperience() {
    $featuredLimit = $this->getFeaturedLimit();
    $standardLimit = $this->getStandardLimit();

    if ($this->restrictToValidLocation) {
      $featuredItems = VisitEurope_Experience::getWithValidLocation($this->post->ID, $featuredLimit, $this->type);
      $standardItems = VisitEurope_Experience::getWithValidLocation($this->post->ID, $standardLimit, $this->type);
    } else {
      $metaQuery = [];

      if ($this->post->post_type == VisitEurope_CPT_Destination::POST_TYPE) {
        $metaQuery = VisitEurope_Experience::getMetaQueryForDestinationID($this->post->ID);
      }

      $featuredItems = VisitEurope_Experience::get([
        'posts_per_page' => $featuredLimit,
        'orderby' => $this->orderBy,
        'order' => $this->order,
        'meta_query' => $metaQuery,
      ]);

      $standardItems = VisitEurope_Experience::get([
        'posts_per_page' => $standardLimit,
        'orderby' => $this->orderBy,
        'order' => $this->order,
        'meta_query' => $metaQuery,
      ]);
    }

    $featured = $this->presentExperienceItems($featuredItems);
    $standard = $this->presentExperienceItems($standardItems);

    return array_merge($featured, $standard);
  }

  /**
   * Get items for Products
   *
   * @return array
   */
  private function getItemsForProducts() {
    $featuredLimit = $this->getFeaturedLimit();
    $standardLimit = $this->getStandardLimit();

    if ($this->restrictToValidLocation) {
      $featuredItems = VisitEurope_Experience::getWithValidLocation($this->post->ID, $featuredLimit, $this->type);
      $standardItems = VisitEurope_Experience::getWithValidLocation($this->post->ID, $standardLimit, $this->type);
    } else {
      $metaQuery = [];

      if ($this->post->post_type == VisitEurope_CPT_Destination::POST_TYPE) {
        $metaQuery = VisitEurope_Experience::getMetaQueryForDestinationID($this->post->ID);
      }

      $featuredItems = VisitEurope_Experience::getProducts([
        'posts_per_page' => $featuredLimit,
        'orderby' => $this->orderBy,
        'order' => $this->order,
        'meta_query' => $metaQuery,
      ]);

      $standardItems = VisitEurope_Experience::getProducts([
        'posts_per_page' => $standardLimit,
        'orderby' => $this->orderBy,
        'order' => $this->order,
        'meta_query' => $metaQuery,
      ]);
    }

    $featured = $this->presentExperienceItems($featuredItems);
    $standard = $this->presentExperienceItems($standardItems);

    return array_merge($featured, $standard);
  }

  /**
   * Get items for Recipes
   *
   * @return array
   */
  private function getItemsForRecipes() {
    $featuredLimit = $this->getFeaturedLimit();
    $standardLimit = $this->getStandardLimit();

    if ($this->restrictToValidLocation) {
      $featuredItems = VisitEurope_Experience::getWithValidLocation($this->post->ID, $featuredLimit, $this->type);
      $standardItems = VisitEurope_Experience::getWithValidLocation($this->post->ID, $standardLimit, $this->type);
    } else {
      $metaQuery = [];

      if ($this->post->post_type == VisitEurope_CPT_Destination::POST_TYPE) {
        $metaQuery = VisitEurope_Experience::getMetaQueryForDestinationID($this->post->ID);
      }

      $featuredItems = VisitEurope_Experience::getRecipes([
        'posts_per_page' => $featuredLimit,
        'orderby' => $this->orderBy,
        'order' => $this->order,
        'meta_query' => $metaQuery,
      ]);

      $standardItems = VisitEurope_Experience::getRecipes([
        'posts_per_page' => $standardLimit,
        'orderby' => $this->orderBy,
        'order' => $this->order,
        'meta_query' => $metaQuery,
      ]);
    }

    $featured = $this->presentExperienceItems($featuredItems);
    $standard = $this->presentExperienceItems($standardItems);

    return array_merge($featured, $standard);
  }

  /**
   * Get items for Region
   *
   * @return array
   */
  private function getItemsForRegion() {
    $featuredLimit = $this->getFeaturedLimit();
    $standardLimit = $this->getStandardLimit();

    $featuredItems = VisitEurope_Destination::getRegions([
      'posts_per_page' => $featuredLimit,
      'orderby' => $this->orderBy,
      'order' => $this->order,
    ]);

    $standardItems = VisitEurope_Destination::getRegions([
      'posts_per_page' => $standardLimit,
      'orderby' => $this->orderBy,
      'order' => $this->order,
    ]);

    $featured = $this->presentRegionItems($featuredItems);
    $standard = $this->presentRegionItems($standardItems);

    return array_merge($featured, $standard);
  }

  /**
   * Get items for Routes
   *
   * @return array
   */
  private function getItemsForRoutes() {
    $featuredLimit = $this->getFeaturedLimit();
    $standardLimit = $this->getStandardLimit();

    if ($this->restrictToValidLocation) {
      $featuredItems = VisitEurope_Experience::getWithValidLocation($this->post->ID, $featuredLimit, $this->type);
      $standardItems = VisitEurope_Experience::getWithValidLocation($this->post->ID, $standardLimit, $this->type);
    } else {
      $metaQuery = [];

      if ($this->post->post_type == VisitEurope_CPT_Destination::POST_TYPE) {
        $metaQuery = VisitEurope_Experience::getMetaQueryForDestinationID($this->post->ID);
      }

      $featuredItems = VisitEurope_Experience::getRoutes([
        'posts_per_page' => $featuredLimit,
        'orderby' => $this->orderBy,
        'order' => $this->order,
        'meta_query' => $metaQuery,
      ]);

      $standardItems = VisitEurope_Experience::getRoutes([
        'posts_per_page' => $standardLimit,
        'orderby' => $this->orderBy,
        'order' => $this->order,
        'meta_query' => $metaQuery,
      ]);
    }

    $featured = $this->presentExperienceItems($featuredItems);
    $standard = $this->presentExperienceItems($standardItems);

    return array_merge($featured, $standard);
  }

  /**
   * Get items for Trips
   *
   * @return array
   */
  private function getItemsForTrip() {
    $featuredLimit = $this->getFeaturedLimit();
    $standardLimit = $this->getStandardLimit();

    $featuredItems = VisitEurope_Trip::get([
      'posts_per_page' => $featuredLimit,
      'orderby' => $this->orderBy,
      'order' => $this->order,
    ]);

    $standardItems = VisitEurope_Trip::get([
      'posts_per_page' => $standardLimit,
      'orderby' => $this->orderBy,
      'order' => $this->order,
    ]);

    $featured = $this->presentTripItems($featuredItems);
    $standard = $this->presentTripItems($standardItems);

    return array_merge($featured, $standard);
  }

  /**
   * Present Country items
   *
   * @param array $items
   * @return array
   */
  private function presentCountryItems($items) {
    $return = [];

    foreach ($items as $key => $item) {
      $veDestination = new VisitEurope_Destination($item);
      $return[] = $veDestination->presentPost();
    }

    return $return;
  }

  /**
   * Present Experience items
   *
   * @param array $items
   * @return array
   */
  private function presentExperienceItems($items) {
    $return = [];

    foreach ($items as $key => $item) {
      $veExperience = new VisitEurope_Experience($item);
      $return[] = $veExperience->presentPost();
    }

    return $return;
  }

  /**
   * Present Region items
   *
   * @param array $items
   * @return array
   */
  private function presentRegionItems($items) {
    $return = [];

    foreach ($items as $key => $item) {
      $veDestination = new VisitEurope_Destination($item);
      $return[] = $veDestination->presentPost();
    }

    return $return;
  }

  /**
   * Present Trip items
   *
   * @param array $items
   * @return array
   */
  private function presentTripItems($items) {
    $return = [];

    foreach ($items as $key => $item) {
      $veTrip = new VisitEurope_Trip($item);
      $return[] = $veTrip->presentPost();
    }

    return $return;
  }

  /**
   * Get the featured items. We need at least the required number of results.
   *
   * @return array
   */
  public function getFeaturedItems() {
    if (empty($this->items)) {
      $this->items = $this->getItems();
    }

    if (empty($this->items)) {
      return [];
    }

    $featuredLimit = $this->getFeaturedLimit();
    $standardLimit = $this->getStandardLimit();

    if ($featuredLimit == 0 || empty($this->items)) {
      return [];
    }

    if (!$this->isHalfWidth) {
      return array_slice($this->items, 0, $featuredLimit);
    } else {
      if ($this->orientation == self::ORIENTATION_LEFT) { // Show featured first
        return array_slice($this->items, 0, $featuredLimit);

      } else {
        return array_slice($this->items, $standardLimit, $featuredLimit);
      }
    }
  }

  /**
   * Get the standard items. We need at least the required number of results.
   *
   * @return array
   */
  public function getStandardItems() {
    if (empty($this->items)) {
      $this->items = $this->getItems();
    }

    if (empty($this->items)) {
      return [];
    }

    $featuredLimit = $this->getFeaturedLimit();
    $standardLimit = $this->getStandardLimit();
    $items = [];

    if ($standardLimit == 0 || empty($this->items)) {
      return [];
    }

    if (!$this->isHalfWidth) {
      $items = array_slice($this->items, $featuredLimit);
    } else {
      if ($this->orientation == self::ORIENTATION_LEFT) { // Show standard second
        $items = array_slice($this->items, $featuredLimit);

      } else {
        $items = array_slice($this->items, 0, $standardLimit);
      }
    }

    if ($this->includeAdvert) {
      $items = $this->insertAdvertIntoItems($items);
    }

    return $items;
  }

  /**
   * Inserts an advertisement randomly into the array
   *
   * @param array $items
   * @return array
   */
  private function insertAdvertIntoItems($items) {
    if (!is_array($items)) {
      return [];
    } else if (empty($items) || $this->getAdvertLimit() != 1) {
      return $items;
    }

    $return = [];
    $randomKey = rand(0, count($items) - 1);
    $posts = VisitEurope_Advertisement::getFeature([
      'posts_per_page' => $this->getAdvertLimit()
    ]);

    foreach ($items as $key => $params) {
      if ($key == $randomKey && !empty($posts)) {
        $fc = new VisitEurope_Advertisement($posts[0]);
        $post = $fc->presentPost();

        if (is_array($post) && !empty($post)) {
          $itemParams = $post;
          $itemParams['type'] = 'advertisement';
        }

        $return[] = $itemParams;
      }

      $return[] = $params;
    }

    return $return;
  }

  /**
   * Returns the featured limit by the given type
   *
   * @return integer
   */
  private function getFeaturedLimit() {
    return 2;
  }

  /**
   * Returns the standard limit by the given type
   *
   * @return integer
   */
  private function getStandardLimit() {
    if ($this->isHalfWidth) {
      return 3;
    }

    $types = [
      VisitEurope_Destination::TYPE_COUNTRY,
      VisitEurope_Destination::TYPE_REGION
    ];

    $advertLimit = $this->getAdvertLimit();

    return in_array($this->type, $types) ? 6 : 6 - $advertLimit;
  }

  /**
   * Returns the advert limit by the given type
   *
   * @return integer
   */
  private function getAdvertLimit() {
    if (!$this->includeAdvert) {
      return 0;
    }

    $types = [
      VisitEurope_Destination::TYPE_COUNTRY,
      VisitEurope_Destination::TYPE_REGION
    ];

    // Ensure we actually have live advertisements to display
    $posts = get_posts([
      'post_status' => 'publish',
      'post_type' => VisitEurope_CPT_Advertisement::POST_TYPE,
      'orderby' => 'rand',
      'posts_per_page' => 1,
    ]);

    if (empty($posts)) {
      return 0;
    }

    return in_array($this->type, $types) ? 0 : 1;
  }

  /**
   * Present FeatureGrid
   *
   * @return array
   */
  public function presentLayout() {
    $onMap = get_sub_field('on_map', $this->post->ID);
    $title = get_sub_field('title', $this->post->ID);
    $type = self::getTypeFromRaw(get_sub_field('type', $this->post->ID));
    $url = get_sub_field('url', $this->post->ID);

    $this->includeAdvert = false;
    $this->restrictToValidLocation = true;

    $featured = $this->getFeaturedItems();
    $standard = $this->getStandardItems();

    if (!is_array($featured) || !is_array($standard) || empty($featured) || empty($standard) || $onMap !== true) {
      return [];
    }

    return [
      'featured' => $featured,
      'standard' => $standard,
      'title' => $title ? $title : null,
      'url' => $url ? $url : null,
    ];
  }

  /**
   * Get the type from the raw type
   *
   * @param string $rawType
   * @return string
   */
  private static function getTypeFromRaw($rawType) {
    $type = null;

    switch ($rawType) {
      case 'Articles';
        $type = VisitEurope_Experience::TYPE_ARTICLE;
        break;
      case 'Destinations':
        $type = VisitEurope_Destination::TYPE_ALL;
        break;
      case 'Events':
        $type = VisitEurope_Experience::TYPE_EVENT;
        break;
      case 'Experiences';
        $type = VisitEurope_Experience::TYPE_ALL;
        break;
      case 'Galleries':
        $type = VisitEurope_Experience::TYPE_GALLERY;
        break;
      case 'Products':
        $type = VisitEurope_Experience::TYPE_PRODUCT;
        break;
      case 'Recipes':
        $type = VisitEurope_Experience::TYPE_RECIPE;
        break;
      case 'Routes':
        $type = VisitEurope_Experience::TYPE_ROUTE;
        break;
      case 'Trips':
        $type = VisitEurope_Trip::TYPE_ALL;
        break;
      default:
        break;
    }

    return $type;
  }

}
