<?php
if (!defined('ABSPATH')) exit;

/**
 * Setup for Visit Europe FlexibleContent: HeaderBanner
 *
 * @package Visit_Europe
 */
class VisitEurope_ACF_HeaderBanner {

  const TYPE_DEFAULT = 'DEFAULT';
  const TYPE_STRIP = 'STRIP';

}
