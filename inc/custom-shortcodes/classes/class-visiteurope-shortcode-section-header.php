<?php
if (!defined('ABSPATH')) exit;

/**
 * Custom shortcodes
 *
 * @package Visit_Europe
 */
class VisitEurope_Shortcode_Section_Header {

  public function setup() {
    add_shortcode('ve_section_header', [self::class, 'render']);
  }

  /**
   * Renders the section header shortcode for Visit Europe
   * A type must be provided
   *
   * @param array $params
   * @return string
   */
  public static function render($params) {
    $title = self::getTitleFromParams($params);
    $url = self::getUrlFromParams($params);
    $event = self::getEventFromParams($params);
    $shortcodePath = dirname(__FILE__) . '/../views/section-header.php';

    if (!file_exists($shortcodePath)) {
      return;
    }

    ob_start();
    include $shortcodePath;
    return ob_get_clean();
  }

  /**
   * Determines the title from the params
   *
   * @param array $params
   * @return string
   */
  private static function getTitleFromParams($params = []) {
    $title = 'Visit Europe';

    if (is_array($params) && isset($params['title']) && !empty($params['title'])) {
      $title = $params['title'];
    }

    return $title;
  }

  /**
   * Determines the url from the params
   *
   * @param array $params
   * @return string
   */
  private static function getUrlFromParams($params = []) {
    $url = '';

    if (is_array($params) && isset($params['url']) && !empty($params['url'])) {
      $url = $params['url'];
    }

    return $url;
  }

  /**
   * Determines the event from the params
   *
   * @param array $params
   * @return string
   */
  private static function getEventFromParams($params = []) {
    $event = '';
    $supportedEvents = ['launchDestinationQuestions'];

    if (is_array($params) && isset($params['event']) && !empty($params['event']) && in_array($params['event'], $supportedEvents)) {
      $event = $params['event'];
    }

    return $event;
  }

}
