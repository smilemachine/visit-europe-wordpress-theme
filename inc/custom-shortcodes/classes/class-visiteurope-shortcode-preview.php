<?php
if (!defined('ABSPATH')) exit;

/**
 * Custom shortcodes
 *
 * @package Visit_Europe
 */
class VisitEurope_Shortcode_Preview {

  public function setup() {
    add_shortcode('ve_preview', [self::class, 'render']);
  }

  /**
   * Renders the preview shortcode for Visit Europe
   * A type must be provided
   *
   * @param array $params
   * @return string
   */
  public static function render($params) {
    $defaultParams = self::getDefaultParams();

    if (is_array($params)) {
      $params = array_merge($defaultParams, $params);
    } else {
      $params = $defaultParams;
    }

    $type = self::getTypeFromParams($params);

    // Ensure the file exists
    $shortcodePath = dirname(__FILE__) . '/../views/preview-' . $type . '.php';

    if (!file_exists($shortcodePath)) {
      return;
    }

    ob_start();
    include $shortcodePath;
    return ob_get_clean();
  }

  /**
   * Determines the type from the params
   *
   * @param array $params
   * @return string
   */
  private static function getTypeFromParams($params = []) {
    $type = '';
    $destinationTypes = VisitEurope_Destination::getTypes();
    $destinationTypeKeys = array_map('strtolower', array_keys($destinationTypes));
    $experienceTypes = VisitEurope_Experience::getTypes();
    $experienceTypeKeys = array_map('strtolower', array_keys($experienceTypes));
    $types = array_merge($destinationTypeKeys, $experienceTypeKeys, ['trip', 'advertisement', 'external']);

    if (is_array($params) && isset($params['type']) && in_array(strtolower($params['type']), $types)) {
      $type = strtolower($params['type']);
    }

    return $type;
  }

  /**
   * Returns the default params for a preview grid
   *
   * @return array
   */
  public static function getDefaultParams() {
    return [
      'save' => "",
      'title' => "",
      'body' => "",
      'imageinitial' => "",
      'imagefull' => "",
    ];
  }

}
