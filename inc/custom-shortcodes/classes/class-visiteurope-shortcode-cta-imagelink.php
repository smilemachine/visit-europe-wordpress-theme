<?php
if (!defined('ABSPATH')) exit;

/**
 * Custom shortcodes
 *
 * @package Visit_Europe
 */
class VisitEurope_Shortcode_CTA_Imagelink {

  public function setup() {
    add_shortcode('ve_cta_imagelink', [self::class, 'render']);
  }

  /**
   * Renders the cta imagelink shortcode for Visit Europe
   *
   * @param array $params
   * @return string
   */
  public static function render($params) {
    $imageinitial = self::getimageinitialFromParams($params);
    $imagefull = self::getimagefullFromParams($params);
    $title = self::getTitleFromParams($params);
    $url = self::getUrlFromParams($params);

    if (empty($title) || empty($url) || empty($imageinitial) || empty($imagefull)) {
      return;
    }

    $shortcodePath = dirname(__FILE__) . '/../views/cta-imagelink.php';

    if (!file_exists($shortcodePath)) {
      return;
    }

    ob_start();
    include $shortcodePath;
    return ob_get_clean();
  }

  /**
   * Determines the initial image from the params
   *
   * @param array $params
   * @return string
   */
  private static function getimageinitialFromParams($params = []) {
    $image = '';

    if (is_array($params) && isset($params['imageinitial']) && !empty($params['imageinitial'])) {
      $image = $params['imageinitial'];
    }

    return $image;
  }

  /**
   * Determines the full image from the params
   *
   * @param array $params
   * @return string
   */
  private static function getimagefullFromParams($params = []) {
    $image = '';

    if (is_array($params) && isset($params['imagefull']) && !empty($params['imagefull'])) {
      $image = $params['imagefull'];
    }

    return $image;
  }

  /**
   * Determines the title from the params
   *
   * @param array $params
   * @return string
   */
  private static function getTitleFromParams($params = []) {
    $title = 'Europe';

    if (is_array($params) && isset($params['title']) && !empty($params['title'])) {
      $title = $params['title'];
    }

    return $title;
  }

  /**
   * Determines the url from the params
   *
   * @param array $params
   * @return string
   */
  private static function getUrlFromParams($params = []) {
    $url = '';

    if (is_array($params) && isset($params['url']) && !empty($params['url'])) {
      $url = $params['url'];
    }

    return $url;
  }

}
