<?php
if (!defined('ABSPATH')) exit;

/**
 * External preview shortcode view
 *
 * @package Visit_Europe
 */

?>
<article class="preview experience-preview external-preview">
  <a href="<?php echo $params['url']; ?>" target="_blank">
    <section is="blurry-background-image"
      :initial="'<?php echo $params['imageinitial']; ?>'"
      :full="'<?php echo $params['imagefull']; ?>'">
      <span class="tag label label-primary"><?php echo _x('External', 'Tag: External', VisitEurope_Theme::KEY); ?></span>
    </section>
    <section class="content">
      <header>
        <h4><?php echo esc_html($params['title']); ?></h4>
      </header>
      <div class="blurb">
        <p><?php echo strip_tags($params['body']); ?></p>
      </div>
    </section>
  </a>
</article>
