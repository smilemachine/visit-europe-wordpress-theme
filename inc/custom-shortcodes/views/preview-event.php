<?php
if (!defined('ABSPATH')) exit;

/**
 * Event Experience preview shortcode view
 *
 * @package Visit_Europe
 */

$id = intval($params['id']);
$type = (string) $params['type'];

?>
<article class="preview experience-preview event-preview">
  <a href="<?php echo $params['url']; ?>"
    @click.prevent="previewItemWasClicked(<?php echo $id; ?>, '<?php echo $type; ?>', $event)">
    <section is="blurry-background-image"
      :initial="'<?php echo $params['imageinitial']; ?>'"
      :full="'<?php echo $params['imagefull']; ?>'">
      <span class="tag label label-primary"><?php echo _x('Event', 'Tag: Event', VisitEurope_Theme::KEY); ?></span>
      <?php if (isset($params['date'])) { ?>
        <span class="date label label-default">
          <?php echo $params['date']; ?>
        </span>
      <?php } ?>
    </section>
    <section class="content">
      <span class="save save-icon">
      </span>
      <header>
        <h4><?php echo esc_html($params['title']); ?></h4>
      </header>
      <div class="blurb">
        <p><?php echo strip_tags($params['body']); ?></p>
      </div>
    </section>
  </a>
</article>
