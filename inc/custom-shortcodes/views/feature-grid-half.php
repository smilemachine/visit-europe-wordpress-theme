<?php
if (!defined('ABSPATH')) exit;

/**
 * Grid shortcode view for half
 *
 * @package Visit_Europe
 */

$containerClasses = [
  'feature-grid',
  'feature-grid-size-' . $size,
  'feature-grid-orientation-' . $orientation,
  'feature-grid-type-' . $type,
];

?>
<section class="<?php echo implode(' ', $containerClasses); ?>">
  <div class="grid-container">
    <section class="featured">
      <div class="featured-container">
        <?php
          foreach ($featured as $params) {
            echo do_shortcode('[ve_preview ' . $params . ']');
          }
        ?>
      </div>
    </section>
    <section class="standard">
      <div class="standard-container">
        <?php
          foreach ($standard as $params) {
            echo do_shortcode('[ve_preview ' . $params . ']');
          }
        ?>
      </div>
    </section>
  </div>
</section>
