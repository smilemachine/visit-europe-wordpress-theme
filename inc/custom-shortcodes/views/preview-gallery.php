<?php
if (!defined('ABSPATH')) exit;

/**
 * Gallery Experience preview shortcode view
 *
 * @package Visit_Europe
 */

$id = intval($params['id']);
$type = (string) $params['type'];

?>
<article class="preview experience-preview gallery-preview">
  <a href="<?php echo $params['url']; ?>"
    @click.prevent="previewItemWasClicked(<?php echo $id; ?>, '<?php echo $type; ?>', $event)">
    <section is="blurry-background-image"
      :initial="'<?php echo $params['imageinitial']; ?>'"
      :full="'<?php echo $params['imagefull']; ?>'">
      <span class="tag label label-primary"><?php echo _x('Gallery', 'Tag: Gallery', VisitEurope_Theme::KEY); ?></span>
    </section>
    <section class="content">
      <span class="save save-icon">
      </span>
      <header>
        <h4><?php echo esc_html($params['title']); ?></h4>
      </header>
      <div class="blurb">
        <p><?php echo strip_tags($params['body']); ?></p>
      </div>
    </section>
  </a>
</article>
