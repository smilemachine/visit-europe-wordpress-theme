<?php
if (!defined('ABSPATH')) exit;

/**
 * Section header shortcode view
 *
 * @package Visit_Europe
 */

?>
<?php if (isset($title) && !empty($title)) { ?>
  <?php
    $hasUrl = false;
    $hasEvent = false;

    if (isset($url) && !empty($url)) {
      $hasUrl = true;
    }

    if (isset($event) && !empty($event)) {
      $hasEvent = true;
    }
  ?>
  <header class="section-header <?php echo $hasUrl ? 'section-header-w-link' : null; ?>">
    <?php if ($hasUrl && !$hasEvent) { ?>
      <a href="<?php echo $url; ?>" class="btn btn-default">
        <?php echo VisitEurope_Content::getLocale('sectionHeader.link'); ?>
      </a>
    <?php } ?>
    <?php if ($hasEvent) { ?>
      <a href="#" class="btn btn-default" @click.prevent="<?php echo $event; ?>">
        <?php echo VisitEurope_Content::getLocale('sectionHeader.link'); ?>
      </a>
    <?php } ?>
    <h2><span><?php echo $title; ?></span></h2>
  </header>
<?php } ?>
