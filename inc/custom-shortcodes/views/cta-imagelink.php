<?php
if (!defined('ABSPATH')) exit;

/**
 * Advert cta imagelink view
 *
 * @package Visit_Europe
 */

?>
<article class="cta-imagelink">
  <a href="<?php echo $url; ?>">
    <section is="blurry-background-image"
      :initial="'<?php echo $imageinitial; ?>'"
      :full="'<?php echo $imagefull; ?>'">
      <header>
        <h4><?php echo $title; ?></h4>
      </header>
    </section>
  </a>
</article>
