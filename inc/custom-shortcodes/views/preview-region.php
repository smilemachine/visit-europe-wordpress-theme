<?php
if (!defined('ABSPATH')) exit;

/**
 * Region Destination preview shortcode view
 *
 * @package Visit_Europe
 */

$id = intval($params['id']);
$type = (string) $params['type'];

?>
<article class="destination-preview region-preview">
  <a href="<?php echo $params['url']; ?>"
    @click.prevent="previewItemWasClicked(<?php echo $id; ?>, '<?php echo $type; ?>', $event)">
    <section is="blurry-background-image"
      :initial="'<?php echo $params['imageinitial']; ?>'"
      :full="'<?php echo $params['imagefull']; ?>'">
      <span class="save save-icon">
      </span>
      <header>
        <h4><?php echo esc_html($params['title']); ?></h4>
      </header>
    </section>
  </a>
</article>
