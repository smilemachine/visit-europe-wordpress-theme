<?php
if (!defined('ABSPATH')) exit;

/**
 * Advert preview shortcode view
 *
 * @package Visit_Europe
 */

?>
<article class="advert-preview">
  <a href="<?php echo $params['url']; ?>" target="_blank">
    <section is="blurry-background-image"
      :initial="'<?php echo $params['imageinitial']; ?>'"
      :full="'<?php echo $params['imagefull']; ?>'">
      <p class="body">
        <?php echo $params['body']; ?>
      </p>
      <span class="btn btn-primary">
        <?php echo $params['button']; ?>
      </span>
    </section>
  </a>
</article>
