<?php
/**
 * The template for displaying trips
 *
 * Template name: Trips
 * @package Visit_Europe
 */

?>
<?php get_header(); ?>
	<div id="trips">
		<?php echo VisitEurope_Helper::getTemplatePart('header-banner/header-banner'); ?>
		<section class="container intro">
			<?php
				while (have_posts()) {
					the_post();
					echo VisitEurope_Helper::getTemplatePart('content/page');
				}
			?>
		</section>
    <section class="container cta-map-trips">
      <a href="#" @click.prevent="launchInteractiveMap('trips')" class="btn btn-primary btn-lg btn-interactive-map">
        <?php echo VisitEurope_Content::getLocale('interactiveMap.label'); ?>
      </a>
    </section>
		<?php echo VisitEurope_Helper::getTemplatePart('acf/flexible-content/flexible-content'); ?>
	</div>
<?php get_footer(); ?>
