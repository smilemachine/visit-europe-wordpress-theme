<?php
if (!defined('ABSPATH')) exit;

/**
 * The template for displaying a user trip
 *
 * Template name: Experience Event Create
 * @package Visit_Europe
 */

if (!VisitEurope_Feature::isActive(VisitEurope_Feature::USER_ACCOUNTS)) {
  VisitEurope_Helper::throw404();
}

$user = VisitEurope_User::getCurrentMember();

if (!$user) {
  wp_redirect('/#user-verification');
}

global $post;

$countries = [];
$countryPosts = VisitEurope_Destination::getCountries([
  'orderby'=>'post_title',
  'order' => 'ASC',
  'posts_per_page' => -1
]);

if (is_array($countryPosts) && !empty($countryPosts)) {
  foreach ($countryPosts as $countryPost) {
    $countries[] = [
      'key' => $countryPost->ID,
      'title' => $countryPost->post_title,
    ];
  }
}

$interests = VisitEurope_Settings::getInterestGroupsForDropdown();

?>
<?php get_header(); ?>
  <?php
    echo VisitEurope_Helper::getTemplatePart('header-banner/header-banner', [
      'layout' => VisitEurope_ACF_HeaderBanner::TYPE_STRIP,
      'back' => true,
    ]);
  ?>
  <section class="container">
    <experience-event-create
      :countries='<?php echo VisitEurope_Helper::arrayToObjectString($countries); ?>'
      :interests='<?php echo VisitEurope_Helper::arrayToObjectString($interests); ?>'
      :private="false">
    </experience-event-create>
  </section>
<?php get_footer(); ?>
