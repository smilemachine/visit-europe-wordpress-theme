<?php
if (!defined('ABSPATH')) exit;

/**
 * The template for displaying a user trip
 *
 * @package Visit_Europe
 */

if (!VisitEurope_Feature::isActive(VisitEurope_Feature::MY_TRIPS)) {
  VisitEurope_Helper::throw404();
}

$user = VisitEurope_User::getCurrentMember();

if (!$user) {
  wp_redirect('/');
}

global $post;

$ut = new VisitEurope_UserTrip($post);

if (!$ut->isOwnedBy(VisitEurope_User::getCurrentUserID())) {
  VisitEurope_Helper::throw404();
}

$userTrip = $ut->presentPost();
$items = $ut->presentItems();
$itemsString = VisitEurope_Helper::arrayToObjectString($items);

// If this userTrip is a planned trip, redirect to that trip
if (VisitEurope_UserTrip::isPlanned($userTrip['id'])) {
  if ($trip = get_field('trip', $userTrip['id'])) {
    wp_redirect(get_the_permalink($trip->ID));
    exit;
  } else {
    VisitEurope_Helper::throw404();
  }
}

?>
<?php get_header(); ?>
	<my-trips-header-banner
    :copyright="'<?php echo $userTrip['copyright']; ?>'"
    :copyright-url="'<?php echo $userTrip['copyrighturl']; ?>'"
    :id="<?php echo intval($post->ID); ?>"
    :imageinitial="'<?php echo $userTrip['imageinitial']; ?>'"
    :imagefull="'<?php echo $userTrip['imagefull']; ?>'"
    :editable="<?php echo VisitEurope_UserTrip::isCustom($post->ID) ? 'true' : 'false'; ?>"
    :title="'<?php echo VisitEurope_Helper::formatStringForFrontend($post->post_title); ?>'">
  </my-trips-header-banner>
  <section class="container intro">
    <div class="entry-content">
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras lectus ex, porttitor quis aliquet quis, interdum quis orci. Nunc pharetra sapien mi, in accumsan dolor placerat a. Vestibulum tempor auctor risus ac tincidunt. Maecenas varius nisl eget orci tristique, in vehicula est mattis. Morbi sollicitudin dolor mauris, a mattis justo blandit vitae. Phasellus bibendum, magna maximus iaculis auctor, sem orci ultrices nisi, non volutpat mauris neque in elit.</p>
    </div>
  </section>
  <section class="container cta-map-usertrips">
    <a href="#" @click.prevent="launchInteractiveMap('userTrip', <?php echo $post->ID; ?>)" class="btn btn-primary btn-lg btn-interactive-map">
      <?php echo VisitEurope_Content::getLocale('interactiveMap.label'); ?>
    </a>
  </section>
  <my-trips-items
    :id="<?php echo $post->ID; ?>"
    :items='<?php echo $itemsString; ?>'>
  </my-trips-items>
<?php get_footer(); ?>
