<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for destination weather
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Visit_Europe
 */

global $post;

$veDestination = new VisitEurope_Destination($post);
$weather = $veDestination->presentPostWeather();
$weatherString = VisitEurope_Helper::arrayToObjectString($weather);

?>
<aside class="weather">
  <weather :item='<?php echo $weatherString; ?>'></weather>
</aside>
