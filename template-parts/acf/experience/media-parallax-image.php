<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF Experience content.media.parallax-image
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Visit_Europe
 */

$id = 'parallax-' . uniqid();

?>
<?php if (isset($item) && is_array($item) && isset($item['image']) && is_array($item['image'])) { ?>
  <section id="<?php echo $id; ?>" class="experience-parallax container">
    <div class="parallax-background"
      style="background-image: url(<?php echo $item['image']['sizes']['large']; ?>);"
      data-center="background-position: 50% 50%;"
      data-top="background-position: 50% 100%;"
      data-bottom="background-position: 50% 0%;"
      data-anchor-target="#<?php echo $id; ?>">
    </div>
  </section>
<?php } ?>
