<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF Experience content.media.crowdriff
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Visit_Europe
 */

$searchAlbums = $item['crowdriff_albums'];
$searchTerm = $item['crowdriff_search_term'];

echo VisitEurope_Helper::getTemplatePart('acf/flexible-content/media-crowdriff-gallery', [
  'searchAlbums' => $searchAlbums,
  'searchTerm' => $searchTerm,
]);
