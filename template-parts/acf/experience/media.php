<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF Experience content.media
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Visit_Europe
 */

if (isset($item) && is_array($item) && isset($item['type'])) {
  $type = str_replace(' ', '-', strtolower($item['type']));

  echo VisitEurope_Helper::getTemplatePart('acf/experience/media-' . $type, [
    'item' => $item
  ]);
}
