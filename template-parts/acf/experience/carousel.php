<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF Experience content.carousel
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Visit_Europe
 */

global $post;

$items = [];

if (isset($item['slides']) && is_array($item['slides']) && !empty($item['slides'])) {
  $items = VisitEurope_ACF_FC::presentExperienceCarousel($item['slides']);
}

$itemsString = VisitEurope_Helper::arrayToObjectString($items);
$showThumbnails = false;

if ($post->post_type == VisitEurope_CPT_Experience::POST_TYPE
  && VisitEurope_Experience::getTypeForPostID($post->ID) == VisitEurope_Experience::TYPE_GALLERY) {
  $showThumbnails = true;
}


?>
<section class="container carousel">
  <carousel-large
    :id="'carousel-large-<?php echo uniqid(); ?>'"
    :items='<?php echo $itemsString; ?>'
    :show-thumbnails="<?php echo $showThumbnails ? 'true' : 'false'; ?>">
  </carousel-large>
</section>
