<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF Experience content.content
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Visit_Europe
 */
?>
<?php if (isset($item) && is_array($item) && isset($item['body'])) { ?>
<section class="experience-content container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
      <?php echo apply_filters('the_content', $item['body']); ?>
    </div>
  </div>
</section>
<?php } ?>
