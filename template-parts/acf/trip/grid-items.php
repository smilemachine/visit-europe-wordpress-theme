<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for Trip grid items
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Visit_Europe
 */

global $post;

?>
<?php if (isset($items) && is_array($items) && !empty($items)) { ?>
  <section class="container standard-grid section-inverse experiences">
    <div class="inner container">
      <?php echo do_shortcode('[ve_section_header title="' . VisitEurope_Content::getLocale('trip.onTheWay') . '"]'); ?>
      <section class="standard-grid-size-full">
        <div class="grid-container">
          <section class="standard">
            <div class="standard-container">
              <?php
                foreach ($items as $item) {
                  if ($item['type'] == 'Experience') {
                    $experience = $item['experience'];
                    $internalItem = [
                      'title' => $experience->post_title,
                      'body' => get_field('blurb', $experience->ID),
                      'imageinitial' => VisitEurope_Helper::getPostThumbnailUrl($experience->ID, 'initial'),
                      'imagefull' => VisitEurope_Helper::getPostThumbnailUrl($experience->ID, 'thumbnail'),
                      'url' => get_permalink($experience->ID),
                      'type' => VisitEurope_Experience::getTypeForPostID($experience->ID),
                    ];

                    echo do_shortcode('[ve_preview ' . VisitEurope_ACF_FC::getParamsFromItem($internalItem) . ']');
                  } else {
                    $externalItem = [
                      'title' => $item['title'],
                      'body' => $item['body'],
                      'imageinitial' => $item['image']['sizes']['initial'],
                      'imagefull' => $item['image']['sizes']['thumbnail'],
                      'url' => $item['url'],
                      'type' => 'External',
                    ];

                    echo do_shortcode('[ve_preview ' . VisitEurope_ACF_FC::getParamsFromItem($externalItem) . ']');
                  }
                }
              ?>
            </div>
          </section>
        </div>
      </section>
    </div>
  </section>
<?php } ?>
