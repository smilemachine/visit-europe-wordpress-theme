<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF Trip content.about
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Visit_Europe
 */

$items = get_field('about');

?>
<section class="about trip-about">
  <header>
    <h4>
      <?php echo VisitEurope_Content::getLocale('flexibleContent.about'); ?>
    </h4>
  </header>
  <?php if (is_array($items) && !empty($items)) { ?>
    <ul class="items">
      <?php foreach ($items as $item) { ?>
        <?php
          $url = $item['url'];

          if (strpos($url, 'http') === false) {
            $url = 'http://' . $url;
          }
        ?>
        <li>
          <a href="<?php echo $url; ?>" target="_blank"
            title="<?php echo $item['title']; ?>">
            <?php echo $item['title']; ?>
          </a>
        </li>
      <?php } ?>
    </ul>
  <?php } ?>
  <?php echo VisitEurope_Helper::getTemplatePart('share'); ?>
</section>
