<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: cta.essential-information-grid
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Visit_Europe
 */

$limit = intval(get_sub_field('limit'));
$items = VisitEurope_Settings::getEssentialInformation($limit);
$viewAllUrl = VisitEurope_Content::getUrl('pages.essentialInformation');

?>
<section class="container cta-type-essential-information-grid">
  <?php echo do_shortcode('[ve_section_header title="' . _x('Essential information about Europe', 'CTA Essential Info title', VisitEurope_Theme::KEY) . '" url="' . $viewAllUrl . '"]'); ?>
  <div class="row">
    <?php foreach ($items as $item) { ?>
      <div class="col-xs-12 col-sm-6 col-md-3">
        <?php
          echo do_shortcode('[ve_cta_imagelink title="' . $item['title'] . '" imageinitial="' . $item['imageInitial'] . '" imagefull="' . $item['imageFull'] . '" url="' . $item['url'] . '"]');
        ?>
      </div>
    <?php } ?>
  </div>
</section>
