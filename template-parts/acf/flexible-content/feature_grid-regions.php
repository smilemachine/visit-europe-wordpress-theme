<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: feature_grid.regions
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Visit_Europe
 */

if (get_page_template_slug() != 'template-regions.php') {
  VisitEurope_Content::clearUniqueCache(VisitEurope_Content::CACHE_DESTINATION);
}

VisitEurope_Content::addUniqueCache(VisitEurope_Content::CACHE_DESTINATION);

global $post;

$isInverse = intval(get_sub_field('inverse'));
$headerTitle = trim(get_sub_field('title'));
$headerUrl = null;
$htmlClasses = ['container', 'feature-grid', 'regions'];
$htmlClasses[] = ($isInverse ? 'section-inverse' : 'section-default');
$type = VisitEurope_Destination::TYPE_REGION;

$featureGrid = new VisitEurope_ACF_FC_FeatureGrid($post);
$featureGrid->setTypeFromRaw($type);

if (get_sub_field('show_view_all', $post->ID) === true) {
  $headerUrl = $featureGrid->getViewAllUrl();
}

?>
<section class="<?php echo implode(' ', $htmlClasses); ?>">
  <?php
    if (!empty($headerTitle)) {
      echo do_shortcode('[ve_section_header title="' . $headerTitle . '" url="' . $headerUrl . '"]');
    }
  ?>
  <div class="row">
    <?php for ($i=1; $i<=2; $i++) { ?>
      <?php
        $featureGrid = new VisitEurope_ACF_FC_FeatureGrid($post);
        $featureGrid->setTypeFromRaw($type);
        $featureGrid->setHalfWidth();

        if (isset($orderBy) && !empty($orderBy)) {
          $featureGrid->setOrderBy('title');
        }

        if (!isset($orientation)) {
          $orientation = VisitEurope_ACF_FC_FeatureGrid::ORIENTATION_LEFT;
        }

        $featureGrid->setOrientation($orientation);

        $featured = $featureGrid->getFeaturedItems();
        $standard = $featureGrid->getStandardItems();
      ?>
      <?php if (!empty($featured) || !empty($standard)) { ?>
        <div class="col-md-6">
          <?php
            echo VisitEurope_Helper::getTemplatePart('acf/flexible-content/feature_grid-halfwidth', [
              'type' => $type,
              'featured' => $featured,
              'standard' => $standard,
              'orientation' => $orientation
            ]);
          ?>
        </div>
      <?php } ?>
    <?php } ?>
  </div>
</section>
