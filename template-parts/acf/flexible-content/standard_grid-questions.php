<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: standard_grid.articles
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Visit_Europe
 */

$headerTitle = trim(get_sub_field('title'));
$headerUrl = '#';
$limit = intval(get_sub_field('limit'));
$questions = VisitEurope_Question::getWhereType(get_the_ID());

if ($limit < 4) {
  $limit = 4;
}

?>
<?php if (is_array($questions) && !empty($questions)) { ?>
  <section class="container standard-grid questions section-inverse">
    <nav v-if="elements.destinationQuestions.visible" class="destination-questions-nav">
      <a href="#" @click.prevent="closeDestinationQuestions">
        <span class="">&lt;</span>
        <?php echo VisitEurope_Content::getLocale('destination.questions.back'); ?>
      </a>
    </nav>
    <?php if (!empty($headerTitle)) { ?>
      <div v-if="!elements.destinationQuestions.visible">
        <?php
          echo do_shortcode('[ve_section_header title="' . $headerTitle . '" url="' . $headerUrl . '" event="launchDestinationQuestions"]');
        ?>
      </div>
      <div v-if="elements.destinationQuestions.visible">
        <?php
          echo do_shortcode('[ve_section_header title="' . $headerTitle . '"]');
        ?>
      </div>
    <?php } ?>
    <div class="standard-grid-size-full standard-grid-type-question">
      <section class="standard">
        <div class="standard-container">
          <?php foreach ($questions as $key => $question) { ?>
            <?php
              $imageInitialUrl = VisitEurope_Helper::getPostThumbnailUrl($question->ID, 'initial');
              $imageFullUrl = VisitEurope_Helper::getPostThumbnailUrl($question->ID, 'thumbnail');
              $dynamicQuestion = '';

              if ($key + 1 > $limit) {
                $dynamicQuestion = ':class="{ hide: !elements.destinationQuestions.visible }"';
              }
            ?>
            <article class="question-preview" <?php echo $dynamicQuestion; ?>>
              <section class="content">
                <section is="blurry-background-image"
                  :initial="'<?php echo $imageInitialUrl; ?>'"
                  :full="'<?php echo $imageFullUrl; ?>'">
                </section>
                <header>
                  <h4>"<?php echo esc_html($question->post_title); ?>"</h4>
                </header>
                <p><?php echo apply_filters('the_content', $question->post_content); ?></p>
              </section>
            </article>
          <?php } ?>
        </div>
      </section>
    </div>
    <nav v-if="elements.destinationQuestions.visible" class="destination-questions-nav">
      <a href="#" @click.prevent="closeDestinationQuestions">
        <span class="">&lt;</span>
        <?php echo VisitEurope_Content::getLocale('destination.questions.back'); ?>
      </a>
    </nav>
  </section>
<?php } ?>
