<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: standard_grid
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Visit_Europe
 */

$filename = 'standard_grid-' . strtolower(get_sub_field('type'));
echo VisitEurope_Helper::getTemplatePart('acf/flexible-content/' . $filename);
