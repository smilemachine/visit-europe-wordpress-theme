<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: interests_grid
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Visit_Europe
 */

VisitEurope_Content::clearUniqueCache(VisitEurope_Content::CACHE_EXPERIENCE);

global $post;

$title = VisitEurope_Content::getLocale('flexibleContent.interestsGrid.title');
$limit = intval(get_sub_field('limit'));
$type = VisitEurope_ACF_FC_InterestsGrid::getTypeFromRaw(get_sub_field('type'));
$interestsGrid = new VisitEurope_ACF_FC_InterestsGrid($post, $type, $limit);
$items = $interestsGrid->getStandardItems();
$filters = $interestsGrid->getFilters();

$itemsString = VisitEurope_Helper::arrayToObjectString($items);
$filtersString = VisitEurope_Helper::arrayToObjectString($filters);

?>
<?php if (!empty($filters)) { ?>
  <section class="container interests-grid section-default">
    <?php echo do_shortcode('[ve_section_header title="' . $title . '"]'); ?>
    <filterable-grid
      :filters='<?php echo $filtersString; ?>'
      :items='<?php echo $itemsString; ?>'
      @itemwasclicked="navigateToItem">
    </filterable-grid>
  </section>
<?php } ?>
