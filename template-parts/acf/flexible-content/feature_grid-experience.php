<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: feature_grid.recipes
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Visit_Europe
 */

global $post;

$featureGrid = new VisitEurope_ACF_FC_FeatureGrid($post, $type);
$featured = $featureGrid->getFeaturedItems();
$standard = $featureGrid->getStandardItems();

$headerTitle = trim(get_sub_field('title', $post->ID));
$headerUrl = null;
$htmlClasses = ['container', 'feature-grid', $typeSlug];

if (get_sub_field('show_view_all', $post->ID) === true) {
  $headerUrl = $featureGrid->getViewAllUrl();
}

$isInverse = intval(get_sub_field('inverse', $post->ID));
$htmlClasses[] = ($isInverse ? 'section-inverse' : 'section-default');

?>
<?php if (is_array($featured) && !empty($featured) && is_array($standard) && !empty($standard)) { ?>
  <section class="<?php echo implode(' ', $htmlClasses); ?>">
    <div class="inner container">
      <?php
        if (!empty($headerTitle)) {
          echo do_shortcode('[ve_section_header title="' . $headerTitle . '" url="' . $headerUrl . '"]');
        }

        echo VisitEurope_Helper::getTemplatePart('acf/flexible-content/feature_grid-fullwidth', [
          'type' => $type,
          'featured' => $featured,
          'standard' => $standard,
        ]);
      ?>
    </div>
  </section>
<?php } ?>
