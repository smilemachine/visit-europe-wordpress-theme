<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: cta.interests-grid
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Visit_Europe
 */

$limit = intval(get_sub_field('limit'));
$items = VisitEurope_Settings::getInterestGroups($limit);

?>
<?php if (is_array($items) && !empty($items)) { ?>
  <section class="container cta-type-interests-grid">
    <?php echo do_shortcode('[ve_section_header title="' . _x('Discover Europe by your interests', 'CTA Interests title', VisitEurope_Theme::KEY) . '" url="' . VisitEurope_Content::getUrl('pages.interests') . '"]'); ?>
    <div class="row">
      <?php foreach ($items as $item) { ?>
        <div class="col-xs-12 col-sm-6 col-md-3">
          <?php
            $imageInitial = $item['image']['sizes']['initial'];
            $imageFull = $item['image']['sizes']['thumbnail'];
            $url = VisitEurope_Content::getUrl('pages.experiencesSearch') . '?interest=' . strtolower($item['title']);

            echo do_shortcode('[ve_cta_imagelink title="' . $item['title'] . '" imageinitial="' . $imageInitial . '" imagefull="' . $imageFull . '" url="' . $url . '"]');
          ?>
        </div>
      <?php } ?>
    </div>
  </section>
<?php } ?>
