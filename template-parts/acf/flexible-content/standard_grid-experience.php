<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: standard_grid.experience
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Visit_Europe
 */

VisitEurope_Content::clearUniqueCache(VisitEurope_Content::CACHE_EXPERIENCE);

global $post;

if (!isset($limit) || $limit == 0) {
  $limit = intval(get_sub_field('limit'));
}

if (!isset($interestGroupName)) {
  $interestGroupName = get_sub_field('interest_group');
}

if (!isset($itemIDs) || !is_array($itemIDs)) {
  $itemIDs = null;
}

$headerUrl = null;

if (!isset($headerTitle)) {
  $headerTitle = trim(get_sub_field('title'));
}

$standardGrid = new VisitEurope_ACF_FC_StandardGrid($post);
$standardGrid->setTypeFromRaw($type);
$standardGrid->setLimit($limit);
$standardGrid->setInterestsByGroupName($interestGroupName);
$standardGrid->restrictToItemIDs($itemIDs);

$standard = $standardGrid->getStandardItems();

if (get_sub_field('show_view_all', $post->ID) === true) {
  $headerUrl = $standardGrid->getViewAllUrl($interestGroupName);
}

?>
<?php if (is_array($standard) && !empty($standard)) { ?>
  <section class="container standard-grid standard-<?php echo strtolower($type); ?> section-default experiences">
    <?php
      if (!empty($headerTitle)) {
        echo do_shortcode('[ve_section_header title="' . $headerTitle . '" url="' . $headerUrl . '"]');
      }

      echo VisitEurope_Helper::getTemplatePart('acf/flexible-content/standard_grid-fullwidth', [
        'type' => VisitEurope_Experience::TYPE_EVENT,
        'typeSlug' => 'standard-events',
        'standard' => $standard,
      ]);
    ?>
  </section>
<?php } ?>
