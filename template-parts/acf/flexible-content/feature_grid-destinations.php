<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: feature_grid.destinations
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Visit_Europe
 */

global $post;

VisitEurope_Content::addUniqueCache(VisitEurope_Content::CACHE_DESTINATION);

$isInverse = intval(get_sub_field('inverse'));
$htmlClasses = ['container', 'feature-grid', 'destinations'];
$htmlClasses[] = ($isInverse ? 'section-inverse' : 'section-default');

?>
<section class="<?php echo implode(' ', $htmlClasses); ?>">
  <div class="row">
    <div class="col-md-6">
      <?php
        $orientation = VisitEurope_ACF_FC_FeatureGrid::ORIENTATION_LEFT;
        $type = VisitEurope_Destination::TYPE_REGION;

        $featureGrid = new VisitEurope_ACF_FC_FeatureGrid($post);
        $featureGrid->setTypeFromRaw($type);
        $featureGrid->setHalfWidth();
        $featureGrid->setOrientation($orientation);

        $featured = $featureGrid->getFeaturedItems();
        $standard = $featureGrid->getStandardItems();

        echo do_shortcode('[ve_section_header title="' . _x('By region', 'Destinations feature grid region title', VisitEurope_Theme::KEY) . '" url="' . VisitEurope_Content::getUrl('pages.destinationsRegions') . '"]');

        echo VisitEurope_Helper::getTemplatePart('acf/flexible-content/feature_grid-halfwidth', [
          'type' => $type,
          'featured' => $featured,
          'standard' => $standard,
          'orientation' => $orientation,
        ]);
      ?>
    </div>
    <div class="col-md-6">
      <?php
        $orientation = VisitEurope_ACF_FC_FeatureGrid::ORIENTATION_RIGHT;
        $type = VisitEurope_Destination::TYPE_COUNTRY;

        $featureGrid = new VisitEurope_ACF_FC_FeatureGrid($post);
        $featureGrid->setTypeFromRaw($type);
        $featureGrid->setHalfWidth();
        $featureGrid->setOrientation($orientation);

        $featured = $featureGrid->getFeaturedItems();
        $standard = $featureGrid->getStandardItems();

        echo do_shortcode('[ve_section_header title="' . _x('By country', 'Destinations feature grid country title', VisitEurope_Theme::KEY) . '" url="' . VisitEurope_Content::getUrl('pages.destinationsCountries') . '"]');

        echo VisitEurope_Helper::getTemplatePart('acf/flexible-content/feature_grid-halfwidth', [
          'type' => $type,
          'featured' => $featured,
          'standard' => $standard,
          'orientation' => $orientation,
        ]);
      ?>
    </div>
  </div>
</section>
