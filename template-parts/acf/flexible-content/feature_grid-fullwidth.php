<?php
if (!defined('ABSPATH')) exit;

/**
 * Feature grid: full width
 *
 * @package Visit_Europe
 */

if (!isset($orientation)) {
  $orientation = VisitEurope_ACF_FC_FeatureGrid::ORIENTATION_LEFT;
}

if (!isset($type) || !isset($featured) || !isset($standard) || !is_array($featured) || empty($featured) || !is_array($standard) || empty($standard)) {
  // Failed validation
} else {

$containerClasses = [
  'feature-grid',
  'feature-grid-size-full',
  'feature-grid-orientation-' . strtolower($orientation),
  'feature-grid-type-' . strtolower($type),
];

?>
<section class="<?php echo implode(' ', $containerClasses); ?>">
  <div class="grid-container">
    <section class="featured">
      <div class="featured-container">
        <?php
          foreach ($featured as $item) {
            echo do_shortcode('[ve_preview ' . VisitEurope_ACF_FC::getParamsFromItem($item) . ']');
          }
        ?>
      </div>
    </section>
    <section class="standard">
      <div class="standard-container">
        <?php
          foreach ($standard as $item) {
            echo do_shortcode('[ve_preview ' . VisitEurope_ACF_FC::getParamsFromItem($item) . ']');
          }
        ?>
      </div>
    </section>
  </div>
</section>

<?php } ?>
