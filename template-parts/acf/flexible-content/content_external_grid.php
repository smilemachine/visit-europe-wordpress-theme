<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: content_external_grid
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Visit_Europe
 */

$title = get_sub_field('title');
$content = get_sub_field('content');
$items = get_sub_field('items');

?>
<section class="container content-external-grid section-default">
  <?php if (!empty($title)) { ?>
    <?php echo do_shortcode('[ve_section_header title="' . $title . '"]'); ?>
  <?php } ?>
  <div class="row">
    <div class="col-sm-6 content">
      <?php echo apply_filters('the_content', $content); ?>
    </div>
    <?php if (is_array($items) && !empty($items)) { ?>
      <div class="col-sm-6 visual">
        <div class="row">
          <?php foreach ($items as $item) { ?>
            <?php
              $itemBackgroundImageUrl = '';
              $itemImageUrl = $item['image']['sizes']['thumbnail'];
              $itemTitle = $item['title'];
              $itemUrl = $item['url'];

              if ($item['background_image']) {
                $itemBackgroundImageUrl = $item['background_image']['sizes']['thumbnail'];
              }
            ?>
            <div class="col-sm-6">
              <div class="external-item"
                style="background-image: url(<?php echo $itemBackgroundImageUrl; ?>);">
                <a href="<?php echo $itemUrl; ?>" target="_blank" class="item">
                  <span class="title">
                    <?php echo $itemTitle; ?>
                  </span>
                  <div class="image"
                    style="background-image: url(<?php echo $itemImageUrl; ?>);">
                  </div>
                </a>
              </div>
            </div>
          <?php } ?>
        </div>
      </div>
    <?php } ?>
  </div>
</section>
