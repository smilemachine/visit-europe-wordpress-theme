<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: standard_grid.trips
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Visit_Europe
 */

global $post;

$type = VisitEurope_Trip::TYPE_ALL;

if (!isset($headerTitle) || !$headerTitle) {
  $headerTitle = trim(get_sub_field('title'));
}

if (!isset($headerUrl) || !$headerUrl) {
  $headerUrl = trim(get_sub_field('url'));
}

if (!isset($limit) || !$limit) {
  $limit = get_sub_field('limit');
}

if (!$limit) {
  $limit = 4;
}

if (!isset($itemIDs)) {
  $itemIDs = null;
}

$standardGrid = new VisitEurope_ACF_FC_StandardGrid(null, $type);
$standardGrid->setLimit(intval($limit));
$standardGrid->restrictToItemIDs($itemIDs);
$standard = $standardGrid->getStandardItems();

?>
<section class="container standard-grid standard-trips section-default">
  <?php
    if (!empty($headerTitle)) {
      echo do_shortcode('[ve_section_header title="' . $headerTitle . '" url="' . $headerUrl . '"]');
    }

    echo VisitEurope_Helper::getTemplatePart('acf/flexible-content/standard_grid-fullwidth', [
      'type' => $type,
      'standard' => $standard,
    ]);
  ?>
</section>
