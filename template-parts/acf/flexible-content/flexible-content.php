<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Visit_Europe
 */

$flexibleContentKey = 'content_tab';

if (isset($flexibleContentTab) && intval($flexibleContentTab) > 0) {
  $flexibleContentKey .= intval($flexibleContentTab);
} else {
  $flexibleContentKey .= 1;
}

$i = 0;
$totalItems = 0;
$flexibleContentObject = get_field_object($flexibleContentKey);

if (is_array($flexibleContentObject) && isset($flexibleContentObject['value'])) {
  $totalItems = count($flexibleContentObject['value']);
}

if (have_rows($flexibleContentKey)) {
  while (have_rows($flexibleContentKey)) {
    $i++;

    if ($i == $totalItems) {
      echo '<div class="last-section">';
    }

    the_row();
    echo VisitEurope_Helper::getTemplatePart('acf/flexible-content/' . get_row_layout());

    if ($i == $totalItems) {
      echo '</div>';
    }
  }
}
