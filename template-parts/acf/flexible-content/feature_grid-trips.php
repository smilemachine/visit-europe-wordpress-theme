<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: feature_grid.experiences
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Visit_Europe
 */

global $post;

$type = VisitEurope_Trip::TYPE_ALL;
$featureGrid = new VisitEurope_ACF_FC_FeatureGrid($post, $type);
$featured = $featureGrid->getFeaturedItems();
$standard = $featureGrid->getStandardItems();

$headerTitle = trim(get_sub_field('title'));
$headerUrl = trim(get_sub_field('url'));
$htmlClasses = ['container', 'feature-grid', 'featured-trips'];

$isInverse = intval(get_sub_field('inverse'));
$htmlClasses[] = ($isInverse ? 'section-inverse' : 'section-default');

?>
<?php if (!empty($featured) || !empty($standard)) { ?>
  <section class="<?php echo implode(' ', $htmlClasses); ?>">
    <div class="inner container">
      <?php
        if (!empty($headerTitle)) {
          echo do_shortcode('[ve_section_header title="' . $headerTitle . '" url="' . $headerUrl . '"]');
        }

        echo VisitEurope_Helper::getTemplatePart('acf/flexible-content/feature_grid-fullwidth', [
          'type' => $type,
          'featured' => $featured,
          'standard' => $standard,
        ]);
      ?>
    </div>
  </section>
<?php } ?>
