<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: cta.event-search-banner
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Visit_Europe
 */

 $countries = [];
 $countryPosts = VisitEurope_Destination::getCountries([
   'orderby'=>'post_title',
   'posts_per_page' => -1
 ]);

 if (is_array($countryPosts) && !empty($countryPosts)) {
   foreach ($countryPosts as $countryPost) {
     $countries[] = [
       'key' => $countryPost->ID,
       'title' => $countryPost->post_title,
     ];
   }
 }

if ($image = get_sub_field('image')) {
  $imageInitial = $image['sizes']['initial'];
  $imageFull = $image['sizes']['large'];
} else {
  $imageInitial = get_template_directory_uri() . '/assets/img/cta-event-search-banner.jpg';
  $imageFull = $imageInitial;
}

?>
<section class="container cta-banner cta-type-event-search-banner cta-form">
  <div class="inner container">
    <cta-event-search-banner
      :destinations='<?php echo VisitEurope_Helper::arrayToObjectString($countries); ?>'
      :image-initial="'<?php echo $imageInitial; ?>'"
      :image-full="'<?php echo $imageFull; ?>'">
    </cta-event-search-banner>
  </div>
</section>
