<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF Experience content.media.crowdriff
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Visit_Europe
 */

global $post;

if (!isset($searchAlbums) || !is_array($searchAlbums)) {
  $searchAlbums = get_sub_field('crowdriff_albums', $post->ID);
}

if (!isset($searchTerm) || !is_string($searchTerm)) {
  $searchTerm = get_sub_field('crowdriff_search_term', $post->ID);
}

$crowdriff = new VisitEurope_Service_CrowdRiff();
$results = $crowdriff->search($searchTerm, $searchAlbums, 5);
$title = _x('Gallery', 'Crowdriff gallery: Title', VisitEurope_Theme::KEY);

?>
<?php if (is_array($results) && !empty($results) && count($results == 5)) { ?>
  <?php
    $featured = array_pop($results);
    $standard = $results;
  ?>
  <section class="container media-crowdriff-gallery section-default">
    <?php echo do_shortcode('[ve_section_header title="' . $title . '"]'); ?>
    <div class="inner">
      <div class="standard">
        <?php foreach ($standard as $item) { ?>
          <a href="<?php echo $item['url']; ?>" target="_blank" title="<?php echo $item['title']; ?>"
            class="crowdriff-item" @click.prevent="doNothing">
            <div is="blurry-background-image"
              :initial="'<?php echo $item['imageinitial']; ?>'"
              :full="'<?php echo $item['imagefull']; ?>'">
              <div class="source source-<?php echo $item['source']; ?>"></div>
            </div>
          </a>
        <?php } ?>
      </div>
      <div class="featured">
        <a href="<?php echo $featured['url']; ?>" target="_blank" title="<?php echo $featured['title']; ?>"
          class="crowdriff-item" @click.prevent="doNothing">
          <div is="blurry-background-image"
            :initial="'<?php echo $featured['imageinitial']; ?>'"
            :full="'<?php echo $featured['imagefull']; ?>'">
            <div class="source source-<?php echo $featured['source']; ?>"></div>
          </div>
        </a>
      </div>
    </div>
  </section>
<?php } ?>
