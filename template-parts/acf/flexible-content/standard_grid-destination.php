<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: standard_grid.destination
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Visit_Europe
 */

VisitEurope_Content::clearUniqueCache(VisitEurope_Content::CACHE_DESTINATION);

global $post;

if (!isset($headerTitle) || !$headerTitle) {
  $headerTitle = trim(get_sub_field('title'));
}

if (!isset($headerUrl) || !$headerUrl) {
  $headerUrl = trim(get_sub_field('url'));
}

if (!isset($limit) || !$limit) {
  $limit = get_sub_field('limit');
}

if (!isset($type) || !$type) {
  $type = get_sub_field('type');
}

if (!$limit) {
  $limit = 4;
}

if (!isset($itemIDs) || !is_array($itemIDs) || empty($itemIDs)) {
  $itemIDs = [];
}

$standardGrid = new VisitEurope_ACF_FC_StandardGrid($post);
$standardGrid->setTypeFromRaw($type);
$standardGrid->setLimit($limit);
$standardGrid->restrictToItemIDs($itemIDs);

$standard = $standardGrid->getStandardItems();

?>
<section class="container standard-grid standard-<?php strtolower($type); ?> section-default destinations">
  <?php
    if (!empty($headerTitle)) {
      echo do_shortcode('[ve_section_header title="' . $headerTitle . '" url="' . $headerUrl . '"]');
    }

    echo VisitEurope_Helper::getTemplatePart('acf/flexible-content/standard_grid-fullwidth', [
      'type' => $type,
      'standard' => $standard,
    ]);
  ?>
</section>
