<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: accordion
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Visit_Europe
 */

$title = get_sub_field('title');
$items = get_sub_field('items');

?>
<section class="container accordion section-default">
  <?php if (!empty($title)) { ?>
    <?php echo do_shortcode('[ve_section_header title="' . $title . '"]'); ?>
  <?php } ?>
  <accordion
    :columns="2"
    :items='<?php echo VisitEurope_Helper::arrayToObjectString($items); ?>'>
  </accordion>
</section>
