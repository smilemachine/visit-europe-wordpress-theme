<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: cta
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Visit_Europe
 */

$type = str_replace(' ', '-', strtolower(get_sub_field('type')));
$filename = 'cta-' . $type;
echo VisitEurope_Helper::getTemplatePart('acf/flexible-content/' . $filename);
