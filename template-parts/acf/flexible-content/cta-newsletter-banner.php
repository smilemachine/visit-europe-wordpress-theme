<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: cta.newsletter-banner
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Visit_Europe
 */

if ($image = get_sub_field('image')) {
  $imageInitial = $image['sizes']['initial'];
  $imageFull = $image['sizes']['large'];
} else {
  $imageInitial = get_template_directory_uri() . '/assets/img/cta-newsletter-banner.jpg';
  $imageFull = $imageInitial;
}

?>
<section class="container cta-banner cta-type-newsletter-banner cta-form">
  <div class="inner container">
    <cta-newsletter-banner
      :image-initial="'<?php echo $imageInitial; ?>'"
      :image-full="'<?php echo $imageFull; ?>'">
    </cta-newsletter-banner>
  </div>
</section>
