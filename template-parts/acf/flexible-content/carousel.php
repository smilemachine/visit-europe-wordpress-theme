<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: Carousel
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Visit_Europe
 */

global $post;

$limit = get_sub_field('limit', $post->ID);
$rawTitle = get_sub_field('title', $post->ID);
$rawType = get_sub_field('type', $post->ID);

$fc = new VisitEurope_ACF_FC_Carousel($post);
$fc->setTypeFromRaw($rawType);
$fc->setTitleFromRaw($rawTitle);
$title = $fc->getTitle();
$items = $fc->presentLayout();
$itemsString = "[]";

if (isset($items['content'])) {
  $itemsString = VisitEurope_Helper::arrayToObjectString($items['content']);
}

?>
<section class="container carousel">
  <carousel-large
    :id="'carousel-large-<?php echo uniqid(); ?>'"
    :items='<?php echo $itemsString; ?>'
    :title="'<?php echo $title; ?>'">
  </carousel-large>
</section>
