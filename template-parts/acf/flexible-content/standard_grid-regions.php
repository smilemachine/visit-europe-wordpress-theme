<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: standard_grid.regions
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Visit_Europe
 */

if (!isset($headerTitle) || empty($headerTitle)) {
  $headerTitle = '';
}

if (!isset($headerUrl) || empty($headerUrl)) {
  $headerUrl = '';
}

echo VisitEurope_Helper::getTemplatePart('acf/flexible-content/standard_grid-destination', [
  'type' => VisitEurope_Destination::TYPE_REGION,
  'headerTitle' => $headerTitle,
  'headerUrl' => $headerUrl,
]);
