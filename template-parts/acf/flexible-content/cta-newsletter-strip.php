<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: cta.newsletter-strip
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Visit_Europe
 */

?>
<section class="container cta-strip cta-type-newsletter-strip cta-form">
  <div class="inner container">
    <cta-newsletter-strip></cta-newsletter-strip>
  </div>
</section>
