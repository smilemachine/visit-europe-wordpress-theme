<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: cta.advertisement-banner
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Visit_Europe
 */

$title = get_sub_field('title');
$subtitle = get_sub_field('subtitle');
$button = get_sub_field('button');
$url = get_sub_field('url');

$image = get_sub_field('image');
$imageInitial = '';
$imageFull = '';

if ($image) {
  $imageInitial = $image['sizes']['initial'];
  $imageFull = $image['sizes']['large'];
}

?>
<?php if (!empty($imageInitial) && !empty($imageFull) && !empty($title) && !empty($subtitle) && !empty($button) && !empty($url)) { ?>
<section class="container cta-type-advertisement-banner">
  <div class="inner container">
    <a href="<?php echo $url; ?>" target="_blank">
      <blurry-background-image
        :initial="'<?php echo $imageInitial; ?>'"
        :full="'<?php echo $imageFull; ?>'">
        <div class="content">
          <div class="col-sm-8">
            <h3><?php echo $title; ?></h3>
            <p><?php echo $subtitle; ?></p>
          </div>
          <div class="col-sm-4">
            <div class="btn btn-primary btn-block">
              <?php echo $button; ?>
            </div>
          </div>
        </div>
      </blurry-background-image>
    </a>
  </div>
</section>
<?php } ?>
