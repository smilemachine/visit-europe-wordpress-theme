<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: cta.interactive-map-banner
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Visit_Europe
 */

if ($image = get_sub_field('image')) {
  $imageUrl = $image['sizes']['large'];
} else {
  $imageUrl = get_template_directory_uri() . '/assets/img/cta-interactive-map-banner.jpg';
}

?>
<section class="container map cta-type-interactive-map-banner">
  <div class="map-container" @click="launchInteractiveMap('destinations')"
    style="background-image: url(<?php echo $imageUrl; ?>);">
    <span class="btn btn-large btn-primary">
      <?php echo VisitEurope_Content::getLocale('interactiveMap.label'); ?>
    </span>
  </div>
</section>
