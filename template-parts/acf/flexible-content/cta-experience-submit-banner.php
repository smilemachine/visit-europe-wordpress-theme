<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: cta.experience-submit-banner
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Visit_Europe
 */

$title = VisitEurope_Content::getLocale('cta.experienceSubmitBanner.title');
$button = VisitEurope_Content::getLocale('cta.experienceSubmitBanner.button');
$url = VisitEurope_Content::getUrl('pages.experiencesEventCreate');

if ($image = get_sub_field('image')) {
  $imageInitialUrl = $image['sizes']['initial'];
  $imageFullUrl = $image['sizes']['large'];
} else {
  $imageInitialUrl = get_template_directory_uri() . '/assets/img/cta-experience-submit-banner.jpg';
  $imageFullUrl = $imageInitial;
}

?>
<?php if (VisitEurope_Feature::isActive(VisitEurope_Feature::MY_TRIPS)) { ?>
  <section class="container cta-banner cta-type-experience-submit-banner cta-form">
    <?php if (VisitEurope_User::getCurrentMember()) { ?>
      <a href="<?php echo $url; ?>" class="inner">
    <?php } else { ?>
      <a href="#" @click.prevent="launchUserVerification(null, '<?php echo $url; ?>')">
    <?php } ?>
      <div is="blurry-background-image"
        :initial="'<?php echo $imageInitialUrl; ?>'"
        :full="'<?php echo $imageFullUrl; ?>'">
        <header>
          <h1><?php echo $title; ?></h1>
        </header>
        <div class="btn-container">
          <span class="btn btn-primary btn-lg">
            <?php echo $button; ?>
          </span>
        </div>
      </div>
    </a>
  </section>
<?php } ?>
