<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: feature_grid.experiences
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Visit_Europe
 */

VisitEurope_Content::addUniqueCache(VisitEurope_Content::CACHE_EXPERIENCE);

echo VisitEurope_Helper::getTemplatePart('acf/flexible-content/feature_grid-experience', [
  'type' => VisitEurope_Experience::TYPE_ALL,
  'typeSlug' => 'featured-experiences',
]);
