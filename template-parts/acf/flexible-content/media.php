<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: media
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Visit_Europe
 */

$type = str_replace(' ', '-', strtolower(get_sub_field('type')));
$filename = 'media-' . $type;
echo VisitEurope_Helper::getTemplatePart('acf/flexible-content/' . $filename);
