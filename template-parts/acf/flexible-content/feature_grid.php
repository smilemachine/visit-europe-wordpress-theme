<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: feature_grid
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Visit_Europe
 */

$filename = 'feature_grid-' . strtolower(get_sub_field('type'));
echo VisitEurope_Helper::getTemplatePart('acf/flexible-content/' . $filename);
