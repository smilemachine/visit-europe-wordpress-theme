<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: cta.experience-search-banner
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Visit_Europe
 */

$types = [
  [
    'key' => VisitEurope_Experience::TYPE_ARTICLE,
    'title' => VisitEurope_Content::getLocale('experiences.types.article'),
  ],
  [
    'key' => VisitEurope_Experience::TYPE_GALLERY,
    'title' => VisitEurope_Content::getLocale('experiences.types.gallery')
  ],
  [
    'key' => VisitEurope_Experience::TYPE_PRODUCT,
    'title' => VisitEurope_Content::getLocale('experiences.types.product')
  ],
  [
    'key' => VisitEurope_Experience::TYPE_RECIPE,
    'title' => VisitEurope_Content::getLocale('experiences.types.recipe')
  ],
  [
    'key' => VisitEurope_Experience::TYPE_ROUTE,
    'title' => VisitEurope_Content::getLocale('experiences.types.route')
  ],
];
$interests = VisitEurope_Settings::getInterestGroupsForDropdown();
$countries = [];
$countryPosts = VisitEurope_Destination::getCountries([
  'orderby'=>'post_title',
  'posts_per_page' => -1
]);

if (is_array($countryPosts) && !empty($countryPosts)) {
  foreach ($countryPosts as $countryPost) {
    $countries[] = [
      'key' => $countryPost->ID,
      'title' => $countryPost->post_title,
    ];
  }
}

$regions = [];
$regionPosts = VisitEurope_Destination::getRegions([
  'orderby'=>'post_title',
  'posts_per_page' => -1
]);

if (is_array($regionPosts) && !empty($regionPosts)) {
  foreach ($regionPosts as $regionPost) {
    $regions[] = [
      'key' => $regionPost->ID,
      'title' => $regionPost->post_title,
    ];
  }
}

if ($image = get_sub_field('image')) {
  $imageInitial = $image['sizes']['initial'];
  $imageFull = $image['sizes']['large'];
} else {
  $imageInitial = get_template_directory_uri() . '/assets/img/cta-experience-search-banner.jpg';
  $imageFull = $imageInitial;
}

?>
<section class="container cta-banner cta-type-experience-search-banner cta-form">
  <div class="inner container">
    <cta-experience-search-banner
      :countries='<?php echo VisitEurope_Helper::arrayToObjectString($countries); ?>'
      :interests='<?php echo VisitEurope_Helper::arrayToObjectString($interests); ?>'
      :regions='<?php echo VisitEurope_Helper::arrayToObjectString($regions); ?>'
      :types='<?php echo VisitEurope_Helper::arrayToObjectString($types); ?>'
      :image-initial="'<?php echo $imageInitial; ?>'"
      :image-full="'<?php echo $imageFull; ?>'">
    </cta-experience-search-banner>
  </div>
</section>
