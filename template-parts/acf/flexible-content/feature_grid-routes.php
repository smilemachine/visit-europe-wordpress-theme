<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: feature_grid.routes
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Visit_Europe
 */

echo VisitEurope_Helper::getTemplatePart('acf/flexible-content/feature_grid-experience', [
  'type' => VisitEurope_Experience::TYPE_ROUTE,
  'typeSlug' => 'featured-routes',
]);
