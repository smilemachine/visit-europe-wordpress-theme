<?php
if (!defined('ABSPATH')) exit;

/**
 * Standard grid: full width
 *
 * @package Visit_Europe
 */

if (!isset($type) || !isset($standard) || !is_array($standard) || empty($standard)) {
  // Failed validation
} else {

$containerClasses = [
  'standard-grid-size-full',
  'standard-grid-type-' . strtolower($type),
];

?>
<section class="<?php echo implode(' ', $containerClasses); ?>">
  <div class="grid-container">
    <section class="standard">
      <div class="standard-container">
        <?php
          foreach ($standard as $item) {
            echo do_shortcode('[ve_preview ' . VisitEurope_ACF_FC::getParamsFromItem($item) . ']');
          }
        ?>
      </div>
    </section>
  </div>
</section>

<?php } ?>
