<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for pointers
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Visit_Europe
 */

$pointerIterator = 0;
$pointerLimit = null;

if (!isset($pointers) || empty($pointers)) {
  $pointers = get_field('pointers');
}

$columnClass = 'column-1';

if (isset($limit) && intval($limit) > 0) {
  $pointerLimit = intval($limit);
}

if (isset($columns) && intval($columns) == 2) {
  $columnClass = 'column-2';
}

?>
<?php if (is_array($pointers) && !empty($pointers)) { ?>
  <aside class="pointers">
    <ul class="<?php echo $columnClass; ?>">
      <?php foreach ($pointers as $pointer) { ?>
        <?php
          $type = VisitEurope_ACF_FC::getPointerTypeFromRawType($pointer['type']);
          $class = VisitEurope_ACF_FC::getPointerHtmlClassFromType($type);
          $title = VisitEurope_ACF_FC::getPointerTitleFromType($type);
          $subtitle = $pointer['subtitle'];
          $url = $pointer['url'];
        ?>
        <?php if (!empty($title)) { ?>
          <?php
            if ($pointerLimit && $pointerIterator >= $pointerLimit) {
              break;
            }
          ?>
          <li class="pointer pointer-<?php echo $class ?>">
            <span class="title"><?php echo $title ?></span>
            <?php if (!empty($url)) { ?>
              <a href="<?php echo $url; ?>" target="_blank" class="subtitle">
                <?php echo $subtitle; ?>
              </a>
            <?php } else { ?>
              <span class="subtitle"><?php echo $subtitle; ?></span>
            <?php } ?>
            <?php $pointerIterator++; ?>
          </li>
        <?php } ?>
      <?php } ?>
    </ul>
  </aside>
<?php } ?>
