<?php
/**
 * The template for displaying a single experience.article
 *
 * @package Visit_Europe
 */

global $post;

// Setup the destination
$destination = get_post(get_field('article_destination'));
$destinationUrl = get_permalink($destination->ID);
$destinationType = VisitEurope_Destination::getTypeForPostID($destination->ID);
$destinationTitle = $destination->post_title;
$destinationImageInitialUrl = VisitEurope_Helper::getPostThumbnailUrl($destination->ID, 'initial');
$destinationImageFullUrl = VisitEurope_Helper::getPostThumbnailUrl($destination->ID, 'medium');

// Banner images
$imageInitialUrl = VisitEurope_Helper::getPostThumbnailUrl(get_the_ID(), 'initial');
$imageFullUrl = VisitEurope_Helper::getPostThumbnailUrl(get_the_ID(), 'large');

$content = get_field('experience_content');
$title = $post->post_title;

?>
<?php get_header(); ?>
	<div id="experience-article">
		<?php
			echo VisitEurope_Helper::getTemplatePart('header-banner/header-banner', [
				'subtitle' => VisitEurope_Content::getLocale('experiences.types.article'),
				'title' => $title,
				'save' => [
					'id' => $post->ID,
					'type' => VisitEurope_Experience::getTypeForPostID($post->ID),
					'url' => get_the_permalink($post->ID),
				],
			]);
		?>
		<section class="content">
      <article>
				<div class="container">
					<div class="row">
		        <div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
	            <header>
	              <h2 class="title"><?php echo $title; ?></h2>
	            </header>
							<div class="information">
								<p class="date">
	                <?php echo VisitEurope_Helper::getLocalisedPublishedDateString($post->post_date); ?>
	              </p>
							</div>
						</div>
					</div>
				</div>
        <?php if (is_array($content) && !empty($content)) { ?>
          <div class="content">
            <?php
              foreach ($content as $item) {
                echo VisitEurope_Helper::getTemplatePart('acf/experience/' . $item['acf_fc_layout'], [
                  'item' => $item,
                ]);
              }
            ?>
          </div>
        <?php } ?>
      </article>
			<aside class="container">
				<div class="row">
	        <div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
						<div class="extra-information">
							<div class="destination">
								<?php echo do_shortcode('[ve_preview type="' . $destinationType . '" title="' . $destinationTitle . '" imageinitial="' . $destinationImageInitialUrl . '" imagefull="' . $destinationImageFullUrl . '" url="' . $destinationUrl . '"]'); ?>
							</div>
							<?php echo VisitEurope_Helper::getTemplatePart('acf/experience/about'); ?>
						</div>
	        </div>
	      </div>
			</aside>
		</section>
		<?php echo VisitEurope_Helper::getTemplatePart('acf/flexible-content/flexible-content'); ?>
	</div>
<?php get_footer(); ?>
