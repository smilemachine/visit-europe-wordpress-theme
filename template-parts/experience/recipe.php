<?php
/**
 * The template for displaying a single experience.recipe
 *
 * @package Visit_Europe
 */

global $post;

$recipe = new VisitEurope_Experience_Recipe($post);

if (!$recipe->hasValidDestination()) {
	VisitEurope_Helper::throw404();
}

// Destination
$destinationUrl = $recipe->getDestinationUrl();
$destinationType = $recipe->getDestinationType();
$destinationTitle = $recipe->getDestinationTitle();
$destinationImageInitialUrl = $recipe->getDestinationImageUrl('initial');
$destinationImageFullUrl = $recipe->getDestinationImageUrl('large');

// Map
$mapLocation = $recipe->getMapLocation();
$mapMarkers = $recipe->getMapMarkers();
$mapZoom = $recipe->getMapZoom();
$mapCenterString = VisitEurope_Helper::arrayToObjectString($mapLocation);
$mapMarkersString = VisitEurope_Helper::arrayToObjectString($mapMarkers);

// Header Images
$imageInitialUrl = $recipe->getHeaderImageUrl('initial');
$imageFullUrl = $recipe->getHeaderImageUrl('large');

// Content
$content = get_field('experience_content');
$title = $post->post_title;

// Ingredients
$ingredientsTitle = get_field('ingredients_title');
$ingredientsColumnOne = get_field('ingredients_column_one');
$ingredientsColumnTwo = get_field('ingredients_column_two');
$ingredientsHasTwoColumns = is_array($ingredientsColumnTwo) && !empty($ingredientsColumnTwo);

?>
<?php get_header(); ?>
	<div id="experience-recipe">
		<?php
			echo VisitEurope_Helper::getTemplatePart('header-banner/header-banner', [
				'subtitle' => VisitEurope_Content::getLocale('experiences.types.recipe'),
				'title' => $title,
				'save' => [
					'id' => $post->ID,
					'type' => VisitEurope_Experience::getTypeForPostID($post->ID),
					'url' => get_the_permalink($post->ID),
				],
			]);
		?>
		<section class="container map">
			<div class="map-content">
				<div class="overview">
					<span class="tag label label-primary">
						<?php echo VisitEurope_Content::getLocale('experiences.types.recipe'); ?>
					</span>
					<?php echo VisitEurope_Helper::getTemplatePart('acf/pointers'); ?>
					<div class="btn btn-primary btn-lg" @click="launchRecipeModal">
						<?php echo VisitEurope_Content::getLocale('experiences.recipe.ingredients.cta'); ?>
					</div>
				</div>
				<visit-europe-gmap class="map"
					:center='<?php echo $mapCenterString; ?>'
					:markers='[<?php echo $mapMarkersString; ?>]'
					:markers-are-clickable="false"
					:zoom="<?php echo $mapZoom; ?>">
				</visit-europe-gmap>
			</div>
		</section>
		<section class="content">
      <article>
				<div class="container">
					<div class="row">
		        <div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
	            <header>
	              <h2 class="title"><?php echo $title; ?></h2>
	            </header>
							<div class="information">
								<p class="date">
	                <?php echo VisitEurope_Helper::getLocalisedPublishedDateString($post->post_date); ?>
	              </p>
							</div>
						</div>
					</div>
				</div>
        <?php if (is_array($content) && !empty($content)) { ?>
          <div class="content">
            <?php
              foreach ($content as $item) {
                echo VisitEurope_Helper::getTemplatePart('acf/experience/' . $item['acf_fc_layout'], [
                  'item' => $item,
                ]);
              }
            ?>
          </div>
        <?php } ?>
      </article>
		<aside class="container">
			<div class="row">
				<div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
					<div class="extra-information">
						<div class="destination">
							<?php echo do_shortcode('[ve_preview type="' . $destinationType . '" title="' . $destinationTitle . '" imageinitial="' . $destinationImageInitialUrl . '" imagefull="' . $destinationImageFullUrl . '" url="' . $destinationUrl . '"]'); ?>
						</div>
						<?php echo VisitEurope_Helper::getTemplatePart('acf/experience/about'); ?>
					</div>
				</div>
			</div>
		</aside>
		<?php echo VisitEurope_Helper::getTemplatePart('acf/flexible-content/flexible-content'); ?>
		<modal v-if="elements.recipeModal.visible"
			:close-event="elements.recipeModal.closeEvent"
			:id="'recipe-modal'"
			:header-image-initial="'<?php echo $imageFullUrl; ?>'"
			:header-image-full="'<?php echo $imageFullUrl; ?>'"
			:subtitle="'<?php echo VisitEurope_Content::getLocale('experiences.recipe.ingredients.heading'); ?>'"
			:title="'<?php echo VisitEurope_Helper::formatStringForFrontend($ingredientsTitle); ?>'">
			<div class="recipe-ingredients <?php echo $ingredientsHasTwoColumns ? 'x2' : 'x1'; ?>" slot="body">
				<div class="column-one">
					<?php foreach ($ingredientsColumnOne as $ingredientGroup) { ?>
						<?php $ingredientGroupList = explode("\n", $ingredientGroup['list']); ?>
						<div class="ingredients-group">
							<header>
								<h4><?php echo $ingredientGroup['title']; ?></h4>
							</header>
							<ul>
								<?php foreach ($ingredientGroupList as $ingredientItem) { ?>
									<li><?php echo $ingredientItem; ?></li>
								<?php } ?>
							</ul>
						</div>
					<?php } ?>
				</div>
				<?php if ($ingredientsHasTwoColumns) { ?>
					<div class="column-two">
						<?php foreach ($ingredientsColumnTwo as $ingredientGroup) { ?>
							<?php $ingredientGroupList = explode("\n", $ingredientGroup['list']); ?>
							<div class="ingredients-group">
								<header>
									<h4><?php echo $ingredientGroup['title']; ?></h4>
								</header>
								<ul>
									<?php foreach ($ingredientGroupList as $ingredientItem) { ?>
										<li><?php echo $ingredientItem; ?></li>
									<?php } ?>
								</ul>
							</div>
						<?php } ?>
					</div>
				<?php } ?>
			</div>
		</modal>
	</div>
<?php get_footer(); ?>
