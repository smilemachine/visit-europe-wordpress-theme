<?php
/**
 * The template for displaying a single experience.event
 *
 * @package Visit_Europe
 */

if (!VisitEurope_Feature::isActive(VisitEurope_Feature::MY_TRIPS)) {
  VisitEurope_Helper::throw404();
}

global $post;

$event = new VisitEurope_Experience_Event($post);
$user = get_field('event_user', $post->ID);

if (!$user || !is_array($user) || !isset($user['ID']) || $user['ID'] != VisitEurope_User::getCurrentUserID()) {
  VisitEurope_Helper::throw404();
}

// Destination
$destinationID = $event->getDestinationID();
$destinationUrl = $event->getDestinationUrl();
$destinationType = $event->getDestinationType();
$destinationTitle = $event->getDestinationTitle();
$destinationImageInitialUrl = $event->getDestinationImageUrl('initial');
$destinationImageFullUrl = $event->getDestinationImageUrl('large');

// Map
$mapLocation = $event->getMapLocation('event');
$mapMarkers = $event->getMapMarkers('event');
$mapZoom = $event->getMapZoom();
$mapCenterString = VisitEurope_Helper::arrayToObjectString($mapLocation);
$mapMarkersString = VisitEurope_Helper::arrayToObjectString($mapMarkers);

// Header Images
$imageInitialUrl = $event->getHeaderImageUrl('initial');
$imageFullUrl = $event->getHeaderImageUrl('large');

// Content
$about = get_field('about', $post->ID);
$city = null;
$content = null;
$dates = null;
$experienceContent = get_field('experience_content');
$images = [];
$links = [];
$title = $post->post_title;
$pointers = $event->getPointers();

if (is_array($about) && !empty($about)) {
  foreach ($about as $aboutItem) {
    if (is_array($aboutItem) && isset($aboutItem['url'])) {
      $links[] = $aboutItem['url'];
    }
  }
}

if (is_array($pointers) && !empty($pointers)) {
  foreach ($pointers as $pointer) {
    if (isset($pointer['type']) && $pointer['type'] == 'When') {
      $dates = $pointer['subtitle'];
    }

    if (isset($pointer['type']) && $pointer['type'] == 'City') {
      $city = $pointer['subtitle'];
    }
  }
}

if (is_array($experienceContent) && !empty($experienceContent)) {
  foreach ($experienceContent as $experienceContentItem) {
    if (is_array($experienceContentItem)) {
      if (isset($experienceContentItem['acf_fc_layout']) && $experienceContentItem['acf_fc_layout'] == 'content'
        && isset($experienceContentItem['body'])) {
        $content = $experienceContentItem['body'];
      }

      if (isset($experienceContentItem['acf_fc_layout']) && $experienceContentItem['acf_fc_layout'] == 'carousel'
        && isset($experienceContentItem['slides']) && is_array($experienceContentItem['slides'])) {
        foreach ($experienceContentItem['slides'] as $slide) {
          if (isset($slide['image']) && is_array($slide['image'])) {
            $images[] = $slide['image']['sizes']['thumbnail'];
          }
        }
      }
    }
  }
}

// Back
$backUrl = VisitEurope_Content::getUrl('pages.accountTrips');

?>
<?php get_header(); ?>
	<div id="experience-event-private">
		<section class="container header-banner layout-strip slides-0">
      <div class="banner-inner">
        <div>
          <h1>
            <a href="<?php echo $backUrl; ?>" class="back">
              Back to my trips
            </a>
          </h1>
        </div>
      </div>
    </section>
    <section class="container">
      <header>
        <h1><?php echo $title; ?></h1>
      </header>
      <div class="row">
        <div class="col-sm-6">
          <div class="event-dates">
            <?php echo $dates; ?>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="event-location">
            <a href="<?php echo get_the_permalink($destinationID); ?>" class="event-destination">
              <?php echo $destinationTitle; ?>
            </a>
            <span class="event-city"><?php echo $city; ?></span>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 col-md-push-6">
          <div class="event-map">
            <visit-europe-gmap class="map"
    					:center='<?php echo $mapCenterString; ?>'
    					:markers='<?php echo $mapMarkersString; ?>'
    					:markers-are-clickable="false"
    					:zoom="<?php echo $mapZoom; ?>">
    				</visit-europe-gmap>
          </div>
        </div>
        <div class="col-md-6 col-md-pull-6">
          <div class="event-content">
            <?php echo $content; ?>
          </div>
          <?php if (is_array($images) && !empty($images)) { ?>
            <div class="event-images">
              <div class="row">
                <?php foreach ($images as $image) { ?>
                  <div class="col-sm-6 col-lg-6">
                    <blurry-background-image
                      :initial="'<?php echo $image; ?>'"
                      :full="'<?php echo $image; ?>'">
                    </blurry-background-image>
                  </div>
                <?php } ?>
              </div>
            </div>
          <?php } ?>
          <div class="event-links">
            <?php foreach ($links as $link) { ?>
              <a href="<?php echo $link; ?>" target="_blank">
                <?php echo $link; ?>
              </a>
            <?php } ?>
          </div>
        </div>
      </div>
    </section>
		<?php echo VisitEurope_Helper::getTemplatePart('acf/flexible-content/flexible-content'); ?>
	</div>
<?php get_footer(); ?>
