<?php
/**
 * The template for displaying a single experience.gallery
 *
 * @package Visit_Europe
 */

global $post;

$blurb = get_field('blurb');

// Banner images
$imageInitialUrl = VisitEurope_Helper::getPostThumbnailUrl($post->ID, 'initial');
$imageFullUrl = VisitEurope_Helper::getPostThumbnailUrl($post->ID, 'large');

// Content
$content = get_field('experience_content');
$title = $post->post_title;

?>
<?php get_header(); ?>
	<div id="experience-gallery">
		<?php
			echo VisitEurope_Helper::getTemplatePart('header-banner/header-banner', [
				'subtitle' => VisitEurope_Content::getLocale('experiences.types.gallery'),
				'title' => $title,
				'save' => [
					'id' => $post->ID,
					'type' => VisitEurope_Experience::getTypeForPostID($post->ID),
					'url' => get_the_permalink($post->ID),
				],
			]);
		?>
		<section class="container intro">
			<div class="row">
				<div class="col-sm-12 col-md-8">
					<?php echo $blurb; ?>
				</div>
				<div class="col-sm-12 col-md-4 btn-interactive-map-container">
				  <span class="btn btn-lg btn-primary btn-block btn-interactive-map"
						@click="launchInteractiveMap('experienceGallery', <?php echo $post->ID; ?>)">
						<?php echo VisitEurope_Content::getLocale('interactiveMap.label'); ?>
					</span>
				</div>
			</div>
		</section>
		<?php if (is_array($content) && !empty($content)) { ?>
			<div class="content">
				<?php
					foreach ($content as $item) {
						echo VisitEurope_Helper::getTemplatePart('acf/experience/' . $item['acf_fc_layout'], [
							'item' => $item,
						]);
					}
				?>
			</div>
		<?php } ?>
	</div>
<?php get_footer(); ?>
