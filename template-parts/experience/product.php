<?php
/**
 * The template for displaying a single experience.product
 *
 * @package Visit_Europe
 */

global $post;

$product = new VisitEurope_Experience_Product($post);

if (!$product->hasValidDestination()) {
	VisitEurope_Helper::throw404();
}

// Destination
$destinationID = $product->getDestinationID();
$destinationUrl = $product->getDestinationUrl();
$destinationType = $product->getDestinationType();
$destinationTitle = $product->getDestinationTitle();
$destinationImageInitialUrl = $product->getDestinationImageUrl('initial');
$destinationImageFullUrl = $product->getDestinationImageUrl('large');

// Map
$mapLocation = $product->getMapLocation('product');
$mapMarkers = $product->getMapMarkers();
$mapZoom = $product->getMapZoom();
$mapCenterString = VisitEurope_Helper::arrayToObjectString($mapLocation);
$mapMarkersString = VisitEurope_Helper::arrayToObjectString($mapMarkers);

// Header Images
$imageInitialUrl = $product->getHeaderImageUrl('initial');
$imageFullUrl = $product->getHeaderImageUrl('large');

// Content
$content = get_field('experience_content');
$title = get_the_title();

?>
<?php get_header(); ?>
	<div id="experience-product">
		<?php
			echo VisitEurope_Helper::getTemplatePart('header-banner/header-banner', [
				'subtitle' => VisitEurope_Content::getLocale('experiences.types.product'),
				'title' => $title,
				'save' => [
					'id' => $post->ID,
					'type' => VisitEurope_Experience::getTypeForPostID($post->ID),
					'url' => get_the_permalink($post->ID),
				],
			]);
		?>
		<section class="container map">
			<div class="map-content">
				<div class="overview">
					<span class="tag label label-primary">
						<?php echo VisitEurope_Content::getLocale('experiences.types.product'); ?>
					</span>
					<?php echo VisitEurope_Helper::getTemplatePart('acf/pointers'); ?>
				</div>
				<visit-europe-gmap class="map"
					:center='<?php echo $mapCenterString; ?>'
					:fit-map-to-marker-bounds="false"
					:markers='[<?php echo $mapMarkersString; ?>]'
					:markers-are-clickable="false"
					:zoom="<?php echo $mapZoom; ?>">
				</visit-europe-gmap>
			</div>
		</section>
		<section class="content">
      <article>
				<div class="container">
					<div class="row">
		        <div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
	            <header>
	              <h2 class="title"><?php echo $title; ?></h2>
	            </header>
							<div class="information">
								<p class="date">
	                <?php echo VisitEurope_Helper::getLocalisedPublishedDateString($post->post_date); ?>
	              </p>
							</div>
						</div>
					</div>
				</div>
        <?php if (is_array($content) && !empty($content)) { ?>
          <div class="content">
            <?php
              foreach ($content as $item) {
                echo VisitEurope_Helper::getTemplatePart('acf/experience/' . $item['acf_fc_layout'], [
                  'item' => $item,
                ]);
              }
            ?>
          </div>
        <?php } ?>
      </article>
			<aside class="container">
				<div class="row">
	        <div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
						<div class="extra-information">
							<div class="destination">
								<?php echo do_shortcode('[ve_preview id="' . $destinationID . '" type="' . $destinationType . '" title="' . $destinationTitle . '" imageinitial="' . $destinationImageInitialUrl . '" imagefull="' . $destinationImageFullUrl . '" url="' . $destinationUrl . '"]'); ?>
							</div>
							<?php echo VisitEurope_Helper::getTemplatePart('acf/experience/about'); ?>
						</>
	        </div>
	      </div>
			</aside>
		</section>
		<?php echo VisitEurope_Helper::getTemplatePart('acf/flexible-content/flexible-content'); ?>
	</div>
<?php get_footer(); ?>
