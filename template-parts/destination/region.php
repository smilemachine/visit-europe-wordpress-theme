<?php
if (!defined('ABSPATH')) exit;

/**
 * The template for displaying a single destination: region
 *
 * @package Visit_Europe
 */

global $post;

VisitEurope_Content::addUniqueCache();
$locales = VisitEurope_Content::getLocales();
$vd = new VisitEurope_Destination($post);

// Setup the map
$mapLocation = [
	'lat' => (float) get_field('map_lat', $post->ID),
	'lng' => (float) get_field('map_lng', $post->ID)
];
$mapCenter = VisitEurope_Helper::arrayToObjectString($mapLocation);
$markers = $vd->getMarkers();
$mapMarkers = VisitEurope_Helper::arrayToObjectString($markers);
$mapZoom = intval(get_field('zoom', $post->ID));

// Populate the tabs
$tabNames = [];
$tabContent = [];
$i = 0;

foreach ($locales['destination']['tabs'] as $key => $value) {
	$i++;
	$flexibleContentTab = $i;
	ob_start();
	echo VisitEurope_Helper::getTemplatePart('acf/flexible-content/flexible-content', [
		'flexibleContentTab' => $flexibleContentTab
	]);
	$content = ob_get_clean();

	if (empty($content)) {
		continue;
	}

	$tabContent[$key] = $content;
	$tabNames[$key] = $value;
}

$tabsString = VisitEurope_Helper::arrayToObjectString($tabNames);

// Banner images
$imageInitialUrl = VisitEurope_Helper::getPostThumbnailUrl($post->ID, 'initial');
$imageFullUrl = VisitEurope_Helper::getPostThumbnailUrl($post->ID, 'large');

?>
<?php get_header(); ?>
	<div id="destination" class="destination-region">
		<?php if (!empty($imageInitialUrl) && !empty($imageFullUrl)) { ?>
			<section class="container header-banner layout-default">
				<?php
					echo VisitEurope_Helper::getTemplatePart('header-banner/slide', [
						'imageInitialUrl' => $imageInitialUrl,
						'imageFullUrl' => $imageFullUrl,
						'copyright' => VisitEurope_Helper::getPostThumbnailCopyright($post->ID),
						'copyrightUrl' => VisitEurope_Helper::getPostThumbnailCopyrightUrl($post->ID),
						'subtitle' => VisitEurope_Content::getLocale('destinations.subtitle'),
						'title' => $post->post_title,
						'save' => [
							'id' => $post->ID,
							'type' => VisitEurope_Destination::getTypeForPostID($post->ID),
							'url' => get_the_permalink($post->ID),
						],
					]);
				?>
			</section>
		<?php } ?>
    <destination-nav
			:id="<?php echo $post->ID; ?>"
			:selected-tab="elements.tabs.selected"
			:tabs='<?php echo $tabsString; ?>'
			:title="'<?php echo $post->post_title; ?>'">
		</destination-nav>
		<section class="destination-content">
			<div v-if="isSelectedTab('tab1')">
				<?php
					ob_start();
					echo VisitEurope_Helper::getTemplatePart('acf/pointers', [
						'limit' => 3
					]);
					$pointers = ob_get_clean();
				?>
	      <section class="container map <?php echo empty($pointers) ? '' : 'has-pointers'; ?>">
					<?php echo $pointers; ?>
					<div class="map-content">
						<visit-europe-gmap class="map"
			        :center='<?php echo $mapCenter; ?>'
							:fit-map-to-marker-bounds="true"
			        :markers='<?php echo $mapMarkers; ?>'
							:markers-are-clickable="true"
							:zoom="6">
			      </visit-europe-gmap>
					</div>
	      </section>
				<section class="container intro">
					<div class="intro-content">
						<div class="content">
							<h2><?php echo VisitEurope_Content::getLocale('destination.information.title'); ?></h2>
							<?php the_content(); ?>
						</div>
					</div>
				</section>
			</div>
			<?php $i = 0; ?>
			<?php foreach ($tabContent as $key => $value) { ?>
				<div class="tab tab-<?php echo $key; ?>" v-show="isSelectedTab('<?php echo $key; ?>')"
					:class="{ 'questions-only':elements.destinationQuestions.visible }">
		      <?php echo $value; ?>
		    </div>
			<?php } ?>
		</section>
	</div>
<?php get_footer(); ?>
