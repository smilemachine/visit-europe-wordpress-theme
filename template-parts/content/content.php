<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Visit_Europe
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php
			if (is_singular()) {
				the_title('<h1 class="entry-title">', '</h1>');
			} else {
				the_title('<h2 class="entry-title"><a href="' . esc_url(get_permalink()) . '" rel="bookmark">', '</a></h2>');
			}
		?>
		<?php if ('post' === get_post_type()) { ?>
			<div class="entry-meta">
				<?php visit_europe_posted_on(); ?>
			</div>
		<?php } ?>
	</header>
	<div class="entry-content">
		<?php
			the_content(sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__('Continue reading<span class="screen-reader-text"> "%s"</span>', 'visit-europe-wordpress-theme'),
					[
						'span' => [
							'class' => [],
						],
					]
				),
				get_the_title()
			));

			wp_link_pages([
				'before' => '<div class="page-links">' . esc_html__('Pages:', 'visit-europe-wordpress-theme'),
				'after'  => '</div>',
			]);
		?>
	</div>
	<footer class="entry-footer">
		<?php visit_europe_entry_footer(); ?>
	</footer>
</article>
