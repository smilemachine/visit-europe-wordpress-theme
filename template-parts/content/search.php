<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for displaying results in search pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Visit_Europe
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title(sprintf('<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url(get_permalink())), '</a></h2>'); ?>
		<?php if ('post' === get_post_type()) { ?>
		<div class="entry-meta">
			<?php visit_europe_posted_on(); ?>
		</div>
		<?php } ?>
	</header>
	<div class="entry-summary">
		<?php the_excerpt(); ?>
	</div>
	<footer class="entry-footer">
		<?php visit_europe_entry_footer(); ?>
	</footer>
</article>
