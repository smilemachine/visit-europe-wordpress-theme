<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Visit_Europe
 */

?>
<section class="no-results not-found">
	<header class="page-header">
		<h1 class="page-title"><?php esc_html_e('Nothing Found', 'visit-europe-wordpress-theme'); ?></h1>
	</header>
	<div class="page-content">
		<?php if (is_search()) { ?>
			<p><?php esc_html_e('Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'visit-europe-wordpress-theme'); ?></p>
			<?php get_search_form(); ?>
		<?php } else { ?>
			<p><?php esc_html_e('It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'visit-europe-wordpress-theme'); ?></p>
			<?php get_search_form(); ?>
		<?php } ?>
	</div>
</section>
