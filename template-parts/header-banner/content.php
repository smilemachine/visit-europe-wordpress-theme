<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF header banners: content
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Visit_Europe
 */

foreach ($slide['items'] as $item) {
  $imageInitialUrl = VisitEurope_Helper::getPostThumbnailUrl($item->ID, 'initial');
  $imageFullUrl = VisitEurope_Helper::getPostThumbnailUrl($item->ID, 'large');

  if (empty($imageInitialUrl) || empty($imageFullUrl)) {
    continue;
  }

  echo VisitEurope_Helper::getTemplatePart('header-banner/slide', [
    'button' => _x('See more', 'Header banner: See more', VisitEurope_Theme::KEY),
    'imageInitialUrl' => $imageInitialUrl,
    'imageFullUrl' => $imageFullUrl,
    'copyright' => VisitEurope_Helper::getPostThumbnailCopyright($item->ID),
    'copyrightUrl' => VisitEurope_Helper::getPostThumbnailCopyrightUrl($item->ID),
    'subtitle' => ucfirst(strtolower(VisitEurope_Helper::getTypeFromPost($item))),
    'title' => $item->post_title,
    'url' => get_the_permalink($item->ID),
    'save' => [
      'id' => $item->ID,
      'type' => VisitEurope_Helper::getTypeFromPost($item),
      'url' => get_the_permalink($item->ID),
    ],
  ]);
}
