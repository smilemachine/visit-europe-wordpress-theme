<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF header banners
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Visit_Europe
 */

global $post;

$slides = get_field('slides');

if (!isset($layout) || empty($layout)) {
  $layout = VisitEurope_ACF_HeaderBanner::TYPE_DEFAULT;
}

if (!isset($title) || empty($title)) {
  $title = $post->post_title;
}

if (!isset($subtitle)) {
  $subtitle = '';
}

$totalSlides = 0;

if (is_array($slides) && !empty($slides)) {
  foreach ($slides as $slide) {
    if ($slide['type'] == 'Content') {
      $totalSlides += count($slide['items']);
    } else {
      $totalSlides++;
    }
  }
}

if (!isset($save) || !is_array($save)) {
  $save = null;
}

?>
<section class="container header-banner layout-<?php echo strtolower($layout); ?> slides-<?php echo $totalSlides; ?>">
  <?php if (is_array($slides) && !empty($slides)) { ?>
    <?php if ($totalSlides > 1) { ?>
      <carousel-header>
    <?php } ?>
      <?php
        foreach ($slides as $slide) {
          $template = 'header-banner/' . strtolower($slide['type']);
          echo VisitEurope_Helper::getTemplatePart($template, [
  					'slide' => $slide,
            'save' => (count($slides) == 1 ? $save : false),
  				]);
        }
      ?>
    <?php if ($totalSlides > 1) { ?>
      </carousel-header>
    <?php } ?>
  <?php } else { ?>
    <?php
      echo VisitEurope_Helper::getTemplatePart('header-banner/custom', [
        'title' => $title,
        'imageInitialUrl' => VisitEurope_Helper::getPostThumbnailUrl($post->ID, 'initial'),
        'imageFullUrl' => VisitEurope_Helper::getPostThumbnailUrl($post->ID, 'large'),
        'copyright' => VisitEurope_Helper::getPostThumbnailCopyright($post->ID),
        'copyrightUrl' => VisitEurope_Helper::getPostThumbnailCopyrightUrl($post->ID),
        'button' => '',
        'subtitle' => $subtitle,
        'url' => '',
        'save' => $save,
      ]);
    ?>
  <?php } ?>
</section>
<?php if (!VisitEurope_Theme::hasAcceptedCookies()) { ?>
  <section class="container header-cookies" v-if="elements.headerCookies.visible">
    <div class="inner">
      <p>
        <?php echo _x('We use cookies. By using this website, you accept the use of cookies which helps us provide you more interesting and adapted content.', 'Cookie banner copy', VisitEurope_Theme::KEY); ?>
      </p>
      <a href="#" class="btn btn-primary btn-block" @click.prevent="acceptCookies">
        <?php echo _x('Accept', 'Cookie: Accept', VisitEurope_Theme::KEY); ?>
      </a>
    </div>
  </section>
<?php } ?>
