<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF header banners: custom
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Visit_Europe
 */

if (!isset($imageInitialUrl)) {
  $imageInitialUrl = $slide['image']['sizes']['initial'];
}

if (!isset($imageFullUrl)) {
  $imageFullUrl = $slide['image']['sizes']['large'];
}

if (!isset($button)) {
  $button = $slide['button'];
}

if (!isset($subtitle)) {
  $subtitle = $slide['subtitle'];
}

if (!isset($title)) {
  $title = $slide['title'];
}

if (!isset($url)) {
  $url = $slide['url'];
}

if (!isset($copyright)) {
  $copyright = VisitEurope_Helper::getAttachmentCopyright($slide['image']['ID']);
}

if (!isset($copyrightUrl)) {
  $copyrightUrl = VisitEurope_Helper::getAttachmentCopyrightUrl($slide['image']['ID']);
}

if (!isset($save) || !is_array($save)) {
  $save = null;
}

echo VisitEurope_Helper::getTemplatePart('header-banner/slide', [
  'button' => $button,
  'imageInitialUrl' => $imageInitialUrl,
  'imageFullUrl' => $imageFullUrl,
  'copyright' => $copyright,
  'copyrightUrl' => $copyrightUrl,
  'subtitle' => $subtitle,
  'title' => $title,
  'url' => $url,
  'save' => $save,
]);
