<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF header banners: content
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Visit_Europe
 */

$hasSave = false;
$saveID = null;
$saveType = null;
$saveUrl = null;

if (isset($save) && is_array($save) && isset($save['id']) && isset($save['type']) && isset($save['url'])
  && !empty($save['id']) && !empty($save['type']) && !empty($save['url'])) {
  $hasSave = true;
  $saveID = intval($save['id']);
  $saveType = (string) $save['type'];
  $saveUrl = (string) $save['url'];
}

?>
<?php if (isset($imageInitialUrl) && isset($imageFullUrl) && isset($subtitle) && isset($title)) { ?>
  <div>
    <blurry-background-image
      class="banner-slide"
      :initial="'<?php echo $imageInitialUrl; ?>'"
      :full="'<?php echo $imageFullUrl; ?>'">
      <?php if (isset($copyright) && !empty($copyright)) { ?>
        <div class="copyright">
          <?php if (isset($copyrightUrl) && !empty($copyrightUrl) && $copyrightUrl != '#') { ?>
            <a href="<?php echo $copyrightUrl; ?>" target="_blank">
              <?php echo $copyright; ?>
            </a>
          <?php } else { ?>
            <span><?php echo $copyright; ?></span>
          <?php } ?>
        </div>
      <?php } ?>
      <?php if ($hasSave) { ?>
        <a href="<?php echo $saveUrl; ?>" class="save save-icon"
          @click.prevent="launchMyTripsModal(<?php echo $saveID; ?>, '<?php echo $saveType; ?>', '<?php echo $saveUrl; ?>')">
        </a>
      <?php } ?>
      <div class="banner-inner">
        <div>
          <h1>
            <small><?php echo $subtitle; ?></small>
            <?php echo $title; ?>
          </h1>
          <?php if (isset($url) && isset($button) && !empty($url) && !empty($button)) { ?>
            <a href="<?php echo $url; ?>" class="btn btn-primary btn-lg"
              <?php echo (isset($action) && is_string($action) ? $action : null); ?>>
              <?php echo $button; ?>
            </a>
          <?php } ?>
        </div>
      </div>
    </blurry-background-image>
  </div>
<?php } ?>
