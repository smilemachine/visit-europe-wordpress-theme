<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for share
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Visit_Europe
 */

global $post;

$url = urlencode(get_the_permalink($post->ID));

$facebookUrl = 'https://www.facebook.com/sharer/sharer.php?u=' . $url;
$twitterUrl = 'https://twitter.com/home?status=' . urlencode($post->post_title) . ' ' . $url;
$googlePlusUrl = 'https://plus.google.com/share?url=' . $url;

$emailSubject = str_replace(':title', $post->post_title, _x(':title', 'Email share: Subject', VisitEurope_Theme::KEY));
$emailBody = str_replace(':url', get_the_permalink($post->ID), _x("I found this on www.visiteurope.com and thought you might like it too. :url", 'Email share: Body', VisitEurope_Theme::KEY));
$emailUrl = 'mailto:?subject=' . $emailSubject . '&body=' . $emailBody;

?>

<div class="social">
  <span class="share">
    <?php echo VisitEurope_Content::getLocale('social.share'); ?>
  </span>
  <ul>
    <li>
      <a href="<?php echo $facebookUrl; ?>"
        target="_blank" class="social-type-facebook"
        @click.prevent="launchSocialShare('<?php echo $facebookUrl; ?>')">
      </a>
    </li>
    <li>
      <a href="<?php echo $twitterUrl; ?>"
        target="_blank" class="social-type-twitter"
        @click.prevent="launchSocialShare('<?php echo $twitterUrl; ?>')">
      </a>
    </li>
    <li>
      <a href="<?php echo $googlePlusUrl; ?>"
        target="_blank" class="social-type-googleplus"
        @click.prevent="launchSocialShare('<?php echo $googlePlusUrl; ?>')">
      </a>
    </li>
    <li>
      <a href="<?php echo $emailUrl; ?>"class="social-type-email"></a>
    </li>
  </ul>
</div>
