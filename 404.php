<?php
if (!defined('ABSPATH')) exit;

/**
 * The template for displaying 404 page
 *
 * @package Visit_Europe
 */

$title = VisitEurope_Content::getLocale('error.404.title');
$subtitle = VisitEurope_Content::getLocale('error.404.subtitle');
$button = VisitEurope_Content::getLocale('error.404.button');
$image = get_field('404_image', 'option');
$imageInitialUrl = '';
$imageFullUrl = '';

if ($image) {
  $imageInitialUrl = $image['sizes']['initial'];
  $imageFullUrl = $image['sizes']['large'];
}

?>
<?php get_header(); ?>
<div id="error">
  <div class="inner" is="blurry-background-image"
    :initial="'<?php echo $imageInitialUrl; ?>'"
    :full="'<?php echo $imageFullUrl; ?>'">
    <header>
      <h1>404</h1>
    </header>
    <p class="title">
      <?php echo $title; ?>
    </p>
    <p class="subtitle">
      <a href="<?php echo get_site_url('', null); ?>">
        <?php echo $subtitle; ?>
      </a>
    </p>
    <a href="<?php echo get_site_url('', null); ?>" class="btn btn-primary btn-lg">
      <?php echo $button; ?>
    </a>
  </div>
</div>

<?php get_footer(); ?>
