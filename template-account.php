<?php
if (!defined('ABSPATH')) exit;

/**
 * The template for displaying the user's account
 *
 * Template name: Account
 * @package Visit_Europe
 */

if (!VisitEurope_Feature::isActive(VisitEurope_Feature::USER_ACCOUNTS)) {
  VisitEurope_Helper::throw404();
}

$user = VisitEurope_User::getCurrentMember();

if (!$user) {
  wp_redirect('/');
}

?>
<?php get_header(); ?>
	<div id="account">
    <?php
      echo VisitEurope_Helper::getTemplatePart('header-banner/header-banner', [
        'layout' => VisitEurope_ACF_HeaderBanner::TYPE_STRIP,
      ]);
    ?>
		<section class="container intro">
      <user-account
        :id="<?php echo $user->ID; ?>"
        :initial-email="'<?php echo $user->data->user_email; ?>'">
      </user-account>
    </section>
		<?php echo VisitEurope_Helper::getTemplatePart('acf/flexible-content/flexible-content'); ?>
	</div>
<?php get_footer(); ?>
