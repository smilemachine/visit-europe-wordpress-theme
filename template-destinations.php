<?php
if (!defined('ABSPATH')) exit;

/**
 * The template for displaying destinations
 *
 * Template name: Destinations
 * @package Visit_Europe
 */

?>
<?php get_header(); ?>
	<div id="destinations">
		<?php echo VisitEurope_Helper::getTemplatePart('header-banner/header-banner'); ?>
    <destination-nav></destination-nav>
		<section class="container intro">
     <?php
				while (have_posts()) {
					the_post();
					echo VisitEurope_Helper::getTemplatePart('content/page');
				}
			?>
   </section>
		<?php echo VisitEurope_Helper::getTemplatePart('acf/flexible-content/flexible-content'); ?>
	</div>
<?php get_footer(); ?>
