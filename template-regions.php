<?php
if (!defined('ABSPATH')) exit;

/**
 * The template for displaying a list of regions
 *
 * Template name: Regions
 * @package Visit_Europe
 */

$numberOfRegions = count(VisitEurope_Destination::getRegions());
$limitPerBlock = 5;
$blocks = 0;
$rows = 0;

if ($numberOfRegions > 0) {
  $blocks = ceil($numberOfRegions / $limitPerBlock);
  $rows = ceil($blocks / 2);
}

?>
<?php get_header(); ?>
	<div id="destinations-regions">
    <?php echo VisitEurope_Helper::getTemplatePart('header-banner/header-banner'); ?>
    <destination-nav></destination-nav>
		<section class="container intro">
      <?php
				while (have_posts()) {
					the_post();
					echo VisitEurope_Helper::getTemplatePart('content/page');
				}
			?>
    </section>
    <?php for ($j=1; $j<=$rows; $j++) { ?>
      <?php
        if (is_int($j/2)) {
          $orientation = VisitEurope_ACF_FC_FeatureGrid::ORIENTATION_RIGHT;
        } else {
          $orientation = VisitEurope_ACF_FC_FeatureGrid::ORIENTATION_LEFT;
        }

        echo VisitEurope_Helper::getTemplatePart('acf/flexible-content/feature_grid-regions', [
          'orientation' => $orientation,
          'orderBy' => 'title',
        ]);
      ?>
    <?php } ?>
		<?php echo VisitEurope_Helper::getTemplatePart('acf/flexible-content/flexible-content'); ?>
	</div>
<?php get_footer(); ?>
