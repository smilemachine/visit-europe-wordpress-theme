<?php
if (!defined('ABSPATH')) exit;

/**
 * Reference the search template
 *
 * @package Visit_Europe
 */

include 'template-search.php';
