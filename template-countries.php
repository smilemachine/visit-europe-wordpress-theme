<?php
if (!defined('ABSPATH')) exit;

/**
 * The template for displaying a list of countries
 *
 * Template name: Countries
 * @package Visit_Europe
 */

$numberOfCountries = count(VisitEurope_Destination::getCountries());
$limitPerBlock = 5;
$blocks = 0;
$rows = 0;

if ($numberOfCountries > 0) {
  $blocks = ceil($numberOfCountries / $limitPerBlock);
  $rows = ceil($blocks / 2);
}

?>
<?php get_header(); ?>
	<div id="destinations-countries">
    <?php echo VisitEurope_Helper::getTemplatePart('header-banner/header-banner'); ?>
    <destination-nav></destination-nav>
		<section class="container intro">
      <?php
				while (have_posts()) {
					the_post();
					echo VisitEurope_Helper::getTemplatePart('content/page');
				}
			?>
    </section>
    <?php for ($j=1; $j<=$rows; $j++) { ?>
      <?php
        if (is_int($j/2)) {
          $orientation = VisitEurope_ACF_FC_FeatureGrid::ORIENTATION_RIGHT;
        } else {
          $orientation = VisitEurope_ACF_FC_FeatureGrid::ORIENTATION_LEFT;
        }

        echo VisitEurope_Helper::getTemplatePart('acf/flexible-content/feature_grid-countries', [
          'orientation' => $orientation,
          'orderBy' => 'title',
        ]);
      ?>
    <?php } ?>
		<?php echo VisitEurope_Helper::getTemplatePart('acf/flexible-content/flexible-content'); ?>
	</div>
<?php get_footer(); ?>
