<?php
if (!defined('ABSPATH')) exit;

/**
 * The template for displaying a user trip
 *
 * Template name: Account Trips Create
 * @package Visit_Europe
 */

if (!VisitEurope_Feature::isActive(VisitEurope_Feature::MY_TRIPS)) {
  VisitEurope_Helper::throw404();
}

$user = VisitEurope_User::getCurrentMember();

if (!$user) {
  wp_redirect('/#user-verification');
}

global $post;

$countries = [];
$countryPosts = VisitEurope_Destination::getCountries([
  'orderby'=>'post_title',
  'order' => 'ASC',
  'posts_per_page' => -1
]);

if (is_array($countryPosts) && !empty($countryPosts)) {
  foreach ($countryPosts as $countryPost) {
    $countries[] = [
      'key' => $countryPost->ID,
      'title' => $countryPost->post_title,
    ];
  }
}

$interests = VisitEurope_Settings::getInterestGroupsForDropdown();

// Ensure we have a valid trip
$tripID = null;

if (isset($_GET['trip']) && intval($_GET['trip']) > 0) {
  $tripID = intval($_GET['trip']);
  $ut = new VisitEurope_UserTrip(get_post($tripID));

  if (!$ut->isOwnedBy(VisitEurope_User::getCurrentUserID())) {
    VisitEurope_Helper::throw404();
  }
}

if (is_null($tripID)) {
  VisitEurope_Helper::throw404();
}

?>
<?php get_header(); ?>
  <?php
    echo VisitEurope_Helper::getTemplatePart('header-banner/header-banner', [
      'layout' => VisitEurope_ACF_HeaderBanner::TYPE_STRIP,
      'back' => true,
    ]);
  ?>
  <section class="container">
  	<my-trips-create-event
      :countries='<?php echo VisitEurope_Helper::arrayToObjectString($countries); ?>'
      :interests='<?php echo VisitEurope_Helper::arrayToObjectString($interests); ?>'
      :trip-id="<?php echo intval($tripID); ?>">
    </my-trips-create-event>
  </section>
<?php get_footer(); ?>
