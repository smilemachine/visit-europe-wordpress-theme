import _ from 'lodash';

var InteractiveMapDestinationHelper = function() {

  /**
   * Return an array of all the items for the given module
   *
   * @param object module
   * @return array
   */
  var getItemsForModule = function(module) {
    var items = [];

    switch (module.layout) {
      case 'carousel':
        items = module.content.content
        break;
      case 'interests_grid':
        items = module.content.standard;
        break;
      case 'feature_grid':
        _.forEach(module.content.featured, function(item, index) {
          items.push(item);
        });
        _.forEach(module.content.standard, function(item, index) {
          items.push(item);
        });
        break;
      case 'standard_grid':
        items = module.content.standard;
        break;
      default:
        break;
    }

    return _.compact(items);
  }

  /**
   * Returns an array of markers from all the modules within a destination
   *
   * @param object modules
   * @return array
   */
  var getMarkersFromModules = function(modules) {
    var markers = [];
    var moduleItems = [];
    var items = {};

    // Loop through each module add their items to the moduleItems array
    _.forOwn(modules, function(tab, tabKey) {
      _.forEach(tab, function(module, moduleKey) {
        moduleItems.push(getItemsForModule(module));
      });
    });

    // Return the unique items
    _.filter(_.flatten(moduleItems), function(item, key) {
      if (!_.isUndefined(item.id) && _.isUndefined(items[item.id]) && !_.isUndefined(item.map)) {
        var marker = item.map;

        marker.metadata = { item: item }
        markers.push(marker);

        items[item.id] = true;
        return true;
      }

      return false;
    });

    return markers;
  }

  /**
   * Get the tab of the given item, with priority to the current tab
   *
   * @param object modules
   * @param string currentTabKey
   * @param object item
   * @return mixed
   */
  var getTabKeyOfItem = function(tabs, currentTabKey, item) {
    if (_.isUndefined(tabs[currentTabKey])) {
      return null;
    }

    var tabData = {
      key: null,
      module: null
    };

    // Check current tab
    var module = checkModulesForItem(tabs[currentTabKey], item);

    if (!_.isEmpty(module)) {
      tabData.key = currentTabKey;
      tabData.module = module;
      return tabData;
    }

    // Check the remaining tabs
    _.forOwn(tabs, function(tabModules, tabKey) {
      if (tabKey !== currentTabKey) {
        var module = checkModulesForItem(tabModules, item)

        if (!_.isEmpty(module)) {
          tabData.key = tabKey;
          tabData.module = module;
          return false;
        }
      }
    });

    return tabData;
  }

  /**
   * Checks whether or not this item is in the given modules
   *
   * @param array modules
   * @param object item
   * @return string
   */
  var checkModulesForItem = function(modules, item) {
    var modulesWithItem = _.filter(modules, function(module, key) {
      return checkModuleForItem(module, item);
    });

    if (!_.isEmpty(modulesWithItem)) {
      return _.first(modulesWithItem).layout;
    }

    return '';
  }

  /**
   * Checks whether or not this item is in the given module
   *
   * @param object module
   * @param object item
   * @return boolean
   */
  var checkModuleForItem = function(module, item) {
    var items = getItemsForModule(module);

    var itemsWithItem = _.filter(items, function(value, key) {
      return !_.isUndefined(item.id) && value.id === item.id;
    });

    return !_.isEmpty(itemsWithItem);
  }

  return {
    getMarkersFromModules: getMarkersFromModules,
    getTabKeyOfItem: getTabKeyOfItem
  }
}();

export default InteractiveMapDestinationHelper;
