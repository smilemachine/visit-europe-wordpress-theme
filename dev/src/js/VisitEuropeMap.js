import _ from 'lodash';

var VisitEuropeMap = function() {
  if (_.isUndefined(window.google) || _.isUndefined(window.google.maps)) {
    console.log('MapOverlay: Google has not been loaded.');
    return;
  }

  var overlay = function(bounds, image, map) {
    this.bounds_ = bounds;
    this.image_ = image;
    this.map_ = map;
    this.div_ = null;
    this.setMap(map);
  };

  overlay.prototype = new google.maps.OverlayView();

  overlay.prototype.onAdd = function() {
    var div = document.createElement('div');
    div.style.borderStyle = 'none';
    div.style.borderWidth = '0px';
    div.style.position = 'absolute';

    var img = document.createElement('img');
    img.src = this.image_;
    img.style.width = '100%';
    img.style.height = '100%';
    img.style.position = 'absolute';
    div.appendChild(img);

    this.div_ = div;

    var panes = this.getPanes();
    panes.overlayLayer.appendChild(div);
  }

  overlay.prototype.draw = function() {
    var overlayProjection = this.getProjection();
    var sw = overlayProjection.fromLatLngToDivPixel(this.bounds_.getSouthWest());
    var ne = overlayProjection.fromLatLngToDivPixel(this.bounds_.getNorthEast());
    var div = this.div_;
    div.style.left = sw.x + 'px';
    div.style.top = ne.y + 'px';
    div.style.width = (ne.x - sw.x) + 'px';
    div.style.height = (sw.y - ne.y) + 'px';

    if (!_.isUndefined(div.getElementsByTagName('img')[0])) {
      div.getElementsByTagName('img')[0].src = this.image_;
    }
  }

  overlay.prototype.onRemove = function() {
    this.div_.parentNode.removeChild(this.div_);
    this.div_ = null;
  }

  overlay.prototype.update = function(bounds, image) {
    this.bounds_ = bounds;
    this.image_ = image;
    this.draw();
  }

  overlay.prototype.destroy = function() {
    this.setMap(null);
  }

  let infoBox = require('google-maps-infobox');

  var infoBoxContent = function(id, url, imageUrl, title) {
    return '<article id="info-box-preview-' + id + '" class="info-box-preview">'
      + (_.isUndefined(url) ? '<div class="inner">' : '<a href="' + url + '" class="inner">')
      + '<section class="visual" style="background-image: url(' + imageUrl + ');"></section>'
      + '<section class="content"><p>' + title + '</p></section>'
      + (_.isUndefined(url) ? '</div>' : '</a>')
      + '</article>';
  }

  return {
    options: {
      default: {
        lat: 46.8,
        lng: 12.9
      }
    },
    overlay: overlay,
    infoBox: infoBox,
    infoBoxContent: infoBoxContent
  }
};

export default VisitEuropeMap
