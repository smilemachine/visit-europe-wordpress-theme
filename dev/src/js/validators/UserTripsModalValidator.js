import _ from 'lodash';
import UserTripsValidator from './UserTripsValidator';
import Validator from './Validator';

var UserTripsModalValidator = function() {

  /**
   * Determines whether or not the structure is valid
   *
   * @param object item
   * @return boolean
   */
  function isValidStructure(item) {
    return Validator.isValidStructure(item, {
      item: {},
      userTrips: [],
    });
  }

  return {
    validate: function(item) {
      if (!isValidStructure(item)) { return false; }
      if (!UserTripsValidator.validate(item.userTrips)) { return false; }

      return true;
    }
  }
}();

export default UserTripsModalValidator;
