import _ from 'lodash';
import ExperiencePointersValidator from './ExperiencePointersValidator';
import Validator from './Validator';

var ExperiencePreview = function() {

  /**
   * Determines whether or not the structure is valid
   *
   * @param object item
   * @return boolean
   */
  function isValidStructure(item) {
    return Validator.isValidStructure(item, {
      id: 0,
      body: '',
      imageinitial: '',
      imagefull: '',
      title: '',
      type: '',
      url: '',
      date: '',
      pointers: []
    });
  }

  return {
    validate: function(item) {
      if (!isValidStructure(item)) { return false; }
      if (!ExperiencePointersValidator.validate(item.pointers)) { return false; }

      return true;
    }
  }
}();

export default ExperiencePreview;
