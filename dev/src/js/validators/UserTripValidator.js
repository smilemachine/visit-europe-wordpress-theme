import _ from 'lodash';
import PreviewItemValidator from './PreviewItemValidator';
import Validator from './Validator';

var UserTripValidator = function() {

  /**
   * Determines whether or not the structure is valid
   *
   * @param object item
   * @return boolean
   */
  function isValidStructure(item) {
    return Validator.isValidStructure(item, {
      copyright: '',
      copyrighturl: '',
      id: 0,
      imagefull: '',
      imageinitial: '',
      items: [],
      title: '',
      type: '',
      url: ''
    });
  }

  /**
   * Returns an array of valid PreviewItems or PreviewItemDestinations
   *
   * @param array items
   * @return array
   */
  function getValidItems(items) {
    return _.filter(items, function (item, index) {
      return PreviewItemValidator.validateItem(item);
    });
  }

  /**
   * Determines whether or not all the given items are valid
   *
   * @param array items
   * @return bool
   */
  function hasValidItems(items) {
    var validItems = [];

    if (_.isArray(items) && items.length > 0) {
      validItems = getValidItems(items);
    }

    return validItems.length == items.length;
  }

  return {
    validate: function(item) {
      if (!isValidStructure(item)) { console.log(1); return false; }
      if (!hasValidItems(item.items)) { console.log(2); return false; }

      return true;
    }
  }
}();

export default UserTripValidator;
