import _ from 'lodash';
import Validator from './Validator';

var DestinationWeatherValidator = function() {

  /**
   * Determines whether or not the item is valid
   *
   * @param object item
   * @return boolean
   */
  function isValidStructure(item) {
    return Validator.isValidStructure(item, {
      months: {},
      seasons: {}
    });
  }

  /**
   * Returns an array of valid items
   *
   * @param array items
   * @return array
   */
  function getValidMonths(items) {
    var validItems = [];

    _.forOwn(items, function(value, key) {
      validItems.push(value);
    });

    return validItems;
  }

  /**
   * Determines whether or not all the given items are valid
   *
   * @param array items
   * @return bool
   */
  function hasValidMonths(items) {
    var validItems = [];

    if (_.isObject(items)) {
      validItems = getValidMonths(items);
    }

    return validItems.length == _.keys(items).length;
  }

  /**
   * Returns an array of valid items
   *
   * @param array items
   * @return array
   */
  function getValidSeasons(items) {
    var validItems = [];

    _.forOwn(items, function(value, key) {
      validItems.push(value);
    });

    return validItems;
  }

  /**
   * Determines whether or not all the given items are valid
   *
   * @param array items
   * @return bool
   */
  function hasValidSeasons(items) {
    var validItems = [];

    if (_.isObject(items)) {
      validItems = getValidSeasons(items);
    }

    return validItems.length == _.keys(items).length;
  }

  return {
    validate: function(item) {
      if (!isValidStructure(item)) { return false; }
      if (!hasValidMonths(item.months)) { return false; }
      if (!hasValidSeasons(item.seasons)) { return false; }

      return true;
    }
  }
}();

export default DestinationWeatherValidator;
