import _ from 'lodash';
import DestinationExtrasValidator from './DestinationExtrasValidator';
import DestinationModulesValidator from './DestinationModulesValidator';
import FilterItemsValidator from './FilterItemsValidator';
import PreviewItemMapValidator from './PreviewItemMapValidator';
import Validator from './Validator';

var DestinationValidator = function() {

  /**
   * Determines whether or not the structure is valid
   *
   * @param object item
   * @return boolean
   */
  function isValidStructure(item) {
    var initialValidation = Validator.isValidStructure(item, {
      id: 0,
      body: '',
      filters: [],
      imageinitial: '',
      imagefull: '',
      map: {},
      save: true,
      title: '',
      type: '',
      url: ''
    });

    if (!initialValidation) {
      return false;
    }

    if (!_.isUndefined(item.extras) && !_.isObject(item.extras)) {
      return false;
    }

    if (!_.isUndefined(item.modules) && !_.isObject(item.modules)) {
      return false;
    }

    return true;
  }

  return {
    validate: function(item) {
      if (!isValidStructure(item)) { return false; }
      if (!FilterItemsValidator.validate(item.filters)) { return false; }
      if (!PreviewItemMapValidator.validate(item.map)) { return false; }

      if (!_.isUndefined(item.extras) && !DestinationExtrasValidator.validate(item.extras)) {
        return false;
      }

      if (!_.isUndefined(item.modules) && !DestinationModulesValidator.validate(item.modules)) {
        return false;
      }

      return true;
    }
  }
}();

export default DestinationValidator;
