import _ from 'lodash';
import Validator from './Validator';

var InspireMeBudgetsValidator = function() {

  /**
   * Determines whether or not the structure is valid
   *
   * @param object item
   * @return boolean
   */
  function isValidStructure(item) {
    let initialValidation = Validator.isValidStructure(item, {
      title: '',
      budgets: [],
      imageinitial: '',
      imagefull: ''
    });

    if (!initialValidation) {
      return false;
    }

    return Validator.isListOfNumbers(item.budgets);
  }

  /**
   * Returns an array of valid items
   *
   * @param array items
   * @return array
   */
  function getValidItems(items) {
    return _.filter(items, function (item, index) {
      return isValidStructure(item);
    });
  }

  /**
   * Determines whether or not all the given items are valid
   *
   * @param array items
   * @return bool
   */
  function hasValidItems(items) {
    var validItems = [];

    if (_.isArray(items) && items.length > 0) {
      validItems = getValidItems(items);
    }

    return validItems.length == items.length;
  }

  return {
    validate: function(items) {
      return hasValidItems(items);
    }
  }
}();

export default InspireMeBudgetsValidator;
