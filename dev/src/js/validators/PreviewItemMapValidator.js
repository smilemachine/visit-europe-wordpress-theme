import _ from 'lodash';
import Validator from './Validator';
import LocationValidator from './LocationValidator';
import OverlaysValidator from './OverlaysValidator';

var PreviewItemMapValidator = function() {

  /**
   * Determines whether or not the structure is valid
   *
   * @param object item
   * @return boolean
   */
  function isValidStructure(item) {
    let initialValidation = Validator.isValidStructure(item, {
      icon: '',
      location: {},
    });

    if (!initialValidation) {
      return false;
    }

    if (!_.isUndefined(item.overlays)) {
      return _.isArray(item.overlays);
    }

    return true;
  }

  return {
    validate: function(item) {
      if (!isValidStructure(item)) { return false; }
      if (!LocationValidator.validate(item.location)) { return false; }

      if (!_.isUndefined(item.overlays) && !OverlaysValidator.validate(item.overlays)) {
        return false;
      }

      return true;
    }
  }
}();

export default PreviewItemMapValidator;
