import _ from 'lodash';
import ExperienceGalleryItemsValidator from './ExperienceGalleryItemsValidator';
import FilterItemsValidator from './FilterItemsValidator';
import Validator from './Validator';

var ExperienceGallery = function() {

  /**
   * Determines whether or not the structure is valid
   *
   * @param object item
   * @return boolean
   */
  function isValidStructure(item) {
    return Validator.isValidStructure(item, {
      id: 0,
      body: '',
      filters: [],
      imageinitial: '',
      imagefull: '',
      save: true,
      title: '',
      type: '',
      url: '',
      items: []
    });
  }

  return {
    validate: function(item) {
      if (!isValidStructure(item)) { return false; }
      if (!FilterItemsValidator.validate(item.filters)) { return false; }
      if (!ExperienceGalleryItemsValidator.validate(item.items)) { return false; }

      return true;
    }
  }
}();

export default ExperienceGallery;
