import _ from 'lodash';
import InspireMeAudiencesValidator from './InspireMeAudiencesValidator';
import InspireMeBudgetsValidator from './InspireMeBudgetsValidator';
import InspireMeDesiresValidator from './InspireMeDesiresValidator';
import InspireMeSeasonsValidator from './InspireMeSeasonsValidator';
import InspireMeTemperaturesValidator from './InspireMeTemperaturesValidator';
import Validator from './Validator';

var InspireMeValidator = function() {

  /**
   * Determines whether or not the structure is valid
   *
   * @param object item
   * @return boolean
   */
  function isValidStructure(item) {
    return Validator.isValidStructure(item, {
      audiences: [],
      budgets: [],
      desires: [],
      imageinitial: '',
      imagefull: '',
      seasons: [],
      temperatures: []
    });
  }

  return {
    validate: function(item) {
      if (!isValidStructure(item)) { return false; }
      if (!InspireMeAudiencesValidator.validate(item.audiences)) { return false; }
      if (!InspireMeBudgetsValidator.validate(item.budgets)) { return false; }
      if (!InspireMeDesiresValidator.validate(item.desires)) { return false; }
      if (!InspireMeSeasonsValidator.validate(item.seasons)) { return false; }
      if (!InspireMeTemperaturesValidator.validate(item.temperatures)) { return false; }

      return true;
    }
  }
}();

export default InspireMeValidator;
