import _ from 'lodash';
import VisitEurope from './../VisitEurope';

var Validator = function() {

  /**
   * Returns the type of the given object
   *
   * @param mixed item
   * @return string
   */
  function toType(item) {
    return {}.toString.call(item).split(' ')[1].slice(0, -1).toLowerCase();
  }

  /**
   * Returns an array resulting in key.type for data validation purposes
   *
   * @param object item
   * @return array
   */
  function getArrayOfKeysAndTypes(item) {
    if (_.isObject(item)) {
      var items = _.map(item, function(value, key) {
        return key + '.' + toType(value);
      })

      return items.sort();
    }

    return [];
  }

  /**
   * Determines whether or not the top level structure of the given item is valid
   *
   * @param object item
   * @param object structure
   * @return boolean
   */
  var isValidStructure = function(item, structure) {
    if (_.isUndefined(item) || _.isUndefined(structure)) {
      console.log('Invalid: item', item);
      console.log('Invalid: structure', structure);
      return false;
    }

    var rules = getArrayOfKeysAndTypes(structure);
    var value = getArrayOfKeysAndTypes(item);
    var valid = true;

    _.each(rules, function (rule) {
      if (_.indexOf(value, rule) == -1) {
        console.log('rule not found', rule);
        console.log(value);
        valid = false;
      }
    });

    if (!valid) {
      console.log('Top level structure does not match', structure);
      console.log('Rules', rules);
      console.log('Value', value);
    }

    return valid;
  }

  /**
   * Determines if the provided variable is a list of IDs
   *
   * @param array items
   * @return boolean
   */
  var isListOfNumbers = function(items) {
    if (!_.isArray(items)) {
      return false;
    }

    var validItems = _.filter(items, function(value, key) {
      return _.isNumber(key) && _.isNumber(value);
    });

    return validItems.length == items.length;
  }

  /**
   * Determines if the provided variable is a list of Strings
   *
   * @param array items
   * @return boolean
   */
  var isListOfStrings = function(items) {
    if (!_.isArray(items)) {
      return false;
    }

    var validItems = _.filter(items, function(value, key) {
      return _.isString(value);
    });

    return validItems.length == items.length;
  }

  /**
   * Determines if the provided variable is a list of ID: String
   *
   * @param array items
   * @return boolean
   */
  var isListWithID = function(items) {
    if (!_.isArray(items)) {
      return false;
    }

    var validItems = _.filter(items, function(value, key) {
      return _.isNumber(key) && _.isString(value);
    });

    return validItems.length == items.length;
  }

  /**
   * Determines if the provided variable is a list of Key: Value
   *
   * @param object items
   * @return boolean
   */
  var isListWithKey = function(items) {
    if (!_.isObject(items)) {
      return false;
    }

    var validItems = [];

    _.forOwn(items, function(value, key) {
      if ((_.isString(key) || _.isNumber(key)) && (_.isString(value) || _.isNumber(value))) {
        validItems.push(value);
      }
    });

    return validItems.length == _.keys(items).length;
  }

  /**
   * Determines if the provided variable is an array of { key:x, title:x }
   *
   * @param object items
   * @return boolean
   */
  var isArrayWithKeyAndTitle = function(items) {
    if (!_.isObject(items)) {
      return false;
    }

    var validItems = [];

    _.forOwn(items, function(value, index) {
      if (!_.isUndefined(value.key) && !_.isUndefined(value.title)
        && (_.isString(value.key) || _.isNumber(value.key))
        && (_.isString(value.title) || _.isNumber(value.title))) {
        validItems.push(value);
      }
    });

    return validItems.length == items.length;
  }

  /**
   * Determines if the provided variable is a number
   *
   * @param number value
   * @param boolean isPositive
   * @return boolean
   */
  var isNumber = function(value, isPositive) {
    if (!_.isUndefined(isPositive)) {
      return _.isNumber(value) && (isPositive === true ? value >= 0 : false);
    } else {
      return _.isNumber(value);
    }
  }

  return {
    isArrayWithKeyAndTitle: isArrayWithKeyAndTitle,
    isListOfNumbers: isListOfNumbers,
    isListOfStrings: isListOfStrings,
    isListWithID: isListWithID,
    isListWithKey: isListWithKey,
    isNumber: isNumber,
    isValidStructure: isValidStructure
  }
}();

export default Validator;
