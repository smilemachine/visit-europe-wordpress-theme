import _ from 'lodash';
import Validator from './Validator';

var ExperiencePointersValidator = function() {

  /**
   * Determines whether or not the item is valid
   *
   * @param object item
   * @return boolean
   */
  function isValidStructure(item) {
    return Validator.isValidStructure(item, {
      type: '',
      subtitle: '',
      title: '',
      url: ''
    });
  }

  /**
   * Returns an array of valid items
   *
   * @param array items
   * @return array
   */
  function getValidItems(items) {
    return _.filter(items, function (item, index) {
      return isValidStructure(item);
    });
  }

  /**
   * Determines whether or not all the given items are valid
   *
   * @param array items
   * @return bool
   */
  function hasValidItems(items) {
    var validItems = [];

    if (_.isArray(items) && items.length > 0) {
      validItems = getValidItems(items);
    }

    return validItems.length == items.length;
  }

  return {
    validate: function(items) {
      return hasValidItems(items);
    }
  }
}();

export default ExperiencePointersValidator;
