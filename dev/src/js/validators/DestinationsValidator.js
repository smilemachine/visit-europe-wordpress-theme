import _ from 'lodash';
import DestinationValidator from './DestinationValidator';
import Validator from './Validator';

var DestinationsValidator = function() {

  /**
   * Determines whether or not the Filter structure is valid
   *
   * @param object item
   * @return boolean
   */
  function isValidStructure(item) {
    return Validator.isValidStructure(item, {
      countries: [],
      regions: []
    });
  }

  /**
   * Returns an array of valid Filter Items
   *
   * @param array items
   * @return array
   */
  function getValidItems(items) {
    return _.filter(items, function (item, index) {
      return DestinationValidator.validate(item);
    });
  }

  /**
   * Determines whether or not all the given items are valid
   *
   * @param array items
   * @return bool
   */
  function hasValidItems(items) {
    var validItems = [];

    if (_.isArray(items) && items.length > 0) {
      validItems = getValidItems(items);
    }

    return validItems.length == items.length;
  }

  return {
    validate: function(item) {
      if (!isValidStructure(item)) { return false; }
      if (!hasValidItems(item.countries)) { return false; }
      if (!hasValidItems(item.regions)) { return false; }

      return true;
    }
  }
}();

export default DestinationsValidator;
