import Validator from './Validator';
import DestinationPointersValidator from './DestinationPointersValidator';
import DestinationWeatherValidator from './DestinationWeatherValidator';
import PreviewItemMapValidator from './PreviewItemMapValidator';

var DestinationExtrasValidator = function() {

  /**
   * Determines whether or not the item is valid
   *
   * @param object item
   * @return boolean
   */
  function isValidStructure(item) {
    return Validator.isValidStructure(item, {
      map: {},
      pointers: [],
      weather: {}
    });
  }

  return {
    validate: function(item) {
      if (!isValidStructure(item)) { return false; }
      if (!PreviewItemMapValidator.validate(item.map)) { return false; }
      if (!DestinationPointersValidator.validate(item.pointers)) { return false; }
      if (!DestinationWeatherValidator.validate(item.weather)) { return false; }

      return true;
    }
  }
}();

export default DestinationExtrasValidator;
