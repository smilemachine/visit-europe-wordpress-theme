import _ from 'lodash';
import Validator from './Validator';
import LocationValidator from './LocationValidator';

var PreviewItemDestinationValidator = function() {

  /**
   * Determines whether or not the structure is valid
   *
   * @param object item
   * @return boolean
   */
  function isValidPreviewItemStructure(item) {
    return Validator.isValidStructure(item, {
      id: 0,
      imageinitial: '',
      imagefull: '',
      title: '',
      url: ''
    });
  }

  /**
   * Returns an array of valid PreviewItem
   *
   * @param array items
   * @return array
   */
  function getValidPreviewItems(items) {
    return _.filter(items, function (item, index) {
      return isValidPreviewItemStructure(item);
    });
  }

  /**
   * Returns an array of valid PreviewItem filter
   *
   * @param array items
   * @return array
   */
  function getValidPreviewItemFilters(items) {
    return _.filter(items, function (item, index) {
      return _.isNumber(item);
    });
  }

  /**
   * Determines whether or not all the given PreviewItem are valid
   *
   * @param array items
   * @return bool
   */
  function hasValidPreviewItems(items) {
    var validItems = [];

    if (_.isArray(items) && items.length > 0) {
      validItems = getValidPreviewItems(items);
    }

    return validItems.length == items.length;
  }

  /**
   * Determines whether or not all the given PreviewItem are valid
   *
   * @param array items
   * @return bool
   */
  function isValidPreviewItemFilters(items) {
    var validItems = [];

    if (_.isArray(items) && items.length > 0) {
      validItems = getValidPreviewItemFilters(items);
    }

    return validItems.length == items.length;
  }

  return {
    validate: function(items) {
      return hasValidPreviewItems(items);
    },
    validateItem: function(item) {
      return hasValidPreviewItems([item]);
    }
  }
}();

export default PreviewItemDestinationValidator;
