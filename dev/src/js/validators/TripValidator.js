import _ from 'lodash';
import FilterItemsValidator from './FilterItemsValidator';
import PreviewItemMapValidator from './PreviewItemMapValidator';
import Validator from './Validator';

var TripValidator = function() {

  /**
   * Determines whether or not the structure is valid
   *
   * @param object item
   * @return boolean
   */
  function isValidStructure(item) {
    let initialValidation = Validator.isValidStructure(item, {
      id: 0,
      body: '',
      filters: [],
      imageinitial: '',
      imagefull: '',
      map: {},
      save: true,
      title: '',
      type: '',
      url: ''
    });

    if (!initialValidation) {
      return false;
    }

    if (!FilterItemsValidator.validate(item.filters)) {
      return false;
    }

    if (!PreviewItemMapValidator.validate(item.map)) {
      return false;
    }

    return true;
  }

  return {
    validate: function(item) {
      if (!isValidStructure(item)) { return false; }
      if (!FilterItemsValidator.validate(item.filters)) { return false; }
      if (!PreviewItemMapValidator.validate(item.map)) { return false; }

      return true;
    }
  }
}();

export default TripValidator;
