import _ from 'lodash';
import Validator from './Validator';
import LocationValidator from './LocationValidator';

var PreviewItemValidator = function() {

  /**
   * Determines whether or not the structure is valid
   *
   * @param object item
   * @return boolean
   */
  function isValidPreviewItemStructure(item) {
    var initialValidation = Validator.isValidStructure(item, {
      id: 0,
      body: '',
      copyright: '',
      copyrighturl: '',
      imageinitial: '',
      imagefull: '',
      save: true,
      title: '',
      type: '',
      url: ''
    });

    if (!initialValidation) {
      return false;
    }

    if (!_.isUndefined(item.items) && !_.isArray(item.items)) {
      return false;
    }

    if (!_.isUndefined(item.map) && !isValidPreviewItemMapStructure(item.map)) {
      return false;
    }

    if (!_.isUndefined(item.filters) && !_.isArray(item.filters)
      && !isValidPreviewItemFiltersStructure(item.filters)) {
      return false;
    }

    return true;
  }

  /**
   * Determines whether or not the PreviewItem map structure is valid
   *
   * @param object item
   * @return boolean
   */
  function isValidPreviewItemMapStructure(item) {
    var initialValidation = Validator.isValidStructure(item, {
      location: {},
      icon: ''
    });

    if (!initialValidation) {
      return false;
    }

    return LocationValidator.validate(item.location);
  }

  /**
   * Determines whether or not the PreviewItem filters structure is valid
   *
   * @param array items
   * @return boolean
   */
  function isValidPreviewItemFiltersStructure(items) {
    return _.isArray(items);
  }

  /**
   * Returns an array of valid PreviewItem
   *
   * @param array items
   * @return array
   */
  function getValidPreviewItems(items) {
    return _.filter(items, function (item, index) {
      return isValidPreviewItemStructure(item);
    });
  }

  /**
   * Returns an array of valid PreviewItem filter
   *
   * @param array items
   * @return array
   */
  function getValidPreviewItemFilters(items) {
    return _.filter(items, function (item, index) {
      return _.isNumber(item);
    });
  }

  /**
   * Determines whether or not all the given PreviewItem are valid
   *
   * @param array items
   * @return bool
   */
  function hasValidPreviewItems(items) {
    var validItems = [];

    if (_.isArray(items) && items.length > 0) {
      validItems = getValidPreviewItems(items);
    }

    return validItems.length == items.length;
  }

  /**
   * Determines whether or not all the given PreviewItem are valid
   *
   * @param array items
   * @return bool
   */
  function isValidPreviewItemFilters(items) {
    var validItems = [];

    if (_.isArray(items) && items.length > 0) {
      validItems = getValidPreviewItemFilters(items);
    }

    return validItems.length == items.length;
  }

  return {
    validate: function(items) {
      return hasValidPreviewItems(items);
    },
    validateItem: function(item) {
      return hasValidPreviewItems([item]);
    }
  }
}();

export default PreviewItemValidator;
