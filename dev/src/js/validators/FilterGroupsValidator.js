import _ from 'lodash';
import Validator from './Validator';

var FilterGroupsValidator = function() {

  /**
   * Determines whether or not the Filter structure is valid
   *
   * @param object item
   * @return boolean
   */
  function isValidStructure(item) {
    var initialValidation = Validator.isValidStructure(item, {
      filters: [],
      title: ''
    });

    if (!initialValidation) {
      return false;
    }

    return hasValidFilters(item.filters);
  }

  /**
   * Returns an array of valid FilterGroups
   *
   * @param array items
   * @return array
   */
  function getValidFilterGroups(items) {
    return _.filter(items, function (item, index) {
      return isValidStructure(item);
    });
  }

  /**
   * Returns an array of valid Filter Items
   *
   * @param array items
   * @return array
   */
  function getValidFilters(items) {
    return _.filter(items, function (item, index) {
      return _.isNumber(item);
    });
  }

  /**
   * Determines whether or not all the given FilterGroups are valid
   *
   * @param array items
   * @return bool
   */
  function hasValidFilterGroups(items) {
    var validItems = [];

    if (_.isArray(items) && items.length > 0) {
      validItems = getValidFilterGroups(items);
    }

    return validItems.length == items.length;
  }

  /**
   * Determines whether or not all the given FilterGroups are valid
   *
   * @param array items
   * @return bool
   */
  function hasValidFilters(items) {
    var validItems = [];

    if (_.isArray(items) && items.length > 0) {
      validItems = getValidFilters(items);
    }

    return validItems.length == items.length;
  }

  return {
    validate: function(items) {
      return hasValidFilterGroups(items);
    }
  }
}();

export default FilterGroupsValidator;
