import _ from 'lodash';
import Validator from './Validator';

var PreviewAdvertValidator = function() {

  /**
   * Determines whether or not the structure is valid
   *
   * @param object item
   * @return boolean
   */
  function isValidPreviewItemStructure(item) {
    return Validator.isValidStructure(item, {
      id: 0,
      body: '',
      button: '',
      imageinitial: '',
      imagefull: '',
      title: '',
      type: '',
      url: ''
    });
  }

  /**
   * Returns an array of valid PreviewItem
   *
   * @param array items
   * @return array
   */
  function getValidPreviewItems(items) {
    return _.filter(items, function (item, index) {
      return isValidPreviewItemStructure(item);
    });
  }

  /**
   * Determines whether or not all the given PreviewItem are valid
   *
   * @param array items
   * @return bool
   */
  function hasValidPreviewItems(items) {
    var validItems = [];

    if (_.isArray(items) && items.length > 0) {
      validItems = getValidPreviewItems(items);
    }

    return validItems.length == items.length;
  }

  return {
    validate: function(items) {
      return hasValidPreviewItems(items);
    },
    validateItem: function(item) {
      return hasValidPreviewItems([item]);
    }
  }
}();

export default PreviewAdvertValidator;
