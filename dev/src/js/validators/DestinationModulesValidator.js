import _ from 'lodash';
import Validator from './Validator';

var DestinationModulesValidator = function() {

  /**
   * Determines whether or not the structure is valid
   *
   * @param object item
   * @return boolean
   */
  function isValidStructure(item) {
    return Validator.isValidStructure(item, {
      layout: '',
      content: {}
    });
  }

  /**
   * Returns an array of valid components
   *
   * @param array items
   * @return array
   */
  function getValidComponents(items) {
    return _.filter(items, function (item, index) {
      return isValidStructure(item)
    });
  }

  /**
   * Returns an array of valid items
   *
   * @param object items
   * @return array
   */
  function getValidItems(items) {
    let validItems = [];

    _.forOwn(items, function (item, index) {
      if (_.isArray(item)) {
        let validComponents = getValidComponents(item);

        if (validComponents.length == item.length) {
          validItems.push(item);
        }
      }
    });

    return validItems;
  }

  /**
   * Determines whether or not all the given items are valid
   *
   * @param object items
   * @return bool
   */
  function hasValidItems(items) {
    var validItems = [];

    if (_.isObject(items) && _.keys(items).length > 0) {
      validItems = getValidItems(items);
    }

    return validItems.length == _.keys(items).length;
  }

  return {
    validate: function(items) {
      return hasValidItems(items);
    }
  }
}();

export default DestinationModulesValidator;
