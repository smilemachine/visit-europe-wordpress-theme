import _ from 'lodash';
import Validator from './Validator';

var InspireMeDesireWeatherValidator = function() {


  /**
   * Returns an array of valid items
   *
   * @param array items
   * @return array
   */
  function getValidItems(items) {
    var validItems = [];

    _.forOwn(items, function(value, key) {
      if (_.isString(key) && Validator.isListOfStrings(value)) {
        validItems.push(value);
      }
    });

    return validItems;
  }

  /**
   * Determines whether or not all the given items are valid
   *
   * @param array items
   * @return bool
   */
  function hasValidItems(items) {
    var validItems = [];

    if (_.isObject(items)) {
      validItems = getValidItems(items);
    }

    return validItems.length == _.keys(items).length;
  }

  return {
    validate: function(items) {
      return hasValidItems(items);
    }
  }
}();

export default InspireMeDesireWeatherValidator;
