import _ from 'lodash';
import Validator from './Validator';

var InspireMeSearchValidator = function() {

  /**
   * Determines whether or not the structure is valid
   *
   * @param object item
   * @return boolean
   */
  function isValidStructure(item) {
    return Validator.isValidStructure(item, {
      destinations: [],
      trips: []
    });
  }

  return {
    validate: function(item) {
      return isValidStructure(item);
    }
  }
}();

export default InspireMeSearchValidator;
