import _ from 'lodash';
import Validator from './Validator';

var FilterItemsValidator = function() {

  /**
   * Returns an array of valid items
   *
   * @param array items
   * @return array
   */
  function getValidItems(items) {
    return _.filter(items, function (item, index) {
      return _.isNumber(item);
    });
  }

  /**
   * Determines whether or not all the given items are valid
   *
   * @param array items
   * @return bool
   */
  function hasValidItems(items) {
    var validItems = [];

    if (_.isArray(items) && items.length > 0) {
      validItems = getValidItems(items);
    }

    return validItems.length == items.length;
  }

  return {
    validate: function(items) {
      return hasValidItems(items);
    }
  }
}();

export default FilterItemsValidator;
