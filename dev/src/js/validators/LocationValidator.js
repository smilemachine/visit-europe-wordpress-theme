import Validator from './Validator';

var LocationValidator = function() {

  /**
   * Determines whether or not the structure is valid
   *
   * @param object item
   * @return boolean
   */
  function isValidStructure(item) {
    return Validator.isValidStructure(item, {
      lat: 0,
      lng: 0
    });
  }

  return {
    validate: function(item) {
      return isValidStructure(item);
    }
  }
}();

export default LocationValidator;
