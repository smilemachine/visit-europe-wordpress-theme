import _ from 'lodash';
import VisitEurope from './../VisitEurope';

var FormValidator = function() {

  /**
   * Returns whether or not a Vuelidate form is valid
   *
   * @param Vuelidate v
   * @return boolean
   */
  var isValid = function(v) {
    return !v.$invalid && !v.$error;
  }

  /**
   * Returns whether or not a Vuelidate form is dirty
   *
   * @param Vuelidate v
   * @return boolean
   */
  var isDirty = function(v) {
    return v.$dirty;
  }

  /**
   * Returns form error messages
   *
   * @param Vuelidate v
   * @return object
   */
  var errorMessages = function(v) {
    var errors = {};

    _.forOwn(v, function(v1, k1) {
      if (k1[0] !== '$') {
        var fieldName = _.lowerCase(k1);

        // If a translation is defined for that field, use it instead
        if (!_.isUndefined(VisitEuropeLocales.validation.fields[k1])) {
          fieldName = VisitEuropeLocales.validation.fields[k1];
        }

        _.forOwn(v1, function(v2, k2) {
          if (k2[0] !== '$' && v2 !== true) {
            switch (k2) {
              case 'required':
                var string = VisitEuropeLocales.validation.required;
                var params = { field:fieldName };
                errors[k1] = VisitEurope.helper.injectParamsIntoString(string, params);
                break;
              case 'email':
                var string = VisitEuropeLocales.validation.email;
                var params = { field:fieldName };
                errors[k1] = VisitEurope.helper.injectParamsIntoString(string, params);
                break;
              case 'maxLength':
                var string = VisitEuropeLocales.validation.maxLength;
                var params = {
                  field:fieldName,
                  max: v1.$params[k2].max
                };
                errors[k1] = VisitEurope.helper.injectParamsIntoString(string, params);
                break;
              case 'minLength':
                var string = VisitEuropeLocales.validation.minLength;
                var params = {
                  field:fieldName,
                  min: v1.$params[k2].min
                };
                errors[k1] = VisitEurope.helper.injectParamsIntoString(string, params);
                break;
              case 'between':
                var string = VisitEuropeLocales.validation.between;
                var params = {
                  field:fieldName,
                  max: v1.$params[k2].max,
                  min: v1.$params[k2].min
                };
                errors[k1] = VisitEurope.helper.injectParamsIntoString(string, params);
                break;
              case 'sameAs':
                var string = VisitEuropeLocales.validation.sameAs;
                var params = {
                  field:fieldName,
                  same: _.lowerCase(v1.$params[k2].eq)
                };
                errors[k1] = VisitEurope.helper.injectParamsIntoString(string, params);
                break;
              default:
                var string = VisitEuropeLocales.validation.generic;
                var params = { field:fieldName };
                errors[k1] = VisitEurope.helper.injectParamsIntoString(string, params);
                break;
            }
          }
        });
      }
    });

    return errors;
  }

  /**
   * Returns form error messages for a given attribute
   *
   * @param Vuelidate v
   * @param string key
   * @return string
   */
  var errorMessagesForAttribute = function(v, attribute) {
    let errors = errorMessages(v);

    if (_.isUndefined(errors[attribute])) {
      return '';
    }

    return errors[attribute];
  }

  return {
    isValid: isValid,
    isDirty: isDirty,
    errorMessages: errorMessages,
    errorMessagesForAttribute: errorMessagesForAttribute
  }
}();

export default FormValidator;
