import _ from 'lodash';
import Validator from './Validator';

var OverlayItemValidator = function() {

  /**
   * Determines whether or not the structure is valid
   *
   * @param object item
   * @return boolean
   */
  function isValidStructure(item) {
    let initialValidation = Validator.isValidStructure(item, {
      imageurl: '',
      bounds: {}
    });

    if (!initialValidation) {
      return false;
    }

    return Validator.isValidStructure(item.bounds, {
      north: 0.0,
      south: 0.0,
      east: 0.0,
      west: 0.0
    });
  }

  return {
    validate: function(item) {
      return isValidStructure(item);
    }
  }
}();

export default OverlayItemValidator;
