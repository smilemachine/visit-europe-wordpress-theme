import _ from 'lodash';
import PreviewItemMapValidator from './PreviewItemMapValidator';
import Validator from './Validator';

var ExperienceGalleryItemsValidator = function() {

  /**
   * Returns an array of valid items
   *
   * @param array items
   * @return array
   */
  function getValidItems(items) {
    return _.filter(items, function (item, index) {
      var initialValidation = Validator.isValidStructure(item, {
        title: '',
        body: '',
        id: 0,
        imageinitial: '',
        imagefull: '',
        map: {}
      });

      if (!initialValidation) {
        return false;
      }

      if (!_.isUndefined(item.extras) && !_.isObject(item.extras)) {
        return false;
      }

      return PreviewItemMapValidator.validate(item.map);
    });
  }

  /**
   * Determines whether or not all the given items are valid
   *
   * @param array items
   * @return bool
   */
  function hasValidItems(items) {
    var validItems = [];

    if (_.isArray(items) && items.length > 0) {
      validItems = getValidItems(items);
    }

    return validItems.length == items.length;
  }

  return {
    validate: function(items) {
      return hasValidItems(items);
    }
  }
}();

export default ExperienceGalleryItemsValidator;
