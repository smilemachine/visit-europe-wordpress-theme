import _ from 'lodash';
import Vue from 'vue';
import VueCookie from 'vue-cookie';
import VueResource from 'vue-resource';

Vue.use(VueCookie);
Vue.use(VueResource);

Vue.http.options.root = VisitEuropeUrls.api;

let _Vue = new Vue;

var VisitEuropeApi = function() {

  /**
   * Render an error message from a http response
   *
   * @param Response response
   * @return string
   */
  var errorMessageFromResponse = function(response) {
    var error = VisitEuropeLocales.error.api.unknown;

    if (!_.isUndefined(response) && !_.isUndefined(response.body) && !_.isUndefined(response.body.errors) && _.isArray(response.body.errors)) {
      error = _.join(response.body.errors, '. ');
    }

    return error;
  }

  /**
   * Attempt to fetch a new API token
   *
   * @param function successFunction
   * @param function errorFunction
   * @return Promise
   */
  var getApiTokenAndRetry = function(successFunction, errorFunction) {
    let timestamp = new Date().getTime();

    _Vue.$http.get('/?ve_api=1&cachebuster=' + timestamp)
      .then(response => {
        response.json()
          .then(function(json) {
            if (!_.isUndefined(json.veRestToken)) {
              _Vue.$cookie.set('veRestToken', json.veRestToken, {
                expires: '1H'
              });
              successFunction()
            }
          })
          .catch(function(error) {
            errorFunction()
          });
      }, response => {
        errorFunction();
      });
  }

  /**
   * Returns the stored api token
   *
   * @return string
   */
  var getStoredRestToken = function() {
    let token = _Vue.$cookie.get('veRestToken');
    return _.isString(token) && !_.isEmpty(token) ? token : '';
  }

  /**
   * Determines whether or not we have a stored api token
   *
   * @return boolean
   */
  var hasFreshApiToken = function() {
    let token = getStoredRestToken();
    return !_.isUndefined(token) && !_.isNull(token) && !_.isEmpty(token);
  }

  /**
   * Get all destinations
   *
   * @return Promise
   */
  var getDestinations = function() {
    return _Vue.$http.get('destinations');
  }

  /**
   * Get a single destination by id
   *
   * @param integer id
   * @return Promise
   */
  var getDestination = function(id) {
    return _Vue.$http.get('destinations/' + id);
  }

  /**
   * Get a single experience by id
   *
   * @param integer id
   * @return Promise
   */
  var getExperience = function(id) {
    return _Vue.$http.get('experiences/' + id);
  }

  /**
   * Get a single experience preview by id
   *
   * @param integer id
   * @return Promise
   */
  var getExperiencePreview = function(id) {
    return _Vue.$http.get('experiences/preview/' + id, {
      headers: {
        'XP-WP-Nonce': getStoredRestToken()
      }
    });
  }

  /**
   * Get inspire me
   *
   * @return Promise
   */
  var getInspireMe = function() {
    return _Vue.$http.get('inspire-me');
  }

  /**
   * Get inspire me search
   *
   * @return Promise
   */
  var getInspireMeSearch = function(data) {
    return _Vue.$http.get('inspire-me/search', {
      params: data
    });
  }

  /**
   * Get search results from the given searchTerm
   *
   * @param string searchTerm
   * @return Promise
   */
  var getSearch = function(searchTerm) {
    return _Vue.$http.get('search', {
      params: {
        searchTerm: searchTerm
      }
    });
  }

  /**
   * Get all trips
   *
   * @return Promise
   */
  var getTrips = function() {
    return _Vue.$http.get('trips', {
      _wpnonce: getStoredRestToken(),
    });
  }

  /**
   * Get all items for a user trip
   *
   * @param integer userTripID
   * @return Promise
   */
  var getUserTripMap = function(userTripID) {
    return _Vue.$http.get('user/trips/' + userTripID + '/map', {
      _wpnonce: getStoredRestToken(),
    });
  }

  /**
   * Determines if response is a 403 forbidden response
   *
   * @param Response response
   * @return boolean
   */
  var isResponseForbidden = function(response) {
    return !_.isUndefined(response) && !_.isUndefined(response.status) && response.status == 403;
  }

  /**
   * Sends a password forgot email
   *
   * @param integer userID
   * @param string email
   * @param string
   * @return Promise
   */
  var postAccountUpdateAccount = function(userID, email, password) {
    return _Vue.$http.post('user/account', {
      _wpnonce: getStoredRestToken(),
      params: {
        id: userID,
        email: email,
        password: password
      }
    });
  }

  /**
   * Creates a custom event from the front-end
   *
   * @param object data
   * @return Promise
   */
  var postExperienceEventCreate = function(data) {
    return _Vue.$http.post('experiences/event/create', {
      _wpnonce: getStoredRestToken(),
      params: data
    });
  }

  /**
   * Sends a geocode request
   *
   * @param string cityName
   * @param string countryName
   * @return Promise
   */
  var postGoogleGeocode = function(cityName, countryName) {
    return _Vue.$http.post('google/geocode', {
      params: {
        city: cityName,
        country: countryName
      }
    });
  }

  /**
   * Post newsletter
   *
   * @param string email
   * @return Promise
   */
  var postNewsletterSubscription = function(email) {
    return _Vue.$http.post('newsletter', {
      params: {
        email: email
      }
    });
  }

  /**
   * Gets user trips modal
   *
   * @param integer itemID
   * @param string itemType
   * @return Promise
   */
  var postUserTripsModal = function(itemID, itemType) {
    return _Vue.$http.post('user/trips/modal', {
      _wpnonce: getStoredRestToken(),
      params: {
        id: itemID,
        type: itemType
      }
    });
  }

  /**
   * Gets user trips modal
   *
   * @param string name
   * @param integer itemID
   * @param string itemType
   * @return Promise
   */
  var postUserTripsModalCreate = function(name, itemID, itemType) {
    return _Vue.$http.post('user/trips/create', {
      _wpnonce: getStoredRestToken(),
      params: {
        name: name,
        id: itemID,
        type: itemType
      }
    });
  }

  /**
   * Save an item to the given user trip
   *
   * @param integer userTripID
   * @param integer itemID
   * @param string itemType
   * @return Promise
   */
  var postUserTripsSaveItem = function(userTripID, itemID, itemType) {
    return _Vue.$http.post('user/trips/' + userTripID + '/save-item', {
      _wpnonce: getStoredRestToken(),
      params: {
        id: itemID,
        type: itemType
      }
    });
  }

  /**
   * Updates the details of a user trip
   *
   * @param integer userTripID
   * @param object data
   * @return Promise
   */
  var postUserTripUpdateDetails = function(userTripID, data) {
    return _Vue.$http.post('user/trips/' + userTripID + '/details', {
      _wpnonce: getStoredRestToken(),
      params: data
    });
  }

  /**
   * Updates the details of a user trip
   *
   * @param integer userTripID
   * @param object itemIDs
   * @return Promise
   */
  var postUserTripUpdateItemsOrder = function(userTripID, itemIDs) {
    return _Vue.$http.post('user/trips/' + userTripID + '/reorder', {
      _wpnonce: getStoredRestToken(),
      params: {
        ids: itemIDs
      }
    });
  }

  /**
   * Sends a password forgot email
   *
   * @param integer userID
   * @param string accessToken
   * @return Promise
   */
  var postUserVerificationForgot = function(email) {
    return _Vue.$http.post('user/forgot', {
      params: {
        email: email
      }
    });
  }

  /**
   * Log the user in
   *
   * @param integer userID
   * @param string accessToken
   * @return Promise
   */
  var postUserVerificationLogin = function(email, password) {
    return _Vue.$http.post('user/login', {
      params: {
        email: email,
        password: password
      }
    });
  }

  /**
   * Log the user in with Facebook
   *
   * @param integer userID
   * @param string accessToken
   * @return Promise
   */
  var postUserVerificationLoginWithFacebook = function(userID, accessToken) {
    return _Vue.$http.post('user/login/facebook', {
      params: {
        id: userID,
        token: accessToken
      }
    });
  }

  /**
   * Log the user in with Google
   *
   * @param string accessToken
   * @return Promise
   */
  var postUserVerificationLoginWithGoogle = function(accessToken) {
    return _Vue.$http.post('user/login/google', {
      params: {
        token: accessToken
      }
    });
  }

  /**
   * Register a new account
   *
   * @param string email
   * @param string password
   * @return Promise
   */
  var postUserVerificationRegister = function(email, password) {
    return _Vue.$http.post('user/register', {
      params: {
        email: email,
        password: password
      }
    });
  }

  /**
   * Resets a password
   *
   * @param string email
   * @param string password
   * @return Promise
   */
  var postUserVerificationReset = function(email, password, token) {
    return _Vue.$http.post('user/reset', {
      params: {
        email: email,
        password: password,
        token: token
      }
    });
  }

  return {
    errorMessageFromResponse: errorMessageFromResponse,
    getApiTokenAndRetry: getApiTokenAndRetry,
    getDestinations: getDestinations,
    getDestination: getDestination,
    getExperience: getExperience,
    getExperiencePreview: getExperiencePreview,
    getInspireMe: getInspireMe,
    getInspireMeSearch: getInspireMeSearch,
    getSearch: getSearch,
    getTrips: getTrips,
    getUserTripMap: getUserTripMap,
    hasFreshApiToken: hasFreshApiToken,
    isResponseForbidden: isResponseForbidden,
    postAccountUpdateAccount: postAccountUpdateAccount,
    postExperienceEventCreate: postExperienceEventCreate,
    postGoogleGeocode: postGoogleGeocode,
    postNewsletterSubscription: postNewsletterSubscription,
    postUserTripsModal: postUserTripsModal,
    postUserTripsModalCreate: postUserTripsModalCreate,
    postUserTripsSaveItem: postUserTripsSaveItem,
    postUserTripUpdateDetails: postUserTripUpdateDetails,
    postUserTripUpdateItemsOrder: postUserTripUpdateItemsOrder,
    postUserVerificationForgot: postUserVerificationForgot,
    postUserVerificationLogin: postUserVerificationLogin,
    postUserVerificationLoginWithFacebook: postUserVerificationLoginWithFacebook,
    postUserVerificationLoginWithGoogle: postUserVerificationLoginWithGoogle,
    postUserVerificationRegister: postUserVerificationRegister,
    postUserVerificationReset: postUserVerificationReset
  }
}();

export default VisitEuropeApi
