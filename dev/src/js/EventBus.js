import _ from 'lodash';
import Vue from 'vue';

export const EventBus = new Vue({
  methods: {
    listen(app) {
      this.$on('clearAppHeight', function() {
        app.clearAppHeight();
      });

      this.$on('closeHeaderSearch', function() {
        app.closeHeaderSearch();
      });

      this.$on('closeInspireMeModal', function() {
        app.closeInspireMeModal();
      });

      this.$on('closeInteractiveMap', function() {
        app.closeInteractiveMap();
      });

      this.$on('closeMessageModal', function() {
        app.closeMessageModal();
      });

      this.$on('closeMyTripsModal', function() {
        app.closeMyTripsModal();
      });

      this.$on('closeRecipeModal', function() {
        app.closeRecipeModal();
      });

      this.$on('closeUserVerification', function() {
        app.closeUserVerification();
      });

      this.$on('closeVideoFullscreen', function() {
        app.closeVideoFullscreen();
      });

      this.$on('disableBodyScroll', function() {
        app.disableBodyScroll();
      });

      this.$on('enableBodyScroll', function() {
        app.enableBodyScroll();
      });

      this.$on('eventSearchBannerWasSubmitted', function(searchTerm, destination, dateFrom, dateTo) {
        var url = VisitEuropeUrls.pages.eventsSearch;
        var params = {
          term: searchTerm,
          destination: destination,
          from: dateFrom,
          to: dateTo
        };

        window.location = url + VisitEurope.helper.toQueryString(params);
      });

      this.$on('experienceSearchBannerWasSubmitted', function(type, interest, destination) {
        var url = VisitEuropeUrls.pages.experiencesSearch;
        var params = {
          type: type,
          interest: interest,
          destination: destination
        };

        window.location = url + VisitEurope.helper.toQueryString(params);
      });

      this.$on('inspireMeWasSubmitted', function(audience, budget, desires, season, temperature) {
        var url = VisitEuropeUrls.pages.inspireMeSearch;
        var params = {
          audience: audience,
          budget: budget,
          desires: desires,
          season: season,
          temperature: temperature
        };

        window.location = url + VisitEurope.helper.toQueryString(params);
      });

      this.$on('launchInspireMeModal', function() {
        app.launchInspireMeModal();
      });

      this.$on('launchInteractiveMap', function(layout, id, params) {
        app.launchInteractiveMap(layout, id, params);
      });

      this.$on('launchUserVerification', function(email, redirectTo) {
        app.launchUserVerification(email, redirectTo);
      });

      this.$on('newsletterStripWasSubmitted', function(email) {
        let closeEvent = 'newsletterConfirmationWasClosed';
        let body = '<h3>' + VisitEuropeLocales.cta.newsletterStrip.confirmationTitle + '</h3>' +
          '<p>' + VisitEuropeLocales.cta.newsletterStrip.confirmationMessage + '</p>';

        app.launchMessageModal(body, closeEvent, 'newsletter-submitted');
      });

      this.$on('previewItemWasClicked', function(itemID, itemType, $event) {
        app.previewItemWasClicked(itemID, itemType, $event);
      });

      this.$on('setAppHeight', function(height) {
        app.setAppHeight(height);
      });

      // this.$on('signupModalWasSubmitted', function(email) {
      //   app.launchMessageModal(VisitEuropeLocales.cta.signupModal.confirmationMessage, 'signup-submitted');
      // });

      this.$on('tabWasSelected', function(key) {
        app.selectTab(key);
      });

      this.$on('updateInspireMeHeader', function(value) {
        if (Number(value) > 0) {
          let subtitle = VisitEuropeLocales.inspireMe.search.subtitle;
          app.elements.inspireMeSearch.title = VisitEuropeLocales.inspireMe.search.title;
          app.elements.inspireMeSearch.subtitle = subtitle.replace(':number', Number(value));
        } else {
          app.elements.inspireMeSearch.title = VisitEuropeLocales.inspireMe.search.titleNoResults;
          app.elements.inspireMeSearch.subtitle = VisitEuropeLocales.inspireMe.search.subtitleNoResults;
        }
      });
    }
  }
});
