import 'babel-polyfill';
import Admin from './components/Admin.vue';
import VisitEurope from './VisitEurope';

Vue.config.productionTip = false
Vue.config.devTools = true

Vue.component('admin', Admin);

window.VisitEurope = VisitEurope;
