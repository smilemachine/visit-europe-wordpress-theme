<template>
<div id="interactive-map-trips" class="sidebar-inner">
  <div class="sidebar-inner-content">
    <section class="sidebar-header">
      <header>
        <h1>
          <small>{{ subtitle }}</small>
          {{ title }}
        </h1>
      </header>
    </section>
    <section id="interactive-map-trips-sidebar-content" class="sidebar-content">
      <div class="sidebar-content-inner trips-list">
        <section v-if="hasTrips" class="sidebar-content-trip">
          <div class="standard-grid">
            <div class="standard">
              <interactive-map-trips-item
                v-for="(trip, index) in trips"
                :key="index"
                :id="trip.id"
                :imageinitial="trip.imageinitial"
                :imagefull="trip.imagefull"
                :title="trip.title"
                :url="trip.url"
                :overlays="trip.map.overlays"
                @updateMapOverlays="updateMapOverlays">
              </interactive-map-trips-item>
            </div>
          </div>
        </section>
      </div>
    </section>
  </div>
</div>
</template>

<script>
import jQuery from 'jquery';
import InteractiveMapTripsItem from './InteractiveMapTripsItem.vue';
import PerfectScrollbar from 'perfect-scrollbar/jquery';
import TripsValidator from './../validators/TripsValidator';
import VisitEuropeApi from './../VisitEuropeApi';

export default {
  name: 'interactive-map-trips',
  components: {
    InteractiveMapTripsItem
  },
  data() {
    return {
      trips: [],
      subtitle: VisitEuropeLocales.trips.subtitle,
      title: VisitEuropeLocales.trips.title
    };
  },
  mounted() {
    jQuery('#interactive-map-trips').perfectScrollbar({
      suppressScrollX: true,
      suppressScrollY: false
    });

    this.fetchTrips();
  },
  methods: {
    fetchTrips() {
      window.InteractiveMapEventBus.$emit('loadingStarted');

      VisitEuropeApi.getTrips()
        .then(response => {
          this.handleSuccessResponse(response);
        }, response => {
          this.handleErrorResponse(response);
        });
    },
    handleSuccessResponse(response) {
      response.json()
        .then(function(json) {
          if (TripsValidator.validate(json)) {
            this.trips = json;
          } else {
            this.handleErrorResponse(response);
          }

          jQuery('#interactive-map-trips').perfectScrollbar('update');
          window.InteractiveMapEventBus.$emit('loadingCompleted');
        }.bind(this))
        .catch(function(error) {
          console.log(error);
          this.handleErrorResponse(response);
        }.bind(this));
    },
    handleErrorResponse(response) {
      if (VisitEuropeApi.isResponseForbidden(response) && !VisitEuropeApi.hasFreshApiToken()) {
        VisitEuropeApi.getApiTokenAndRetry(this.fetchTrips, this.handleErrorResponse)
      } else {
        jQuery('#interactive-map-trips').perfectScrollbar('update');
        window.InteractiveMapEventBus.$emit('loadingCompleted');
      }
    },
    updateMapOverlays(overlays) {
      this.$emit('updateMapOverlays', overlays);
    }
  },
  computed: {
    hasTrips() {
      return this.trips.length > 0;
    }
  }
}
</script>
