import 'babel-polyfill';
import Vue from 'vue';

import _ from 'lodash';
import * as VueGoogleMaps from 'vue2-google-maps';
import App from './components/App.vue';
import Accordion from './components/Accordion.vue';
import BlurryBackgroundImage from './components/BlurryBackgroundImage.vue';
import CarouselHeader from './components/CarouselHeader.vue';
import CarouselLarge from './components/CarouselLarge.vue';
import CtaNewsletterBanner from './components/CtaNewsletterBanner.vue';
import CtaNewsletterStrip from './components/CtaNewsletterStrip.vue';
import CtaEventSearchBanner from './components/CtaEventSearchBanner.vue';
import CtaExperienceSearchBanner from './components/CtaExperienceSearchBanner.vue';
import DestinationNav from './components/DestinationNav.vue';
import { EventBus } from './EventBus';
import ExperienceEventCreate from './components/ExperienceEventCreate';
import FilterableGrid from './components/FilterableGrid.vue';
import HeaderSearch from './components/HeaderSearch.vue';
import InspireMeModal from './components/InspireMeModal.vue';
import InspireMe from './components/InspireMe.vue';
import InteractiveMap from './components/InteractiveMap.vue';
import MessageModal from './components/MessageModal.vue';
import Modal from './components/Modal.vue';
import MyTripsCreateEvent from './components/MyTripsCreateEvent.vue';
import MyTripsHeaderBanner from './components/MyTripsHeaderBanner.vue';
import MyTripsItems from './components/MyTripsItems.vue';
import MyTripsModal from './components/MyTripsModal.vue';
import SectionHeader from './components/SectionHeader.vue';
import Shave from 'shave';
import UserAccount from './components/UserAccount.vue';
import UserVerification from './components/UserVerification.vue';
import VideoFullscreen from './components/VideoFullscreen.vue';
import VisitEurope from './VisitEurope';
import VisitEuropeGMap from './components/VisitEuropeGMap.vue';
import VisitEuropeGMapOverlayGenerator from './components/VisitEuropeGMapOverlayGenerator.vue';
import VueCookie from 'vue-cookie';
import Vuelidate from 'vuelidate'
import Weather from './components/Weather.vue';

const bootstrap = require('bootstrap');
const skrollr = require('skrollr');
const VueUpload = require('@websanova/vue-upload');

Vue.config.productionTip = false
Vue.config.devTools = (process.env.NODE_ENV != 'production');

window.EventBus = EventBus;
window.InteractiveMapEventBus = new Vue();
window.VisitEurope = VisitEurope;
window.Vue = Vue;

Vue.component('app', App);
Vue.component('accordion', Accordion);
Vue.component('blurry-background-image', BlurryBackgroundImage);
Vue.component('carousel-header', CarouselHeader);
Vue.component('carousel-large', CarouselLarge);
Vue.component('cta-newsletter-banner', CtaNewsletterBanner);
Vue.component('cta-newsletter-strip', CtaNewsletterStrip);
Vue.component('cta-experience-search-banner', CtaExperienceSearchBanner);
Vue.component('cta-event-search-banner', CtaEventSearchBanner);
Vue.component('destination-nav', DestinationNav);
Vue.component('experience-event-create', ExperienceEventCreate);
Vue.component('filterable-grid', FilterableGrid);
Vue.component('header-search', HeaderSearch);
Vue.component('inspire-me-modal', InspireMeModal);
Vue.component('inspire-me', InspireMe);
Vue.component('interactive-map', InteractiveMap);
Vue.component('message-modal', MessageModal);
Vue.component('modal', Modal);
Vue.component('my-trips-create-event', MyTripsCreateEvent);
Vue.component('my-trips-header-banner', MyTripsHeaderBanner);
Vue.component('my-trips-items', MyTripsItems);
Vue.component('my-trips-modal', MyTripsModal);
Vue.component('section-header', SectionHeader);
Vue.component('user-account', UserAccount);
Vue.component('user-verification', UserVerification);
Vue.component('video-fullscreen', VideoFullscreen);
Vue.component('visit-europe-gmap', VisitEuropeGMap);
Vue.component('visit-europe-gmap-overlay-generator', VisitEuropeGMapOverlayGenerator);
Vue.component('weather', Weather);

Vue.use(VueCookie);
Vue.use(Vuelidate);
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyB6lVECTJKwWMLOmj10etMWf55tG0B0Bdg'
  }
});
Vue.use(VueUpload);

new Vue({
  el: '#app',
  data() {
    var hasAcceptedCookies = false;
    let acceptedCookieValue = this.$cookie.get('hasAcceptedCookies');

    if (!_.isUndefined(acceptedCookieValue) && acceptedCookieValue == true) {
      hasAcceptedCookies = true;
    }

    return {
      elements: {
        destinationQuestions: {
          visible: false
        },
        inspireMeModal: {
          visible: false
        },
        inspireMeSearch: {
          title: '',
          subtitle: ''
        },
        interactiveMap: {
          id: null,
          initialLayout: VisitEurope.options.interactiveMap.layouts.destinations,
          params: null,
          visible: false
        },
        headerCookies: {
          visible: !hasAcceptedCookies
        },
        headerSearch: {
          visible: false
        },
        messageModal: {
          body: '',
          closeEvent: '',
          outerClass: '',
          visible: false
        },
        myTripsModal: {
          initialLayout: VisitEurope.options.myTripsModal.layouts.select,
          itemID: null,
          itemType: null,
          redirectTo: '',
          visible: false
        },
        recipeModal: {
          closeEvent: 'closeRecipeModal',
          visible: false
        },
        tabs: {
          selected: 'tab1'
        },
        userVerification: {
          email: '',
          initialLayout: VisitEurope.options.userVerification.layouts.login,
          redirectTo: '',
          token: '',
          visible: false
        },
        videoFullscreen: {
          visible: false,
          url: null
        }
      }
    }
  },
  created() {
    window.EventBus.listen(this);
  },
  mounted() {
    this.setupEllipsis();
    this.setupParallax();

    // If #map is in the url, launch the map
    if (window.location.hash == '#map') {
      this.launchInteractiveMap('destinations');
    }

    // If #verified is in the url, launch verification as verified
    if (window.location.hash == '#verified') {
      this.elements.userVerification.visible = true;
      this.elements.userVerification.initialLayout = VisitEurope.options.userVerification.layouts.verified;
    }

    // If #login is in the url, launch verification as login
    if (window.location.hash == '#login') {
      this.elements.userVerification.visible = true;
      this.elements.userVerification.initialLayout = VisitEurope.options.userVerification.layouts.login;
    }

    let urlParams = new URLSearchParams(window.location.search);

    // If query param is forgot, launch the verification
    if (urlParams.has('forgot') && urlParams.has('token')) {
      this.elements.userVerification.visible = true;
      this.elements.userVerification.email = urlParams.get('forgot');
      this.elements.userVerification.token = urlParams.get('token');
      this.elements.userVerification.initialLayout = VisitEurope.options.userVerification.layouts.reset;
    }
  },
  methods: {
    acceptCookies() {
      this.elements.headerCookies.visible = false;
      this.$cookie.set('hasAcceptedCookies', true, {
        expires: '1Y'
      });
    },
    applyEllipsis() {
      Shave('.preview .content .blurb', 40);
    },
    clearAppHeight() {
      jQuery('#app').removeClass('restrict');
      jQuery('#app').css({
        height: 'auto'
      });
    },
    closeDestinationQuestions() {
      this.elements.destinationQuestions.visible = false;
    },
    closeHeaderSearch() {
      this.elements.headerSearch.visible = false;
      this.enableBodyScroll();
    },
    closeInspireMeModal() {
      this.elements.inspireMeModal.visible = false;
      jQuery('body').removeClass('modal-inspire-me');
    },
    closeInteractiveMap() {
      this.elements.interactiveMap.visible = false;
      this.enableBodyScroll();
    },
    closeMessageModal() {
      this.elements.messageModal.visible = false;
      this.enableBodyScroll();
    },
    closeMyTripsModal() {
      this.elements.myTripsModal.visible = false;
      this.enableBodyScroll();
    },
    closeRecipeModal() {
      this.elements.recipeModal.visible = false;
      this.enableBodyScroll();
    },
    closeUserVerification() {
      this.elements.userVerification.visible = false;
      this.enableBodyScroll();
    },
    closeVideoFullscreen() {
      this.elements.videoFullscreen.url = null;
      this.elements.videoFullscreen.visible = false;
    },
    disableBodyScroll() {
      jQuery('body').addClass('modal-open');
    },
    doNothing() {
      // Used to disable links
    },
    enableBodyScroll() {
      jQuery('body').removeClass('modal-open');
    },
    goBack() {
      window.history.back();
    },
    goForward() {
      window.history.forward();
    },
    isSelectedTab(key) {
      return this.elements.tabs.selected == key
    },
    previewItemWasClicked(itemID, itemType, $event) {
      if ($event.target.className == "save save-icon") {
        this.elements.myTripsModal.itemID = Number(itemID);
        this.elements.myTripsModal.itemType = itemType.toLowerCase();
        this.elements.myTripsModal.redirectTo = jQuery($event.currentTarget).attr('href');
        this.launchMyTripsModal();
      } else {
        window.location = jQuery($event.currentTarget).attr('href');
      }
    },
    scrollToID(id) {
      if (jQuery('#' + id).length > 0) {
        jQuery('html, body').animate({
          scrollTop: jQuery('#' + id).offset().top - 50
        }, 500);
      }
    },
    selectTab(key) {
      this.elements.tabs.selected = key;
      this.elements.destinationQuestions.visible = false

      setTimeout(function () {
        this.applyEllipsis();
      }.bind(this), 50);
    },
    setAppHeight(height) {
      jQuery('#app').addClass('restrict');
      jQuery('#app').css({
        height: height
      });
    },
    setupEllipsis() {
      this.applyEllipsis();
    },
    setupParallax() {
      if (!VisitEurope.helper.isMobileOrTablet()) {
        if (jQuery('.experience-parallax.container').length > 0) {
          skrollr.init();
        }
      } else {
        jQuery('body').addClass('is-mobile');
      }
    },
    launchDestinationQuestions() {
      this.elements.destinationQuestions.visible = true;
    },
    launchHeaderSearch() {
      this.elements.headerSearch.visible = true;
      this.disableBodyScroll();
    },
    launchInspireMeModal() {
      this.elements.inspireMeModal.visible = true;
      jQuery('body').addClass('modal-inspire-me');
    },
    launchInteractiveMap(layoutKey, id, params) {
      let layout = VisitEurope.options.interactiveMap.layouts[layoutKey]

      if (_.isUndefined(layout)) {
        this.elements.interactiveMap.visible = false;
        return;
      }

      if (!_.isUndefined(id) && _.isNumber(id)) {
        this.elements.interactiveMap.id = id;
      } else {
        this.elements.interactiveMap.id = null;
      }

      if (!_.isUndefined(params) && _.isObject(params)) {
        this.elements.interactiveMap.params = params;
      } else {
        this.elements.interactiveMap.params = null;
      }

      this.elements.interactiveMap.initialLayout = layout;
      this.elements.interactiveMap.visible = true;
      this.disableBodyScroll();
    },
    launchMessageModal(body, closeEvent, outerClass) {
      this.elements.messageModal.body = body;
      this.elements.messageModal.outerClass = outerClass;
      this.elements.messageModal.visible = true;
      this.disableBodyScroll();

      if (!_.isUndefined(closeEvent)) {
        this.elements.messageModal.closeEvent = closeEvent;
      }
    },
    launchMyTripsModalCreate() {
      this.elements.myTripsModal.itemID = null;
      this.elements.myTripsModal.itemType = null;
      this.elements.myTripsModal.initialLayout = VisitEurope.options.myTripsModal.layouts.create;
      this.elements.myTripsModal.visible = true;
    },
    launchMyTripsModal(itemID, itemType, redirectTo) {
      this.elements.myTripsModal.visible = true;

      if (!_.isUndefined(itemID)) {
        this.elements.myTripsModal.itemID = itemID;
      }

      if (!_.isUndefined(itemType)) {
        this.elements.myTripsModal.itemType = itemType;
      }

      if (!_.isUndefined(redirectTo)) {
        this.elements.myTripsModal.redirectTo = redirectTo;
      }

      this.disableBodyScroll();
    },
    launchRecipeModal() {
      this.elements.recipeModal.visible = true;
      this.disableBodyScroll();
    },
    launchSocialShare(url) {
      let width = 550;
      let height = 350;
      let left = Number((jQuery(window).width/2) - (width / 2));
      let top = Number((jQuery(window).height/2) - (height/2));

      window.open(url, 'Share', 'width=' + width + ', height=' + height + ', top=' + top + ', left=' + left + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0, resizeable=no');
    },
    launchUserVerification(email, redirectTo) {
      this.elements.userVerification.email = _.isString(email) ? email : '';
      this.elements.userVerification.visible = true;
      this.elements.userVerification.redirectTo = _.isString(redirectTo) ? redirectTo : '';
      this.disableBodyScroll();
    },
    launchVideoFullscreen(url) {
      this.elements.videoFullscreen.visible = true;
      this.elements.videoFullscreen.url = url;
    },
    navigateToItem(item) {
      if (!_.isUndefined(item.url)) {
        window.location = item.url;
      }
    }
  }
});
