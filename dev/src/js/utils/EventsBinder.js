/**
 * Direct copy of eventsBinder from vue2-google-maps to parse webpack UglifyJs
 *
 * @package vue2-google-maps
 */
import _ from 'lodash';

export default (vueElement, googleMapObject, events) => {
  _.forEach(events, (eventName) => {
    const exposedName = eventName;
    googleMapObject.addListener(eventName, (ev) => {
      vueElement.$emit(exposedName, ev);
    });
  });
};
