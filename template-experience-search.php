<?php
if (!defined('ABSPATH')) exit;

/**
 * The template for displaying experiences.search
 *
 * Template name: Experience Search
 * @package Visit_Europe
 */

global $post;

$search = new VisitEurope_Experience_Search();
$search->fill($_GET);
$itemIDs = $search->getResultsIDs();
$type = $search->getType();

ob_start();
echo VisitEurope_Helper::getTemplatePart('acf/flexible-content/standard_grid-experience', [
  'type' => !is_null($type) ? $type : 'Experiences',
  'itemIDs' => $itemIDs,
]);
$results = ob_get_clean();

?>
<?php get_header(); ?>
	<div id="experiences" class="search">
		<?php
      echo VisitEurope_Helper::getTemplatePart('header-banner/header-banner', [
        'layout' => VisitEurope_ACF_HeaderBanner::TYPE_STRIP,
        'subtitle' => count($itemIDs) . ' ' . VisitEurope_Content::getLocale('search.results'),
      ]);
    ?>
    <?php if (!empty($results)) { ?>
      <?php echo $results; ?>
    <?php } else { ?>
      <section class="container search-empty">
        <p><?php echo VisitEurope_Content::getLocale('search.experiencesNoResults'); ?></p>
      </section>
    <?php } ?>
    <?php echo VisitEurope_Helper::getTemplatePart('acf/flexible-content/flexible-content'); ?>
	</div>
<?php get_footer(); ?>
