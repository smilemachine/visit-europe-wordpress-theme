<?php
if (!defined('ABSPATH')) exit;

require_once dirname(__FILE__) . '/inc/class-visiteurope-autoloader.php';
require_once dirname(__FILE__) . '/inc/class-visiteurope.php';
require_once dirname(__FILE__) . '/inc/vendor/class-wp-bootstrap-navwalker.php';

$autoloader = new VisitEurope_Autoloader();
$autoloader->setup();

$visitEurope = new VisitEurope();
$visitEurope->setup();
