<?php
/**
 * The footer for Visit Europe
 *
 * @package Visit_Europe
 */

$socialAccounts = VisitEurope_Settings::getSocialAccounts();
$userVerificationToken = "''";

if ($user = VisitEurope_User_Verification::userAttemptingToReset()) {
	$userVerificationToken = "'" . VisitEurope_Helper::getAttributeFromGET('token') . "'";
}

?>
		<?php if (!is_404()) { ?>
			</main>
			<?php echo VisitEurope_Helper::getTemplatePart('acf/flexible-content/cta-newsletter-strip'); ?>
			<footer id="footer" class="container">
				<div id="footer-top">
					<div class="row">
						<div class="col-sm-8 col-md-6">
							<?php
								wp_nav_menu([
									'menu' => 'menu-footer',
									'theme_location' => 'menu-footer',
									'depth' => 1,
									'container' => 'div',
									'container_class' => '',
									'container_id' => '',
									'menu_class' => 'nav footer-nav',
									'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
									'walker' => new WP_Bootstrap_Navwalker()
								]);
							?>
						</div>
					</div>
				</div>
				<div id="footer-bottom">
					<div class="row">
						<div class="col-md-6">
							<?php echo date('Y'); ?> &copy; <?php echo _x('European Travel Commission. ALL Rights Reserved', 'Footer copyright', VisitEurope_Theme::KEY); ?>
						</div>
						<?php if (is_array($socialAccounts) && !empty($socialAccounts)) { ?>
							<div class="col-md-6">
								<ul class="social">
									<?php foreach ($socialAccounts as $socialType => $socialAccount) { ?>
										<li>
											<a href="<?php echo $socialAccount; ?>" target="_blank" class="social social-type-<?php echo $socialType; ?>"></a>
										</li>
									<?php } ?>
								</ul>
							</div>
						<?php } ?>
					</div>
				</div>
			</footer>
			<inspire-me-modal v-if="elements.inspireMeModal.visible"></inspire-me-modal>
			<interactive-map v-if="elements.interactiveMap.visible"
				:initial-layout="elements.interactiveMap.initialLayout"
				:id="elements.interactiveMap.id"
				:params="elements.interactiveMap.params">
			</interactive-map>
			<header-search v-if="elements.headerSearch.visible">
			</header-search>
			<user-verification v-if="elements.userVerification.visible"
				:initial-email="elements.userVerification.email"
				:initial-layout="elements.userVerification.initialLayout"
				:redirect-to="elements.userVerification.redirectTo"
				:token="elements.userVerification.token">
			</user-verification>
			<message-modal v-if="elements.messageModal.visible"
				:body="elements.messageModal.body"
				:close-event="elements.messageModal.closeEvent"
				:outer-class="elements.messageModal.outerClass">
			</message-modal>
			<my-trips-modal v-if="elements.myTripsModal.visible"
				:initial-layout="elements.myTripsModal.initialLayout"
				:item-id="elements.myTripsModal.itemID"
				:item-type="elements.myTripsModal.itemType"
				:redirect-to="elements.myTripsModal.redirectTo">
			</my-trips-modal>
			<video-fullscreen v-if="elements.videoFullscreen.visible"
				:url="elements.videoFullscreen.url">
			</video-fullscreen>
		<?php } ?>
	</div>
<?php wp_footer(); ?>
</body>
</html>
