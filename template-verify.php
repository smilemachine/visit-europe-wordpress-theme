<?php
if (!defined('ABSPATH')) exit;

/**
 * The template for displaying the user's account
 *
 * Template name: Verify
 * @package Visit_Europe
 */

if (VisitEurope_User_Verification::verify()) {
  wp_redirect(get_site_url('', null) . '/' . ICL_LANGUAGE_CODE . '/?#verified');
} else {
  wp_redirect(get_site_url('', null) . '/' . ICL_LANGUAGE_CODE . '/');
}
