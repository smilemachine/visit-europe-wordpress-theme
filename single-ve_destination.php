<?php
if (!defined('ABSPATH')) exit;

/**
 * The template for displaying a single destination
 *
 * @package Visit_Europe
 */

global $post;

$type = strtolower(VisitEurope_Destination::getTypeForPostID($post->ID));

echo VisitEurope_Helper::getTemplatePart('destination/' . $type);
