<?php
/**
 * The template for displaying a temporary page
 *
 * @package Visit_Europe
 */

VisitEurope_Content::addUniqueCache(VisitEurope_Content::CACHE_DESTINATION);

if (!defined('WP_DEBUG') || WP_DEBUG != true) {
  VisitEurope_Helper::throw404();
}

?>
<?php get_header(); ?>
	<div id="page">
		<?php echo VisitEurope_Helper::getTemplatePart('header-banner/header-banner'); ?>
		<section class="container intro">
      <visit-europe-gmap-overlay-generator>
      </visit-europe-gmap-overlay-generator>
    </section>
		<?php echo VisitEurope_Helper::getTemplatePart('acf/flexible-content/flexible-content'); ?>
	</div>
<?php get_footer(); ?>
