<?php
/**
 * The template for displaying a single experience
 *
 * @package Visit_Europe
 */

$type = VisitEurope_Helper::getSlug(get_field('type'));
echo VisitEurope_Helper::getTemplatePart('experience/' . $type);
