<?php
/**
 * The template for displaying a standard page
 *
 * @package Visit_Europe
 */

?>
<?php get_header(); ?>
	<div id="page">
		<?php echo VisitEurope_Helper::getTemplatePart('header-banner/header-banner'); ?>
		<section class="container intro">
			<?php
				while (have_posts()) {
					the_post();
					echo VisitEurope_Helper::getTemplatePart('content/page');
				}
			?>
		</section>
		<?php echo VisitEurope_Helper::getTemplatePart('acf/flexible-content/flexible-content'); ?>
	</div>
<?php get_footer(); ?>
