<?php
if (!defined('ABSPATH')) exit;

/**
 * The template for displaying the user's trips
 *
 * Template name: Account Trips
 * @package Visit_Europe
 */

if (!VisitEurope_Feature::isActive(VisitEurope_Feature::MY_TRIPS)) {
  VisitEurope_Helper::throw404();
}

$user = VisitEurope_User::getCurrentMember();

if (!$user) {
  wp_redirect('/');
}

$customTrips = [];
$customTripPosts = VisitEurope_UserTrip::getWhereType(VisitEurope_UserTrip::TYPE_CUSTOM);
$plannedTripIDs = [];
$plannedTripPosts = VisitEurope_UserTrip::getWhereType(VisitEurope_UserTrip::TYPE_PLANNED);

if (is_array($customTripPosts) && !empty($customTripPosts)) {
  foreach ($customTripPosts as $customTripPost) {
    $ut = new VisitEurope_UserTrip($customTripPost);
    $customTrips[] = $ut->presentPost();
  }
}

if (is_array($plannedTripPosts) && !empty($plannedTripPosts)) {
  foreach ($plannedTripPosts as $plannedTripPost) {
    $trip = get_field('trip', $plannedTripPost->ID);
    $plannedTripIDs[] = $trip->ID;
  }
}

?>
<?php get_header(); ?>
	<div id="account">
    <?php
      echo VisitEurope_Helper::getTemplatePart('header-banner/header-banner', [
        'layout' => VisitEurope_ACF_HeaderBanner::TYPE_STRIP,
      ]);
    ?>
    <section class="container intro">
      <?php
				while (have_posts()) {
					the_post();
					echo VisitEurope_Helper::getTemplatePart('content/page');
				}
			?>
    </section>
    <section class="custom-trips">
      <section class="container standard-grid standard-trips section-default">
        <section class="standard-grid-size-full standard-grid-type-trip">
          <div class="grid-container">
            <section class="standard">
              <div class="standard-container">
                <article class="preview create-preview">
                  <a href="#" @click.prevent="launchMyTripsModalCreate">
                    <section class="visual">
                      <header>
                        <h4><?php echo VisitEurope_Content::getLocale('myTrips.preview.create'); ?></h4>
                      </header>
                    </section>
                  </a>
                </article>
                <?php if (is_array($customTrips) && !empty($customTrips)) { ?>
                  <?php foreach ($customTrips as $customTrip) { ?>
                    <article class="preview trip-preview usertrip-custom-preview">
                      <a href="<?php echo $customTrip['url']; ?>">
                        <section is="blurry-background-image"
                          :initial="'<?php echo $customTrip['imageinitial']; ?>'"
                          :full="'<?php echo $customTrip['imagefull']; ?>'">
                          <?php if (count($customTrip['items']) == 0) { ?>
                            <div class="new-usertrip">
                              <span>
                                <?php echo VisitEurope_Content::getLocale('myTrips.preview.empty'); ?>
                              </span>
                            </div>
                          <?php } ?>
                        </section>
                        <section class="content">
                          <header>
                            <h4><?php echo esc_html($customTrip['title']); ?></h4>
                          </header>
                          <div class="count">
                            <?php echo count($customTrip['items']) . ' ' . VisitEurope_Content::getLocale('myTrips.preview.saved'); ?>
                          </div>
                        </section>
                      </a>
                    </article>
                  <?php } ?>
                <?php } ?>
              </div>
            </section>
          </div>
        </section>
      </section>
    </section>
    <?php if (is_array($plannedTripIDs) && !empty($plannedTripIDs)) { ?>
      <section class="planned-trips">
        <div class="container">
          <h2><?php echo VisitEurope_Content::getLocale('myTrips.plannedTripsTitle'); ?></h2>
        </div>
        <?php
          echo VisitEurope_Helper::getTemplatePart('acf/flexible-content/standard_grid-trips', [
            'itemIDs' => $plannedTripIDs,
            'limit' => 999
          ]);
        ?>
      </section>
    <?php } ?>
    <section class="suggested-trips">
      <div class="container">
        <h2><?php echo VisitEurope_Content::getLocale('myTrips.suggestedTripsTitle'); ?></h2>
      </div>
      <?php
        echo VisitEurope_Helper::getTemplatePart('acf/flexible-content/standard_grid-trips', [
          'limit' => 4
        ]);
      ?>
    </section>
		<?php echo VisitEurope_Helper::getTemplatePart('acf/flexible-content/flexible-content'); ?>
	</div>
<?php get_footer(); ?>
