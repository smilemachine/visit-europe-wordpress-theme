<?php
if (!defined('ABSPATH')) exit;

/**
 * The template for displaying experiences
 *
 * Template name: Experiences
 * @package Visit_Europe
 */

?>
<?php get_header(); ?>
	<div id="experiences">
		<?php echo VisitEurope_Helper::getTemplatePart('header-banner/header-banner'); ?>
		<section class="container intro">
     <?php
				while (have_posts()) {
					the_post();
					echo VisitEurope_Helper::getTemplatePart('content/page');
				}
			?>
   </section>
		<?php echo VisitEurope_Helper::getTemplatePart('acf/flexible-content/flexible-content'); ?>
	</div>
<?php get_footer(); ?>
