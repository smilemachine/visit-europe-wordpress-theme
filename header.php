<?php
/**
 * The header for Visit Europe
 *
 * @package Visit_Europe
 */

$homeUrl = get_site_url(null, ICL_LANGUAGE_CODE) . '/';
$isInspireMe = get_page_template_slug() == 'template-inspire-me.php';
$isMyTrips = get_page_template_slug() == 'template-account-trips.php';
$googleAppID = (string) get_field('google_app_id', 'option');
$languages = icl_get_languages('skip_missing=0');

// Add Chinese
// $languages['zh'] = [
// 	'url' => 'visiteurope.com.cn',
// 	'native_name' => ''
// ];

?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo('charset'); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.png" />
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<script>
	  window.fbAsyncInit = function() {
	    FB.init({
	      appId: '<?php echo VisitEurope_Content::getUrl('services.facebook.appID'); ?>',
	      cookie: true,
	      xfbml: true,
	      version: 'v2.7'
	    });
	    FB.AppEvents.logPageView();
	  };

	  (function(d, s, id){
	     var js, fjs = d.getElementsByTagName(s)[0];
	     if (d.getElementById(id)) {return;}
	     js = d.createElement(s); js.id = id;
	     js.src = "https://connect.facebook.net/en_US/sdk.js";
	     fjs.parentNode.insertBefore(js, fjs);
	   }(document, 'script', 'facebook-jssdk'));
	</script>
	<script src="https://apis.google.com/js/api.js" async defer></script>
	<div id="app">
		<?php if (!is_404()) { ?>
			<nav class="navbar navbar-fixed-top">
			  <div class="container">
					<span id="interactive-map-close" class="btn btn-primary"
						v-if="elements.interactiveMap.visible"
						@click="closeInteractiveMap">&nbsp;</span>
					<div class="navbar-container navbar-default">
				    <div class="navbar-header">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-header" aria-expanded="false">
				        <span class="sr-only"><?php esc_html_e('Menu', 'visit-europe-wordpress-theme'); ?></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				      </button>
				      <a class="navbar-brand" href="<?php echo $homeUrl; ?>"></a>
						</div>
						<div id="navbar-header" class="navbar-collapse collapse">
							<?php
								wp_nav_menu([
									'menu' => 'menu-header',
				          'theme_location' => 'menu-header',
				          'depth' => 2,
				          'container' => 'div',
				          'container_class' => '',
				          'container_id' => '',
				          'menu_class' => 'nav navbar-nav',
				          'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
									'walker' => new WP_Bootstrap_Navwalker()
								]);
							?>
							<ul class="nav navbar-nav navbar-right">
								<li class="nav-search">
									<a href="<?php echo get_site_url(null, 'search'); ?>"
										@click.prevent="launchHeaderSearch">
										<?php echo VisitEurope_Content::getLocale('navbar.search'); ?>
									</a>
								</li>
								<?php if (VisitEurope_Feature::isActive(VisitEurope_Feature::MY_TRIPS)) { ?>
									<li class="nav-my-trips <?php echo $isMyTrips ? 'current-menu-item current_page_item' : null; ?>">
										<?php if (VisitEurope_User::getCurrentMember()) { ?>
											<a href="<?php echo VisitEurope_Content::getUrl('pages.accountTrips'); ?>">
												<?php echo VisitEurope_Content::getLocale('navbar.myTrips'); ?>
											</a>
										<?php } else { ?>
											<a href="#" @click.prevent="launchUserVerification(null, '<?php echo VisitEurope_Content::getUrl('pages.accountTrips'); ?>')">
												<?php echo VisitEurope_Content::getLocale('navbar.myTrips'); ?>
											</a>
										<?php } ?>
									</li>
								<?php } ?>
								<?php if (VisitEurope_Feature::isActive(VisitEurope_Feature::INSPIRE_ME)) { ?>
									<li class="nav-inspire-me <?php echo $isInspireMe ? 'current-menu-item current_page_item' : null; ?>">
										<a href="#"
											@click.prevent="launchInspireMeModal">
											<?php echo VisitEurope_Content::getLocale('navbar.inspireMe'); ?>
										</a>
									</li>
								<?php } ?>
								<?php if (VisitEurope_Feature::isActive(VisitEurope_Feature::USER_ACCOUNTS)) { ?>
									<?php if (VisitEurope_User::getCurrentMember()) { ?>
										<li class="nav-my-account">
											<a class="dropdown-toggle" data-toggle="dropdown" href="#">
												<?php echo VisitEurope_Content::getLocale('navbar.myAccount'); ?>
											</a>
											<ul class="dropdown-menu">
												<li>
													<a href="<?php echo get_site_url('', 'account'); ?>">
														<?php echo VisitEurope_Content::getLocale('navbar.myAccount'); ?>
													</a>
												</li>
												<li>
													<a href="<?php echo wp_logout_url('/' . ICL_LANGUAGE_CODE . '/'); ?>">
														<?php echo VisitEurope_Content::getLocale('navbar.logout'); ?>
													</a>
												</li>
											</ul>
										</li>
									<?php } else { ?>
										<li class="nav-my-account">
											<a href="#" @click.prevent="launchUserVerification(null, '<?php echo VisitEurope_Content::getUrl('pages.account'); ?>')"></a>
										</li>
									<?php } ?>
								<?php } ?>
								<?php if (is_array($languages) && !empty($languages)) { ?>
									<li class="nav-language language-selector">
										<a class="dropdown-toggle" data-toggle="dropdown" href="#">
											<?php echo ICL_LANGUAGE_NAME; ?> <span class="caret"></span>
										</a>
						        <ul class="dropdown-menu">
											<?php foreach ($languages as $language) { ?>
						          	<li>
													<a href="<?php echo $language['url']; ?>">
														<?php echo $language['native_name']; ?>
													</a>
												</li>
											<?php } ?>
						        </ul>
									</li>
								<?php } ?>
							</ul>
						</div>
					</div>
			  </div>
			</nav>
			<main id="content">
		<?php } ?>
